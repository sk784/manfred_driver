/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers;

import android.content.Context;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import it.cosenonjaviste.daggermock.DaggerMockRule;
import ru.manfred.manfreddriver.di.component.TripComponent;
import ru.manfred.manfreddriver.di.modules.AppModule;
import ru.manfred.manfreddriver.model.api.Event;
import ru.manfred.manfreddriver.model.api.Order;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by begemot on 04.11.17.
 */
@RunWith(MockitoJUnitRunner.class)
public class ManfredEventManagerTest {
    private ManfredEventManager manfredEventManager;
    private List<Event> eventList;
    @Mock
    private Context mMockContext;

    @Rule public final DaggerMockRule<TripComponent> rule = new DaggerMockRule<>(TripComponent.class, new AppModule(mMockContext));


    @Before
    public void setUp() throws Exception {
        ManfredTripManager tripManager = new ManfredTripManager(mMockContext);
        manfredEventManager = new ManfredEventManager(tripManager);
        eventList=new ArrayList<>();
        createEvents();
    }

    private void createEvents() {
        eventList.add(new Event("new_order",generateOrder(new Date(),3)));
        eventList.add(new Event("new_order",generateOrder(new Date(1507142718000L),2))); //4 October 2017 г., 18:45:18
    }

    private Order generateOrder(Date created,int id){
        Order order = new Order();
        order.setComments(randomString(20));
        order.setAddress(randomString(15));
        order.setCreated(created);
        order.setId(id);
        return order;
    }

    private String randomString(final int length) {
        Random r = new Random(); // perhaps make it a class variable so you don't make a new one every time
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < length; i++) {
            char c = (char)(r.nextInt((int)(Character.MAX_VALUE)));
            sb.append(c);
        }
        return sb.toString();
    }

    @Test
    public void newEvents() throws Exception {
        manfredEventManager.addNewOrderListener(new EventManager.EventManagerListener() {
            @Override
            public void newEvent(Order order) {
                assertThat(order.getId(),is(3L));
            }
        });
        manfredEventManager.newEvents(eventList);

    }



}