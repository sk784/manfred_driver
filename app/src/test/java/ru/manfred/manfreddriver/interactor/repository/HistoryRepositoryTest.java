/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.interactor.repository;

import android.support.annotation.NonNull;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ru.manfred.manfreddriver.model.api.Order;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by begemot on 01.10.17.
 */
public class HistoryRepositoryTest {

    private HistoryRepository historyRepository;
    private HistoryRepository emptyHistoryRepository;
    private List<Order> sampleOrders;
    @Before
    public void setUp(){
        sampleOrders=new ArrayList<>();
        try {
            sampleOrders.addAll(createOrders());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        List<Order>emptyOrders = new ArrayList<>();
        emptyHistoryRepository= new HistoryRepository(emptyOrders);
        final Date mockDate = new Date(1505910120000L); //20.09.17 12:22
        final LocalDate mockLocalDate = new LocalDate(1505910120000L);//20.09.17 12:22
        historyRepository=new HistoryRepository(sampleOrders){
            @NonNull
            @Override
            Date getDate(){
                return mockDate;
            }

            @NonNull
            @Override
            LocalDate getLocalDate(){
                return mockLocalDate;
            }
        };
    }

    private List<Order> createOrders() throws ParseException {
        List<Order>orders=new ArrayList<>();
        DateFormat formatter = new SimpleDateFormat("dd.MM.yy HH:mm");

        Order order1=new Order();
        order1.setCreated(formatter.parse("17.09.17 3:00"));//01.09.17 3:00
        order1.setRide_start_time(formatter.parse("17.09.17 3:10"));//01.09.17 3:10
        order1.setRide_finish_time(formatter.parse("17.09.17 4:00"));//01.09.17 4:00
        orders.add(order1);

        Order order2=new Order();
        order2.setCreated(formatter.parse("17.09.17 11:00"));//01.09.17 11:00
        order2.setRide_start_time(formatter.parse("17.09.17 11:10"));//01.09.17 11:10
        order2.setRide_finish_time(formatter.parse("17.09.17 14:10"));//01.09.17 14:10
        orders.add(order2);

        Order order3=new Order();
        order3.setCreated(formatter.parse("20.09.17 16:00"));//01.09.17 16:00
        order3.setRide_start_time(formatter.parse("20.09.17 16:10"));//01.09.17 16:10
        order3.setRide_finish_time(formatter.parse("20.09.17 17:55"));//01.09.17 17:55
        orders.add(order3);

        Order order4=new Order();
        order4.setCreated(formatter.parse("15.08.17 19:00"));//04.09.17 19:00
        order4.setRide_start_time(formatter.parse("15.08.17 21:00"));//04.09.17 21:00
        order4.setRide_finish_time(formatter.parse("15.08.17 23:00"));//04.09.17 23:00
        orders.add(order4);

        Order order5=new Order();
        order5.setCreated(formatter.parse("20.09.17 11:00"));//04.09.17 19:00
        order5.setRide_start_time(formatter.parse("20.09.17 21:00"));//04.09.17 21:00
        order5.setRide_finish_time(formatter.parse("20.09.17 23:00"));//04.09.17 23:00
        orders.add(order5);
        return orders;
    }

    @Test
    public void filterOrdersTodayObjects() throws Exception {
        assertThat(historyRepository.filterOrders(HistoryRepository.HistoryInterval.TODAY).size(),is(equalTo(3)));
    }

    @Test
    public void filterOrderWithEmptyDates(){
        assertThat(emptyHistoryRepository.filterOrders(HistoryRepository.HistoryInterval.TODAY).size(),is(equalTo(1)));
    }

    @Test
    public void currentDayForTest() throws Exception {
        assertThat(historyRepository.currentDayForTest(sampleOrders).size(),is(equalTo(2)));
    }

    @Test
    public void currentWeekObjects() throws Exception {
        assertThat(historyRepository.filterOrders(HistoryRepository.HistoryInterval.THIS_WEEK).size(),is(equalTo(6)));
    }

    @Test
    public void currentMonthObjects() throws Exception {
        assertThat(historyRepository.filterOrders(HistoryRepository.HistoryInterval.THIS_MONTH).size(),is(equalTo(6)));
    }


}