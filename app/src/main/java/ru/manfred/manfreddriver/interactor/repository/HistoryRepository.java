/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.interactor.repository;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ru.manfred.manfreddriver.managers.history.HistoryManager;
import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.utils.HistoryDateSorter;
import ru.manfred.manfreddriver.viewmodel.ListItem;


/**
 * Created by begemot on 01.10.17.
 */

@Deprecated
public class HistoryRepository {
    private static final String TAG = "HistoryRepository";
    private List<Order> history;
    private List<String> logs = new ArrayList<>();
    private SimpleDateFormat sdfr = new SimpleDateFormat("d MMMM", new Locale("ru"));
    private Context context;//just for sending logs


    /*
    normal
     */
    @Deprecated
    HistoryRepository(List<Order> history) {
        //this.history = sortOrders(history);
        this.history = history;
    }

    /*
    for logging
     */
    public HistoryRepository(List<Order> history, Context context) {
        //this.history = sortOrders(history);
        this.history = history;
    }


    private List<Order> sortOrders(List<Order> orders) {
        List<Object> history = new ArrayList<>();
        SimpleDateFormat sdfr = new SimpleDateFormat("d MMMM", new Locale("ru"));
        //filteredHistory.addAll(orders);
        Collections.sort(orders, new Comparator<Order>() {
            public int compare(Order o1, Order o2) {
                if (o1.getRide_start_time() == null || o2.getRide_start_time() == null)
                    return 0;
                return o1.getRide_start_time().compareTo(o2.getRide_start_time());
            }
        });
        logFirebaseSorting("Starting fill dates, size of filteredHistory is " + orders.size());
        return orders;
    }

    private void logFirebaseSorting(String text) {
        //Log.d(TAG, "logFirebaseSorting: "+text);
        //logs.add(TAG+": "+text);
    }

    /*
    adding captions for date list
     */
    private List<ListItem> listWithDates(List<Order> orders) {
        HistoryDateSorter historyDateSorter = new HistoryDateSorter(orders);
        return historyDateSorter.getItems();
    }

    List<ListItem> filterOrders(HistoryManager.HistoryInterval historyInterval) {
        List<ListItem> sortedOrders = new ArrayList<>();
        switch (historyInterval) {
            case TODAY:
                sortedOrders.addAll(listWithDates(currentDay(history)));
                break;
            case THIS_WEEK:
                sortedOrders.addAll(listWithDates(currentWeek(history)));
                break;
            case THIS_MONTH:
                sortedOrders.addAll(listWithDates(currentMonth(history)));
                break;

        }
        //Log.d(TAG, "filterOrders: size of filtered orders is "+sortedOrders.size());
        return sortedOrders;
    }


    private void sendLogs() {
        if (context == null) return;
        String listString = "";

        for (String s : logs) {
            listString += s + "\n";
        }

        String[] TO = {"begemotoff@gmail.com"};
        Uri uri = Uri.parse("mailto:begemotoff@gmail.com")
                .buildUpon()
                .appendQueryParameter("subject", "ManfredLogs")
                .appendQueryParameter("body", listString)
                .build();

        /*String mailto = "mailto:begemotoff@gmail.com" +
                "&subject=" + Uri.encode("ManfredLogs") +
                "&body=" + Uri.encode(listString);*/
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, uri);
        //emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        context.startActivity(Intent.createChooser(emailIntent, "Send logs...").setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    private boolean compareDate(Date first, Date second) {
        logFirebaseSorting("comparing " + new DateTime(first).withTimeAtStartOfDay() + " with " + new DateTime(second).withTimeAtStartOfDay());
        //System.out.println("comparing "+new DateTime(first).withTimeAtStartOfDay()+" with "+new DateTime(second).withTimeAtStartOfDay());
        //boolean equal = DateTimeComparator.getDateOnlyInstance().compare(first, second) != 0;
        boolean equal = new DateTime(first).withTimeAtStartOfDay().isEqual(new DateTime(second).withTimeAtStartOfDay());
        logFirebaseSorting("result is " + equal);
        return equal;
    }

    List<Order> currentDayForTest(List<Order> allHistory) {
        return currentDay(allHistory);
    }

    private List<Order> currentDay(List<Order> allHistory) {
        Date today = getDate();
        //System.out.println("orders "+allHistory.size());
        //System.out.println("currentDay "+today.toString());
        List<Order> todayList = new ArrayList<>();
        for (Order order : allHistory) {
            if (order.getRide_start_time() != null) {
                //LocalDate secondDate = new LocalDate(order.getRide_start_time());
                //System.out.println("order "+order.getRide_start_time());
                if (compareDate(today, order.getRide_start_time())) {
                    todayList.add(order);
                }
            }
        }
        return todayList;
    }

    @NonNull
    Date getDate() {
        return new Date();
    }

    @NonNull
    LocalDate getLocalDate() {
        return new LocalDate();
    }

    private List<Order> currentWeek(List<Order> allHistory) {
        LocalDate startWeek = getLocalDate().minusWeeks(1);
        LocalDate endWeek = getLocalDate();
        Log.d(TAG, "currentWeek: start " + startWeek + " end: " + endWeek);
        startWeek = startWeek.plusDays(1);
        org.joda.time.Interval interval = new org.joda.time.Interval(startWeek.toDateTimeAtStartOfDay(), endWeek.toDateTimeAtStartOfDay());
        List<Order> weekOrders = new ArrayList<>();
        for (Order historyItem : allHistory) {
            LocalDate forCompare = new LocalDate(historyItem.getCreated());
            //Log.d(TAG, "currentWeek: comparing "+forCompare.toString());
            //forCompare = forCompare.withYear(DateTime.now().getYear());
            if (interval.contains(forCompare.toDateTimeAtStartOfDay())) {
                weekOrders.add(historyItem);
            }
        }
        return weekOrders;
    }

    private List<Order> currentMonth(List<Order> allHistory) {
        LocalDate startMonth = getLocalDate();
        LocalDate endMonth = startMonth.minusMonths(1);
        startMonth = startMonth.plusDays(1);
        org.joda.time.Interval interval = new org.joda.time.Interval(endMonth.toDateTimeAtStartOfDay(), startMonth.toDateTimeAtStartOfDay());
        List<Order> monthOrders = new ArrayList<>();
        for (Order historyItem : allHistory) {
            LocalDate forCompare = new LocalDate(historyItem.getCreated());
            forCompare = forCompare.withYear(DateTime.now().getYear());
            if (interval.contains(forCompare.toDateTimeAtStartOfDay())) {
                monthOrders.add(historyItem);
            }
        }
        return monthOrders;
    }
}
