/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.interactor.repository;

import org.jetbrains.annotations.Nullable;

import ru.manfred.manfreddriver.interactor.api.ManfredResultCallback;
import ru.manfred.manfreddriver.model.GoogleMaps.DirectionResults;
import ru.manfred.manfreddriver.model.api.AcceptingOrder;
import ru.manfred.manfreddriver.model.api.Airport;
import ru.manfred.manfreddriver.model.api.GeoData;
import ru.manfred.manfreddriver.model.api.ManfredError;
import ru.manfred.manfreddriver.model.api.Order;

public interface RideRepository {

    void finishTrip(GeoData geoData, RepoAnswer repoAnswer);

    void acceptOrder(AcceptingOrder acceptingOrder, AcceptCallback acceptCallback);

    void confirmPreOrder(Order order, AcceptCallback acceptCallback);

    void declinePreOrder(Order order, AcceptCallback acceptCallback);

    void startPreOrder(Order order, int timeToCostumer, AcceptCallback acceptCallback);

    void getActualCost(AcceptCallback acceptCallback);

    @Deprecated
    void toAirport(Airport airport);

    void cancelToAirport(AcceptCallback acceptCallback);

    void startTrip(AcceptCallback acceptCallback);

    void checkPayment(ManfredResultCallback callback);

    void onClient(ManfredResultCallback callback);

    void delayOnOrder(ManfredResultCallback callback, int lateTimeInSec);

    void sendLocation(GeoData geoData, ResponseCallback responseCallback);

    void getDirection(GeoData from, GeoData to, RepoAnswer<DirectionResults> resultCallback);

    public interface AcceptCallback {
        void error(ManfredError manfredError);

        void accepted(@Nullable Order order);

        void debugMessage(String message);

        void serverNotAvailable();
    }

    public interface RepoAnswer<T>{
        void allOk(T answer);
        void error(String error);
    }
}
