/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.interactor.repository;

import android.support.annotation.Nullable;

import ru.manfred.manfreddriver.managers.logging.AuthError;
import ru.manfred.manfreddriver.model.responces.PhoneDTO;
import ru.manfred.manfreddriver.model.responces.SMSCodeRequest;

public interface UserRepository {
    void attemptLogin(PhoneDTO phone, AuthorisationCallback answerCallback);
    void sendCode(SMSCodeRequest smsCodeRequest, AuthorisationCallback<String>authorisationCallback);
    void changeStatus(boolean isFree, AnswerCallback answerCallback);
    void logout(AnswerCallback answerCallback);

    void setNewFCMToken(String FCMtoken, AnswerCallback answerCallback);

    public interface AuthorisationCallback<T>{
        void success(@Nullable T answer);
        void error(AuthError error);
    }

    public interface AnswerCallback {
        void success();
        void notSuccess(String error);
        void authorisationRequired(String error);
        void serverNotAvailable();
    }
}
