/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.interactor.api;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by begemot on 21.02.18.
 */

public class FloatDeserializer implements JsonDeserializer<Float> {
    private static final String TAG = "FloatDeserializer";

    @Override
    public Float deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String value = json.getAsString();
        //Log.d(TAG, "deserialize: "+value);
        return json.getAsFloat();
    }
}
