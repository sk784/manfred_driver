/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.interactor.repository;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ru.manfred.manfreddriver.BuildConfig;
import ru.manfred.manfreddriver.managers.network.ManfredNetworkManager;
import ru.manfred.manfreddriver.managers.network.NetworkQuery;
import ru.manfred.manfreddriver.managers.network.WebSocketAnswer;
import ru.manfred.manfreddriver.managers.network.WebSocketAnswerCallback;
import ru.manfred.manfreddriver.model.api.AppVersion;
import ru.manfred.manfreddriver.model.api.Driver;
import ru.manfred.manfreddriver.model.api.HistoryRequestDTO;
import ru.manfred.manfreddriver.model.api.LogNet;
import ru.manfred.manfreddriver.model.api.ManfredError;
import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.model.api.OrderList;
import ru.manfred.manfreddriver.model.responces.CarList;
import ru.manfred.manfreddriver.model.responces.DriverStatus;
import ru.manfred.manfreddriver.model.responces.IsFreeResponse;
import ru.manfred.manfreddriver.model.responces.NeedUpdate;


/**
 * Created by begemot on 11.09.17.
 * Repository for driver
 */

public class WebSocketDriverRepository implements DriverRepository {
    private String token;
    private Context context;
    //private FirebaseAnalytics mFirebaseAnalytics;
    private static final String TAG = "DriverRepository";
    private ManfredNetworkManager manfredNetworkManager;

    /*
    Test repository
     */
    public WebSocketDriverRepository(String token, Context context) {
        this.token = token;
        this.context = context;
        //mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
    }

    /*
    Normal repository
     */
    public WebSocketDriverRepository(String token, ManfredNetworkManager manfredNetworkManager) {
        this.token = token;
        this.context = null;
        this.manfredNetworkManager=manfredNetworkManager;
    }

    @Override
    public void driverInfo(final infoCallback cb) {
        //ManfredNetworkManager manfredNetworkManager = CheckEventService.getManfredNetworkManager();
        WebSocketAnswerCallback<WebSocketAnswer<Driver>> driverWebSocketAnswerCallback
                = new WebSocketAnswerCallback<WebSocketAnswer<Driver>>() {
            @Override
            public void messageReceived(WebSocketAnswer<Driver> message) {
                if (message == null) {
                    cb.errorCallback();
                    return;
                }
                Log.d(TAG, "driverInfo: "+message);
                Driver driver = message.getPayload();
                if (driver != null) {
                    Log.d(TAG, "driverInfo: driver is "+driver.toString());
                    cb.fineCallback(driver);
                } else {
                    Log.d(TAG, "driverInfo: driver unknown");
                    cb.errorCallback();
                }
            }

            @Override
            public void error(ManfredError manfredError) {
                cb.errorCallback();
            }
        };

        NetworkQuery networkQuery = new NetworkQuery.OneRequestBuilder<Void,Driver>("user/profile")
                .setCallback(driverWebSocketAnswerCallback,Driver.class)
                .setToken(token)
                .buildOneRequest();
        manfredNetworkManager.addQuery(networkQuery);
    }

    @Override
    public void getHistoryByDate(Date firstDate, Date lastDate, final HistoryCallback2 historyCallback) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String firstDateString = dateFormat.format(firstDate);
        String secondDateString = dateFormat.format(lastDate);
        HistoryRequestDTO historyRequestData = new HistoryRequestDTO(firstDateString,secondDateString);
        Log.d(TAG, "getHistoryByDate: requesting "+historyRequestData.toString());
        WebSocketAnswerCallback<WebSocketAnswer<OrderList>>historyAnswer = new WebSocketAnswerCallback<WebSocketAnswer<OrderList>>() {
            @Override
            public void messageReceived(WebSocketAnswer<OrderList> message) {
                if (message.getPayload() == null) {
                    Log.d(TAG, "onResponse: null histories");
                    historyCallback.history(new ArrayList<Order>());
                } else {
                    Log.d(TAG, "onResponse: we take "+message.getPayload().getOrders().size()+" orders");
                    List<Order> clearedHistory = removeOrdersWithoutCost(message.getPayload().getOrders());
                    //Log.d(TAG, "onResponse: after removing we have "+clearedHistory.size());
                    historyCallback.history(clearedHistory);
                }
            }

            @Override
            public void error(ManfredError manfredError) {
                historyCallback.error(manfredError.getText());
            }
        };
        NetworkQuery networkQuery = new NetworkQuery.OneRequestBuilder<HistoryRequestDTO,OrderList>("order/list")
                .setDataForTransmit(historyRequestData)
                .setCallback(historyAnswer,OrderList.class)
                .setToken(token)
                .buildOneRequest();
        manfredNetworkManager.addQuery(networkQuery);
    }

    private List<Order> removeOrdersWithoutCost(List<Order> orders) {
        Log.d(TAG, "removeOrdersWithoutCost: take " + orders.size() + " orders");
        List<Order> clearedHistory = new ArrayList<>();
        for (Order order : orders) {
            if (order.getTotal_cost()!=null && order.getTotal_cost()!=0.0f) {
                clearedHistory.add(order);
            }else {
                Log.d(TAG, "removeOrdersWithoutCost: wrong cost in "+order.getId()+" is "+order.getTotal_cost());
            }
        }
        Log.d(TAG, "removeOrdersWithoutCost: after cleaning size is " + clearedHistory.size());
        return clearedHistory;
    }

    @Override
    public void getBusyStatus(ResponseCallback<IsFreeResponse> callback){
        WebSocketAnswerCallback<WebSocketAnswer<IsFreeResponse>>busyCallback =
                new WebSocketAnswerCallback<WebSocketAnswer<IsFreeResponse>>() {
                    @Override
                    public void messageReceived(WebSocketAnswer<IsFreeResponse> message) {
                        callback.allOk(message.getPayload());
                    }

                    @Override
                    public void error(ManfredError manfredError) {
                        callback.error(manfredError);
                    }
                };
        NetworkQuery query = new NetworkQuery.OneRequestBuilder<Void,IsFreeResponse>("user/check_is_free")
                .setCallback(busyCallback,IsFreeResponse.class)
                .setToken(token)
                .buildOneRequest();
        manfredNetworkManager.addQuery(query);
    }

    @Override
    public void getStatus(ResponseCallback<DriverStatus> callback) {
        //ManfredNetworkManager manfredNetworkManager = CheckEventService.getManfredNetworkManager();
        WebSocketAnswerCallback<WebSocketAnswer<DriverStatus>>statusCallback =
                new WebSocketAnswerCallback<WebSocketAnswer<DriverStatus>>() {
            @Override
            public void messageReceived(WebSocketAnswer<DriverStatus> message) {
                callback.allOk(message.getPayload());
            }

            @Override
            public void error(ManfredError manfredError) {
                callback.error(manfredError);
            }
        };
        NetworkQuery query = new NetworkQuery.OneRequestBuilder<Void,DriverStatus>("user/state")
                .setCallback(statusCallback,DriverStatus.class)
                .setToken(token)
                .buildOneRequest();
        manfredNetworkManager.addQuery(query);
    }

    @Override
    public void getPreOrders(ResponseCallback<List<Order>> callback){
        WebSocketAnswerCallback<WebSocketAnswer<OrderList>> preOrdersCallback = new WebSocketAnswerCallback<WebSocketAnswer<OrderList>>() {
            @Override
            public void messageReceived(WebSocketAnswer<OrderList> message) {
                callback.allOk(message.getPayload().getOrders());
            }

            @Override
            public void error(ManfredError manfredError) {
                callback.error(manfredError);
            }
        };
        NetworkQuery query = new NetworkQuery.OneRequestBuilder<Void,OrderList>("order/preorders")
                .setCallback(preOrdersCallback,OrderList.class)
                .setToken(token)
                .buildOneRequest();
        manfredNetworkManager.addQuery(query);
    }

    private Order cropAdres(Order order) {
        order.setAddress(order.getAddress().replaceAll("улица", "ул."));
        order.setAddress(order.getAddress().replaceAll("проспект", "пр-т"));
        order.setDest_address(order.getDest_address().replaceAll("улица", "ул."));
        order.setDest_address(order.getDest_address().replaceAll("проспект", "пр-т"));
        return order;
    }

    @Override
    public void getCarsForMap(NetAnswerCallback<CarList> callback) {
        WebSocketAnswerCallback<WebSocketAnswer<CarList>> answerCallback = new WebSocketAnswerCallback<WebSocketAnswer<CarList>>() {
            @Override
            public void messageReceived(WebSocketAnswer<CarList> message) {
                callback.allFine(message.getPayload());
            }

            @Override
            public void error(ManfredError manfredError) {
                callback.error(manfredError.getText());
            }
        };
        NetworkQuery query = new NetworkQuery.OneRequestBuilder<Void,CarList>("car_list")
                .setCallback(answerCallback,CarList.class)
                .setToken(token)
                .buildOneRequest();
        manfredNetworkManager.addQuery(query);
    }

    @Override
    public void sendLogs(List<LogNet> logs, NetAnswerCallbackWithoutPayload netAnswerCallback) {
        WebSocketAnswerCallback<WebSocketAnswer> logAnswer = new WebSocketAnswerCallback<WebSocketAnswer>() {
            @Override
            public void messageReceived(WebSocketAnswer message) {
                netAnswerCallback.allFine();
            }

            @Override
            public void error(ManfredError manfredError) {
                netAnswerCallback.error(manfredError.getText());
            }
        };
        NetworkQuery networkQuery = new NetworkQuery.OneRequestBuilder<List<LogNet>,Void>("add_log_list")
                .setDataForTransmit(logs)
                .setCallback(logAnswer,null)
                .setToken(token)
                .buildOneRequest();
        manfredNetworkManager.addQuery(networkQuery);
    }

    @Override
    public void checkForUpdate(NetAnswerCallback<Boolean> isNeedUpdate){
        WebSocketAnswerCallback<WebSocketAnswer<NeedUpdate>>updateAnswer = new WebSocketAnswerCallback<WebSocketAnswer<NeedUpdate>>() {
            @Override
            public void messageReceived(WebSocketAnswer<NeedUpdate> message) {
                isNeedUpdate.allFine(message.getPayload().getNeedUpdate());
            }

            @Override
            public void error(ManfredError manfredError) {
                isNeedUpdate.error(manfredError.getText());
            }
        };
        NetworkQuery networkQuery = new NetworkQuery.OneRequestBuilder<AppVersion,NeedUpdate>("check_update")
                .setDataForTransmit(new AppVersion(BuildConfig.VERSION_NAME))
                .setCallback(updateAnswer,NeedUpdate.class)
                .buildOneRequest();
        manfredNetworkManager.addQuery(networkQuery);
    }

    @Override
    public void getLocationNameByLatLng(Location location, NetAnswerCallback<LocationResponseModel> responseCallback) {
        WebSocketAnswerCallback<WebSocketAnswer<LocationResponseModel>>updateAnswer = new WebSocketAnswerCallback<WebSocketAnswer<LocationResponseModel>>() {
            @Override
            public void messageReceived(WebSocketAnswer<LocationResponseModel> message) {
                responseCallback.allFine(message.getPayload());
            }

            @Override
            public void error(ManfredError manfredError) {
                responseCallback.error(manfredError.getText());
            }
        };
        NetworkQuery networkQuery = new NetworkQuery.OneRequestBuilder<LocationRequestModel,LocationResponseModel>("region/get_address")
                .setDataForTransmit(new LocationRequestModel(location))
                .setCallback(updateAnswer,LocationResponseModel.class)
                .buildOneRequest();
        manfredNetworkManager.addQuery(networkQuery);
    }
}
