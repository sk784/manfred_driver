/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.interactor.repository;

import android.util.Log;

import java.util.concurrent.TimeUnit;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.interactor.api.ManfredResultCallback;
import ru.manfred.manfreddriver.managers.network.ManfredNetworkManager;
import ru.manfred.manfreddriver.managers.network.NetworkQuery;
import ru.manfred.manfreddriver.managers.network.WebSocketAnswer;
import ru.manfred.manfreddriver.managers.network.WebSocketAnswerCallback;
import ru.manfred.manfreddriver.model.GoogleMaps.DirectionResults;
import ru.manfred.manfreddriver.model.api.AcceptingOrder;
import ru.manfred.manfreddriver.model.api.AcceptingPreOrder;
import ru.manfred.manfreddriver.model.api.Airport;
import ru.manfred.manfreddriver.model.api.DelaySize;
import ru.manfred.manfreddriver.model.api.DirectionRequest;
import ru.manfred.manfreddriver.model.api.GeoData;
import ru.manfred.manfreddriver.model.api.ManfredError;
import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.model.responces.OrderResponce;

/**
 * Created by begemot on 13.09.17.
 */

public class WebSocketRideRepository implements RideRepository {
    private static final String TAG = "RideRepository";
    private String token;
    private ManfredNetworkManager manfredNetworkManager;

    public WebSocketRideRepository(String token, ManfredNetworkManager manfredNetworkManager) {
        this.token = token;
        this.manfredNetworkManager = manfredNetworkManager;
    }



    @Override
    public void finishTrip(GeoData geoData, RepoAnswer repoAnswer){
        final long start = System.currentTimeMillis();
        WebSocketAnswerCallback webSocketAnswerCallback = new WebSocketAnswerCallback() {
            @Override
            public void messageReceived(WebSocketAnswer message) {
                final long durationInMilliseconds = System.currentTimeMillis()-start;
                Log.d(TAG, "finishTrip messageReceived took "+ durationInMilliseconds + "ms.");
                repoAnswer.allOk(null);
            }

            @Override
            public void error(ManfredError manfredError) {
                final long durationInMilliseconds = System.currentTimeMillis()-start;
                Log.d(TAG, "finishTrip messageReceived took "+ durationInMilliseconds + "ms.");
                repoAnswer.error(manfredError.getText());
            }
        };
        NetworkQuery networkQuery = new NetworkQuery.OneRequestBuilder<GeoData,Void>("ride/finish")
                .setDataForTransmit(geoData)
                .setCallback(webSocketAnswerCallback,null)
                .setToken(token)
                .buildOneRequest();
        manfredNetworkManager.addQuery(networkQuery);
    }

    @Override
    public void acceptOrder(AcceptingOrder acceptingOrder, final AcceptCallback acceptCallback) {
        //AcceptingOrder acceptingOrder = new AcceptingOrder(id);
        Log.d(TAG, "accepting order... " + acceptingOrder.toString());
        WebSocketAnswerCallback<WebSocketAnswer<OrderResponce>> webSocketAnswerCallback =
                new WebSocketAnswerCallback<WebSocketAnswer<OrderResponce>>() {
            @Override
            public void messageReceived(WebSocketAnswer<OrderResponce> message) {
                Log.d(TAG, "onResponse: " + message.getResultCode());
                String debug = "We take from server: " + message.getResultCode() + ", " + message.getMessage();
                acceptCallback.debugMessage(debug);
                Order order = message.getPayload().getOrder();
                Log.d(TAG, "onResponse: order " + order.getId() + " with status " + order.getStatus());
                acceptCallback.accepted(order);
            }

            @Override
            public void error(ManfredError manfredError) {
                switch (manfredError.getResultCode()) {
                    case "invalid_order_status":
                        acceptCallback.error(manfredError);
                        break;
                    case "prepayment_error":
                        acceptCallback.error(manfredError);
                        break;
                    case "order_cancelled_by_passenger":
                        acceptCallback.error(manfredError);
                        break;
                    case "bad_request":
                        acceptCallback.error(manfredError);
                        break;
                    default:
                        acceptCallback.error(manfredError);
                        break;
                }
            }
        };
        NetworkQuery networkQuery = new NetworkQuery.OneRequestBuilder<AcceptingOrder,OrderResponce>("ride/acceptOrder")
                .setDataForTransmit(acceptingOrder)
                .setCallback(webSocketAnswerCallback,OrderResponce.class)
                .setToken(token)
                .buildOneRequest();
        //CheckEventService.getManfredNetworkManager().addQuery(networkQuery);
        manfredNetworkManager.addQuery(networkQuery);

    }

    @Override
    public void confirmPreOrder(Order order, final AcceptCallback acceptCallback) {
        confirmer(order, acceptCallback, true);
    }

    @Override
    public void declinePreOrder(Order order, final AcceptCallback acceptCallback) {
        confirmer(order, acceptCallback, false);
    }

    private void confirmer(Order order, AcceptCallback acceptCallback, boolean accept) {
        Log.d(TAG, "confirmer: accept now? " + accept);
        WebSocketAnswerCallback<WebSocketAnswer>answerCallback = new WebSocketAnswerCallback<WebSocketAnswer>() {
            @Override
            public void messageReceived(WebSocketAnswer message) {
                acceptCallback.debugMessage("we take from server " + message.getMessage());
                acceptCallback.accepted(order);
            }

            @Override
            public void error(ManfredError manfredError) {
                acceptCallback.error(manfredError);
            }
        };
        NetworkQuery query = new NetworkQuery.OneRequestBuilder<AcceptingPreOrder,Void>("order/confirm_preorder_reminder")
                .setDataForTransmit(new AcceptingPreOrder(order.getId(), accept))
                .setToken(token)
                .setCallback(answerCallback,null)
                .buildOneRequest();
        //CheckEventService.getManfredNetworkManager().addQuery(query);
        manfredNetworkManager.addQuery(query);
    }

    @Override
    public void startPreOrder(Order order, int timeToCostumer, final AcceptCallback acceptCallback) {
        WebSocketAnswerCallback preOrderResponse = new WebSocketAnswerCallback() {
            @Override
            public void messageReceived(WebSocketAnswer message) {
                order.setTime_to_costumer(timeToCostumer+(int)TimeUnit.MINUTES.toSeconds(5));
                acceptCallback.accepted(order);
            }

            @Override
            public void error(ManfredError manfredError) {
                acceptCallback.error(manfredError);
            }
        };
        NetworkQuery query = new NetworkQuery.OneRequestBuilder<AcceptingOrder,Void>("order/start_preorder")
                .setDataForTransmit(new AcceptingOrder(order.getId(), timeToCostumer))
                .setToken(token)
                .setCallback(preOrderResponse,AcceptingOrder.class)
                .buildOneRequest();
        ManfredApplication.getNetworkManger().addQuery(query);
    }

    @Override
    public void getActualCost(AcceptCallback acceptCallback){
        WebSocketAnswerCallback<WebSocketAnswer<Order>> answerCallback = new WebSocketAnswerCallback<WebSocketAnswer<Order>>() {
            @Override
            public void messageReceived(WebSocketAnswer<Order> message) {
                acceptCallback.accepted(message.getPayload());
            }

            @Override
            public void error(ManfredError manfredError) {
                acceptCallback.error(manfredError);
            }
        };
        NetworkQuery networkQuery = new NetworkQuery.OneRequestBuilder<Void,Order>("order/actual_cost")
                .setToken(token)
                .setCallback(answerCallback,Order.class)
                .buildOneRequest();
        //CheckEventService.getManfredNetworkManager().addQuery(networkQuery);
        manfredNetworkManager.addQuery(networkQuery);
    }

    @Override
    @Deprecated
    public void toAirport(Airport airport) {}

    @Override
    public void cancelToAirport(final AcceptCallback acceptCallback) {
        WebSocketAnswerCallback cancelTransferAnswer = new WebSocketAnswerCallback() {
            @Override
            public void messageReceived(WebSocketAnswer message) {
                acceptCallback.accepted(null);
            }

            @Override
            public void error(ManfredError manfredError) {
                acceptCallback.error(manfredError);
            }
        };
        NetworkQuery networkQuery = new NetworkQuery.OneRequestBuilder<Void,Void>("order/cancel_to_airport")
                .setCallback(cancelTransferAnswer,null)
                .setToken(token)
                .buildOneRequest();
        ManfredApplication.getNetworkManger().addQuery(networkQuery);
    }

    @Override
    public void startTrip(final AcceptCallback acceptCallback) {
        WebSocketAnswerCallback webSocketAnswerCallback = new WebSocketAnswerCallback() {
            @Override
            public void messageReceived(WebSocketAnswer message) {
                acceptCallback.accepted(null);
            }

            @Override
            public void error(ManfredError manfredError) {
                acceptCallback.error(manfredError);
            }
        };
        NetworkQuery networkQuery = new NetworkQuery.OneRequestBuilder<Void, Void>("ride/start")
                .setToken(token)
                .setCallback(webSocketAnswerCallback,null)
                .buildOneRequest();
        //CheckEventService.getManfredNetworkManager().addQuery(networkQuery);
        manfredNetworkManager.addQuery(networkQuery);
    }

    @Override
    public void checkPayment(final ManfredResultCallback callback) {
        WebSocketAnswerCallback<WebSocketAnswer<OrderResponce>> checkPaymentAnswer = new WebSocketAnswerCallback<WebSocketAnswer<OrderResponce>>() {
            @Override
            public void messageReceived(WebSocketAnswer<OrderResponce> message) {
                callback.allOk(message.getPayload().getOrder());
            }

            @Override
            public void error(ManfredError manfredError) {
                callback.error("Ошибка", manfredError.getText());
            }
        };
        NetworkQuery networkQuery = new NetworkQuery.OneRequestBuilder<Void,OrderResponce>("order/force_payment")
                .setToken(token)
                .setCallback(checkPaymentAnswer,OrderResponce.class)
                .buildOneRequest();
        manfredNetworkManager.addQuery(networkQuery);
    }

    @Override
    public void onClient(final ManfredResultCallback callback) {
        WebSocketAnswerCallback<WebSocketAnswer> onClient = new WebSocketAnswerCallback<WebSocketAnswer>() {
            @Override
            public void messageReceived(WebSocketAnswer message) {
                callback.allOk(null);
            }

            @Override
            public void error(ManfredError manfredError) {
                callback.error("Ошибка", manfredError.getText());
            }
        };
        NetworkQuery onClientQuery = new NetworkQuery.OneRequestBuilder<Void,Void>("ride/driver_ready")
                .setToken(token)
                .setCallback(onClient,null)
                .buildOneRequest();
        //CheckEventService.getManfredNetworkManager().addQuery(onClientQuery);
        manfredNetworkManager.addQuery(onClientQuery);
    }

    @Override
    public void delayOnOrder(final ManfredResultCallback callback, int lateTimeInSec){
        WebSocketAnswerCallback<WebSocketAnswer>answerCallback = new WebSocketAnswerCallback<WebSocketAnswer>() {
            @Override
            public void messageReceived(WebSocketAnswer message) {
                callback.allOk(null);
            }

            @Override
            public void error(ManfredError manfredError) {
                callback.error("Ошибка", manfredError.getText());
            }
        };
        NetworkQuery delayQuery = new NetworkQuery.OneRequestBuilder<DelaySize,Void>("ride/driver_delay")
                .setDataForTransmit(new DelaySize(lateTimeInSec))
                .setToken(token)
                .setCallback(answerCallback,null)
                .buildOneRequest();

        //CheckEventService.getManfredNetworkManager().addQuery(delayQuery);
        manfredNetworkManager.addQuery(delayQuery);
    }

    @Override
    public void sendLocation(GeoData geoData, ResponseCallback responseCallback){
        //Log.d(TAG, "sendLocation: ");
        WebSocketAnswerCallback<WebSocketAnswer>answerCallback = new WebSocketAnswerCallback<WebSocketAnswer>() {
            @Override
            public void messageReceived(WebSocketAnswer message) {
                //Log.d(TAG, "messageReceived: "+message);
                responseCallback.allOk(null);
            }

            @Override
            public void error(ManfredError manfredError) {
                responseCallback.error(manfredError);
            }
        };
        NetworkQuery sendLocation = new NetworkQuery.OneRequestBuilder<GeoData,Void>("ride/location")
                .setDataForTransmit(geoData)
                .setToken(token)
                .setCallback(answerCallback,null)
                .buildOneRequest();
        manfredNetworkManager.addQuery(sendLocation);
        //Log.d(TAG, "sendLocation: query added "+sendLocation);
    }

    @Override
    public void getDirection(GeoData from, GeoData to, RepoAnswer<DirectionResults> resultCallback){
        WebSocketAnswerCallback<WebSocketAnswer<DirectionResults>>directionResult = new WebSocketAnswerCallback<WebSocketAnswer<DirectionResults>>() {
            @Override
            public void messageReceived(WebSocketAnswer<DirectionResults> message) {
                //resultCallback.allOk(message.getPayload());
                resultCallback.allOk(message.getPayload());
            }

            @Override
            public void error(ManfredError manfredError) {
                resultCallback.error(manfredError.getText());
            }
        };

        NetworkQuery directionQuery = new NetworkQuery.OneRequestBuilder<DirectionRequest,DirectionResults>("ride/routes")
                .setDataForTransmit(new DirectionRequest(from,to))
                .setToken(token)
                .setCallback(directionResult,DirectionResults.class)
                .buildOneRequest();
        manfredNetworkManager.addQuery(directionQuery);
    }


}
