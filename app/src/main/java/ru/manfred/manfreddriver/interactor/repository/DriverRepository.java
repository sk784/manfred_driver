/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.interactor.repository;

import android.location.Location;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

import ru.manfred.manfreddriver.model.api.Driver;
import ru.manfred.manfreddriver.model.api.LogNet;
import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.model.responces.CarList;
import ru.manfred.manfreddriver.model.responces.DriverStatus;
import ru.manfred.manfreddriver.model.responces.IsFreeResponse;

public interface DriverRepository {
    void driverInfo(infoCallback cb);

    void getHistoryByDate(Date firstDate, Date lastDate, HistoryCallback2 historyCallback);

    void getBusyStatus(ResponseCallback<IsFreeResponse> callback);

    void getStatus(ResponseCallback<DriverStatus> callback);

    void getPreOrders(ResponseCallback<List<Order>> callback);

    void getCarsForMap(NetAnswerCallback<CarList> callback);

    void sendLogs(List<LogNet> logs, NetAnswerCallbackWithoutPayload netAnswerCallback);

    void checkForUpdate(NetAnswerCallback<Boolean> isNeedUpdate);

    void getLocationNameByLatLng(final Location location, final NetAnswerCallback<WebSocketDriverRepository.LocationResponseModel> responseCallback);

    interface HistoryCallback2 {
        void history(List<Order> history);

        void error(String error);

        void serverNotAvailable();
    }

    interface infoCallback {
        void fineCallback(Driver driver);

        void errorCallback();
    }

    interface NetAnswerCallback<T> {
        void allFine(T answer);

        void error(String error);

        void serverNotAvailable();
    }

    public interface NetAnswerCallbackWithoutPayload {
        void allFine();

        void error(String error);

        void serverNotAvailable();
    }

    class LocationResponseModel {
        @SerializedName("address")
        private final String address;
        @SerializedName("city")
        private final String city;

        public LocationResponseModel(String address, String city) {
            this.address = address;
            this.city = city;
        }

        public String getAddress() {
            return address;
        }

        public String getCity() {
            return city;
        }
    }

    class LocationRequestModel {
        @SerializedName("latitude")
        private final double latitude;
        @SerializedName("longitude")
        private final double longitude;

        public LocationRequestModel(Location location) {
            this.latitude = location.getLatitude();
            this.longitude = location.getLongitude();
        }

        public double getLatitude() {
            return latitude;
        }

        public double getLongitude() {
            return longitude;
        }
    }
}
