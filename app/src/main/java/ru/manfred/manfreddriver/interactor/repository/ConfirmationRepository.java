/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.interactor.repository;

public interface ConfirmationRepository {
    void orderReceived(int id);

    void orderCanceled(int id);

    void orderShowed(int id);
}
