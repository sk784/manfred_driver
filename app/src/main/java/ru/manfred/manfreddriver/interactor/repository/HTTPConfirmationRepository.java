/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.interactor.repository;

import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.manfred.manfreddriver.interactor.api.ApiFactory;
import ru.manfred.manfreddriver.model.api.ManfredResponse;
import ru.manfred.manfreddriver.model.responces.Id;

public class HTTPConfirmationRepository implements ConfirmationRepository {
    private static final String TAG = "HTTPConfirmationReposit";
    private String token;

    public HTTPConfirmationRepository(String token) {
        this.token = token;
    }

    @Override
    public void orderReceived(int id) {
        Log.d(TAG, "orderReceived: " + id + "setToken is " + token);
        ApiFactory.getRideService().newOrderReceived(token, new Id(id)).enqueue(new Callback<ManfredResponse>() {
            @Override
            public void onResponse(Call<ManfredResponse> call, Response<ManfredResponse> response) {
                Log.d(TAG, "orderReceived," + id + "confirmed from server");
            }

            @Override
            public void onFailure(Call<ManfredResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    @Override
    public void orderCanceled(int id) {
        ApiFactory.getRideService().newOrderCancelReceived(token, new Id(id)).enqueue(new Callback<ManfredResponse>() {
            @Override
            public void onResponse(Call<ManfredResponse> call, Response<ManfredResponse> response) {
                Log.d(TAG, "orderCanceled");
            }

            @Override
            public void onFailure(Call<ManfredResponse> call, Throwable t) {

            }
        });

    }

    @Override
    public void orderShowed(int id) {
        ApiFactory.getRideService().newOrderShowed(token, new Id(id)).enqueue(new Callback<ManfredResponse>() {
            @Override
            public void onResponse(Call<ManfredResponse> call, Response<ManfredResponse> response) {
                Log.d(TAG, "orderShowed");
            }

            @Override
            public void onFailure(Call<ManfredResponse> call, Throwable t) {

            }
        });

    }
}
