/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.interactor.repository;

import ru.manfred.manfreddriver.managers.network.NetworkManager;
import ru.manfred.manfreddriver.managers.network.NetworkQuery;
import ru.manfred.manfreddriver.managers.network.WebSocketAnswer;
import ru.manfred.manfreddriver.managers.network.WebSocketAnswerCallback;
import ru.manfred.manfreddriver.model.api.ManfredError;
import ru.manfred.manfreddriver.model.responces.Id;

/**
 * Created by begemot on 02.11.17.
 */

public class WebSocketConfirmationRepository implements ConfirmationRepository {
    private String token;
    private static final String TAG = "ConfirmationRepository";
    private final NetworkManager networkManager;

    public WebSocketConfirmationRepository(String token, NetworkManager networkManager) {
        this.token = token;
        this.networkManager = networkManager;
    }

    @Override
    public void orderReceived(int id) {
        WebSocketAnswerCallback webSocketAnswerCallback = new WebSocketAnswerCallback() {
            @Override
            public void messageReceived(WebSocketAnswer message) {

            }

            @Override
            public void error(ManfredError manfredError) {

            }
        };
        NetworkQuery networkQuery = new NetworkQuery.OneRequestBuilder<Id,Void>("order/new_order_received")
                .setDataForTransmit(new Id(id))
                .setCallback(webSocketAnswerCallback,null)
                .setToken(token)
                .buildOneRequest();
        networkManager.addQuery(networkQuery);

    }

    @Override
    public void orderCanceled(int id) {
        WebSocketAnswerCallback webSocketAnswerCallback = new WebSocketAnswerCallback() {
            @Override
            public void messageReceived(WebSocketAnswer message) {

            }

            @Override
            public void error(ManfredError manfredError) {

            }
        };
        NetworkQuery networkQuery = new NetworkQuery.OneRequestBuilder<Id,Void>("order/cancel_received")
                .setDataForTransmit(new Id(id))
                .setCallback(webSocketAnswerCallback,null)
                .setToken(token)
                .buildOneRequest();
        networkManager.addQuery(networkQuery);
    }

    @Override
    public void orderShowed(int id) {
        WebSocketAnswerCallback webSocketAnswerCallback = new WebSocketAnswerCallback() {
            @Override
            public void messageReceived(WebSocketAnswer message) {

            }

            @Override
            public void error(ManfredError manfredError) {

            }
        };
        NetworkQuery networkQuery = new NetworkQuery.OneRequestBuilder<Id,Void>("order/new_order_read")
                .setDataForTransmit(new Id(id))
                .setCallback(webSocketAnswerCallback,null)
                .setToken(token)
                .buildOneRequest();
        networkManager.addQuery(networkQuery);
    }
}
