package ru.manfred.manfreddriver.interactor.api.websocket;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocketListener;
import okio.ByteString;
import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.managers.logging.LogTags;
import ru.manfred.manfreddriver.managers.network.EventProcessedAnswer;
import ru.manfred.manfreddriver.managers.network.LogListener;
import ru.manfred.manfreddriver.managers.network.WebSocketAnswer;
import ru.manfred.manfreddriver.managers.network.WebSocketRequest;
import ru.manfred.manfreddriver.utils.SharedPreferencesManager;

public class OKHttpWebSocketClient implements WebSocketClient {
    private static final int NORMAL_CLOSURE_STATUS = 1000;
    private static final String TAG = "OKHttpWebSocketClient";
    private static final long RECONNECT_DELAY_MS = 3000;
    private final MessageListener listener;
    private final String[] hosts;
    private String host;
    @Nullable private okhttp3.WebSocket webSocket =null;
    @Nullable private SocketConnectionListener socketConnectionListener;
    @Nullable private LogListener logListener;

//    public OKHttpWebSocketClient(MessageListener listener, String host) {
//        this.listener = listener;
//        this.host = host;
//    }

    public OKHttpWebSocketClient(MessageListener listener, String[] hosts) {
        this.listener = listener;
        this.hosts = hosts;
        Context context = ManfredApplication.instance.getApplicationContext();
        String lastConnectionHost = SharedPreferencesManager.getLastConnectionHost(context);
        if(lastConnectionHost == null) {
            if (hosts.length > 1) {
                Random random = new Random();
                int index = random.nextInt(hosts.length);
                lastConnectionHost = hosts[index];
            } else {
                lastConnectionHost = hosts[0];
            }
            SharedPreferencesManager.setLastConnectionHost(context, lastConnectionHost);
        }
        this.host = "wss://" + lastConnectionHost + "/ws/api/v1.1/clone-1/driver/";
        Log.d(TAG, "set connection host: " + this.host);
    }

    private WebSocketListener createMessagingListener() {
        Log.d(TAG, "createMessagingListener: ");
        return new WebSocketListener() {
            @Override
            public void onOpen(okhttp3.WebSocket webSocket, Response response) {
                super.onOpen(webSocket, response);
                Log.d(TAG, "onOpen: ");
                if (socketConnectionListener != null) {
                    socketConnectionListener.onConnectionChange(true);
                }
                if (logListener != null) {
                    logListener.addLog("OKHttpWebSocketClient", "onOpen", "", LogTags.OTHER);
                }
            }

            @Override
            public void onMessage(okhttp3.WebSocket webSocket, String text) {
                super.onMessage(webSocket, text);
                listener.onSocketMessage(text);
            }

            @Override
            public void onMessage(okhttp3.WebSocket webSocket, ByteString bytes) {
                super.onMessage(webSocket, bytes);
            }

            @Override
            public void onClosing(okhttp3.WebSocket webSocket, int code, String reason) {
                super.onClosing(webSocket, code, reason);
                if (logListener != null) {
                    logListener.addLog("WebSocketClient", "WebSocketListener",
                            "onClosing: because "+reason+", code is "+code, LogTags.OTHER);
                }
                close();
            }

            @Override
            public void onClosed(okhttp3.WebSocket webSocket, int code, String reason) {
                super.onClosed(webSocket, code, reason);
                if (logListener != null) {
                    logListener.addLog("WebSocketClient", "WebSocketListener",
                            "onClosed: because "+reason+", code is "+code, LogTags.OTHER);
                }
                if (socketConnectionListener != null) {
                    socketConnectionListener.onConnectionChange(false);
                }
                socketDisconnected();
            }

            @Override
            public void onFailure(okhttp3.WebSocket webSocket, Throwable t, @Nullable Response response) {
                super.onFailure(webSocket, t, response);
                Log.d(TAG, "onFailure: "+t.getMessage());
                if (socketConnectionListener != null) {
                    Log.d(TAG, "onFailure: changing connect state");
                    socketConnectionListener.onConnectionChange(false);
                }
                if (logListener != null) {
                    logListener.addLog("WebSocketClient", "WebSocketListener",
                            "onFailure: because "+t.getMessage(), LogTags.OTHER);
                }
                socketDisconnected();
                schedulleReconnect();
            }
        };
    }

    private AtomicBoolean isReconnectingTimed=new AtomicBoolean(false);
    private void schedulleReconnect(){
        if(isReconnectingTimed.get()){
            Log.d(TAG, "reconnect: is scheduled, ignoring");
            return;
        }
        if (logListener != null) {
            logListener.addLog("WebSocketClient", "reconnect",
                    "scheduling for "+ RECONNECT_DELAY_MS +"ms", LogTags.OTHER);
        }
        isReconnectingTimed.set(true);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                connectNextHost();
                isReconnectingTimed.set(false);
            }
        }, RECONNECT_DELAY_MS);
    }

    private void socketDisconnected(){
        this.webSocket=null;
        if (logListener != null) {
            logListener.addLog("OKHttpWebSocketClient", "socketDisconnected",
                    "socket utilisation", LogTags.OTHER);
        }
    }

    @Override
    public void setLogListener(LogListener logListener) {
        this.logListener=logListener;
        logListener.addLog("OKHttpWebSocketClient", "setLogListener",
                "create, logging enabled", LogTags.OTHER);
    }

    @Override
    public void setSocketConnectionListener(@NonNull SocketConnectionListener socketConnectionListener) {
        this.socketConnectionListener=socketConnectionListener;
    }

    @Override
    public void removeConnectionListener() {
        this.socketConnectionListener=null;
    }

    @Override
    public void connect() {
        Log.d(TAG, "connect: ");
        OkHttpClient client = new OkHttpClient.Builder()
                .writeTimeout(10, TimeUnit.SECONDS)
                .build();
        Request request = new Request.Builder()
                .url(host)
                .build();
        this.webSocket = client.newWebSocket(request, createMessagingListener());
    }

    public void connectNextHost() {
        Log.d(TAG, "connectNextHost: ");
        int index = 0;
        for(int i=0; i<hosts.length; ++i) {
            String hostname = hosts[i];
            if(host.contains("wss://" + hostname)) {
                index = i;
                break;
            }
        }
        int nextIndex = (index + 1) % hosts.length;
        String lastConnectionHost = hosts[nextIndex];
        SharedPreferencesManager.setLastConnectionHost(ManfredApplication.instance.getApplicationContext(), lastConnectionHost);
        Log.d(TAG, "set connection host: " + this.host);
        this.host = "wss://" + lastConnectionHost + "/ws/api/v1.1/clone-1/driver/";
        OkHttpClient client = new OkHttpClient.Builder()
                .writeTimeout(10, TimeUnit.SECONDS)
                .build();
        Request request = new Request.Builder()
                .url(host)
                .build();
        this.webSocket = client.newWebSocket(request, createMessagingListener());
    }

    @Override
    public void sendRequest(WebSocketRequest request) {
        //Log.d(TAG, "sendRequest: "+request.getType());
        Gson gson = new Gson();
        String stringRequest = gson.toJson(request);
        if (webSocket != null) {
            boolean sended = webSocket.send(stringRequest);
            if(!sended){
                listener.onSocketMessage(createErrorAnswer(request));
                Log.d(TAG, "sendRequest: not sended");
            }else {
                //Log.d(TAG, "sendRequest: sended");
            }
        }else {
            Log.d(TAG, "sendRequest: websocket is null, recreating");
            connectNextHost();
            webSocket.send(stringRequest);
        }
    }

    private String createErrorAnswer(WebSocketRequest request){
        final WebSocketAnswer webSocketAnswer = new WebSocketAnswer<Void>(new Random().nextLong(), request.getId(),
                (new Date().getTime() / 1000), request.getType(), "Связь не установлена",
                "sending_error", "error", null);
        Gson gson = new GsonBuilder().create();
        return gson.toJson(webSocketAnswer);
    }

    @Override
    public void sendEventReceived(EventProcessedAnswer eventProcessedAnswer) {
        if(webSocket!=null) {
            Log.d(TAG, "event "+eventProcessedAnswer.getIdOfRequest()+" processed, sending answer ");
            Gson gson = new Gson();
            webSocket.send((gson.toJson(eventProcessedAnswer)));
        }
    }

    @Override
    public void close() {
        Log.d(TAG, "close: ");
        if (webSocket != null) {
            webSocket.close(NORMAL_CLOSURE_STATUS,null);
        }
    }

    @Override
    public void inetOff() {
        Log.d(TAG, "inetOff: ");
        //close();
    }

    @Override
    public void inetOn() {
        Log.d(TAG, "inetOn: ");
        //connect();
    }
}
