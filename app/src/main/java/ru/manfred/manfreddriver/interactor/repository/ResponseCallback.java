/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.interactor.repository;

import ru.manfred.manfreddriver.model.api.ManfredError;

/**
 * Created by begemot on 26.11.17.
 */

public interface ResponseCallback<T> {
    public void allOk(T response);

    public void error(ManfredError error);
}
