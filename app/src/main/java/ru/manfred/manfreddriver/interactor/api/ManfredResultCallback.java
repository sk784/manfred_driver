/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.interactor.api;

import ru.manfred.manfreddriver.model.api.Order;

/**
 * Created by begemot on 15.02.18.
 */

public interface ManfredResultCallback {
    void error(String title, String message);

    void allOk(Order order);

    void debugMessage(String message);

    void serverNotAvailable();
}
