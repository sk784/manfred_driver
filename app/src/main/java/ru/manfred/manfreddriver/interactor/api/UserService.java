/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.interactor.api;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import ru.manfred.manfreddriver.model.api.ManfredResponse;
import ru.manfred.manfreddriver.model.api.ResponseAuth;
import ru.manfred.manfreddriver.model.responces.ManfredPushToken;
import ru.manfred.manfreddriver.model.responces.ManfredResponceDriverStatus;
import ru.manfred.manfreddriver.model.responces.ManfredResponseDriver;
import ru.manfred.manfreddriver.model.responces.PhoneDTO;
import ru.manfred.manfreddriver.model.responces.SMSCodeRequest;

/**
 * Created by begemot on 04.07.17.
 */

public interface UserService {

    @GET("v1.1/clone-1/driver/user/logout/")
    Call<ManfredResponceDriverStatus> logout(@Query("token") String token);

    @GET("v1.1/clone-1/driver/user/profile/")
    Call<ManfredResponseDriver> getDriver(@Query("token") String token);

    @POST("v1.1/clone-1/driver/user/set_push_token/")
    Call<ManfredResponseDriver> setFCMToken(@Query("token") String token, @Body ManfredPushToken push_token);

    @POST("/api/v1.1/clone-1/driver/user/register_phone/")
    Call<ManfredResponse> login(@Body PhoneDTO phone);

    @POST("/api/v1.1/clone-1/driver/user/sms_code/")
    Call<ManfredResponse<ResponseAuth>> sendSMSCode(@Body SMSCodeRequest request);

}
