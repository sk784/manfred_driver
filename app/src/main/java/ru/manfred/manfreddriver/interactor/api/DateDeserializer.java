/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.interactor.api;

import android.util.Log;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by begemot on 26.12.17.
 */

public class DateDeserializer implements JsonDeserializer<Date> {
    private static final String TAG = "DateDeserializer";

    @Override
    public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String date = json.getAsString();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));

        try {
            //Log.d(TAG, "try to deserialize: "+date);
            return formatter.parse(date);
        } catch (ParseException e) {
            //System.err.println("Failed to parse Date due to:", e);
            Log.e(TAG, "deserialize: " + e);
            return null;
        }
    }
}
