/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.interactor.api;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import ru.manfred.manfreddriver.model.GoogleMaps.DirectionResults;
import ru.manfred.manfreddriver.model.api.AcceptingOrder;
import ru.manfred.manfreddriver.model.api.AcceptingPreOrder;
import ru.manfred.manfreddriver.model.api.DirectionRequest;
import ru.manfred.manfreddriver.model.api.GeoData;
import ru.manfred.manfreddriver.model.api.ManfredResponse;
import ru.manfred.manfreddriver.model.api.SendLocations;
import ru.manfred.manfreddriver.model.responces.Id;
import ru.manfred.manfreddriver.model.responces.ManfredResponceAcceptingOrder;

/**
 * Created by begemot on 04.07.17.
 */

public interface RideService {
    @POST("v1.1/clone-1/driver/ride/acceptOrder/")
    Call<ManfredResponceAcceptingOrder> acceptOrder(@Query("token") String token, @Body AcceptingOrder acceptingOrder);

    @POST("v1.1/clone-1/driver/order/start_preorder/")
    Call<ManfredResponse> startPreOrder(@Query("token") String token, @Body AcceptingOrder acceptingOrder);

    @POST("v1.1/clone-1/driver/order/confirm_preorder_reminder/")
    Call<ManfredResponse> confirmPreOrder(@Query("token") String token, @Body AcceptingPreOrder acceptingPreOrder);

    @POST("v1.1/clone-1/driver/ride/declineOrder/")
    Call<ManfredResponse> declineOrder(@Query("token") String token, @Body long id);

    @POST("v1.1/clone-1/driver/ride/delayOrder/")
    Call<ManfredResponse> delayOrder(@Query("token") String token, @Body int delayMin);

    @POST("v1.1/clone-1/driver/ride/location/")
    Call<ManfredResponse> sendLocation(@Query("token") String token, @Body GeoData geoData);

    @POST("v1.1/clone-1/driver/ride/driver_ready/")
    Call<ManfredResponse> onClient(@Query("token") String token);

    @POST("v1.1/clone-1/driver/ride/start/")
    Call<ManfredResponse> startTrip(@Query("token") String token);

    @POST("v1.1/clone-1/driver/ride/finish/")
    Call<ManfredResponse> finishTrip(@Query("token") String token, @Body GeoData geoData);

    @POST("v1.1/clone-1/driver/order/set_to_airport/")
    Call<ManfredResponse> toAirport(@Query("token") String token, @Body GeoData geoData);

    @POST("v1.1/clone-1/driver/order/cancel_to_airport/")
    Call<ManfredResponse> cancelToAirport(@Query("token") String token);

    @POST("v1.1/clone-1/driver/order/new_order_received/")
    Call<ManfredResponse> newOrderReceived(@Query("token") String token, @Body Id orderId);

    @POST("v1.1/clone-1/driver/order/cancel_received/")
    Call<ManfredResponse> newOrderCancelReceived(@Query("token") String token, @Body Id orderId);

    @POST("v1.1/clone-1/driver/order/new_order_read/")
    Call<ManfredResponse> newOrderShowed(@Query("token") String token, @Body Id orderId);

    @POST("v1.1/clone-1/driver/ride/routes/")
    Call<ManfredResponse<DirectionResults>>getDirection(@Query("token") String token, @Body DirectionRequest directionRequest);

    @GET("v1.1/clone-1/driver/order/actual_cost/")
    Call<ManfredResponceAcceptingOrder> getActualCost(@Query("token") String token);

    @POST("v1.1/clone-1/driver/ride/locations/")
    Call<ManfredResponse> sendNotAcceptedLocations(@Query("token") String token, @Body SendLocations sendLocations);

    @POST("v1.1/clone-1/driver/ride/client_filtered_locations/")
    Call<ManfredResponse> sendFilteredLocations(@Query("token") String token, @Body SendLocations sendLocations);

}
