/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.interactor.api;


import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.manfred.manfreddriver.BuildConfig;

/**
 * Created by begemot on 04.07.17.
 */

public class ApiFactory {
    private static volatile OkHttpClient.Builder CLIENT = new OkHttpClient.Builder();
    private static GoogleMapsService googleMapsService;

    private static Retrofit retrofit;
    public final static String BASE_URL = BuildConfig.BASE_URL;
    private static UserService userService;
    private static RideService rideService;
    private static DriverService driverService;


    @NonNull
    public static GoogleMapsService getMapService() {
        if(googleMapsService==null){
            googleMapsService=getRetrofit("https://maps.googleapis.com/").create(GoogleMapsService.class);
        }
        return googleMapsService;
    }

    @NonNull
    private static Retrofit getRetrofit(String baseUrl) {
            CLIENT.readTimeout(30, TimeUnit.SECONDS);
            CLIENT.writeTimeout(30, TimeUnit.SECONDS);
            CLIENT.connectTimeout(30, TimeUnit.SECONDS);

            Gson gson = new GsonBuilder()
                    //.setDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
                    .serializeNulls()
                    .registerTypeAdapter(Date.class, new DateDeserializer())
                    .registerTypeAdapter(Float.class, new FloatDeserializer())
                    .create();

            return new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(CLIENT.build())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
    }

    @NonNull
    public static UserService getUserService() {
        if(userService==null){
            userService = getRetrofit(BASE_URL).create(UserService.class);
        }
        return userService;
    }

    @NonNull
    public static RideService getRideService() {
        if(rideService==null){
            rideService = getRetrofit(BASE_URL).create(RideService.class);
        }
        return rideService;
    }

    @NonNull
    public static DriverService getDriverService() {
        if(driverService==null){
            driverService=getRetrofit(BASE_URL).create(DriverService.class);
        }
        return driverService;
    }

}
