/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.interactor.repository;

import android.location.Location;
import android.support.annotation.NonNull;
import android.util.Log;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.manfred.manfreddriver.BuildConfig;
import ru.manfred.manfreddriver.interactor.api.ApiFactory;
import ru.manfred.manfreddriver.model.api.Driver;
import ru.manfred.manfreddriver.model.api.LogNet;
import ru.manfred.manfreddriver.model.api.ManfredError;
import ru.manfred.manfreddriver.model.api.ManfredResponse;
import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.model.api.PathDate;
import ru.manfred.manfreddriver.model.responces.CarList;
import ru.manfred.manfreddriver.model.responces.DriverStatus;
import ru.manfred.manfreddriver.model.responces.IsFreeResponse;
import ru.manfred.manfreddriver.model.responces.ListLog;
import ru.manfred.manfreddriver.model.responces.ManfredResponceDriverStatus;
import ru.manfred.manfreddriver.model.responces.ManfredResponseCarsOnLine;
import ru.manfred.manfreddriver.model.responces.ManfredResponseDriver;
import ru.manfred.manfreddriver.model.responces.ManfredResponseNeedUpdate;
import ru.manfred.manfreddriver.model.responces.ManfredResponseOrderHistory;

public class HTTPDriverRepository implements DriverRepository {
    private String token;
    private static final String TAG = "HTTPDriverRepository";

    public HTTPDriverRepository(String token) {
        this.token = token;
    }

    @Override
    public void driverInfo(infoCallback cb) {
        ApiFactory.getUserService().getDriver(token).enqueue(new retrofit2.Callback<ManfredResponseDriver>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponseDriver> call, @NonNull Response<ManfredResponseDriver> response) {
                ManfredResponseDriver manfredResponseDriver = response.body();
                if (manfredResponseDriver == null) {
                    cb.errorCallback();
                    return;
                }
                Driver driver = manfredResponseDriver.getData();
                if (driver != null) {

                    cb.fineCallback(driver);
                } else {
                    cb.errorCallback();
                }
                //Log.d(TAG, "onResponse: driver setted "+driver.getName());
                //initDatabase();
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponseDriver> call, @NonNull Throwable t) {
                cb.errorCallback();
            }
        });

    }

    @Override
    public void getHistoryByDate(Date firstDate, Date lastDate, HistoryCallback2 historyCallback) {
        ApiFactory.getDriverService().getHistoryWithInterval(token, new PathDate(firstDate), new PathDate(lastDate)).enqueue(new Callback<ManfredResponseOrderHistory>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponseOrderHistory> call, @NonNull Response<ManfredResponseOrderHistory> response) {
                Log.d(TAG, "onResponse: call is " + call.request().toString());
                ManfredResponseOrderHistory manfredResponseOrderHistory = response.body();
                if (manfredResponseOrderHistory != null) {
                    //Log.d(TAG, "onResponse: "+manfredResponseOrderHistory.getResult_code()+", "+manfredResponseOrderHistory.getMessage());
                    if (manfredResponseOrderHistory.getData() == null) {
                        Log.d(TAG, "onResponse: null histories");
                        historyCallback.history(new ArrayList<Order>());
                    } else {
                        Log.d(TAG, "onResponse: we take "+manfredResponseOrderHistory.getData().getOrders().size()+" orders");
                        List<Order> clearedHistory = removeOrdersWithoutCost(manfredResponseOrderHistory.getData().getOrders());
                        Log.d(TAG, "onResponse: after removing we have "+clearedHistory.size());
                        historyCallback.history(clearedHistory);
                    }
                } else {
                    historyCallback.error("пустой ответ");
                }
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponseOrderHistory> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
                if (t instanceof SocketTimeoutException || t instanceof java.net.UnknownHostException) {
                    historyCallback.serverNotAvailable();
                } else {
                    historyCallback.error(t.getMessage());
                }
            }
        });

    }

    private List<Order> removeOrdersWithoutCost(List<Order> orders) {
        Log.d(TAG, "removeOrdersWithoutCost: take " + orders.size() + " orders");
        List<Order> clearedHistory = new ArrayList<>();
        for (Order order : orders) {
            if (order.getTotal_cost()!=null && order.getTotal_cost()!=0.0f) {
                clearedHistory.add(order);
            }
        }
        Log.d(TAG, "removeOrdersWithoutCost: after cleaning size is " + clearedHistory.size());
        return clearedHistory;
    }


    @Override
    public void getBusyStatus(ResponseCallback<IsFreeResponse> callback) {
        ApiFactory.getDriverService().checkForFree(token).enqueue(new Callback<ManfredResponse<IsFreeResponse>>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponse<IsFreeResponse>> call, @NonNull Response<ManfredResponse<IsFreeResponse>> response) {
                if (response.body() != null) {
                    if(response.body().getResult_code().equals("success")){
                        callback.allOk(response.body().getData());
                    }else {
                        callback.error(new ManfredError(response.body().getMessage(), ManfredError.ErrorStatus.NET_ERROR));
                    }
                }else {
                    callback.error(new ManfredError("Сервер вернул пустой ответ", ManfredError.ErrorStatus.NET_ERROR));
                }
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponse<IsFreeResponse>> call, @NonNull Throwable t) {
                if (t instanceof SocketTimeoutException || t instanceof java.net.UnknownHostException) {
                    callback.error(new ManfredError("Сервер недоступен",ManfredError.ErrorStatus.SERVER_UNAVAILABLE_ERROR));
                } else {
                    callback.error(new ManfredError(t.getMessage(),ManfredError.ErrorStatus.NET_ERROR));
                }
            }
        });
    }

    @Override
    public void getStatus(ResponseCallback<DriverStatus> callback) {
        ApiFactory.getDriverService().getStatus(token).enqueue(new Callback<ManfredResponceDriverStatus>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponceDriverStatus> call, @NonNull Response<ManfredResponceDriverStatus> response) {
                ManfredResponceDriverStatus manfredResponceDriverStatus = response.body();
                if (manfredResponceDriverStatus == null) {
                    callback.error(new ManfredError("Сервер вернул неверные данные", ManfredError.ErrorStatus.NET_ERROR));
                } else {
                    callback.allOk(manfredResponceDriverStatus.getDriverStatus());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponceDriverStatus> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t.getClass());
                if (t instanceof SocketTimeoutException || t instanceof java.net.UnknownHostException) {
                    callback.error(new ManfredError(t.getMessage(), ManfredError.ErrorStatus.SERVER_UNAVAILABLE_ERROR));
                } else {
                    callback.error(new ManfredError(t.getMessage(), ManfredError.ErrorStatus.NET_ERROR));
                }
            }
        });

    }

    @Override
    public void getPreOrders(ResponseCallback<List<Order>> callback) {
        ApiFactory.getDriverService().getPreOrders(token).enqueue(new Callback<ManfredResponseOrderHistory>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponseOrderHistory> call, @NonNull Response<ManfredResponseOrderHistory> response) {
                ManfredResponseOrderHistory manfredResponseOrderHistory = response.body();
                if (manfredResponseOrderHistory == null) {
                    callback.error(new ManfredError("сервер вернул пустой ответ", ManfredError.ErrorStatus.NET_ERROR));
                    return;
                }
                if (manfredResponseOrderHistory.getData() != null) {
                    callback.allOk(manfredResponseOrderHistory.getData().getOrders());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponseOrderHistory> call, @NonNull Throwable t) {
                if (t instanceof SocketTimeoutException || t instanceof java.net.UnknownHostException) {
                    callback.error(new ManfredError(t.getMessage(), ManfredError.ErrorStatus.SERVER_UNAVAILABLE_ERROR));
                } else {
                    callback.error(new ManfredError(t.getMessage(), ManfredError.ErrorStatus.NET_ERROR));
                }
            }
        });

    }

    @Override
    public void getCarsForMap(NetAnswerCallback<CarList> callback) {
        ApiFactory.getDriverService().getCarsOnMap(token).enqueue(new Callback<ManfredResponseCarsOnLine>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponseCarsOnLine> call, @NonNull Response<ManfredResponseCarsOnLine> response) {
                ManfredResponseCarsOnLine manfredResponseCarsOnLine = response.body();
                if (manfredResponseCarsOnLine != null) {
                    //Log.d(TAG, "onResponse: we have drivers "+manfredResponseCarsOnLine.getData().getDrivers().size());
                    CarList carList = manfredResponseCarsOnLine.getData();
                    if (carList != null) {
                        callback.allFine(carList);
                    }
                } else {
                    callback.error("null answer");
                }
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponseCarsOnLine> call, @NonNull Throwable t) {
                if (t instanceof SocketTimeoutException || t instanceof java.net.UnknownHostException) {
                    callback.serverNotAvailable();
                } else {
                    callback.error(t.getMessage());
                }
            }
        });

    }

    @Override
    public void sendLogs(List<LogNet> logs, NetAnswerCallbackWithoutPayload netAnswerCallback) {
        ApiFactory.getDriverService().sendLogs(token, new ListLog(logs)).enqueue(new Callback<ManfredResponse>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponse> call, @NonNull Response<ManfredResponse> response) {
                ManfredResponse resp = response.body();
                if (resp != null) {
                    Log.d(TAG, "change status onResponse: " + resp.getResult_code() + ", " + resp.getMessage());
                    if (Objects.equals(resp.getResult_code(), "success")) {
                        netAnswerCallback.allFine();
                    } else {
                        netAnswerCallback.error("null answer");
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponse> call, @NonNull Throwable t) {
                if (t instanceof SocketTimeoutException || t instanceof java.net.UnknownHostException) {
                    netAnswerCallback.serverNotAvailable();
                } else {
                    netAnswerCallback.error(t.getMessage());
                }
            }
        });

    }

    @Override
    public void checkForUpdate(NetAnswerCallback<Boolean> isNeedUpdate) {
        ApiFactory.getDriverService().checkForUpdate("android", BuildConfig.VERSION_NAME)
                .enqueue(new Callback<ManfredResponseNeedUpdate>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponseNeedUpdate> call,
                                   @NonNull Response<ManfredResponseNeedUpdate> response) {
                ManfredResponseNeedUpdate needUpdate = response.body();
                if(needUpdate!=null){
                    isNeedUpdate.allFine(needUpdate.getData().getNeedUpdate());
                }else {
                    isNeedUpdate.error("Пустой ответ");
                }
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponseNeedUpdate> call, @NonNull Throwable t) {
                if (t instanceof SocketTimeoutException || t instanceof java.net.UnknownHostException) {
                    isNeedUpdate.serverNotAvailable();
                } else {
                    isNeedUpdate.error(t.getMessage());
                }
            }
        });

    }

    @Override
    public void getLocationNameByLatLng(Location location, NetAnswerCallback<LocationResponseModel> responseCallback) {
        responseCallback.error("http not supported");
    }
}
