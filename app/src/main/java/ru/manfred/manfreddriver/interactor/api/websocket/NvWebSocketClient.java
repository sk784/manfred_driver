/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.interactor.api.websocket;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketAdapter;
import com.neovisionaries.ws.client.WebSocketCloseCode;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketFactory;
import com.neovisionaries.ws.client.WebSocketFrame;
import com.neovisionaries.ws.client.WebSocketState;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.SSLContext;

import ru.manfred.manfreddriver.managers.logging.LogTags;
import ru.manfred.manfreddriver.managers.network.EventProcessedAnswer;
import ru.manfred.manfreddriver.managers.network.LogListener;
import ru.manfred.manfreddriver.managers.network.WebSocketRequest;
import ru.manfred.manfreddriver.managers.network.WebsocketLog;

//https://github.com/stfalcon-studio/code_example/blob/master/android/websocket/ClientWebSocket.java
@Deprecated
public class NvWebSocketClient implements WebSocketClient {
    private static final String TAG = "Websocket";
    private static final int RECONNECT_DELAY = 5000;
    private MessageListener listener;
    @Nullable private SocketConnectionListener socketConnectionListener;
    private String host;
    private WebSocket ws;
    private List<WebSocketRequest> notSendedRequests = new ArrayList<>();
    private LogListener logListener;
    private List<WebsocketLog>websocketLogs=new ArrayList<>();

    @Override
    public void setLogListener(LogListener logListener) {
        this.logListener = logListener;
        List<WebsocketLog>sandedLogs=new ArrayList<>();
        if(!websocketLogs.isEmpty()){
            for (WebsocketLog websocketLog : websocketLogs){
                addLog(websocketLog);
                sandedLogs.add(websocketLog);
            }
            websocketLogs.removeAll(sandedLogs);
        }
    }

    public NvWebSocketClient(MessageListener listener, String host) {
        this.listener = listener;
        this.host = host;
        Log.i(TAG, "WebSocketClient: server is "+host);
        addLog("WebSocketClient", "WebSocketClient", "creating NvWebSocketClient", LogTags.OTHER);
    }

    private void addLog(String className, String methodName, String text, LogTags logTag){
        Log.d(TAG, "addLog: "+text);
        if(logListener!=null){
            logListener.addLog(className,methodName,text,logTag);
        }else {
            websocketLogs.add(new WebsocketLog(className,methodName,text,logTag));
        }
    }

    private void addLog(WebsocketLog websocketLog){
        if(logListener!=null){
            logListener.addLog(websocketLog.getClassName(),websocketLog.getMethodName(),
                    websocketLog.getText(),websocketLog.getLogTag());
        }
    }

    @Override
    public void setSocketConnectionListener(@NonNull SocketConnectionListener socketConnectionListener) {
        Log.d(TAG, "setSocketConnectionListener: ");
        this.socketConnectionListener = socketConnectionListener;
    }

    @Override
    public void removeConnectionListener(){
        this.socketConnectionListener =null;
    }

    @Override
    public void connect() {
        Log.d(TAG, "connect: ");
        new Thread(() -> {
            if (ws != null) {
                reconnect();
            } else {
                try {
                    WebSocketFactory factory = new WebSocketFactory();
                    SSLContext context = NaiveSSLContext.getInstance("TLS");
                    factory.setSSLContext(context);
                    ws = factory.createSocket(host);
                    ws.addListener(new SocketListener());
                    ws.connect();
                    Log.d(TAG, "connect: connected");
                    addLog("WebSocketClient", "connect", "connected", LogTags.OTHER);
                } catch (WebSocketException | IOException | NoSuchAlgorithmException e) {
                    e.printStackTrace();
                    if (socketConnectionListener != null) {
                        socketConnectionListener.onConnectionChange(false);
                        Log.d(TAG, "run: notify listener about connection");
                        addLog("WebSocketClient", "connect",
                                "notify listener about connection", LogTags.OTHER);
                    }else {
                        Log.d(TAG, "run: socketConnectionListener is null");
                    }
                }
            }
        }).start();
    }

    private volatile boolean isReconnectingTimed=false;
    private void reconnect() {
        if(isReconnectingTimed){
            Log.d(TAG, "reconnect: is scheduled, ignoring");
            return;
        }
        addLog("WebSocketClient", "reconnect", "scheduling for "+ RECONNECT_DELAY +"ms", LogTags.OTHER);
        isReconnectingTimed=true;
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    ws = ws.recreate().connect();
                    if (socketConnectionListener != null) {
                        socketConnectionListener.onConnectionChange(true);
                    }
                    isReconnectingTimed=false;
                } catch (WebSocketException | IOException e) {
                    e.printStackTrace();
                    addLog("WebSocketClient", "connect",
                            "connection error :"+ Arrays.toString(e.getStackTrace()), LogTags.OTHER);
                    if (socketConnectionListener != null) {
                        socketConnectionListener.onConnectionChange(false);
                        Log.d(TAG, "run: notify listener about connection");
                    }else {
                        Log.d(TAG, "run: socketConnectionListener is null");
                    }
                    isReconnectingTimed=false;
                    reconnect();
                }
            }
        }, RECONNECT_DELAY);
    }

    public WebSocket getConnection() {
        return ws;
    }

    @Override
    public void sendRequest(WebSocketRequest request){
        if(ws!=null) {
            String logString = request.getType()+" state of socket is "+ws.getState();
            //Log.d(TAG, "sendRequest: "+logString);
            if(ws.getState()!=WebSocketState.OPEN) {
                addLog("WebSocketClient", "sendRequest", logString, LogTags.OTHER);
            }
            switch (ws.getState()){
                case CREATED:
                    Log.d(TAG, "sendRequest: CREATED...");
                    addToNotSended(request);
                    break;
                case CONNECTING:
                    Log.d(TAG, "sendRequest: "+request.getType()+", but status is CONNECTING...");
                    addToNotSended(request);
                    break;
                case OPEN:
                    Gson gson = new Gson();
                    String stringRequest = gson.toJson(request);
                    //Log.d(TAG, "sendRequest: sending " + stringRequest);
                    ws.sendText(stringRequest);
                    break;
                case CLOSING:
                    Log.d(TAG, "sendRequest: CLOSING...");
                    addToNotSended(request);
                    break;
                case CLOSED:
                    Log.d(TAG, "sendRequest: closed...");
                    addToNotSended(request);
                    connect();
                    break;
            }
        }else {
            Log.d(TAG, "sendRequest: socket is null, add to notSended");
            addToNotSended(request);
        }
    }

    private void addToNotSended(WebSocketRequest request) {
        if(!notSendedRequests.contains(request)) {
            notSendedRequests.add(request);
        }
    }

    @Override
    public void sendEventReceived(EventProcessedAnswer eventProcessedAnswer){
        if(ws!=null) {
            Log.d(TAG, "event "+eventProcessedAnswer.getIdOfRequest()+" processed, sending answer ");
            Gson gson = new Gson();
            ws.sendText(gson.toJson(eventProcessedAnswer));
        }
    }

    @Override
    public void close() {
        ws.disconnect();
    }


    public class SocketListener extends WebSocketAdapter {
        @Override
        public void onPingFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {
            super.onPingFrame(websocket, frame);
            //Log.d(TAG, "onPingFrame: "+frame);
        }

        @Override
        public void onStateChanged(WebSocket websocket, WebSocketState newState) throws Exception {
            super.onStateChanged(websocket, newState);
            addLog("WebSocketClient", "socket state changed to "+newState, "", LogTags.OTHER);
            if(newState==WebSocketState.CLOSED){
                websocket.disconnect("state changed to closed");
                if (socketConnectionListener != null) {
                    socketConnectionListener.onConnectionChange(false);
                }
            }
        }

        @Override
        public void onConnected(WebSocket websocket, Map<String, List<String>> headers) throws Exception {
            super.onConnected(websocket, headers);
            Log.d(TAG, "onConnected: ");
            addLog("WebSocketClient", "onConnected",
                    "we have "+notSendedRequests.size()+" not sended request, resending...", LogTags.OTHER);

            for (WebSocketRequest webSocketRequest : notSendedRequests){
                sendRequest(webSocketRequest);
            }
            addLog("WebSocketClient", "onConnected",
                    "attempt of sending is done", LogTags.OTHER);
        }

        public void onTextMessage(WebSocket websocket, String message) {
            //Log.i(TAG, "Message --> " + message);
            //Log.d(TAG, "onTextMessage: in thread "+Thread.currentThread().getId());
            listener.onSocketMessage(message);
        }

        @Override
        public void onError(WebSocket websocket, WebSocketException cause) {
            Log.i(TAG, "Error -->" + cause.getMessage());
            addLog("WebSocketClient", "onError", cause.getMessage(), LogTags.OTHER);
            reconnect();
        }

        @Override
        public void onDisconnected(WebSocket websocket,
                                   WebSocketFrame serverCloseFrame, WebSocketFrame clientCloseFrame,
                                   boolean closedByServer) {
            if (socketConnectionListener != null) {
                socketConnectionListener.onConnectionChange(false);
            }
            String disconnectString = "serverCloseFrame:"+serverCloseFrame.toString()+", clientCloseFrame:"+clientCloseFrame.toString();
            Log.i(TAG, "onDisconnected "+disconnectString);
            addLog("WebSocketClient", "onDisconnected", disconnectString, LogTags.OTHER);
            if (closedByServer) {
                Log.i(TAG, "onDisconnected: by server");
                reconnect();
            }
        }

        @Override
        public void onUnexpectedError(WebSocket websocket, WebSocketException cause) {
            Log.i(TAG, "Error -->" + cause.getMessage());
            reconnect();
        }

        @Override
        public void onPongFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {
            super.onPongFrame(websocket, frame);
            websocket.sendPing("Are you there?");
        }
    }

    @Override
    public void inetOff(){
        // Close the WebSocket connection.
        Log.d(TAG, "inetOff: ");
        ws.disconnect(
                WebSocketCloseCode.AWAY,
                "Network was lost on the client side.");
    }

    @Override
    public void inetOn(){
        reconnect();
    }

}
