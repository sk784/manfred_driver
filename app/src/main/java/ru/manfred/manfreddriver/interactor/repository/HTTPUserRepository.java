/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.interactor.repository;

import android.support.annotation.NonNull;
import android.util.Log;

import java.net.SocketTimeoutException;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.manfred.manfreddriver.interactor.api.ApiFactory;
import ru.manfred.manfreddriver.managers.logging.AuthError;
import ru.manfred.manfreddriver.model.api.Busy;
import ru.manfred.manfreddriver.model.api.ManfredResponse;
import ru.manfred.manfreddriver.model.api.ResponseAuth;
import ru.manfred.manfreddriver.model.responces.ManfredPushToken;
import ru.manfred.manfreddriver.model.responces.ManfredResponceDriverStatus;
import ru.manfred.manfreddriver.model.responces.ManfredResponseDriver;
import ru.manfred.manfreddriver.model.responces.PhoneDTO;
import ru.manfred.manfreddriver.model.responces.SMSCodeRequest;

public class HTTPUserRepository implements UserRepository {
    private static final String TAG = "HTTPUserRepository";
    private String token;

    public HTTPUserRepository(String token) {
        this.token = token;
    }

    @Override
    public void attemptLogin(PhoneDTO phone, AuthorisationCallback callback) {
        ApiFactory.getUserService().login(phone).enqueue(new Callback<ManfredResponse>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponse> call, @NonNull Response<ManfredResponse> response) {
                ManfredResponse resp = response.body();
                if(resp!=null){
                    switch (resp.getResult_code()) {
                        case "success":
                            callback.success(null);
                            break;
                        case "invalid_phone":
                            callback.error(new AuthError(AuthError.ErrorStatus.INVALID_NUMBER,
                                    "Номер "+phone.getPhone()+" не зарегестирован в системе. Проверьте правильность ввода"));
                            break;
                        default:
                            callback.error(new AuthError(AuthError.ErrorStatus.NETWORK_ERROR,resp.getMessage()));
                            break;
                    }
                }else {
                    callback.error(new AuthError(AuthError.ErrorStatus.NETWORK_ERROR,"Пустой ответ"));
                }

            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponse> call, @NonNull Throwable t) {
                callback.error(new AuthError(AuthError.ErrorStatus.NETWORK_ERROR,t.getMessage()));
            }
        });

    }

    @Override
    public void sendCode(SMSCodeRequest smsCodeRequest, AuthorisationCallback<String> authorisationCallback) {
        Log.d(TAG, "sendCode: "+smsCodeRequest);
        ApiFactory.getUserService().sendSMSCode(smsCodeRequest).enqueue(new Callback<ManfredResponse<ResponseAuth>>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponse<ResponseAuth>> call, @NonNull Response<ManfredResponse<ResponseAuth>> response) {
                ManfredResponse<ResponseAuth> resp = response.body();
                if (resp != null) {
                    Log.d(TAG, "onResponse: "+resp);
                    switch (resp.getResult_code()) {
                        case "auth_success":
                            token=resp.getData().getToken();
                            Log.d(TAG, "sendSms onResponse: auth_success, token is "+resp.getData().getToken());
                            authorisationCallback.success(token);
                            break;
                        case "invalid_sms_code":
                            String fullPhone = smsCodeRequest.getCountryCode()+smsCodeRequest.getPhone();
                            authorisationCallback.error(new AuthError(AuthError.ErrorStatus.INVALID_SMS_CODE,"Код из СМС на номер \n" +fullPhone+
                                    " неверный."));
                            break;
                        case "driver_archived":
                            authorisationCallback.error(new AuthError(AuthError.ErrorStatus.DRIVER_ARCHIVED, "Ваш аккаунт заблокирован. Свяжитесь с оператором."));
                            break;
                        default:
                            authorisationCallback.error(new AuthError(AuthError.ErrorStatus.NETWORK_ERROR, resp.getMessage()));
                            break;
                    }
                } else {
                    authorisationCallback.error(new AuthError(AuthError.ErrorStatus.NETWORK_ERROR, "Пустой ответ"));
                }
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponse<ResponseAuth>> call, @NonNull Throwable t) {
                authorisationCallback.error(new AuthError(AuthError.ErrorStatus.NETWORK_ERROR, t.getLocalizedMessage()));
            }
        });

    }

    @Override
    public void changeStatus(boolean isFree, AnswerCallback answerCallback) {
        ApiFactory.getDriverService().changeStatus(token, new Busy(isFree)).enqueue(new Callback<ManfredResponse>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponse> call, @NonNull Response<ManfredResponse> response) {
                ManfredResponse resp = response.body();
                if (resp != null) {
                    Log.d(TAG, "change status onResponse: " + resp.getResult_code() + ", " + resp.getMessage());
                    if (Objects.equals(resp.getResult_code(), "success")) {
                        answerCallback.success();
                    } else {
                        answerCallback.notSuccess(resp.getResult_code() + ": " + resp.getMessage());
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponse> call, @NonNull Throwable t) {
                if (t instanceof SocketTimeoutException || t instanceof java.net.UnknownHostException) {
                    answerCallback.serverNotAvailable();
                } else {
                    answerCallback.notSuccess(t.getMessage());
                }
            }
        });

    }

    @Override
    public void logout(AnswerCallback answerCallback) {
        ApiFactory.getUserService().logout(token).enqueue(new Callback<ManfredResponceDriverStatus>() {
            @Override
            public void onResponse(Call<ManfredResponceDriverStatus> call, Response<ManfredResponceDriverStatus> response) {
                if (response.body() != null) {
                    if(response.body().getResult_code().equals("success")){
                        answerCallback.success();
                    }else {
                        answerCallback.notSuccess(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ManfredResponceDriverStatus> call, Throwable t) {
                answerCallback.notSuccess(t.getMessage());
            }
        });

    }

    @Override
    public void setNewFCMToken(String FCMtoken, AnswerCallback answerCallback) {
        ApiFactory.getUserService().setFCMToken(token, new ManfredPushToken(FCMtoken)).enqueue(new Callback<ManfredResponseDriver>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponseDriver> call, @NonNull Response<ManfredResponseDriver> response) {
                ManfredResponseDriver resp = response.body();
                Log.d(TAG, "setNewFCMToken onResponse: " + resp.getResult_code() + ", " + resp.getMessage());
                if (Objects.equals(resp.getResult_code(), "success")) {
                    answerCallback.success();
                } else {
                    answerCallback.notSuccess(resp.getResult_code() + ": " + resp.getMessage());
                    Log.d(TAG, "setNewFCMToken: error " + resp.getResult_code() + ": " + resp.getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponseDriver> call, @NonNull Throwable t) {
                if (t instanceof SocketTimeoutException || t instanceof java.net.UnknownHostException) {
                    answerCallback.serverNotAvailable();
                } else {
                    answerCallback.notSuccess("ошибка при установка пуш токена: " + t.getMessage());
                }
            }
        });

    }
}
