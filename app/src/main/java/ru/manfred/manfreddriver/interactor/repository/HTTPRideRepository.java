/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.interactor.repository;

import android.support.annotation.NonNull;
import android.util.Log;

import java.net.SocketTimeoutException;
import java.text.NumberFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.manfred.manfreddriver.interactor.api.ApiFactory;
import ru.manfred.manfreddriver.interactor.api.ManfredResultCallback;
import ru.manfred.manfreddriver.interactor.api.RideService;
import ru.manfred.manfreddriver.model.GoogleMaps.DirectionResults;
import ru.manfred.manfreddriver.model.api.AcceptingOrder;
import ru.manfred.manfreddriver.model.api.AcceptingPreOrder;
import ru.manfred.manfreddriver.model.api.Airport;
import ru.manfred.manfreddriver.model.api.DirectionRequest;
import ru.manfred.manfreddriver.model.api.GeoData;
import ru.manfred.manfreddriver.model.api.ManfredError;
import ru.manfred.manfreddriver.model.api.ManfredResponse;
import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.model.responces.ManfredResponceAcceptingOrder;

public class HTTPRideRepository implements RideRepository {
    private static final String TAG = "HTTPRideRepository";
    private String token;

    public HTTPRideRepository(String token) {
        this.token = token;
    }

    @Override
    public void acceptOrder(AcceptingOrder acceptingOrder, final AcceptCallback acceptCallback) {
        //AcceptingOrder acceptingOrder = new AcceptingOrder(id);
        Log.d(TAG, "accepting order... " + acceptingOrder.toString());

        RideService rideService = ApiFactory.getRideService();
        final Call<ManfredResponceAcceptingOrder> acceptOrder = rideService.acceptOrder(token, acceptingOrder);
        acceptOrder.enqueue(new Callback<ManfredResponceAcceptingOrder>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponceAcceptingOrder> call, @NonNull Response<ManfredResponceAcceptingOrder> response) {
                ManfredResponceAcceptingOrder manfredResponse = response.body();
                if (manfredResponse == null) {
                    acceptCallback.error(new ManfredError("Получен пустой ответ от сервера", ManfredError.ErrorStatus.NET_ERROR));
                    acceptCallback.debugMessage(response.message());
                } else {
                    Log.d(TAG, "onResponse: " + manfredResponse.getResult_code());
                    String debug = "We take from server: " + manfredResponse.getResult_code() + ", " + manfredResponse.getMessage();
                    acceptCallback.debugMessage(debug);
                    switch (manfredResponse.getResult_code()) {
                        case "invalid_order_status":
                            acceptCallback.error(new ManfredError(manfredResponse.getMessage(), manfredResponse.getResult_code(), ManfredError.ErrorStatus.NET_ERROR));
                            break;
                        case "prepayment_error":
                            acceptCallback.error(new ManfredError(manfredResponse.getMessage(), manfredResponse.getResult_code(), ManfredError.ErrorStatus.NET_ERROR));
                            break;
                        case "order_cancelled_by_passenger":
                            acceptCallback.error(new ManfredError(manfredResponse.getMessage(), manfredResponse.getResult_code(), ManfredError.ErrorStatus.NET_ERROR));
                            break;
                        case "bad_request":
                            acceptCallback.error(new ManfredError(manfredResponse.getMessage(), manfredResponse.getResult_code(), ManfredError.ErrorStatus.NET_ERROR));
                            break;
                        case "success":
                            Order order = manfredResponse.getData().getOrder();
                            Log.d(TAG, "onResponse: order " + order.getId() + " with status " + order.getStatus());
                            acceptCallback.accepted(manfredResponse.getData().getOrder());
                            break;
                        default:
                            acceptCallback.error(new ManfredError(manfredResponse.getMessage(), ManfredError.ErrorStatus.NET_ERROR));
                            break;
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponceAcceptingOrder> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
                if (t instanceof SocketTimeoutException) {
                    acceptCallback.serverNotAvailable();
                } else {
                    acceptCallback.error(new ManfredError(t.getMessage(), ManfredError.ErrorStatus.NET_ERROR));
                }
            }
        });
    }



    @Override
    public void finishTrip(GeoData geoData, RepoAnswer repoAnswer) {
        ApiFactory.getRideService().finishTrip(token, geoData).enqueue(new Callback<ManfredResponse>() {
            @Override
            public void onResponse(Call<ManfredResponse> call, Response<ManfredResponse> response) {
                repoAnswer.allOk(null);
            }

            @Override
            public void onFailure(Call<ManfredResponse> call, Throwable t) {
                repoAnswer.error(t.getMessage());
            }
        });

    }

    @Override
    public void confirmPreOrder(Order order, AcceptCallback acceptCallback) {
        RideService rideService = ApiFactory.getRideService();
        confirmer(order, acceptCallback, rideService, true);

    }

    @Override
    public void declinePreOrder(Order order, AcceptCallback acceptCallback) {
        RideService rideService = ApiFactory.getRideService();
        confirmer(order, acceptCallback, rideService, false);
    }

    private void confirmer(Order order, AcceptCallback acceptCallback, RideService rideService, boolean accept) {
        Log.d(TAG, "confirmer: accept now? " + accept);
        final Call<ManfredResponse> confirmPreOrder = rideService.confirmPreOrder(token, new AcceptingPreOrder(order.getId(), accept));
        confirmPreOrder.enqueue(new Callback<ManfredResponse>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponse> call, @NonNull Response<ManfredResponse> response) {
                ManfredResponse manfredResponse = response.body();

                if (manfredResponse == null) {
                    acceptCallback.error(new ManfredError("Получен пустой ответ от сервера", ManfredError.ErrorStatus.NET_ERROR));
                } else {
                    acceptCallback.debugMessage("we take from server " + manfredResponse.getMessage());
                    switch (manfredResponse.getMessage()) {
                        case "OK":
                            acceptCallback.accepted(order);
                            break;
                        default:
                            acceptCallback.error(new ManfredError(manfredResponse.getMessage(), ManfredError.ErrorStatus.NET_ERROR));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponse> call, @NonNull Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    acceptCallback.serverNotAvailable();
                } else {
                    acceptCallback.error(new ManfredError(t.getMessage(), ManfredError.ErrorStatus.NET_ERROR));
                }
            }
        });
    }


    @Override
    public void startPreOrder(Order order, int timeToCostumer, AcceptCallback acceptCallback) {
        RideService rideService = ApiFactory.getRideService();
        //String sId=String.valueOf(id);
        final Call<ManfredResponse> acceptPreOrder = rideService.startPreOrder(token, new AcceptingOrder(order.getId(), timeToCostumer));
        acceptPreOrder.enqueue(new Callback<ManfredResponse>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponse> call, @NonNull Response<ManfredResponse> response) {
                ManfredResponse manfredResponse = response.body();
                if (manfredResponse == null) {
                    acceptCallback.error(new ManfredError("Получен пустой ответ от сервера", ManfredError.ErrorStatus.NET_ERROR));
                } else {
                    switch (manfredResponse.getMessage()) {
                        case "OK":
                            acceptCallback.accepted(order);
                            break;
                        default:
                            acceptCallback.error(new ManfredError(manfredResponse.getMessage(), ManfredError.ErrorStatus.NET_ERROR));
                            break;
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponse> call, @NonNull Throwable t) {
                acceptCallback.error(new ManfredError(t.getMessage(), ManfredError.ErrorStatus.NET_ERROR));
            }
        });

    }

    @Override
    public void getActualCost(AcceptCallback acceptCallback) {
        ApiFactory.getRideService().getActualCost(token).enqueue(new Callback<ManfredResponceAcceptingOrder>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponceAcceptingOrder> call, @NonNull Response<ManfredResponceAcceptingOrder> response) {
                ManfredResponceAcceptingOrder manfredResponse = response.body();
                if (manfredResponse != null) {
                    if(manfredResponse.getResult_code().equals("success")){
                        acceptCallback.accepted(manfredResponse.getData().getOrder());
                    }else {
                        acceptCallback.error(new ManfredError(manfredResponse.getMessage(), ManfredError.ErrorStatus.NET_ERROR));
                    }
                }else {
                    acceptCallback.error(new ManfredError("Получен пустой ответ от сервера", ManfredError.ErrorStatus.NET_ERROR));
                }
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponceAcceptingOrder> call, @NonNull Throwable t) {
                acceptCallback.error(new ManfredError(t.getMessage(), ManfredError.ErrorStatus.NET_ERROR));
            }
        });

    }

    @Override
    public void toAirport(Airport airport) {
        RideService rideService = ApiFactory.getRideService();
        Log.d(TAG, "to airport! " + airport.getName());
        GeoData geoData = new GeoData(airport.getLongitude(), airport.getLatitude(), new Date(), airport.getName(), 0);

        final Call<ManfredResponse> toAirport = rideService.toAirport(token, geoData);
        toAirport.enqueue(new Callback<ManfredResponse>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponse> call, @NonNull Response<ManfredResponse> response) {
                Log.d(TAG, "onResponse: all nice, server know about new destination");
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponse> call, @NonNull Throwable t) {

            }
        });

    }

    @Override
    public void cancelToAirport(AcceptCallback acceptCallback) {
        ApiFactory.getRideService().cancelToAirport(token).enqueue(new Callback<ManfredResponse>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponse> call, @NonNull Response<ManfredResponse> response) {
                ManfredResponse manfredResponse = response.body();
                if (manfredResponse == null) {
                    acceptCallback.error(new ManfredError("Получен пустой ответ от сервера", ManfredError.ErrorStatus.NET_ERROR));
                } else {
                    switch (manfredResponse.getResult_code()) {
                        case "invalid_token":
                            acceptCallback.error(new ManfredError(manfredResponse.getMessage(), manfredResponse.getResult_code(), ManfredError.ErrorStatus.NET_ERROR));
                            break;
                        case "invalid_order":
                            acceptCallback.error(new ManfredError(manfredResponse.getMessage(), manfredResponse.getResult_code(), ManfredError.ErrorStatus.NET_ERROR));
                            break;
                        case "error":
                            acceptCallback.error(new ManfredError(manfredResponse.getMessage(), manfredResponse.getResult_code(), ManfredError.ErrorStatus.NET_ERROR));
                            break;
                        case "success":
                            acceptCallback.accepted(null);
                            break;
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponse> call, @NonNull Throwable t) {
                acceptCallback.error(new ManfredError(t.getMessage(), ManfredError.ErrorStatus.NET_ERROR));
            }
        });

    }

    @Override
    public void startTrip(AcceptCallback acceptCallback) {
        ApiFactory.getRideService().startTrip(token).enqueue(new Callback<ManfredResponse>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponse> call, @NonNull Response<ManfredResponse> response) {
                ManfredResponse manfredResponse = response.body();

                if (manfredResponse == null) {
                    acceptCallback.error(new ManfredError("Получен пустой ответ от сервера", ManfredError.ErrorStatus.NET_ERROR));
                } else {
                    switch (manfredResponse.getResult_code()) {
                        case "invalid_token":
                            acceptCallback.error(new ManfredError(manfredResponse.getMessage(), manfredResponse.getResult_code(), ManfredError.ErrorStatus.NET_ERROR));
                            break;
                        case "invalid_order":
                            acceptCallback.error(new ManfredError(manfredResponse.getMessage(), manfredResponse.getResult_code(), ManfredError.ErrorStatus.NET_ERROR));
                            break;
                        case "error":
                            acceptCallback.error(new ManfredError(manfredResponse.getMessage(), manfredResponse.getResult_code(), ManfredError.ErrorStatus.NET_ERROR));
                            break;
                        case "success":
                            acceptCallback.accepted(null);
                            break;
                        default:
                            acceptCallback.error(new ManfredError(manfredResponse.getMessage(), manfredResponse.getResult_code(), ManfredError.ErrorStatus.NET_ERROR));
                            break;
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponse> call, @NonNull Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    acceptCallback.serverNotAvailable();
                } else {
                    acceptCallback.error(new ManfredError(t.getMessage(), ManfredError.ErrorStatus.NET_ERROR));
                }
            }
        });

    }

    @Override
    public void checkPayment(ManfredResultCallback callback) {
        ApiFactory.getDriverService().checkPayment(token).enqueue(new Callback<ManfredResponceAcceptingOrder>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponceAcceptingOrder> call, @NonNull Response<ManfredResponceAcceptingOrder> response) {
                ManfredResponceAcceptingOrder manfredResponse = response.body();
                if (manfredResponse == null) {
                    callback.error("Ошибка", "Ошибка получения данных");
                } else {
                    Log.d(TAG, "checkPayment onResponse: " + manfredResponse.getResult_code());
                    switch (manfredResponse.getResult_code()) {
                        case "success":
                            callback.allOk(manfredResponse.getData().getOrder());
                            break;
                        default:
                            callback.error(manfredResponse.getResult_code(), manfredResponse.getMessage());
                            break;
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponceAcceptingOrder> call, @NonNull Throwable t) {
                callback.error("Ошибка сервера", t.getMessage());
            }
        });

    }

    @Override
    public void onClient(ManfredResultCallback callback) {
        ApiFactory.getRideService().onClient(token).enqueue(new Callback<ManfredResponse>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponse> call, @NonNull Response<ManfredResponse> response) {
                //Log.d(TAG, "onResponse: on client!");
                ManfredResponse manfredResponse = response.body();
                if (manfredResponse == null) {
                    //Toast.makeText(context,"получен пустой ответ от сервера",Toast.LENGTH_LONG).show();
                    callback.error("Ошибка сервера", "пустой ответ");
                } else {
                    switch (manfredResponse.getResult_code()) {
                        case "success":
                            callback.allOk(null);
                            break;
                        case "invalid_order":
                            Log.d(TAG, "onResponse: message " + manfredResponse.getMessage());
                            //Toast.makeText(context,manfredResponse.getMessage(),Toast.LENGTH_LONG).show();
                            callback.error(manfredResponse.getResult_code(), manfredResponse.getMessage());
                            break;
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponse> call, @NonNull Throwable t) {
                //Toast.makeText(context,t.getMessage(),Toast.LENGTH_LONG).show();
                if (t instanceof SocketTimeoutException) {
                    callback.serverNotAvailable();
                } else {
                    callback.error("Ошибка", t.getMessage());
                }
            }
        });

    }

    @Override
    public void delayOnOrder(ManfredResultCallback callback, int lateTimeInSec) {
        ApiFactory.getRideService().delayOrder(token, lateTimeInSec).enqueue(new Callback<ManfredResponse>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponse> call, @NonNull Response<ManfredResponse> response) {
                //Log.d(TAG, "onResponse: late sending ok"+call.toString());
                if(response.body().getResult_code().equals("success")) {
                    callback.allOk(null);
                }else {
                    callback.error("Ошибка сервера",response.body().getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponse> call, @NonNull Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    callback.serverNotAvailable();
                } else {
                    callback.error("Ошибка", t.getMessage());
                }
            }
        });

    }

    @Override
    public void sendLocation(GeoData geoData, ResponseCallback responseCallback) {
        RideService rideService = ApiFactory.getRideService();
        Call<ManfredResponse> locationCall = rideService.sendLocation(token, geoData);
        locationCall.enqueue(new Callback<ManfredResponse>() {
            @Override
            public void onResponse(Call<ManfredResponse> call, Response<ManfredResponse> response) {
                ManfredResponse manfredResponse = response.body();
                if (manfredResponse != null) {
                    if (!manfredResponse.getStatus().equals("success")) {
                        responseCallback.error(new ManfredError(manfredResponse.getMessage(),ManfredError.ErrorStatus.NET_ERROR));
                    }else {
                        responseCallback.allOk(null);
                    }
                }
            }

            @Override
            public void onFailure(Call<ManfredResponse> call, Throwable t) {
                if (t instanceof SocketTimeoutException || t instanceof java.net.UnknownHostException) {
                    responseCallback.error(new ManfredError("Ошибка сервера",ManfredError.ErrorStatus.SERVER_UNAVAILABLE_ERROR));
                }else {
                    responseCallback.error(new ManfredError(t.getMessage(),ManfredError.ErrorStatus.NET_ERROR));
                }
            }
        });

    }

    @Override
    public void getDirection(GeoData from, GeoData to, RepoAnswer<DirectionResults> resultCallback) {
        RideService rideService = ApiFactory.getRideService();
        rideService.getDirection(token,new DirectionRequest(from,to)).enqueue(new Callback<ManfredResponse<DirectionResults>>() {
            @Override
            public void onResponse(Call<ManfredResponse<DirectionResults>> call, Response<ManfredResponse<DirectionResults>> response) {
                if (response.body() != null) {
                    if (!response.body().getStatus().equals("success")) {
                        resultCallback.error(response.body().getMessage());
                        Log.d(TAG, "onResponse: "+response.body().getMessage());
                    }else {
                        resultCallback.allOk(response.body().getData());
                    }
                }else {
                    resultCallback.error("Сервер вернул пустой ответ");
                }
            }

            @Override
            public void onFailure(Call<ManfredResponse<DirectionResults>> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
                resultCallback.error(t.getMessage());
            }
        });
    }
}
