package ru.manfred.manfreddriver.interactor.api.websocket;

import android.support.annotation.NonNull;

import com.neovisionaries.ws.client.WebSocket;

import ru.manfred.manfreddriver.managers.network.EventProcessedAnswer;
import ru.manfred.manfreddriver.managers.network.LogListener;
import ru.manfred.manfreddriver.managers.network.WebSocketRequest;

public interface WebSocketClient {
    void setLogListener(LogListener logListener);

    void setSocketConnectionListener(@NonNull SocketConnectionListener socketConnectionListener);

    void removeConnectionListener();

    void connect();

    void sendRequest(WebSocketRequest request);

    void sendEventReceived(EventProcessedAnswer eventProcessedAnswer);

    void close();

    void inetOff();

    void inetOn();

    public interface MessageListener {
        void onSocketMessage(String answer);
    }

    public interface SocketConnectionListener {
        void onConnectionChange(boolean isConnected);
    }
}
