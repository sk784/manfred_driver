/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.interactor.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.util.Log;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.db.AppDatabase;
import ru.manfred.manfreddriver.db.entity.GeoDateEntity;
import ru.manfred.manfreddriver.db.entity.LogDB;
import ru.manfred.manfreddriver.model.api.GeoData;
import ru.manfred.manfreddriver.model.api.LogNet;

public class DBRepository {
    private static final String TAG = "DBRepository";
    private final AppDatabase db;
    private MutableLiveData<List<LogDB>> logs = new MutableLiveData<>();
    private MutableLiveData<List<GeoDateEntity>> notSendedCords = new MutableLiveData<>();

    public DBRepository(Context context) {
        db = ((ManfredApplication)context.getApplicationContext()).getDatabase();
    }

    public LiveData<List<GeoDateEntity>> getNotSendedLocations(String username){
        //return db.locationsDao().getDriverLocations(username);
        ExecutorService service =  Executors.newSingleThreadExecutor();
        service.submit(new Runnable() {
            @Override
            public void run() {
                notSendedCords.postValue(db.locationsDao().getDriverLocationsSync(username));
                service.shutdown();
            }
        });
        return notSendedCords;
    }

    public LiveData<List<LogDB>> getStroredLogs(String username){
        ExecutorService service =  Executors.newSingleThreadExecutor();
        service.submit(new Runnable() {
            @Override
            public void run() {
                logs.postValue(db.logDao().getDriverLogsSync(username));
                service.shutdown();
            }
        });
        return logs;
        /*
        Log.d(TAG, "getStroredLogs: subscribing");
        return db.logDao().getDriverLogs(username);*/
    }

    public List<LogDB>getStoredLogsNow(String username){
        return db.logDao().getDriverLogsSync(username);
    }

    public void addLocationToNotSended(GeoData geoData, String username){
        //Log.d(TAG, "addLocationToNotSended: "+geoData.toString());
        GeoDateEntity geoDateEntity = new GeoDateEntity();
        geoDateEntity.geoData=geoData;
        geoDateEntity.driverLogin=username;
        ExecutorService service =  Executors.newSingleThreadExecutor();
        service.submit(new Runnable() {
            @Override
            public void run() {
                db.locationsDao().insert(geoDateEntity);
                notSendedCords.postValue(db.locationsDao().getDriverLocationsSync(username));
                service.shutdown();
            }
        });

    }

    public void addLog(LogNet logNet, String username){
        //Log.d(TAG, "addLog: "+logNet.toString());
        LogDB logDB = new LogDB();
        logDB.logNet=logNet;
        logDB.driverLogin=username;
        ExecutorService service =  Executors.newSingleThreadExecutor();
        service.submit(new Runnable() {
            @Override
            public void run() {
                //Log.d(TAG, "run: adding log");
                db.logDao().insert(logDB);
                logs.postValue(db.logDao().getDriverLogsSync(username));
                service.shutdown();
            }
        });
    }

    public void removeSendedLocations(List<GeoDateEntity>geoDateEntities,String username){
        ExecutorService service =  Executors.newSingleThreadExecutor();
        service.submit(new Runnable() {
            @Override
            public void run() {
                db.locationsDao().deleteLocations(geoDateEntities);
                notSendedCords.postValue(db.locationsDao().getDriverLocationsSync(username));
                service.shutdown();
            }
        });

    }

    public void removeSendedLogs(List<LogDB> logDBS, String username){
        //Log.d(TAG, "removeSendedLogs: "+logDBS.size());
        ExecutorService service =  Executors.newSingleThreadExecutor();
        service.submit(new Runnable() {
            @Override
            public void run() {
                db.logDao().deleteLogs(logDBS);
                logs.postValue(db.logDao().getDriverLogsSync(username));
                service.shutdown();
            }
        });

    }

    private void logItems(List<LogDB> logDBS){
        for (LogDB logDB : logDBS){
            Log.d(TAG, "logItems: id "+logDB.id);
        }
    }

}
