/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.interactor.repository;

import android.util.Log;
import android.widget.Toast;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.managers.logging.AuthError;
import ru.manfred.manfreddriver.managers.network.ManfredNetworkManager;
import ru.manfred.manfreddriver.managers.network.NetworkQuery;
import ru.manfred.manfreddriver.managers.network.WebSocketAnswer;
import ru.manfred.manfreddriver.managers.network.WebSocketAnswerCallback;
import ru.manfred.manfreddriver.model.api.Busy;
import ru.manfred.manfreddriver.model.api.Driver;
import ru.manfred.manfreddriver.model.api.ManfredError;
import ru.manfred.manfreddriver.model.api.ResponseAuth;
import ru.manfred.manfreddriver.model.responces.ManfredPushToken;
import ru.manfred.manfreddriver.model.responces.PhoneDTO;
import ru.manfred.manfreddriver.model.responces.SMSCodeRequest;

/**
 * Created by begemot on 06.09.17.
 */

public class WebSocketUserRepository implements UserRepository {
    private static final String TAG = "UserRepository";
    private String token;
    private ManfredNetworkManager manfredNetworkManager;

    public WebSocketUserRepository(String token, ManfredNetworkManager manfredNetworkManager) {
        this.token = token;
        this.manfredNetworkManager = manfredNetworkManager;
    }

    @Override
    public void attemptLogin(PhoneDTO phone, AuthorisationCallback answerCallback) {
        WebSocketAnswerCallback<WebSocketAnswer> webSocketAnswerCallback = new WebSocketAnswerCallback<WebSocketAnswer>() {
            @Override
            public void messageReceived(WebSocketAnswer message) {
                Log.d(TAG, "messageReceived: "+message.toString());
                answerCallback.success(null);
            }

            @Override
            public void error(ManfredError manfredError) {
                Log.d(TAG, "error: "+manfredError);
                switch (manfredError.getResultCode()) {
                    case "success":
                        answerCallback.success(null);
                        break;
                    case "invalid_phone":
                        answerCallback.error(new AuthError(AuthError.ErrorStatus.INVALID_NUMBER,
                                "Номер не зарегистрирован в системе. Проверьте правильность ввода"));
                        break;
                    default:
                        answerCallback.error(new AuthError(AuthError.ErrorStatus.NETWORK_ERROR,manfredError.getText()));
                        break;
                }
            }
        };
        NetworkQuery networkQuery = new NetworkQuery.OneRequestBuilder<PhoneDTO,Void>("user/register_phone")
                .setDataForTransmit(phone)
                .setCallback(webSocketAnswerCallback,null)
                .buildOneRequest();
        manfredNetworkManager.addQuery(networkQuery);
    }

    @Override
    public void sendCode(SMSCodeRequest smsCodeRequest, AuthorisationCallback<String> authorisationCallback) {
        //ManfredNetworkManager manfredNetworkManager = CheckEventService.getManfredNetworkManager();
        WebSocketAnswerCallback<WebSocketAnswer<ResponseAuth>> callback = new WebSocketAnswerCallback<WebSocketAnswer<ResponseAuth>>() {
            @Override
            public void messageReceived(WebSocketAnswer<ResponseAuth> message) {
                if(message!=null){
                    Log.d(TAG, "sendSms messageReceived: result code is " + message.getResultCode());
                    Log.d(TAG, "sendSms: auth_success "+message.toString());
                    //setToken.postValue(resp.getData().getToken());
                    Log.d(TAG, "sendSms: auth_success, token is "+message.getPayload().getToken());
                    token=message.getPayload().getToken();
                    authorisationCallback.success(token);
                }
            }

            @Override
            public void error(ManfredError manfredError) {
                //authError.postValue(new AuthError(ErrorStatus.NETWORK_ERROR,manfredError.getText()));
                String fullPhone = smsCodeRequest.getCountryCode()+smsCodeRequest.getPhone();
                switch (manfredError.getResultCode()) {
                    case "invalid_sms_code":
                        authorisationCallback.error(new AuthError(AuthError.ErrorStatus.INVALID_SMS_CODE,"Код из СМС на номер \n"
                                +fullPhone+
                                " неверный."));
                        break;
                    case "driver_archived":
                        authorisationCallback.error(new AuthError(AuthError.ErrorStatus.DRIVER_ARCHIVED, "Ваш аккаунт заблокирован. " +
                                "Свяжитесь с оператором."));
                        break;
                    default:
                        authorisationCallback.error(new AuthError(AuthError.ErrorStatus.NETWORK_ERROR, manfredError.getText()));
                        break;
                }
            }
        };
        NetworkQuery networkQuery = new NetworkQuery.OneRequestBuilder<SMSCodeRequest,ResponseAuth>("user/sms_code")
                .setDataForTransmit(smsCodeRequest)
                .setCallback(callback,ResponseAuth.class)
                .buildOneRequest();
        manfredNetworkManager.addQuery(networkQuery);
    }

    @Override
    public void changeStatus(boolean isFree, AnswerCallback answerCallback) {
        Log.d(TAG, "changeStatus: to "+isFree);

        WebSocketAnswerCallback<WebSocketAnswer> changingStatusAnswer = new WebSocketAnswerCallback<WebSocketAnswer>() {
            @Override
            public void messageReceived(WebSocketAnswer message) {
                answerCallback.success();
            }

            @Override
            public void error(ManfredError manfredError) {
                Log.d(TAG, "error: "+manfredError.toString());
                answerCallback.notSuccess(manfredError.getText());
            }
        };
        NetworkQuery changeState = new NetworkQuery.OneRequestBuilder<Busy,Void>("user/update")
                .setDataForTransmit(new Busy(isFree))
                .setCallback(changingStatusAnswer,Busy.class)
                .setToken(token)
                .buildOneRequest();
        //ManfredNetworkManager manfredNetworkManager = CheckEventService.getManfredNetworkManager();
        manfredNetworkManager.addQuery(changeState);
    }

    @Override
    public void logout(AnswerCallback answerCallback) {
        WebSocketAnswerCallback logoutAnswer = new WebSocketAnswerCallback() {
            @Override
            public void messageReceived(WebSocketAnswer message) {
                if(message.getResultCode().equals("success")){
                    answerCallback.success();
                }else {
                    answerCallback.notSuccess(message.getMessage());
                }
            }

            @Override
            public void error(ManfredError manfredError) {
                answerCallback.notSuccess(manfredError.getText());
            }
        };
        NetworkQuery logoutQuery = new NetworkQuery.OneRequestBuilder<Void,Void>("user/logout")
                .setCallback(logoutAnswer,null)
                .setToken(token)
                .buildOneRequest();
        ManfredApplication.getNetworkManger().addQuery(logoutQuery);
    }

    @Override
    public void setNewFCMToken(String FCMtoken, AnswerCallback answerCallback) {
        //ManfredNetworkManager manfredNetworkManager = CheckEventService.getManfredNetworkManager();

        WebSocketAnswerCallback<WebSocketAnswer<Driver>> answer = new WebSocketAnswerCallback<WebSocketAnswer<Driver>>() {
            @Override
            public void messageReceived(WebSocketAnswer<Driver> message) {
                Log.d(TAG, "setNewFCMToken onResponse: " + message.getResultCode() + ", " + message.getMessage());
                answerCallback.success();
            }

            @Override
            public void error(ManfredError manfredError) {
                answerCallback.notSuccess(manfredError.getText());
            }
        };

        NetworkQuery networkQuery = new NetworkQuery.OneRequestBuilder<ManfredPushToken,Driver>("user/set_push_token")
                .setCallback(answer,Driver.class)
                .setDataForTransmit(new ManfredPushToken(FCMtoken))
                .setToken(token)
                .buildOneRequest();
        manfredNetworkManager.addQuery(networkQuery);


    }
}
