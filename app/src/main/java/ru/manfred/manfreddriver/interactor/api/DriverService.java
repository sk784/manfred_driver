/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.interactor.api;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import ru.manfred.manfreddriver.model.api.Busy;
import ru.manfred.manfreddriver.model.api.ManfredEventResponse;
import ru.manfred.manfreddriver.model.api.ManfredResponse;
import ru.manfred.manfreddriver.model.api.PathDate;
import ru.manfred.manfreddriver.model.responces.IsFreeResponse;
import ru.manfred.manfreddriver.model.responces.ListLog;
import ru.manfred.manfreddriver.model.responces.ManfredResponceAcceptingOrder;
import ru.manfred.manfreddriver.model.responces.ManfredResponceDriverStatus;
import ru.manfred.manfreddriver.model.responces.ManfredResponseCarsOnLine;
import ru.manfred.manfreddriver.model.responces.ManfredResponseNeedUpdate;
import ru.manfred.manfreddriver.model.responces.ManfredResponseOrderHistory;

/**
 * Created by begemot on 04.07.17.
 */

public interface DriverService {
    @POST("v1.1/clone-1/driver/user/update/")
    Call<ManfredResponse> changeStatus(@Query("token") String token, @Body Busy busy);

    @POST("v1.1/clone-1/driver/order/force_payment/")
    Call<ManfredResponceAcceptingOrder> checkPayment(@Query("token") String token);

    @GET("v1.1/clone-1/driver/events/")
    Call<ManfredEventResponse> getEvents(@Query("token") String token);

    @GET("v1.1/clone-1/driver/order/list/")
    Call<ManfredResponseOrderHistory> getHistory(@Query("token") String token);

    @GET("v1.1/clone-1/driver/order/list/")
    Call<ManfredResponseOrderHistory> getHistoryWithInterval(@Query("token") String token, @Query("dateFrom") PathDate dateFrom, @Query("dateTo") PathDate dateTo);

    @GET("v1.1/clone-1/driver/order/cancel/")
    Call<ManfredResponse> cancelTrip(@Query("token") String token);

    @GET("v1.1/clone-1/driver/user/state/")
    Call<ManfredResponceDriverStatus> getStatus(@Query("token") String token);

    @GET("v1.1/clone-1/driver/order/preorders/")
    Call<ManfredResponseOrderHistory> getPreOrders(@Query("token") String token);

    @GET("/api/v1.1/clone-1/driver/car_list/")
    Call<ManfredResponseCarsOnLine> getCarsOnMap(@Query("token") String token);

    @POST("/api/v1.1/clone-1/driver/add_log_list/")
    Call<ManfredResponse> sendLogs(@Query("token") String token, @Body ListLog logs);

    @GET("/api/v1.1/clone-1/driver/check_update/")
    Call<ManfredResponseNeedUpdate>checkForUpdate(@Query("platform") String platform, @Query("version") String version);

    @GET("/api/v1.1/clone-1/driver/user/check_is_free/")
    Call<ManfredResponse<IsFreeResponse>>checkForFree(@Query("token") String token);
}
