/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils;

import android.util.Log;

import java.util.List;

import ru.manfred.manfreddriver.model.GoogleMaps.DirectionResults;
import ru.manfred.manfreddriver.model.GoogleMaps.Legs;
import ru.manfred.manfreddriver.model.GoogleMaps.Route;

public class TimeCalculatorFromDirection {
    private static final String TAG = "TimeCalculatorFromDirec";
   public class CalculatedTripProperties{
        private final int durationInSec;
        private final int distanceInMeters;

        public CalculatedTripProperties(int durationInSec, int distanceInMeters) {
            this.durationInSec = durationInSec;
            this.distanceInMeters = distanceInMeters;
        }

        public int getDurationInSec() {
            return durationInSec;
        }

        public int getDistanceInMeters() {
            return distanceInMeters;
        }
    }

    private final CalculatedTripProperties calculatedTripProperties;

    public TimeCalculatorFromDirection(DirectionResults results) {
        List<Route> routes = results.getRoutes();
        int durationInSec = 0;
        int distance = 0;
        for (Route route : routes) {
            for (Legs legs : route.getLegs()) {
                durationInSec = durationInSec + legs.getDuration_in_traffic().getValue();
                distance = distance + legs.getDistance().getValue();
                //Log.d(TAG, "onResponse: distance of lag is "+legs.getDistance().getValue());
                //Log.d(TAG, "onResponse: distance of lag (string) is "+legs.getDistance().getText());
            }
        }
        Log.d(TAG, "TimeCalculatorFromDirection: durationInSec is "+durationInSec);
        if(durationInSec==0)durationInSec=60;
        if(distance<1000)distance=1000;
        calculatedTripProperties = new CalculatedTripProperties(durationInSec, distance);
    }

    public CalculatedTripProperties getCalculatedTripProperties() {
        return calculatedTripProperties;
    }
}
