/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.location.Location;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.interactor.api.ApiFactory;
import ru.manfred.manfreddriver.interactor.repository.DriverRepository;
import ru.manfred.manfreddriver.interactor.repository.WebSocketDriverRepository;
import ru.manfred.manfreddriver.model.GoogleMaps.geocode.AddressComponent;
import ru.manfred.manfreddriver.model.GoogleMaps.geocode.GeocodeResult;


/**
 * Created by begemot on 25.07.17.
 */

public class MapUtils {
    private static final String TAG = "MapUtils";
    public interface MyInterface {
        void polylineReady(PolylineOptions polylineOptions);
    }

    private MapUtils() {
    }

    private static Location prevMarkerLocation;
    public static Marker showMarkerOnMap(@Nullable Marker currLocationMarker, Location location, GoogleMap map) {
        if (location == null) return currLocationMarker;
        //Log.d(TAG, "showMarkerOnMap: "+location);
        if (map == null) return currLocationMarker;
        if (currLocationMarker != null) {
            //currLocationMarker.remove();
            //Log.d(TAG, "showMarkerOnMap: moving marker");
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            currLocationMarker.setPosition(latLng);
            if(prevMarkerLocation!=null){
                currLocationMarker.setRotation(prevMarkerLocation.bearingTo(location));
            }
            prevMarkerLocation=location;
            //Log.d(TAG, "showMarkerOnMap: bearing is "+location.getBearing());
        }else {
            Log.d(TAG, "showMarkerOnMap: creating new marker "+location);
            prevMarkerLocation=location;
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.my_location));
            markerOptions.flat(true);
            currLocationMarker = map.addMarker(markerOptions);
        }
        return currLocationMarker;
    }


    public static Marker showCustomerMarkerOnMap(@Nullable Marker customerMarker, @NonNull Location currentLocation,
                                               Location customerLocation, GoogleMap map, Context context) {
        return showCustomerMarkerOnMap(customerMarker, currentLocation, customerLocation, map, context, true);
    }

    public static Marker showCustomerMarkerOnMap(@Nullable Marker customerMarker, @NonNull Location currentLocation,
                                               Location customerLocation, @Nullable GoogleMap map, Context context, boolean needZoom) {
        if (currentLocation == null || map == null) {
            return null;
        }
        LatLng latLng = new LatLng(customerLocation.getLatitude(), customerLocation.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.picup));
        customerMarker = map.addMarker(markerOptions);
        if (needZoom) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()));
            builder.include(new LatLng(customerLocation.getLatitude(), customerLocation.getLongitude()));
            int width = context.getResources().getDisplayMetrics().widthPixels;
            int height = dpAPixeles(164, context);
            final int minMetric = Math.min(width, height);
            //https://stackoverflow.com/questions/40224820/google-maps-api-v2-newlatlngbounds-using-percentage-padding-throws-error-in-mult
            int padding = (int) (minMetric * 0.10);
            map.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), width, height, padding));
        }
        return customerMarker;
    }

    public static void fitToPolylineAndMarkers(@NonNull Polyline polyline, GoogleMap mMap, Resources resources,
                                               @NonNull List<Marker>markers) {
        if (mMap == null) {
            Log.d(TAG, "fitToPolylineAndMarkers: map is null, not scaling");
            return;
        }
        if(markers.isEmpty() && polyline.getPoints().isEmpty()){
            Log.d(TAG, "fitToPolylineAndMarkers: empty, not calculating");
            return;
        }
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        if (!polyline.getPoints().isEmpty()){
            for (int i = 0; i < polyline.getPoints().size(); i++) {
                builder.include(polyline.getPoints().get(i));
            }
        }
        if(!markers.isEmpty()){
            for (Marker marker : markers){
                if(marker!=null) {
                    builder.include(marker.getPosition());
                }
            }
        }
        Log.d(TAG, "fitToPolylineAndMarkers: ");
        /*
        LatLngBounds bounds = builder.build();
        int width = resources.getDisplayMetrics().widthPixels;
        int padding = (int) (width * 0.10); // offset from edges of the map in pixels
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        mMap.moveCamera(cu);*/
        final int width = resources.getDisplayMetrics().widthPixels;
        final int height = resources.getDisplayMetrics().heightPixels;
        final int minMetric = Math.min(width, height);
        final int padding = (int) (minMetric * 0.40); // offset from edges of the map in pixels
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(builder.build(), width, height, padding);
        mMap.moveCamera(cu);

/*        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), dpAPixeles(30,resources.getDisplayMetrics())));
        mMap.moveCamera(CameraUpdateFactory.zoomTo(mMap.getCameraPosition().zoom - 0.5f));*/
    }

    public static Marker showDestinationMarkerOnMap(@Nullable Location destination, Location currentLocation, Location customerLocation,
                                                    GoogleMap map, Context context, boolean needZoom) {
        if (destination == null) return null;
        if(map==null)return null;
        //Log.d(TAG, "showDestinationMarkerOnMap: dest is "+destination);

        LatLng latLng = new LatLng(destination.getLatitude(), destination.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.destination));
        Marker destinationMarker = map.addMarker(markerOptions);

        if (needZoom) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()));
            builder.include(new LatLng(customerLocation.getLatitude(), customerLocation.getLongitude()));
            builder.include(new LatLng(destination.getLatitude(), destination.getLongitude()));
            int width = context.getResources().getDisplayMetrics().widthPixels;
            int height = dpAPixeles(175, context);
            final int minMetric = Math.min(width, height);
            int padding = (int) (minMetric * 0.10);
            map.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), width, height, padding));
        }

        return destinationMarker;
    }

    private static int dpAPixeles(int dp, Context context) {
        Resources r = context.getResources();
        return (int) TypedValue
                .applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
    }

    private static int dpAPixeles(int dp, DisplayMetrics displayMetrics) {
        return (int) TypedValue
                .applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, displayMetrics);
    }

    public static void startYandexNavigator(double latitude, double longitude, Context activity) {
        Uri uri = Uri.parse("yandexnavi://build_route_on_map?lat_to=" + String.valueOf(latitude) + "&lon_to=" + String.valueOf(longitude));
        Intent intent;
        if (latitude != 0) {
            intent = new Intent(Intent.ACTION_VIEW, uri);
        } else {
            intent = new Intent("ru.yandex.yandexnavi.action.BUILD_ROUTE_ON_MAP");
        }
        intent.setPackage("ru.yandex.yandexnavi");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        // Проверяет, установлено ли приложение.
        PackageManager packageManager = activity.getPackageManager();
        List<ResolveInfo> activities = packageManager.queryIntentActivities(intent, 0);
        boolean isIntentSafe = activities.size() > 0;
        if (isIntentSafe) {
            //Запускает Яндекс.Навигатор.
            activity.startActivity(intent);
        } else {
            Toast installYandex = Toast.makeText(activity, "Установите яндекс навигатор", Toast.LENGTH_LONG);
            float padding = ScreenUtils.convertDpToPixel(70, activity);
            installYandex.setGravity(Gravity.BOTTOM, 0, (int) padding);
            installYandex.show();
        }
    }

    public interface LocationAddressListener {
        void addressRecognised(String address);

        void errorRecognising(String error);
    }

    public static void getAddressFromLocation(final Location location, DriverRepository driverRepository, final LocationAddressListener locationAddressListener) {
        Log.d(TAG, "getAddressFromLocation: ");
        String latlng = Double.toString(location.getLatitude()) + "," + Double.toString(location.getLongitude());
        driverRepository.getLocationNameByLatLng(location, new DriverRepository.NetAnswerCallback<WebSocketDriverRepository.LocationResponseModel>() {
            @Override
            public void allFine(WebSocketDriverRepository.LocationResponseModel answer) {
                String address = answer.getAddress() + ", " + answer.getCity();
                locationAddressListener.addressRecognised(address);
            }

            @Override
            public void error(String error) {
                locationAddressListener.errorRecognising(error);
            }

            @Override
            public void serverNotAvailable() {
                locationAddressListener.errorRecognising("server not availeble");
            }
        });
    }

    private static String getAddress(GeocodeResult geocodeResult) {
        if (geocodeResult.getResults().isEmpty()) return "unknown";
        List<AddressComponent> addressComponents = geocodeResult.getResults().get(0).getAddressComponents();
        String streetNumber = "";
        String route = "";
        String city = "";
        for (AddressComponent addressComponent : addressComponents) {
            if (addressComponent.getTypes().contains("street_number")) {
                streetNumber = addressComponent.getLongName();
            }
            if (addressComponent.getTypes().contains("locality")) {
                city = addressComponent.getShortName();
            }
            if (addressComponent.getTypes().contains("route")) {
                route = addressComponent.getShortName();
            }
        }
        String address = route + " " + streetNumber + ", " + city;
        return address;
    }

}
