/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils;

import java.util.List;

import ru.manfred.manfreddriver.viewmodel.PreOrderHolderVM;

import static android.support.v7.util.DiffUtil.Callback;

/**
 * Created by begemot on 15.03.18.
 */

public class ManfredDiffUtilPreOrdersCallback extends Callback {
    private final List<PreOrderHolderVM> oldList;
    private final List<PreOrderHolderVM> newList;

    public ManfredDiffUtilPreOrdersCallback(List<PreOrderHolderVM> oldList, List<PreOrderHolderVM> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).getPreOrderVM().getOrder().getId() == newList.get(newItemPosition).getPreOrderVM().getOrder().getId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).getPreOrderVM().equals(newList.get(newItemPosition).getPreOrderVM());
    }
}
