/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;


public class LocationUtils {
    public static LatLng LocationToLatLng(Location location){
        double latitude=location.getLatitude();
        double longitude=location.getLongitude();
        return new LatLng(latitude, longitude);
    }
}
