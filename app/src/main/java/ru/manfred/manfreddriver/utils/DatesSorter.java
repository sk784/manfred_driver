/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils;

import java.util.List;

import ru.manfred.manfreddriver.viewmodel.ListItem;

/**
 * Created by begemot on 21.12.17.
 */

public interface DatesSorter {
    List<ListItem> getItems();
}
