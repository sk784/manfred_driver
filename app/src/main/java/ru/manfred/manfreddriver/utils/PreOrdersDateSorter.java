/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils;

import android.support.annotation.Nullable;
import android.text.format.DateUtils;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.presenters.PreOrderPresenter;
import ru.manfred.manfreddriver.viewmodel.DateItemVM;
import ru.manfred.manfreddriver.viewmodel.ListItem;
import ru.manfred.manfreddriver.viewmodel.PreOrderHolderVM;
import ru.manfred.manfreddriver.viewmodel.PreOrderVM;

/**
 * Created by begemot on 21.12.17.
 */

public class PreOrdersDateSorter implements DatesSorter {
    private static final String TAG = "PreOrdersDateSorter";
    private List<Order> preOrders;
    private boolean isMy;

    public PreOrdersDateSorter(@Nullable List<Order> preOrders, boolean isMy) {
        //ManfredApplication.getTripComponent().inject(this);
        Log.d(TAG, "PreOrdersDateSorter: sorting...");
        if(preOrders==null){
            this.preOrders=new ArrayList<>();
            this.isMy=isMy;
            return;
        }

        this.preOrders = preOrders;
        Collections.sort(preOrders, new Comparator<Order>() {
            public int compare(Order o2, Order o1) {
                if (o1.getCar_need_time() == null || o2.getCar_need_time() == null) {
                    return 0;
                }
                return (Long.compare(o2.getCar_need_time().getTime(), o1.getCar_need_time().getTime()));
            }
        });
        /*
        Log.d(TAG, "PreOrdersDateSorter: after sort:");
        for(Order order : preOrders){
            Log.d(TAG, "PreOrdersDateSorter: "+order.getCar_need_time());
        }
        */
        this.preOrders = preOrders;
        this.isMy = isMy;
    }

    @Override
    public List<ListItem> getItems() {
        //https://krtkush.github.io/2016/07/08/android-recyclerview-grouping-data.html
        // We linearly add every item into the consolidatedList.
        List<ListItem> consolidatedList = new ArrayList<>();
        LinkedHashMap<String, List<Order>> DateByOrders = groupDataIntoHashMap(preOrders);
        //Log.d(TAG, "getItems: hashmap keys "+DateByOrders.keySet());

        //SortedSet<String> keys = new TreeSet<String>(DateByOrders.keySet());
        //Log.d(TAG, "getItems: by sorted keys "+keys);
        PreOrderPresenter preOrderPresenter = new PreOrderPresenter(isMy);
        for (String date : DateByOrders.keySet()) {
            DateItemVM dateItem = new DateItemVM(date);
            //dateItem.setDate(date);
            //Log.d(TAG, "getItems: add date "+dateItem.getDate());
            consolidatedList.add(dateItem);
            //grouping by date
            for (Order order : DateByOrders.get(date)) {
                PreOrderHolderVM generalItem = new PreOrderHolderVM(new PreOrderVM(order), preOrderPresenter, null);

                //generalItem.setPojoOfJsonArray(pojoOfJsonArray);
                //Log.d(TAG, "getItems: adding item with start time "+generalItem.getOrder().getRide_start_time().toString());
                consolidatedList.add(generalItem);
            }
        }
        return consolidatedList;
    }

    private LinkedHashMap<String, List<Order>> groupDataIntoHashMap(List<Order> listOfOrders) {

        LinkedHashMap<String, List<Order>> groupedHashMap = new LinkedHashMap<>();
        for (Order order : listOfOrders) {
            SimpleDateFormat sdfr = new SimpleDateFormat("d MMMM", new Locale("ru"));
            String hashMapKey = (DateUtils.isToday(order.getCar_need_time().getTime())) ? "Сегодня" : sdfr.format(order.getCar_need_time());
            //Log.d(TAG, "groupDataIntoHashMap: work with date "+order.getCar_need_time());
            if (groupedHashMap.containsKey(hashMapKey)) {
                // The key is already in the HashMap; add the pojo object
                // against the existing key.
                groupedHashMap.get(hashMapKey).add(order);
                //Log.d(TAG, "groupDataIntoHashMap: we have this in hashMap, add"+order.getCar_need_time()+" to "+hashMapKey);
            } else {
                // The key is not there in the HashMap; create a new key-value pair
                List<Order> list = new ArrayList<>();
                list.add(order);
                groupedHashMap.put(hashMapKey, list);
                //Log.d(TAG, "groupDataIntoHashMap: add "+order.getCar_need_time()+" to "+hashMapKey);
            }
        }
        //Log.d(TAG, "groupDataIntoHashMap: result is "+groupedHashMap);
        return groupedHashMap;
    }
}
