/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils.generators;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import ru.manfred.manfreddriver.model.api.Assistant;
import ru.manfred.manfreddriver.model.api.Order;

/**
 * Created by begemot on 26.12.17.
 */

public class PreOrderGenerator {
    public List<Order> generatePreOrders() {
        List<Order> generated = new ArrayList<>();
        String[] statusList = {"completed", "scheduled_acepted", "scheduled", "cancelled"};
        Random r = new Random();
        for (int i = 0; i < 5; i++) {
            Assistant assistant = new Assistant();
            assistant.setId(ThreadLocalRandom.current().nextLong(1000));
            assistant.setName(randomString(9, r));
            assistant.setPhone(randomPhoneNumber(r));

            Order newOrder = new Order();
            newOrder.setId(ThreadLocalRandom.current().nextLong(1000));
            //newOrder.setStatus(statusList[r.nextInt(statusList.length)]);
            newOrder.setStatus("SCHEDULED");
            newOrder.setAddress(randomString(9, r));
            newOrder.setDest_address(randomString(9, r));
            newOrder.setComments(randomString(20, r));
            //newOrder.setTotal_cost(String.valueOf(r.nextInt()));
            newOrder.setCustomerName(randomString(11, r));
            newOrder.setCar_need_time(randomDate(r));
            newOrder.setNeedAssistant(r.nextBoolean());
            newOrder.setAssist(assistant);
            generated.add(newOrder);
        }
        return generated;
    }

    private String randomString(int length, Random random) {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < length) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }

    private Date randomDate(Random rnd) {
        Date dt;
        long ms;

        // Get an Epoch value roughly between 1940 and 2010
        // -946771200000L = January 1, 1940
        // Add up to 70 years to it (using modulus on the next long)
        ms = -946771200000L + (Math.abs(rnd.nextLong()) % (70L * 365 * 24 * 60 * 60 * 1000));
        // Construct a date
        return new Date(ms);
    }

    private String randomPhoneNumber(Random r) {
        int num1, num2, num3; //3 numbers in area code
        int set2, set3; //sequence 2 and 3 of the phone number

        Random generator = new Random();

        //Area code number; Will not print 8 or 9
        num1 = generator.nextInt(7) + 1; //add 1 so there is no 0 to begin
        num2 = generator.nextInt(8); //randomize to 8 becuase 0 counts as a number in the generator
        num3 = generator.nextInt(8);

        // Sequence two of phone number
        // the plus 100 is so there will always be a 3 digit number
        // randomize to 643 because 0 starts the first placement so if i randomized up to 642 it would only go up yo 641 plus 100
        // and i used 643 so when it adds 100 it will not succeed 742
        set2 = generator.nextInt(643) + 100;

        //Sequence 3 of numebr
        // add 1000 so there will always be 4 numbers
        //8999 so it wont succed 9999 when the 1000 is added
        set3 = generator.nextInt(8999) + 1000;

        return ("(" + num1 + "" + num2 + "" + num3 + ")" + "-" + set2 + "-" + set3);
    }
}
