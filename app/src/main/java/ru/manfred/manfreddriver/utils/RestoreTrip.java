/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.activities.RideActivity;
import ru.manfred.manfreddriver.activities.TripToPassengerActivity;
import ru.manfred.manfreddriver.activities.WaitActivity;
import ru.manfred.manfreddriver.managers.ManfredOrderManager;
import ru.manfred.manfreddriver.managers.ManfredTripManager;
import ru.manfred.manfreddriver.managers.logging.LogTags;
import ru.manfred.manfreddriver.managers.logging.ManfredLoggingManager;
import ru.manfred.manfreddriver.model.api.Order;

/**
 * Created by begemot on 28.08.17.
 * restore trip status after login
 */

public class RestoreTrip {
    private static final String SEARCH_DRIVER = "SEARCH_DRIVER";
    private static final String RIDE_TO_PASSENGER = "RIDE_TO_PASSENGER";
    private static final String WAIT_PASSENGER = "WAIT_PASSENGER";
    private static final String RIDE = "RIDE";
    private static final String COMPLETED = "COMPLETED";
    private static final String CANCELLED = "CANCELLED";
    private static final String TAG = "RestoreTrip";
    @Inject
    ManfredTripManager tripManager;
    @Inject
    ManfredOrderManager orderManager;
    @Inject
    ManfredLoggingManager loggingManager;

    public RestoreTrip() {
        ManfredApplication.getTripComponent().inject(this);
    }

    ;

    public @Nullable
    Intent restoreTrip(@Nullable Order order, Context context) {
        Intent intent = null;
        String orderInfo = order != null ? order.toString() : "order is null";
        //Log.d(TAG, "restoreTrip... " + orderInfo);
        String logString = "restoreTrip... " + orderInfo;
        loggingManager.addLog("RestoreTrip", "restoreTrip", logString, LogTags.OTHER);

        /*
         orders = Order.objects.filter(id = driver.order_id)
         то есть все статусы при которых заказ активен
         кроме search driver, canceled, completed
         */
        if (order == null) return null;
        switch (order.getStatus()) {
            case RIDE_TO_PASSENGER:
                intent = new Intent(context, TripToPassengerActivity.class);
                //orderManager.setCurrentOrder(order);
                //tripManager.setCurrOrder(order);
                tripManager.startRideToPassenger(order);
                break;
            case WAIT_PASSENGER:
                intent = new Intent(context, WaitActivity.class);
                //orderManager.setCurrentOrder(order);
                //tripManager.setCurrOrder(order);
                tripManager.waitingPassenger(order);
                break;
            case RIDE:
                intent = new Intent(context, RideActivity.class);
                //orderManager.setCurrentOrder(order);
                //tripManager.setCurrOrder(order);
                tripManager.startRideToDestination(order);
                break;
        }
        return intent;

    }
}
