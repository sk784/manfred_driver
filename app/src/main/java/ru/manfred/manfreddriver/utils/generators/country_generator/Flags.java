/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils.generators.country_generator;

import java.util.List;

public class Flags {

    private List<Flags> flags;

    public Flags(List<Flags> flags) {
        this.flags = flags;
    }

    public List<Flags> getFlags() {
        return flags;
    }
}