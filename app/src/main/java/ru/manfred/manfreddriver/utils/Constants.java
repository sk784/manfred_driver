/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils;

import java.util.concurrent.TimeUnit;

/**
 * Created by begemot on 30.08.17.
 */

public class Constants {
    public static final String SAVED_ORDER = "SAVED_ORDER";
    public static final String SAVED_TIME_TO_COSTUMER = "SAVED_TIME_TO_COSTUMER";
    public static final String NEW_ORDER_ON_SCREEN = "NEW_ORDER_ON_SCREEN";
    public static final String NO_NEW_ORDER_ON_SCREEN = "NO_NEW_ORDER_ON_SCREEN";
    public static final int ID = 0;
    public static final String SAVED_SHOW_PHONE = "SAVED_SHOW_PHONE";
    public static final String SAVED_DURATION_TIME = "SAVED_DURATION_TIME";
    public static final String RECEIVE_ORDER = "ru.manfred.manfreddriver.action.RECEIVE_ORDER";

    public static final String INTERNET_STATUS = "INTERNET_STATUS";
    public static final String NO_INTERNET = "NO_INTERNET";
    public static final String HAVE_INTERNET = "HAVE_INTERNET";

    public static final String HISTORY_INTERVAL_KEY = "HISTORY_INTERVAL_KEY";
    public static final String ORDER_SHOW_STATUS_FILTER = "ORDER_SHOW_STATUS_FILTER";
    public static final String ORDER_SHOW_STATUS = "ORDER_SHOW_STATUS";

    public static final String AIRPORT = "AIRPORT";

    //ModalActivity, intent constants
    public static final String CANCEL_STRING = "CANCEL_STRING";
    public static final String NEED_CANCEL_TRIP = "NEED_CANCEL_TRIP";

    public static int ORDER_SHOW_ON_SCREEN_SEC = 70;
    public static final long ORDER_SHOW_ON_SCREEN_MS = TimeUnit.SECONDS.toMillis(ORDER_SHOW_ON_SCREEN_SEC);

    public static final String NEED_GO_AWAY = "NEED_GO_AWAY";

    public static final String HISTORY_IN_LIST_ID = "HISTORY_IN_LIST_ID";
    public static final String ALL_HISTORY = "ALL_HISTORY";
    public static final String DRIVER = "DRIVER";
    public static final String HISTORY_START_DATE = "HISTORY_START_DATE";
    public static final String HISTORY_END_DATE = "HISTORY_END_DATE";

    public static final String PREORDER_MY = "PREORDER_MY";
    public static final String PREORDER_IN_LIST_ID = "PREORDER_IN_LIST_ID";

    public static final String PREORDER_KEY = "PREORDER_KEY";

    public static final String TIME_OF_PAYMENT_ERROR = "TIME_OF_PAYMENT_ERROR";

    public static final String FIREBASE_CURR_ORDER_KEY_IN_TRIPMANAGER = "currOrderIdInTripMngr";
    public static final String FIREBASE_CURR_ORDER_KEY_IN_ORDERMANAGER = "currOrderIdInOrderMngr";
}
