/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils.generators.country_generator;

import android.util.Log;

import com.google.gson.Gson;

import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import ru.manfred.manfreddriver.model.api.CountryModel;

public class CountryGenerator {
    private static final String TAG = "CountryGenerator";

    static public List<CountryModel> generateCountries(Reader jsonFile, Locale locale) {
        Gson gson = new Gson();
        //Type listType = new TypeToken<ArrayList<Flag>>(){}.getType();
        Flag[] flagModel = gson.fromJson(jsonFile, Flag[].class);
        List<Flag> countries = Arrays.asList(flagModel);
        return mapToModel(countries, locale);
    }

    private static List<CountryModel> mapToModel(List<Flag> flags, Locale locale) {
        Log.d(TAG, "mapToModel: curr language is "+locale.getLanguage());
        List<CountryModel> modelFlags = new ArrayList<>();
        for (Flag flag : flags) {
            if(locale.getLanguage().equals("ru")){
                modelFlags.add(new CountryModel(flag.getFlag(), flag.getRussianName(), flag.getDialCode()));
            }else {
                modelFlags.add(new CountryModel(flag.getFlag(), flag.getName(), flag.getDialCode()));
            }
        }
        Log.d(TAG, "mapToModel: we generate " + modelFlags.size() + " flags");
        return modelFlags;
    }
}
