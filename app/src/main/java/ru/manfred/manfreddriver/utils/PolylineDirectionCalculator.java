/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

import ru.manfred.manfreddriver.model.GoogleMaps.DirectionResults;
import ru.manfred.manfreddriver.model.GoogleMaps.Route;

public class PolylineDirectionCalculator {
    private final PolylineOptions polylineOptions;
    public PolylineDirectionCalculator(DirectionResults results) {
        List<Route> routes = results.getRoutes();
        List<LatLng> coordsOfRoute = new ArrayList<LatLng>();
        for (Route route : routes) {
            coordsOfRoute.addAll(decodePoly(route.getOverviewPolyLine().getPoints()));
        }
        PolylineOptions mPolylineOptions = new PolylineOptions();
        mPolylineOptions.addAll(coordsOfRoute);
        polylineOptions=mPolylineOptions;
    }

    public PolylineOptions getPolylineOptions() {
        return polylineOptions;
    }

    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((double) lat / 1E5, (double) lng / 1E5);
            poly.add(p);
        }
        return poly;
    }
}
