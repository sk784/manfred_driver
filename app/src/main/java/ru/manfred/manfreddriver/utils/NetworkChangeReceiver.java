/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

/**
 * Created by begemot on 07.09.17.
 */

public class NetworkChangeReceiver extends BroadcastReceiver {
    private static final String TAG = "NetworkChangeReceiver";

    @Override
    public void onReceive(final Context context, final Intent intent1) {
        int status = NetworkUtil.getConnectivityStatus(context);
        Intent intent = new Intent(Constants.INTERNET_STATUS);
        Log.d(TAG, "onReceive: status of connection is " + status);
        if (status == NetworkUtil.TYPE_NOT_CONNECTED) {
            intent.putExtra(Constants.INTERNET_STATUS, false);
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            Log.d(TAG, "onReceive: from " + context.getPackageCodePath() + ", " + intent1);
        } else {
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            intent.putExtra(Constants.INTERNET_STATUS, true);
        }
    }
}
