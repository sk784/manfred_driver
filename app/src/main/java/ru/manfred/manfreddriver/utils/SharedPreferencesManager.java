/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import ru.manfred.manfreddriver.model.api.Credentials;

/**
 * Created by begemot on 10.07.17.
 */

public class SharedPreferencesManager {
    private static final String PREFS_NAME = "ManfredPrefsFile";
    //cache;
    private static Credentials credentials;
    private static final String TAG = "SharedPreferencesManage";

    public static Credentials getCredentials(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        //String login = settings.getString("login", null);
        //Log.d(TAG, "getCredentials: login is "+login);
        //String password = settings.getString("password", null);
        String number = settings.getString("phone", null);
        String token = settings.getString("setToken", null);
        if (number != null) {
            credentials = new Credentials(number, token);
            return new Credentials(number, token);
        } else {
            return null;
        }

    }

    public static Credentials getCachedCredentials() {
        return credentials;
    }

    public static void setCredentials(Context context, Credentials credentials) {
        Log.d(TAG, "setCredentials: saving setToken "+credentials.getToken());
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("phone", credentials.getPhoneNumber());
        //editor.putString("password", credentials.getPassword());
        editor.putString("setToken", credentials.getToken());
        editor.apply();
    }

    public static void removeCredentials(Context context) {
        Log.d(TAG, "removeCredentials: ");
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove("login");
        editor.remove("password");
        editor.remove("phone");
        editor.remove("setToken");
        editor.remove("driver");
        editor.apply();
    }

/*    public static void setDriver(Driver driver, Context context) {
        Log.d(TAG, "setDriver: we save driver " + driver.toString());
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("yyyy-MM-dd");
        Gson gson = gsonBuilder.create();
        String driverString = gson.toJson(driver);
        editor.putString("driver", driverString);
        editor.apply();
    }

    @Nullable
    public static Driver getDriver(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        //Gson gson = new Gson();
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("yyyy-MM-dd");
        Gson gson = gsonBuilder.create();
        Log.d(TAG, "getDriver: raw json is "+settings.getString("driver",""));
        Driver driver = gson.fromJson(settings.getString("driver", ""), Driver.class);
        Log.d(TAG, "getDriver: "+driver.toString());
        return driver;
    }*/

    public static boolean needShowBatteryOptimisation(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        return settings.getBoolean("skipProtectedAppsMessage", true);
    }

    public static void setNeedShowBatteryOptimisation(Context context, boolean isNeed) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("skipProtectedAppsMessage", isNeed);
        editor.apply();
    }

    public static void firstTimeAskingPermission(Context context, String permission, boolean isFirstTime){
        SharedPreferences sharedPreference = context.getSharedPreferences(PREFS_NAME, 0);
        sharedPreference.edit().putBoolean(permission, isFirstTime).apply();
    }

    public static boolean isFirstTimeAskingPermission(Context context, String permission){
        return context.getSharedPreferences(PREFS_NAME, 0).getBoolean(permission, true);
    }

    public static String getLastConnectionHost(Context context) {
        return context.getSharedPreferences(PREFS_NAME, 0).getString("lastConnectionHost", null);
    }

    public static void setLastConnectionHost(Context context, String host) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("lastConnectionHost", host);
        editor.apply();
    }
}
