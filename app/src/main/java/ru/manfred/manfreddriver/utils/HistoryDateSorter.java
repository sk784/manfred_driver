/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.viewmodel.DateItemVM;
import ru.manfred.manfreddriver.viewmodel.ListItem;
import ru.manfred.manfreddriver.viewmodel.OrderHistoryViewModel;

/**
 * Created by begemot on 21.12.17.
 * https://krtkush.github.io/2016/07/08/android-recyclerview-grouping-data.html
 */

public class HistoryDateSorter implements DatesSorter {
    private List<Order> orders;
    private static final String TAG = "HistoryDateSorter";

    public HistoryDateSorter(List<Order> orders) {
        Log.d(TAG, "HistoryDateSorter: sorting "+orders.size());
/*        for (Order order:orders){
            Log.d(TAG, order.toString());
        }*/
        this.orders = orders;
    }

    @Override
    public List<ListItem> getItems() {
        // We linearly add every item into the consolidatedList.
        List<ListItem> consolidatedList = new ArrayList<>();
        LinkedHashMap<String, List<Order>> groupDataIntoHashMap = groupDataIntoHashMap(orders);
        Log.d(TAG, "getItems: hashmap keys "+groupDataIntoHashMap.keySet());

        //SortedSet<String> keys = new TreeSet<String>(groupDataIntoHashMap.keySet());
        //Log.d(TAG, "getItems: by sorted keys "+keys);
        for (String date : groupDataIntoHashMap.keySet()) {
            DateItemVM dateItem = new DateItemVM(date);
            //dateItem.setDate(date);
            //Log.d(TAG, "getItems: add date "+dateItem.getDate());
            consolidatedList.add(dateItem);

            for (Order order : groupDataIntoHashMap.get(date)) {
                OrderHistoryViewModel generalItem = new OrderHistoryViewModel(order);
                //generalItem.setPojoOfJsonArray(pojoOfJsonArray);
                //Log.d(TAG, "getItems: adding item with start time "+generalItem.getOrder().getRide_start_time().toString());
                consolidatedList.add(generalItem);
            }
        }
        return consolidatedList;
    }

    private LinkedHashMap<String, List<Order>> groupDataIntoHashMap(List<Order> listOfOrders) {
        LinkedHashMap<String, List<Order>> groupedHashMap = new LinkedHashMap<>();
        for (Order order : listOfOrders) {
            //if (order.getRide_start_time() == null) continue;

            SimpleDateFormat sdfr = new SimpleDateFormat("d MMMM", new Locale("ru"));
            String hashMapKey;
            //ticket APP-1679
            if (order.getRide_finish_time()!=null){
                hashMapKey = sdfr.format(order.getRide_finish_time());
            }else {
                if(order.getCancel_time()==null)continue;
                hashMapKey = sdfr.format(order.getCancel_time());
            }
/*            if(order.getCar_need_time()!=null){
                hashMapKey =  sdfr.format(order.getCar_need_time());
            }else {
                if (order.getAccept_order_time() == null) continue;
                hashMapKey = sdfr.format(order.getAccept_order_time());
            }*/

            if (groupedHashMap.containsKey(hashMapKey)) {
                // The key is already in the HashMap; add the pojo object
                // against the existing key.
                groupedHashMap.get(hashMapKey).add(order);
                //Log.d(TAG, "groupDataIntoHashMap: we have this in hashMap, add"+order.getRide_start_time()+" to "+hashMapKey);
            } else {
                // The key is not there in the HashMap; create a new key-value pair
                List<Order> list = new ArrayList<>();
                list.add(order);
                groupedHashMap.put(hashMapKey, list);
                //Log.d(TAG, "groupDataIntoHashMap: add "+order.getRide_start_time()+" to "+hashMapKey);
            }
        }
        return groupedHashMap;
    }
}
