/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils;

import android.support.v7.util.DiffUtil;
import android.util.Log;

import java.util.List;

import ru.manfred.manfreddriver.viewmodel.DateItemVM;
import ru.manfred.manfreddriver.viewmodel.ListItem;
import ru.manfred.manfreddriver.viewmodel.PreOrderHolderVM;

import static ru.manfred.manfreddriver.viewmodel.ListItem.TYPE_DATE;
import static ru.manfred.manfreddriver.viewmodel.ListItem.TYPE_GENERAL;

public class ListItemDiffutilCallback extends DiffUtil.Callback {
    private List<ListItem> oldList;
    private List<ListItem> newList;
    private static final String TAG = "ListItemDiffutilCallbac";

    public ListItemDiffutilCallback(List<ListItem> oldList, List<ListItem> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        Log.d(TAG, "areItemsTheSame: item "+oldItemPosition+", new position "+newItemPosition);
        ListItem oldItem = oldList.get(oldItemPosition);
        ListItem newItem = newList.get(newItemPosition);
        /*
        if(oldItem.getType()==newItem.getType()){
            return true;
        }*/
        if(oldItem.getType()==TYPE_GENERAL
                && newItem.getType()==TYPE_GENERAL){
            return ((PreOrderHolderVM) oldItem).getId() == (((PreOrderHolderVM) newItem).getId());
        }
        if(oldItem.getType()==TYPE_DATE
                && newItem.getType()==TYPE_DATE){
            return ((DateItemVM) oldItem).getDate().equals(((DateItemVM) newItem).getDate());
        }
        Log.d(TAG, "areItemsTheSame: not same");
        return false;
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        ListItem oldItem = oldList.get(oldItemPosition);
        ListItem newItem = newList.get(newItemPosition);
        if(oldItem.getType()==TYPE_GENERAL
                && newItem.getType()==TYPE_GENERAL){
            if(((PreOrderHolderVM)oldItem).getId()==(((PreOrderHolderVM)newItem).getId())){
                return true;
            }else {
                return false;
            }
        }
        if(oldItem.getType()==TYPE_DATE
                && newItem.getType()==TYPE_DATE){
            return ((DateItemVM) oldItem).getDate().equals(((DateItemVM) newItem).getDate());
        }
        return false;
    }
}
