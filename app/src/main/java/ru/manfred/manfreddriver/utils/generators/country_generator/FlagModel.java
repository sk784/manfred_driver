/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils.generators.country_generator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FlagModel {

    @SerializedName("flags")
    @Expose
    public Flags flags;

}
