/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils;

import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import ru.manfred.manfreddriver.managers.SnackbarManager;

/**
 * Created by begemot on 20.02.18.
 */

public class UIUtils {
    public static Snackbar generateSnack(View view, @NonNull SnackbarManager.Snack snack) {
        Snackbar snackbar = Snackbar.make(view, snack.getText(), 5000);
        View snackView = snackbar.getView();
        snackView.setBackgroundColor(ContextCompat.getColor(view.getContext(), snack.getBackgroundColour()));
        TextView snackText = snackView.findViewById(android.support.design.R.id.snackbar_text);
        snackText.setTextColor(ContextCompat.getColor(view.getContext(), snack.getTextColour()));
        return snackbar;
    }
}
