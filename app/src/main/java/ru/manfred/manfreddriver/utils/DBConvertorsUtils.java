/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils;

import java.util.ArrayList;
import java.util.List;

import ru.manfred.manfreddriver.db.entity.GeoDateEntity;
import ru.manfred.manfreddriver.db.entity.LogDB;
import ru.manfred.manfreddriver.model.api.GeoData;
import ru.manfred.manfreddriver.model.api.LogNet;

public class DBConvertorsUtils {
    public static List<LogNet> logsDBToLogs(List<LogDB> input) {
        List<LogNet> logNetList = new ArrayList<>();
        for (LogDB logDB : input){
            logNetList.add(logDB.logNet);
        }
        if(logNetList.isEmpty()){
            return null;
        }else {
            return logNetList;
        }
    }

    public static List<GeoData> geoDataDBToGeo(List<GeoDateEntity> geoDateEntities){
        List<GeoData> geoDataList = new ArrayList<>();
        for (GeoDateEntity geoDateEntity : geoDateEntities){
            geoDataList.add(geoDateEntity.geoData);
        }

        if(geoDataList.isEmpty()){
            return null;
        }else {
            return geoDataList;
        }
    }
}
