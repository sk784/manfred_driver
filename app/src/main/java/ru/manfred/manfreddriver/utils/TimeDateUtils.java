/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by begemot on 29.08.17.
 */

public class TimeDateUtils {
    private static final String TAG = "TimeDateUtils";

    public static int TimeDiffInMin(Date start) {
        long diffInMs = new Date().getTime() - start.getTime();
        long diffInMin = TimeUnit.MILLISECONDS.toMinutes(diffInMs);
        return (int) diffInMin;
    }

    /**
     * @param start start time
     * @return time in sec between cureent and start;
     */
    public static long TimeDiffInMSec(Date start) {
        long diffInMs = new Date().getTime() - start.getTime();
        return (int) diffInMs;
    }

    public static long TimeDiffInSec(Date start) {
        return TimeUnit.MILLISECONDS.toSeconds(TimeDiffInMSec(start));
    }

    public static String DateToString(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM yyyy", new Locale("ru"));
        return simpleDateFormat.format(date) + "г.";
    }

    /**
     * @param milliseconds for formatting
     * @return HH:MM
     */
    public static String msToHoursMins(long milliseconds) {
        return String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(milliseconds),
                TimeUnit.MILLISECONDS.toSeconds(milliseconds) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliseconds))
        );
    }

    public static boolean isSameDay(Date date1, Date date2) {
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(date1);
        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(date2);
        boolean sameYear = calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR);
        boolean sameMonth = calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH);
        boolean sameDay = calendar1.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH);
        return (sameDay && sameMonth && sameYear);
    }

    public static Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }

    public static boolean isWithinRange(Date testDate, Date startDate, Date endDate) {
        boolean inRange = false;
        //Log.d(TAG, "isWithinRange: checking date: " + testDate + ", startDate: " + startDate + ", end date: " + endDate);
        inRange = testDate.getTime() >= startDate.getTime() &&
                testDate.getTime() <= endDate.getTime();
        //Log.d(TAG, "isWithinRange: ? " + inRange);
        return inRange;
    }
}
