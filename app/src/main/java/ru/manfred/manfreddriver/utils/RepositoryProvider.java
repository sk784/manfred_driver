/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.Nullable;
import android.util.Log;

import ru.manfred.manfreddriver.interactor.repository.ConfirmationRepository;
import ru.manfred.manfreddriver.interactor.repository.DriverRepository;
import ru.manfred.manfreddriver.interactor.repository.HTTPConfirmationRepository;
import ru.manfred.manfreddriver.interactor.repository.HTTPDriverRepository;
import ru.manfred.manfreddriver.interactor.repository.HTTPRideRepository;
import ru.manfred.manfreddriver.interactor.repository.HTTPUserRepository;
import ru.manfred.manfreddriver.interactor.repository.RideRepository;
import ru.manfred.manfreddriver.interactor.repository.UserRepository;
import ru.manfred.manfreddriver.interactor.repository.WebSocketConfirmationRepository;
import ru.manfred.manfreddriver.interactor.repository.WebSocketDriverRepository;
import ru.manfred.manfreddriver.interactor.repository.WebSocketRideRepository;
import ru.manfred.manfreddriver.interactor.repository.WebSocketUserRepository;
import ru.manfred.manfreddriver.managers.network.ManfredNetworkManager;
import ru.manfred.manfreddriver.managers.network.NetworkManager;

public class RepositoryProvider {
    private static final String TAG = "RepositoryProvider";
    private boolean isWebSocket;
    private MutableLiveData<Boolean>isSocketLiveData=new MutableLiveData<>();
    @Nullable private ManfredNetworkManager networkManager;

    public RepositoryProvider(boolean isWebSocket, @Nullable ManfredNetworkManager networkManager) {
        this.isWebSocket = isWebSocket;
        this.isSocketLiveData.postValue(isWebSocket);
        this.networkManager=networkManager;
    }

    public void setBySocket(ManfredNetworkManager networkManager){
        Log.d(TAG, "setBySocket: ");
        this.networkManager=networkManager;
        this.isWebSocket=true;
        this.isSocketLiveData.postValue(true);
    }

    public void setByHTTP(){
        Log.d(TAG, "setByHTTP: ");
        killSocket();
        this.isWebSocket=false;
        this.isSocketLiveData.postValue(false);
    }

    public DriverRepository getDriverRepository(String token){
        //Log.d(TAG, "getDriverRepository: socket? "+isWebSocket);
        if(isWebSocket){
            return new WebSocketDriverRepository(token,networkManager);
        }else {
            return new HTTPDriverRepository(token);
        }
    }

    public RideRepository getRideRepository(String token){
        if(isWebSocket){
            return new WebSocketRideRepository(token,networkManager);
        }else {
            return new HTTPRideRepository(token);
        }
    }

    private void killSocket(){
        if(networkManager!=null){
            networkManager.disconnect();
            networkManager=null;
        }
    }

    public UserRepository getUserRepository(String token){
        if (isWebSocket){
            return new WebSocketUserRepository(token,networkManager);
        }else {
            return new HTTPUserRepository(token);
        }
    }

    public boolean isWebSocket() {
        return isWebSocket;
    }

    public ConfirmationRepository getConfirmationRepository(String token){
        if(isWebSocket){
            return new WebSocketConfirmationRepository(token,networkManager);
        }else {
            return new HTTPConfirmationRepository(token);
        }
    }

    public LiveData<Boolean> getIsSocketLiveData() {
        return isSocketLiveData;
    }
}
