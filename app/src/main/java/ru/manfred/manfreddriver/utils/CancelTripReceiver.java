/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

@Deprecated
public abstract class CancelTripReceiver extends BroadcastReceiver {
    public static final String CANCEL_ORDER = "ru.manfred.manfreddriver.action.CANCEL_ORDER";

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        Log.d("CancelTripReceiver", "onReceive: take intent");
        cancelTrip();
    }

    protected abstract void cancelTrip();
}
