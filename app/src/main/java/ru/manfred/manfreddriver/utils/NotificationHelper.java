/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import java.util.Date;
import java.util.List;

import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.activities.NewOrderActivity;
import ru.manfred.manfreddriver.activities.SplashActivity;
import ru.manfred.manfreddriver.activities.preorders.PreordersActivity;
import ru.manfred.manfreddriver.model.api.Order;

import static ru.manfred.manfreddriver.activities.MapsActivity.ORDER_ON_SCREEN_DELAY;

/**
 * Created by begemot on 01.10.17.
 */
@Deprecated
public class NotificationHelper {
    private static final String TAG = "NotificationHelper";

    public static void showNewOrderNotification(String messageBody, Order order, Date orderCreateTime,
                                                NotificationManager notificationManager, Context context) {
/*        if (!isBackgroundRunning(context)) {
            Log.d(TAG, "showNewOrderNotification: application on foreground");
        }*/

        Log.d(TAG, "showNotification: " + messageBody);
        String channelId = "manfred_channel_01";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChanel(channelId, notificationManager, "Manfred order", "Заказы manfred");
        }

        RemoteViews mRemoteViews = new RemoteViews(context.getPackageName(), R.layout.custom_notification_small);
        mRemoteViews.setTextViewText(R.id.notif_content, messageBody);

        Intent notifyIntent = new Intent(context, NewOrderActivity.class);
        // Set the Activity to start in a new, empty task
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        // Create the PendingIntent
        PendingIntent notifyPendingIntent = PendingIntent.getActivity(
                context, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT
        );

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContent(mRemoteViews)
                .setAutoCancel(true)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC) //to show content on lock screen
                .setContentIntent(notifyPendingIntent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder
                    .setPriority(NotificationManager.IMPORTANCE_HIGH);
        }

        if (notificationManager != null) {
            Log.d(TAG, "showNotification: notifing...");
            notificationManager.notify(1, notificationBuilder.build());
        }

        //flash screen
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        flashScreen(pm);
        //Log.d(TAG, "showNewOrderNotification: creating notification");
        removeNotification(1, ORDER_ON_SCREEN_DELAY - (TimeDateUtils.TimeDiffInMSec(orderCreateTime)), notificationManager);

    }

    private static void flashScreen(PowerManager pm) {
        if (pm != null) {
            boolean isScreenOn = pm.isInteractive();
            Log.e(TAG, "isScreenOn? " + isScreenOn);
            if (!isScreenOn) {
                PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP
                        | PowerManager.ON_AFTER_RELEASE, "manfred::MyLock");
                wl.acquire(10000);

                PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "manfred::MyLock");
                wl_cpu.acquire(10000);
            }
        }
    }

    private static void showNotification(String messageBody, String title, Context context, int id, PendingIntent resultPendingIntent) {
        Log.d(TAG, "showNotification: " + messageBody);
        String channelId = "manfred_channel_02";
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChanel(channelId, notificationManager, "Manfred events", "События manfred");
        }

        RemoteViews mRemoteViews;

        if(title.isEmpty()){
            mRemoteViews = new RemoteViews(context.getPackageName(), R.layout.notification_without_title);
            mRemoteViews.setTextViewText(R.id.notif_content, messageBody);
        }else {
            mRemoteViews = new RemoteViews(context.getPackageName(), R.layout.custom_notification_small);
            mRemoteViews.setTextViewText(R.id.notif_content, messageBody);
            mRemoteViews.setTextViewText(R.id.notif_title, title);
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContent(mRemoteViews)
                .setAutoCancel(true)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC) //to show content on lock screen
                .setContentIntent(resultPendingIntent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder
                    .setPriority(NotificationManager.IMPORTANCE_HIGH);
        }

        if (notificationManager != null) {
            Log.d(TAG, "showNotification: notifing...");
            notificationManager.notify(id, notificationBuilder.build());
        }

        //flash screen
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        flashScreen(pm);
    }

    @RequiresApi(26)
    private static void createNotificationChanel(String channelId, NotificationManager notificationManager, String s, String s2) {
        NotificationChannel notificationChannel = new NotificationChannel(channelId, s,
                NotificationManager.IMPORTANCE_HIGH);
        // Configure the notification channel.
        notificationChannel.setDescription(s2);
        notificationChannel.enableLights(true);
        notificationChannel.setLightColor(Color.RED);
        notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
        notificationChannel.setImportance(NotificationManager.IMPORTANCE_HIGH);
        notificationChannel.enableVibration(true);
        notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }

    public static void showPreOrderSoonNotification(String messageBody, Context context) {
        if (!isBackgroundRunning(context)) {
            Log.d(TAG, "showPreOrderSoonNotification: application on foreground, not showing");
        } else {
            Intent resultIntent = new Intent(context, SplashActivity.class);
            showNotif("Подтвердите предзаказ", messageBody, context, 2, resultIntent);
        }
    }

    public static void showNotificationIfNotOnScreen(String title, String message, Context context, int id) {
        if (!isBackgroundRunning(context)) {
            Log.d(TAG, "showNotificationIfNotOnScreen: application on foreground, not showing");
        } else {
            Log.d(TAG, "showNotificationIfNotOnScreen: " + message);
            Intent resultIntent = new Intent(context, SplashActivity.class);
            showNotif(title, message, context, id, resultIntent);
        }
    }

    public static void showNewPreOrderNotif(long preOrderId, Context context) {
        if (!isBackgroundRunning(context)) {
            Log.d(TAG, "showNotificationIfNotOnScreen: application on foreground, not showing");
        } else {
            Log.d(TAG, "showNotificationIfNotOnScreen: " + preOrderId);
            Intent resultIntent = new Intent(context, PreordersActivity.class);
            showNotif("", "Новый предзаказ " + preOrderId, context, 300, resultIntent);
        }
    }

    private static void showNotif(String title, String message, Context context, int id, Intent resultIntent) {
        resultIntent.setAction("android.intent.action.MAIN");
        resultIntent.addCategory("android.intent.category.LAUNCHER");
        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        showNotification(message, title, context, id, resultPendingIntent);
    }


    private static void removeNotification(final int id, long delayInMilliseconds, final NotificationManager notificationManager) {
        Log.d(TAG, "removeNotification: notif be destryed at " + delayInMilliseconds);
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            public void run() {
                notificationManager.cancel(id);
                //Log.d(TAG, "run: cancel notification");
            }
        }, delayInMilliseconds);
    }

    public static void removeAllNotificationImmediately(final NotificationManager notificationManager) {
        Log.d(TAG, "removeAllNotificationImmediately:");
        notificationManager.cancelAll();
    }

    private static boolean isBackgroundRunning(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningProcesses = null;
        if (am != null) {
            runningProcesses = am.getRunningAppProcesses();
            if (runningProcesses==null)return false;
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            //If your app is the process in foreground, then it's not in running in background
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }
}
