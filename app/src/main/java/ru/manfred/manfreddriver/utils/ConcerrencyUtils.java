/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils;

import android.util.Log;

public class ConcerrencyUtils {
    private static final String TAG = "ConcerrencyUtils";
    public static int recommendedThreadCount()
    {

        int mRtnValue; //= 2 * Runtime.getRuntime().availableProcessors() + 1;
        if(Runtime.getRuntime().availableProcessors()==1){
            mRtnValue=1;
        }else {
            mRtnValue=Runtime.getRuntime().availableProcessors() - 1;
        }
        Log.d(TAG, "recommendedThreadCount: "+mRtnValue);
        return mRtnValue;
/*        int mRtnValue = 0;
        Runtime runtime = Runtime.getRuntime();
        long maxMemory = runtime.maxMemory();
        long mTotalMemory = runtime.totalMemory();
        long freeMemory = runtime.freeMemory();
        int mAvailableProcessors = runtime.availableProcessors();
        Log.d(TAG, "mAvailableProcessors: "+mAvailableProcessors);

        long mTotalFreeMemory = freeMemory + (maxMemory - mTotalMemory);
        Log.d(TAG, "mTotalFreeMemory: "+mTotalFreeMemory);
        mRtnValue = (int)(mTotalFreeMemory/4200000000l);
        Log.d(TAG, "mRtnValue: "+mRtnValue);

        int mNoOfThreads = mAvailableProcessors-1;
        Log.d(TAG, "recommendedThreadCount: "+mNoOfThreads);
        if(mNoOfThreads < mRtnValue) mRtnValue = mNoOfThreads;
        Log.d(TAG, "recommendedThreadCount: size is "+mRtnValue);
        return mRtnValue;*/

    }
}
