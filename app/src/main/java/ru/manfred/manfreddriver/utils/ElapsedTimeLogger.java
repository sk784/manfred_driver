/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils;

import java.util.concurrent.TimeUnit;

import ru.manfred.manfreddriver.managers.LoggingManager;
import ru.manfred.manfreddriver.managers.logging.LogTags;

public class ElapsedTimeLogger {
    private final LoggingManager loggingManager;
    private final long startTime;
    private final String nameOfResponce;

    public ElapsedTimeLogger(LoggingManager loggingManager, long startTime, String nameOfResponse) {
        this.loggingManager = loggingManager;
        this.startTime = startTime;
        this.nameOfResponce = nameOfResponse;
    }

    public void finish(){
        long timeElapsed = System.currentTimeMillis() - startTime;
        float timeElapsedInSec = timeElapsed / 1000.0f;
        String timeElapsedInSecFormatted = String.format("%.3f", timeElapsedInSec);
        loggingManager.addLog("NetworkRequest", nameOfResponce, "request completed in "+timeElapsedInSecFormatted+"s", LogTags.REQUEST_TIME);
    }
}
