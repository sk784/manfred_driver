/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.interactor.repository.UserRepository;
import ru.manfred.manfreddriver.model.api.Credentials;

public class FCMUtils {
    private static final String TAG = "FCMUtils";

    public static void invalidateAndGetNewFCMToken(Context context) {
        Log.d(TAG, "invalidateFCMToken: ");
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                {
                    try {
                        FirebaseInstanceId.getInstance().deleteInstanceId();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                getAndSendNewToken(context);
            }
        }.execute();
    }

    public static void invalidateFCMToken() {
        Log.d(TAG, "invalidateFCMToken: ");
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                {
                    try {
                        FirebaseInstanceId.getInstance().deleteInstanceId();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                Log.d(TAG, "onPostExecute: invalidate succefully");
            }
        }.execute();
    }


    private static void getAndSendNewToken(Context context) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                String token = FirebaseInstanceId.getInstance().getToken();
                while (token == null)//this is used to get firebase token until its null so it will save you from null pointer exeption
                {
                    token = FirebaseInstanceId.getInstance().getToken();
                    if (token != null) {
                        sendNewPushToken(token, context);
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {

            }
        }.execute();
    }

    private static void sendNewPushToken(String token, Context context) {
        Credentials credentials = ((ManfredApplication)context.getApplicationContext()).getCredentialsManager().getCredentials().getValue();
        if(credentials==null)return;
        UserRepository userRepository = ((ManfredApplication)context.getApplicationContext()).getRepoProvider().getUserRepository(token);
        //Log.d(TAG, "doInBackground: new token is " + token);
        userRepository.setNewFCMToken(token, new UserRepository.AnswerCallback() {
            @Override
            public void success() {
                Log.d(TAG, "success: new push token set on server");
            }

            @Override
            public void notSuccess(String error) {

            }

            @Override
            public void authorisationRequired(String error) {

            }

            @Override
            public void serverNotAvailable() {

            }
        });
    }
}
