/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils;

import android.support.v7.util.DiffUtil;

import java.util.List;

import ru.manfred.manfreddriver.model.api.CountryModel;

public class CountryDiffutilCallback extends DiffUtil.Callback {
    private List<CountryModel> oldCountries;
    private List<CountryModel> newCountries;

    public CountryDiffutilCallback(List<CountryModel> oldCountries, List<CountryModel> newCountries) {
        this.oldCountries = oldCountries;
        this.newCountries = newCountries;
    }

    @Override
    public int getOldListSize() {
        return oldCountries.size();
    }

    @Override
    public int getNewListSize() {
        return newCountries.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        CountryModel oldCountry = oldCountries.get(oldItemPosition);
        CountryModel newCountry = newCountries.get(newItemPosition);
        return oldCountry.getCountryCode().equals(newCountry.getCountryCode());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        CountryModel oldCountry = oldCountries.get(oldItemPosition);
        CountryModel newCountry = newCountries.get(newItemPosition);
        return oldCountry.getCountryCode().equals(newCountry.getCountryCode());
    }
}
