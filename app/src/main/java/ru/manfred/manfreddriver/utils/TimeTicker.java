package ru.manfred.manfreddriver.utils;

import android.arch.lifecycle.GenericLifecycleObserver;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.NonNull;

import java.util.concurrent.TimeUnit;

import static android.arch.lifecycle.Lifecycle.State.DESTROYED;
import static android.arch.lifecycle.Lifecycle.State.STARTED;

public class TimeTicker {
    interface TickCallback{
        /**
         * @param time time from initial value
         */
        void ticket(int time);
    }
    private final TickCallback tickCallback;
    private final TimeUnit timeUnit;
    private final LifecycleOwner owner;

    public TimeTicker(@NonNull LifecycleOwner owner, TickCallback tickCallback, TimeUnit timeUnit) {
        this.tickCallback = tickCallback;
        this.timeUnit = timeUnit;
        this.owner = owner;

    }
}
