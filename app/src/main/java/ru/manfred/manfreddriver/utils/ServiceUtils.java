/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import ru.manfred.manfreddriver.services.CheckEventService;

import static android.content.Context.ACTIVITY_SERVICE;

public class ServiceUtils {
    private static final String TAG = "ServiceUtils";
    public static void startService(Context appContext, Class<?> service){
        Log.d(TAG, "startService: ");
/*        Intent intent = new Intent(appContext,CheckEventService.class);
        appContext.startService(intent);*/
        if(!isBackgroundServiceRunning(appContext, service)){
            Intent intent = new Intent(appContext,CheckEventService.class);
            appContext.startService(intent);
        }
    }

    private static boolean isBackgroundServiceRunning(Context appContext, Class<?> service){
        ActivityManager manager = (ActivityManager)(appContext.getSystemService(ACTIVITY_SERVICE));
        if (manager != null)
        {
            for(ActivityManager.RunningServiceInfo info : manager.getRunningServices(Integer.MAX_VALUE))
            {
                if(service.getName().equals(info.service.getClassName()))
                    return true;
            }
        }
        return false;
    }
}
