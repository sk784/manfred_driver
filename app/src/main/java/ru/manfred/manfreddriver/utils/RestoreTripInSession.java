/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.utils;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.activities.MapsActivity;
import ru.manfred.manfreddriver.interactor.repository.DriverRepository;
import ru.manfred.manfreddriver.interactor.repository.ResponseCallback;
import ru.manfred.manfreddriver.model.api.ManfredError;
import ru.manfred.manfreddriver.model.responces.DriverStatus;

/**
 * Created by begemot on 26.11.17.
 * restore trip from net when we have null order in screen
 */

public class RestoreTripInSession {
    private static final String TAG = "RestoreTripInSession";
    public void restoreTrip(Activity context) {
        Toast.makeText(context, "Обнаружены проблемы, выполняем дополнительный запрос на сервер...", Toast.LENGTH_LONG).show();
        DriverRepository driverRepository =
                ((ManfredApplication)context.getApplication()).getRepoProvider().getDriverRepository(SharedPreferencesManager.getCredentials(context).getToken());
        Log.d(TAG, "restoreTrip: make response for restore state");
        driverRepository.getStatus(new ResponseCallback<DriverStatus>() {
            @Override
            public void allOk(DriverStatus response) {
                RestoreTrip restoreTrip = new RestoreTrip();
                //Order order = driverStatus.getOrder();
                startActivityIfNeed(restoreTrip, response, context);
            }

            @Override
            public void error(ManfredError error) {
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, "Ошибка при получении текущего заказа с сервера", Toast.LENGTH_LONG).show();
                    }
                });
                Intent intent = new Intent(context, MapsActivity.class);
                context.startActivity(intent);
            }
        });
    }

    public void startActivityIfNeed(RestoreTrip restoreTrip, DriverStatus driverStatus, Activity context) {
        Intent intent = restoreTrip.restoreTrip(driverStatus.getOrder(), context);
        if (intent != null) {
            Toast.makeText(context, "Статус восстановлен с сервера", Toast.LENGTH_LONG).show();
            context.startActivity(intent);
        } else {
            Toast.makeText(context, "Активный заказ не найден", Toast.LENGTH_LONG).show();
            intent = new Intent(context, MapsActivity.class);
            context.startActivity(intent);
        }
    }
}
