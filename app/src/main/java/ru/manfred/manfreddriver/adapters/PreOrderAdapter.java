/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ru.manfred.manfreddriver.databinding.MyPreorderHolderBinding;
import ru.manfred.manfreddriver.databinding.PreOrderDateHolderBinding;
import ru.manfred.manfreddriver.databinding.PreorderHolderBinding;
import ru.manfred.manfreddriver.viewmodel.DateItemVM;
import ru.manfred.manfreddriver.viewmodel.ListItem;
import ru.manfred.manfreddriver.viewmodel.PreOrderHolderVM;

import static ru.manfred.manfreddriver.viewmodel.ListItem.TYPE_DATE;
import static ru.manfred.manfreddriver.viewmodel.ListItem.TYPE_GENERAL;

/**
 * Created by begemot on 18.12.17.
 */

public class PreOrderAdapter extends RecyclerView.Adapter<PreOrderAdapter.ViewHolder> {
    private static final String TAG = "PreOrderAdapter";
    private List<ListItem> preOrderVMS;
    private final boolean isMy;

    public PreOrderAdapter(List<ListItem> preOrderVMS, boolean isMy) {
        this.preOrderVMS = preOrderVMS;
        this.isMy = isMy;
    }

    public void setItemsS(List<ListItem> preOrderVMS) {
        this.preOrderVMS = preOrderVMS;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = null;
        switch (viewType){
            case TYPE_GENERAL:
                if(isMy){
                    MyPreorderHolderBinding binding = MyPreorderHolderBinding.inflate(layoutInflater,parent,false);
                    return new MyPreOrderHolder(binding);
                }else {
                    PreorderHolderBinding binding = PreorderHolderBinding.inflate(layoutInflater,parent,false);
                    return new PreOrderHolder(binding);
                }
            case TYPE_DATE:
                PreOrderDateHolderBinding binding = PreOrderDateHolderBinding.inflate(layoutInflater,parent,false);
                return new DateHolder(binding);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.d(TAG, "bind, position = " + position);
        ListItem item = preOrderVMS.get(position);
        holder.bind(item);
    }

    @Override
    public int getItemViewType(int position) {
        return preOrderVMS.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return preOrderVMS.size();
    }

    class DateHolder extends ViewHolder {
        private final PreOrderDateHolderBinding binding;
        public DateHolder(PreOrderDateHolderBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }

        public void bind(ListItem listItem){
            binding.setPreOrder((DateItemVM)listItem);
            binding.executePendingBindings();
        }
    }

    class MyPreOrderHolder extends ViewHolder {
        private final MyPreorderHolderBinding binding;
        public MyPreOrderHolder(MyPreorderHolderBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }

        public void bind(ListItem listItem){
            binding.setPreOrder((PreOrderHolderVM)listItem);
            binding.executePendingBindings();
        }
    }

    class PreOrderHolder extends ViewHolder {
        private final PreorderHolderBinding binding;
        public PreOrderHolder(PreorderHolderBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }

        public void bind(ListItem listItem){
            binding.setPreOrder((PreOrderHolderVM)listItem);
            binding.executePendingBindings();
        }
    }

    public abstract class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        public abstract void bind(ListItem item);
    }
}
