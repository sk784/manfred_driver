/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ru.manfred.manfreddriver.databinding.HistoryDetailsHolderBinding;
import ru.manfred.manfreddriver.viewmodel.OrderHistoryViewModel;

/**
 * Created by begemot on 13.12.17.
 */

public class HistoryDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<OrderHistoryViewModel> history;

    public HistoryDetailsAdapter(List<OrderHistoryViewModel> history) {
        this.history = history;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        HistoryDetailsHolderBinding historyDetailsHolderBinding = HistoryDetailsHolderBinding.inflate(inflater, parent, false);
        return new HistoryDetailHolder(historyDetailsHolderBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HistoryDetailHolder) {
            final HistoryDetailsHolderBinding binding = ((HistoryDetailHolder) holder).getBinding();
            final OrderHistoryViewModel historyItem = history.get(position);
            binding.setHistoryItem(historyItem);
            binding.executePendingBindings();
        }
    }

    @Override
    public int getItemCount() {
        return history.size();
    }

    private class HistoryDetailHolder extends RecyclerView.ViewHolder {
        private HistoryDetailsHolderBinding binding;

        HistoryDetailHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }

        public HistoryDetailsHolderBinding getBinding() {
            return binding;
        }
    }
}
