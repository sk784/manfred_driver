/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.adapters;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ru.manfred.manfreddriver.databinding.DateHolderBinding;
import ru.manfred.manfreddriver.databinding.HistoryOrderHolderBinding;
import ru.manfred.manfreddriver.viewmodel.DateItemVM;
import ru.manfred.manfreddriver.viewmodel.ListItem;
import ru.manfred.manfreddriver.viewmodel.OrderHistoryViewModel;

/**
 * Created by begemot on 15.08.17.
 */

public class HistoryTripAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ListItem> historyModels = new ArrayList<>();
    private static final String TAG = "HistoryTripAdapter";
    //private final int ORDER_ITEM = 0;
    //private final int DATE_ITEM = 1;
    private final Callback callback;

    public interface Callback {
        void itemClicked(OrderHistoryViewModel item);
    }

    public HistoryTripAdapter(List<ListItem> orderHistoryViewModels, Callback callback) {
        this.historyModels = orderHistoryViewModels;
        this.callback = callback;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case ListItem.TYPE_GENERAL:
                HistoryOrderHolderBinding historyOrderHolderBinding = HistoryOrderHolderBinding.inflate(inflater, parent, false);
                viewHolder = new HistoryHolder(historyOrderHolderBinding.getRoot());
                break;
            case ListItem.TYPE_DATE:
                DateHolderBinding dateHolderBinding = DateHolderBinding.inflate(inflater, parent, false);
                viewHolder = new DateHolder(dateHolderBinding.getRoot());
                break;
        }

        return viewHolder;
        /*if(viewType==ORDER_ITEM) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            HistoryOrderHolderBinding binding = HistoryOrderHolderBinding.inflate(inflater, parent, false);
            return new HistoryHolder(binding.getRoot());
        }else {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            DateHolderBinding binding = DateHolderBinding.inflate(inflater, parent, false);
            return new DateHolder(binding.getRoot());
        }
        */
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case ListItem.TYPE_GENERAL:
                OrderHistoryViewModel orderHistoryViewModel = (OrderHistoryViewModel) historyModels.get(position);
                ((HistoryHolder) holder).getBinding().setOrder(orderHistoryViewModel);
                ((HistoryHolder) holder).getBinding().getRoot().setOnClickListener(view -> callback.itemClicked(orderHistoryViewModel));
                ((HistoryHolder) holder).getBinding().executePendingBindings();
                break;
            case ListItem.TYPE_DATE:
                DateItemVM date = (DateItemVM) historyModels.get(position);
                ((DateHolder) holder).getBinding().setDate(date);
                Log.d(TAG, "onBindViewHolder: date is " + date.getDate());
                ((DateHolder) holder).getBinding().executePendingBindings();
                break;
        }
        /*if(holder instanceof HistoryHolder){
            OrderHistoryViewModel orderHistoryViewModel = (OrderHistoryViewModel) historyModels.get(position);
            ((HistoryHolder)holder).getBinding().setOrder(orderHistoryViewModel);
            ((HistoryHolder) holder).getBinding().historyShowDetails.setOnClickListener(view -> {
                callback.itemClicked(orderHistoryViewModel);
            });
            ((HistoryHolder)holder).getBinding().executePendingBindings();
        }else {
            String date = (String) historyModels.get(position);
            ((DateHolder)holder).getBinding().setDate(date);
            ((DateHolder)holder).getBinding().executePendingBindings();

        }*/
    }

    @Override
    public int getItemCount() {
        return historyModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        return historyModels.get(position).getType();
    }

    private class HistoryHolder extends RecyclerView.ViewHolder {
        private HistoryOrderHolderBinding historyOrderHolderBinding;

        public HistoryHolder(View itemView) {
            super(itemView);
            historyOrderHolderBinding = DataBindingUtil.bind(itemView);
        }

        public HistoryOrderHolderBinding getBinding() {
            return historyOrderHolderBinding;
        }
    }

    private class DateHolder extends RecyclerView.ViewHolder {
        private DateHolderBinding dateHolderBinding;

        public DateHolder(View itemView) {
            super(itemView);
            dateHolderBinding = DataBindingUtil.bind(itemView);
        }

        public DateHolderBinding getBinding() {
            return dateHolderBinding;
        }
    }
}
