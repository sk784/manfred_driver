/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ru.manfred.manfreddriver.databinding.AirportFirstHolderBinding;
import ru.manfred.manfreddriver.databinding.AirportHolderBinding;
import ru.manfred.manfreddriver.model.api.Airport;

/**
 * Created by begemot on 29.09.17.
 */

public abstract class AirportAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Object> airports = new ArrayList<>();
    private final int CAPTION_HOLDER = 0;
    private final int AIRPORT_HOLDER = 1;

    public abstract void airportSelected(Airport airport);

    public AirportAdapter(List<Object> airports) {
        this.airports = airports;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case CAPTION_HOLDER:
                AirportFirstHolderBinding airportFirstHolderBinding = AirportFirstHolderBinding.inflate(inflater, parent, false);
                return new CaptionHolder(airportFirstHolderBinding.getRoot());
            case AIRPORT_HOLDER:
                AirportHolderBinding airportHolderBinding = AirportHolderBinding.inflate(inflater, parent, false);
                return new AirportHolder(airportHolderBinding.getRoot());
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof AirportHolder) {
            final Airport airport = (Airport) airports.get(position);
            ((AirportHolder) holder).getBinding().setAirport(airport);
            ((AirportHolder) holder).getBinding().cvAirport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    airportSelected(airport);
                }
            });
            ((AirportHolder) holder).getBinding().executePendingBindings();
        }
    }

    @Override
    public int getItemViewType(int position) {
        return airports.get(position) instanceof Airport ? AIRPORT_HOLDER : CAPTION_HOLDER;
    }

    @Override
    public int getItemCount() {
        return airports.size();
    }

    private class CaptionHolder extends RecyclerView.ViewHolder {
        private AirportFirstHolderBinding binding;

        public CaptionHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }

        public AirportFirstHolderBinding getBinding() {
            return binding;
        }
    }

    private class AirportHolder extends RecyclerView.ViewHolder {
        private AirportHolderBinding binding;

        public AirportHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }

        public AirportHolderBinding getBinding() {
            return binding;
        }
    }
}
