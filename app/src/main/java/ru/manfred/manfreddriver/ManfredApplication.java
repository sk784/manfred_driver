/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver;

import android.app.Application;
import android.app.usage.NetworkStats;
import android.app.usage.NetworkStatsManager;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.arch.lifecycle.ProcessLifecycleOwner;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.RemoteException;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.squareup.leakcanary.LeakCanary;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import io.intercom.android.sdk.Intercom;
import ru.manfred.manfreddriver.db.AppDatabase;
import ru.manfred.manfreddriver.di.component.DaggerHistoryComponent;
import ru.manfred.manfreddriver.di.component.DaggerTripComponent;
import ru.manfred.manfreddriver.di.component.HistoryComponent;
import ru.manfred.manfreddriver.di.component.TripComponent;
import ru.manfred.manfreddriver.di.modules.AppModule;
import ru.manfred.manfreddriver.di.modules.BusyModule;
import ru.manfred.manfreddriver.interactor.repository.DBRepository;
import ru.manfred.manfreddriver.interactor.repository.DriverRepository;
import ru.manfred.manfreddriver.managers.EventServiceManager;
import ru.manfred.manfreddriver.managers.ManfredCredentialsManager;
import ru.manfred.manfreddriver.managers.NewManfredLoginManger;
import ru.manfred.manfreddriver.managers.network.ManfredNetworkManager;
import ru.manfred.manfreddriver.services.CheckEventService;
import ru.manfred.manfreddriver.utils.NetworkChangeReceiver;
import ru.manfred.manfreddriver.utils.NetworkStatsHelper;
import ru.manfred.manfreddriver.utils.RepositoryProvider;
import ru.manfred.manfreddriver.utils.ServiceUtils;
import ru.manfred.manfreddriver.utils.SharedPreferencesManager;

/**
 * Created by begemot on 12.10.17.
 * class for singletons =]
 */

public class ManfredApplication extends Application implements LifecycleObserver {
    private static final String TAG = "ManfredApplication";
    private static TripComponent tripComponent;
    private static HistoryComponent historyComponent;
    private static boolean needUpdate = false;
    private static boolean firstStartAfterLogin = false;
    private static boolean foreground;
    private static ManfredNetworkManager manfredNetworkManager;
    private AppDatabase database;
    private DBRepository dbRepository;
    private ManfredCredentialsManager credentialsManager;
    private NewManfredLoginManger manfredLoginManger;
    //private static ManfredApplication instance;

    public static TripComponent getTripComponent() {
        return tripComponent;
    }

    public static HistoryComponent getHistoryComponent() {
        return historyComponent;
    }

    public static ManfredApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        fix();
        instance = this;
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        if(foreground) {
            ServiceUtils.startService(getApplicationContext(), CheckEventService.class);
        }
        if(BuildConfig.DEBUG)LeakCanary.install(this);
        tripComponent = buildTripComponent();
        historyComponent = buildHistoryComponent();
        //getNetworkManger();
        Intercom.initialize(this, "android_sdk-54fea269e641ae1e1331049336f8ba3eb518cf9a", "joe69oki");
        NetworkChangeReceiver networkChangeReceiver = new NetworkChangeReceiver();
        IntentFilter intentFilterNetw = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        this.registerReceiver(networkChangeReceiver, intentFilterNetw);
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
        database = Room.databaseBuilder(this, AppDatabase.class, "database")
                .build();
        dbRepository= new DBRepository(getApplicationContext());
        checkForUpdate();

        if(BuildConfig.DEBUG) {
            scheduleTrafficCounter();
        }
    }

    private void scheduleTrafficCounter() {
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        Date startDate = new Date();
        Log.d(TAG, "scheduleTrafficCounter: from "+startDate);
        long startTime = System.currentTimeMillis();
        Runnable trafficCheckTask = new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    Log.d(TAG, "run: calculateTraffic()");
                    calculateTraffic(startTime);
                }
            }
        };
        scheduler.scheduleAtFixedRate(trafficCheckTask, 0, 1, TimeUnit.MINUTES);
    }


    public NewManfredLoginManger getManfredLoginManger() {
        if(manfredLoginManger==null){
            manfredLoginManger = new NewManfredLoginManger(getRepoProvider());
        }
        return manfredLoginManger;
    }
    public void freeLoginManager(){
        manfredLoginManger=null;
    }

    //private NetworkManagerProvider networkManagerProvider;
    //public static boolean needLegacySocketImpl=false;
    public static ManfredNetworkManager getNetworkManger(){
        if(manfredNetworkManager==null) {
            manfredNetworkManager = new ManfredNetworkManager();
        }
        return manfredNetworkManager;
    }

    /*
    try to prevent crash
    Fatal Exception: java.util.concurrent.TimeoutException
    com.android.internal.os.BinderInternal$GcWatcher.finalize() timed out after 10 seconds
    java.lang.Daemons$Daemon.isRunning (Daemons.java:78)
    https://stackoverflow.com/questions/24021609/how-to-handle-java-util-concurrent-timeoutexception-android-os-binderproxy-fin
     */
    public static void fix() {
        try {
            Class clazz = Class.forName("java.lang.Daemons$FinalizerWatchdogDaemon");

            Method method = clazz.getSuperclass().getDeclaredMethod("stop");
            method.setAccessible(true);

            Field field = clazz.getDeclaredField("INSTANCE");
            field.setAccessible(true);

            method.invoke(field.get(null));

        }
        catch (Throwable e) {
            e.printStackTrace();
        }
    }

    long initSize=0;
    @RequiresApi(23)
    void calculateTraffic(long startTime){
        NetworkStatsManager networkStatsManager = (NetworkStatsManager) getApplicationContext()
                .getSystemService(Context.NETWORK_STATS_SERVICE);
        PackageManager packageManager = getPackageManager();
        try {
            int applicationId = packageManager.getApplicationInfo(getApplicationContext().getPackageName(),
                    PackageManager.GET_META_DATA).uid;
            NetworkStatsHelper statsHelper = new NetworkStatsHelper(networkStatsManager,applicationId);
            long bytes = statsHelper.getPackageTxBytesMobile(getApplicationContext(), 0);
            if(initSize!=0){
                bytes=bytes-initSize;
            }
            bytes = bytes+statsHelper.getPackageRxBytesMobile(getApplicationContext(), 0);
            NumberFormat formatter = new DecimalFormat("#0.00");

            double mbytes = (int)((bytes/1024.0)/1024.0);
            String strMB = formatter.format(mbytes);
            double kbytes = (int)bytes/1024.0;
            String strKB = formatter.format(kbytes);
            if(initSize==0){
                initSize=bytes;
                Log.d(TAG, "calculateTraffic: initialisation...");
            }else {
                long timeFromStart = System.currentTimeMillis() - startTime;
                Log.d(TAG, "calculateTraffic: " + bytes + " b, " + strKB +
                        " KB, (" + strMB + " MB), " + (timeFromStart/1000)/60+"m from start");
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

    protected TripComponent buildTripComponent() {
        Log.d(TAG, "buildTripComponent: ");
        return DaggerTripComponent.builder()
                .appModule(new AppModule(this))
                .busyModule(new BusyModule())
                .build();
    }

    protected HistoryComponent buildHistoryComponent() {
        return DaggerHistoryComponent.builder()
                .build();
    }

    public static boolean isNeedUpdate() {
        return needUpdate;
    }

    RepositoryProvider repositoryProvider;
    public RepositoryProvider getRepoProvider(){
        if(repositoryProvider==null){
            repositoryProvider = new RepositoryProvider(false,null);
        }
        return repositoryProvider;
    }

    private void checkForUpdate(){
        DriverRepository driverRepository = getRepoProvider().getDriverRepository("");
        driverRepository.checkForUpdate(new DriverRepository.NetAnswerCallback<Boolean>() {
            @Override
            public void allFine(Boolean answer) {
                if(answer){
                    needUpdate=true;
                }
            }

            @Override
            public void error(String error) {

            }

            @Override
            public void serverNotAvailable() {

            }
        });
    }

    public static boolean isFirstStartAfterLogin() {
        Log.d(TAG, "isFirstStartAfterLogin: " + firstStartAfterLogin);
        return firstStartAfterLogin;
    }

    public static void setFirstStartAfterLogin(boolean firstStartAfterLogin) {
        Log.d(TAG, "setFirstStartAfterLogin: " + firstStartAfterLogin);
        ManfredApplication.firstStartAfterLogin = firstStartAfterLogin;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    void onAppBackgrounded() {
        Log.d(TAG, "App in background");

        getServiceManager().goToForeground();
        foreground = false;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    void onAppForegrounded() {
        Log.d(TAG, "App in foreground");
        getServiceManager().goToForeground();
        foreground = true;
    }


    /*
    public static ManfredApplication getInstance() {
        return instance;
    }*/

    public AppDatabase getDatabase() {
        return database;
    }

    public DBRepository getDbRepository() {
        return dbRepository;
    }

    public ManfredCredentialsManager getCredentialsManager(){
        if(credentialsManager==null){
            credentialsManager = new ManfredCredentialsManager(getRepoProvider());
            credentialsManager.setCredentials(SharedPreferencesManager.getCredentials(getApplicationContext()));
        }
        return credentialsManager;
    }

    private EventServiceManager serviceManager;
    public EventServiceManager getServiceManager() {
        if(serviceManager==null){
            serviceManager = new EventServiceManager();
        }
        return serviceManager;
    }

    public static boolean isForeground() {
        return foreground;
    }
}
