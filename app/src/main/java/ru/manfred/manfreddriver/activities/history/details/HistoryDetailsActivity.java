/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.activities.history.details;

import android.arch.lifecycle.Observer;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.LongSparseArray;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.activities.NewOrderActivity;
import ru.manfred.manfreddriver.adapters.HistoryDetailsAdapter;
import ru.manfred.manfreddriver.databinding.ActivityHistoryDetailsBinding;
import ru.manfred.manfreddriver.managers.ManfredTripManager;
import ru.manfred.manfreddriver.managers.TripManager;
import ru.manfred.manfreddriver.managers.history.HistoryManager;
import ru.manfred.manfreddriver.managers.history.ManfredHistoryManager;
import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.utils.Constants;
import ru.manfred.manfreddriver.utils.NetworkUtil;
import ru.manfred.manfreddriver.utils.SharedPreferencesManager;
import ru.manfred.manfreddriver.utils.SnapHelperOneByOne;
import ru.manfred.manfreddriver.viewmodel.OrderHistoryViewModel;

import static ru.manfred.manfreddriver.utils.NetworkUtil.TYPE_NOT_CONNECTED;

public class HistoryDetailsActivity extends AppCompatActivity {
    private static final String TAG = "HistoryDetailsActivity";
    ArrayList<Order> allHistory = new ArrayList<>();
    private LocalBroadcastManager brodcastManager;
    private BroadcastReceiver baseReceiver;
    ActivityHistoryDetailsBinding binding;

    @Inject
    ManfredTripManager tripManager;
    //TripManager.TripManagerListener tripManagerListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: ");
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_history_details);
        ManfredApplication.getTripComponent().inject(this);

        //super.onCreateDrawer();
        Intent intent = getIntent();
        long scrollToId = intent.getLongExtra(Constants.HISTORY_IN_LIST_ID, 0L);
        HistoryManager.HistoryInterval historyInterval = (HistoryManager.HistoryInterval) intent.getSerializableExtra(Constants.HISTORY_INTERVAL_KEY);
        ManfredHistoryManager historyManager =
                new ManfredHistoryManager(((ManfredApplication)getApplication()).getRepoProvider()
                        .getDriverRepository(SharedPreferencesManager.getCredentials(getApplicationContext()).getToken()));
        if (historyInterval != null) {
            switch (historyInterval) {
                case TODAY:
                    historyManager.getTodayHistory(new HistoryManager.HistoryManagerCallback() {
                        @Override
                        public void success(List<Order> historyOrders) {
                            allHistory = new ArrayList<>(historyOrders);
                            initHistoryDetails(binding, scrollToId);
                        }

                        @Override
                        public void error(String message) {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void serverNotAvailable() {
                            binding.noInternetMessage.setVisibility(View.VISIBLE);
                        }
                    });
                    break;
                case THIS_WEEK:
                    historyManager.getWeekHistory(new HistoryManager.HistoryManagerCallback() {
                        @Override
                        public void success(List<Order> historyOrders) {
                            allHistory = new ArrayList<>(historyOrders);
                            initHistoryDetails(binding, scrollToId);
                        }

                        @Override
                        public void error(String message) {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void serverNotAvailable() {
                            binding.noInternetMessage.setVisibility(View.VISIBLE);
                        }
                    });
                    break;
                case THIS_MONTH:
                    historyManager.getMonthHistory(new HistoryManager.HistoryManagerCallback() {
                        @Override
                        public void success(List<Order> historyOrders) {
                            allHistory = new ArrayList<>(historyOrders);
                            initHistoryDetails(binding, scrollToId);
                        }

                        @Override
                        public void error(String message) {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void serverNotAvailable() {
                            binding.noInternetMessage.setVisibility(View.VISIBLE);
                        }
                    });
                    break;
                case CUSTOM_INTERVAL:
                    historyManager.getPeriodHistory((Date) intent.getSerializableExtra(Constants.HISTORY_START_DATE),
                            (Date) intent.getSerializableExtra(Constants.HISTORY_END_DATE),
                            new HistoryManager.HistoryManagerCallback() {
                                @Override
                                public void success(List<Order> historyOrders) {
                                    allHistory = new ArrayList<>(historyOrders);
                                    initHistoryDetails(binding, scrollToId);
                                }

                                @Override
                                public void error(String message) {
                                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void serverNotAvailable() {
                                    binding.noInternetMessage.setVisibility(View.VISIBLE);
                                }
                            });
                    break;
            }

        }

        tripManager.getCurrentState().observe(this, new Observer<TripManager.Status>() {
            @Override
            public void onChanged(@Nullable TripManager.Status status) {
                if(status!=null){
                    if (status.getStatusState() != null) {
                        switch (status.getStatusState()) {
                            case OFFER_TO_RIDE:
                                final Intent intentNewOrder = new Intent(HistoryDetailsActivity.this, NewOrderActivity.class);
                                startActivity(intentNewOrder);
                                break;
                        }
                    }
                }
            }
        });

/*        tripManagerListener = new TripManager.TripManagerListener() {
            @Override
            public void newStatus(TripManager.Status status) {
                switch (status.getStatusState()) {
                    case OFFER_TO_RIDE:
                        final Intent intentNewOrder = new Intent(HistoryDetailsActivity.this, NewOrderActivity.class);
                        startActivity(intentNewOrder);
                        break;
                }
            }
        };*/

    }


    private void initHistoryDetails(ActivityHistoryDetailsBinding binding, long scrollToId) {
        Log.d(TAG, "onCreate: scrolling to " + scrollToId + " history size is " + allHistory.size());
        List<OrderHistoryViewModel> historyViewModels = new ArrayList<>();
        //mapping Order to OrderHistoryViewModel
        for (Order order : allHistory) {
            /*if(order.getRide_start_time() != null) {
                historyViewModels.add(new OrderHistoryViewModel(order));
            }*/
            historyViewModels.add(new OrderHistoryViewModel(order));
        }
        Log.d(TAG, "onCreate: size of models " + historyViewModels.size());
        LongSparseArray<OrderHistoryViewModel> historyViewModelHashMap = new LongSparseArray<>();
        for (OrderHistoryViewModel orderHistoryViewModel : historyViewModels) {
            historyViewModelHashMap.put(orderHistoryViewModel.getOrder().getId(), orderHistoryViewModel);
            Log.d(TAG, "onCreate: adding to hashmap with id " + orderHistoryViewModel.getOrder().getId());
        }
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        HistoryDetailsAdapter historyDetailsAdapter = new HistoryDetailsAdapter(historyViewModels);
        SnapHelperOneByOne snapHelperOneByOne = new SnapHelperOneByOne();
        snapHelperOneByOne.attachToRecyclerView(binding.historyDetailsRecycler);
        binding.historyDetailsRecycler.setAdapter(historyDetailsAdapter);
        binding.historyDetailsRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int position = mLayoutManager.findLastVisibleItemPosition();
                Log.d(TAG, "onScrollStateChanged: scrolled to " + position);
                setTitle("Заказ " + historyViewModels.get(position).getOrder().getId());
            }
        });

        mLayoutManager.onSaveInstanceState();
        binding.historyDetailsRecycler.setLayoutManager(mLayoutManager);
        Log.d(TAG, "onCreate: scrolling to " + historyViewModels.indexOf(historyViewModelHashMap.get(scrollToId)));
        mLayoutManager.scrollToPosition(historyViewModels.indexOf(historyViewModelHashMap.get(scrollToId)));

        initToolbar("Заказ " + historyViewModelHashMap.get(scrollToId).getOrder().getId());
    }

    public void initToolbar(String title) {
        Toolbar myToolbar = findViewById(R.id.include);
        setSupportActionBar(myToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setCustomView(R.layout.custom_action_bar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        setTitle(title);
        myToolbar.setNavigationOnClickListener(view -> finish());
    }

    private void setTitle(String title) {
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(title);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //tripManager.addChangeStatusListener(tripManagerListener);
        brodcastManager = LocalBroadcastManager.getInstance(this);
        baseReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean haveNet = intent.getBooleanExtra(Constants.INTERNET_STATUS, true);
                Log.d(TAG, "onReceive: " + haveNet);
                if (haveNet) {
                    isAvailable();
                    //refreshCurrentTab();
                } else {
                    notAvailable();
                }
            }
        };
        IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        intentFilter.addAction(Constants.INTERNET_STATUS);
        brodcastManager.registerReceiver(baseReceiver, intentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //tripManager.removeChangeStatusListener(tripManagerListener);
        if (baseReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(baseReceiver);
            baseReceiver = null;
        }
    }

    public void isAvailable() {
        Log.d(TAG, "isAvailable: ");
        binding.noInternetMessage.setVisibility(View.GONE);
    }

    public void notAvailable() {
        Log.d(TAG, "internetOff: ");
        binding.noInternetMessage.setVisibility(View.VISIBLE);
        binding.noInternetMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.startAnimation(AnimationUtils.loadAnimation(HistoryDetailsActivity.this,
                        R.anim.slid_down));
                view.setVisibility(View.GONE);
                //refreshCurrentTab();
                checkInetStatus();
            }
        });
    }

    private void checkInetStatus() {
        int inetStatus = NetworkUtil.getConnectivityStatus(getApplicationContext());
        Log.d(TAG, "onClick: inet status is " + inetStatus);
        if (inetStatus == TYPE_NOT_CONNECTED) {
            notAvailable();
        }
    }

}
