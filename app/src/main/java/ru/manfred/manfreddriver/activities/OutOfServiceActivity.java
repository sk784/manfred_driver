package ru.manfred.manfreddriver.activities;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import io.intercom.android.sdk.Intercom;
import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.interactor.repository.DriverRepository;
import ru.manfred.manfreddriver.interactor.repository.ResponseCallback;
import ru.manfred.manfreddriver.model.api.ManfredError;
import ru.manfred.manfreddriver.model.responces.DriverStatus;
import ru.manfred.manfreddriver.services.CheckEventService;
import ru.manfred.manfreddriver.utils.RestoreTrip;
import ru.manfred.manfreddriver.utils.ServiceUtils;
import ru.manfred.manfreddriver.utils.SharedPreferencesManager;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;

public class OutOfServiceActivity extends AppCompatActivity {
    private static final String TAG = "OutOfServiceActivity";
    private final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
    @Nullable private ScheduledFuture scheduledFuture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_out_of_service);

        final Button button = findViewById(R.id.btn_out_of_service_support);
        button.setOnClickListener(v -> Intercom.client().displayMessenger());
    }

    void startCheckForAllNice(){
        scheduledFuture = scheduler.scheduleAtFixedRate(createChecker(),0, 30, TimeUnit.SECONDS);
    }

    void stopCheck(){
        if (scheduledFuture != null) {
            scheduledFuture.cancel(true);
        }
    }

    private Runnable createChecker(){
        final DriverRepository driverRepository =
                ((ManfredApplication)getApplication()).getRepoProvider().getDriverRepository(SharedPreferencesManager.getCredentials(getApplicationContext()).getToken());
        return new Runnable() {
            @Override
            public void run() {
                driverRepository.getStatus(new ResponseCallback<DriverStatus>() {
                    @Override
                    public void allOk(DriverStatus driverStatus) {
                        stopCheck();
                        if (driverStatus.getOrder() == null) {
                            Log.d(TAG, "getStatus: don't need to restore trip, just starting...");
                            startClear();
                        } else {
                            Log.d(TAG, "getStatus: restoring trip..." + driverStatus.getOrder().getId());
                            RestoreTrip restoreTrip = new RestoreTrip();
                            Intent intent = restoreTrip.restoreTrip(driverStatus.getOrder(), getApplicationContext());
                            if (intent != null) {
                                startCheckOrderService();
                                startActivity(intent);
                            } else {
                                intent = new Intent(getApplicationContext(), MapsActivity.class);
                                startActivity(intent);
                            }
                        }
                    }

                    @Override
                    public void error(ManfredError error) {

                    }
                });
            }
        };
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopCheck();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startCheckForAllNice();
    }

    private void startClear(){
        Log.d(TAG, "startClear: ");
        Intent intent = new Intent(this,MapsActivity.class);
        intent.addFlags(FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void startCheckOrderService() {
        ServiceUtils.startService(getApplicationContext(), CheckEventService.class);
    }
}
