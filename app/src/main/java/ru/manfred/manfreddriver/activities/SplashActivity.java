/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import javax.inject.Inject;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.activities.login.LoginActivity;
import ru.manfred.manfreddriver.interactor.repository.DriverRepository;
import ru.manfred.manfreddriver.interactor.repository.ResponseCallback;
import ru.manfred.manfreddriver.managers.StatusManager;
import ru.manfred.manfreddriver.managers.logging.LogTags;
import ru.manfred.manfreddriver.managers.logging.ManfredLoggingManager;
import ru.manfred.manfreddriver.model.api.Credentials;
import ru.manfred.manfreddriver.model.api.ManfredError;
import ru.manfred.manfreddriver.model.responces.DriverStatus;
import ru.manfred.manfreddriver.services.CheckEventService;
import ru.manfred.manfreddriver.utils.RestoreTrip;
import ru.manfred.manfreddriver.utils.ServiceUtils;
import ru.manfred.manfreddriver.utils.SharedPreferencesManager;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;

/**
 * Created by begemot on 30.11.17.
 */

public class SplashActivity extends AppCompatActivity {
    private static final String TAG = "SplashActivity";
    @Inject
    ManfredLoggingManager loggingManager;
    @Inject
    StatusManager statusManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!isTaskRoot()) { //don't start the app again from icon on launcher
            final Intent intent = getIntent();
            final String intentAction = intent.getAction();
            if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && intentAction != null && intentAction.equals(Intent.ACTION_MAIN)) {
                Log.d(TAG, "onCreate: from launcher");
                finish();
                return;
            }
        }

        ManfredApplication.getTripComponent().inject(this);
        //for start from foreground service notifications
        if (ManfredApplication.isForeground()) {
            Log.d(TAG, "onCreate: was foreground, kill splash");
            finish();
        }else {
            Log.d(TAG, "onCreate: starting new instance");
            startNewInstance();
        }
    }

    private void startNewInstance() {
        Credentials credentials = SharedPreferencesManager.getCredentials(getApplicationContext());
        if (credentials != null) {
            Crashlytics.setUserIdentifier(credentials.getPhoneNumber());
            ManfredApplication.setFirstStartAfterLogin(true);
            Log.d(TAG, "startNewInstance: make response for restore state");
            loggingManager.addLog("SplashActivity", "startNewInstance", "", LogTags.OTHER);
            DriverRepository driverRepository = ((ManfredApplication)getApplication()).getRepoProvider().getDriverRepository(credentials.getToken());
            driverRepository.getStatus(new ResponseCallback<DriverStatus>() {
                @Override
                public void allOk(DriverStatus driverStatus) {
                    if (driverStatus == null) {
                        startClear();
                        return;
                    }
                    Log.d(TAG, "getStatus: status is "+driverStatus.toString());
                    if(driverStatus.getFeatures().containsKey("websockets")){
                        boolean isSocket = false;
                        if(driverStatus.getFeatures()!=null){
                            isSocket = driverStatus.getFeatures().get("websockets");
                        }
                        //boolean isNewSocket = driverStatus.getFeatures().get("websocket2");
                        Log.d(TAG, "getStatus: it is socket? "+isSocket);
                        //Log.d(TAG, "new webSocket: ?"+isNewSocket);
                        if(isSocket){
                            ((ManfredApplication)getApplication()).getRepoProvider().setBySocket(ManfredApplication.getNetworkManger());
                        }else {
                            ((ManfredApplication)getApplication()).getRepoProvider().setByHTTP();
                        }
                    }
                    if(driverStatus.getDriver().is_free()){
                        statusManager.setFree();
                    }else {
                        statusManager.setBusy();
                    }
                    if (driverStatus.getOrder() == null) {
                        Log.d(TAG, "getStatus: don't need to restore trip, just starting...");
                        startClear();
                    } else {
                        Log.d(TAG, "getStatus: restoring trip..." + driverStatus.getOrder().getId());
                        RestoreTrip restoreTrip = new RestoreTrip();
                        Intent intent = restoreTrip.restoreTrip(driverStatus.getOrder(), getApplicationContext());
                        if (intent != null) {
                            startCheckOrderService();
                            startActivity(intent);
                        } else {
                            intent = new Intent(getApplicationContext(), MapsActivity.class);
                            startActivity(intent);
                        }
                    }
                }

                @Override
                public void error(ManfredError error) {
                    Log.d(TAG, "getStatus error: "+error);
                    if(error.getType()== ManfredError.ErrorStatus.SERVER_MAINTENANCE){
                        Intent intent = new Intent(SplashActivity.this, OutOfServiceActivity.class);
                        intent.addFlags(FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), error.getText(), Toast.LENGTH_LONG).show();
                                SharedPreferencesManager.removeCredentials(getApplicationContext());
                                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                                intent.addFlags(FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                        });
                    }
                }
            });

        } else {
            Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
            intent.addFlags(FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    private void startClear(){
        Log.d(TAG, "startClear: ");
        Intent intent = new Intent(this,MapsActivity.class);
        intent.addFlags(FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void startCheckOrderService() {
        ServiceUtils.startService(getApplicationContext(),CheckEventService.class);
    }

}
