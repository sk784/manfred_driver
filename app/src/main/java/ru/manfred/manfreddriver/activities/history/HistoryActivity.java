/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.activities.history;

import android.app.ActionBar;
import android.arch.lifecycle.Observer;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import javax.inject.Inject;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.activities.NewOrderActivity;
import ru.manfred.manfreddriver.adapters.PagerAdapter;
import ru.manfred.manfreddriver.databinding.ActivityHistoryBinding;
import ru.manfred.manfreddriver.managers.ManfredTripManager;
import ru.manfred.manfreddriver.managers.TripManager;
import ru.manfred.manfreddriver.managers.history.HistoryManager;
import ru.manfred.manfreddriver.utils.Constants;

public class HistoryActivity extends AppCompatActivity implements HistoryFragment.HistoryFragmentCallback, ServerAvailableStatus {
    private static final String TAG = "HistoryActivity";
    private ActivityHistoryBinding binding;
    private int lastTabIndex = 0;
    //private TripManager.TripManagerListener tripManagerListener;

    private LocalBroadcastManager brodcastManager;
    private BroadcastReceiver baseReceiver;
    //it generate no internet error
    //private NetworkChangeReceiver networkChangeReceiver;

    @Inject
    ManfredTripManager tripManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_history);
        ManfredApplication.getTripComponent().inject(this);
        //((ManfredApplication) getApplication()).setLastShownActivity(this);
        //DriverRepository.clearCache();

        binding.pager.setAdapter(new PagerAdapter(getSupportFragmentManager()));

        initToolbar();

        setupViewPager(binding.pager);
        binding.tabLayout.setupWithViewPager(binding.pager);
        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                lastTabIndex = tab.getPosition();
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        tripManager.getCurrentState().observe(this, new Observer<TripManager.Status>() {
            @Override
            public void onChanged(@Nullable TripManager.Status status) {
                if(status!=null){
                    if (status.getStatusState() != null) {
                        switch (status.getStatusState()) {
                            case OFFER_TO_RIDE:
                                final Intent intentNewOrder = new Intent(HistoryActivity.this, NewOrderActivity.class);
                                startActivity(intentNewOrder);
                                finish();
                                break;
                        }
                    }
                }
            }
        });

/*        tripManagerListener = new TripManager.TripManagerListener() {
            @Override
            public void newStatus(TripManager.Status status) {
                if (status.getStatusState() != null) {
                    switch (status.getStatusState()) {
                        case OFFER_TO_RIDE:
                            final Intent intentNewOrder = new Intent(HistoryActivity.this, NewOrderActivity.class);
                            startActivity(intentNewOrder);
                            finish();
                            break;
                    }
                }
            }
        };*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        //tripManager.addChangeStatusListener(tripManagerListener);

        brodcastManager = LocalBroadcastManager.getInstance(this);
        baseReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean haveNet = intent.getBooleanExtra(Constants.INTERNET_STATUS, true);
                Log.d(TAG, "onReceive: " + haveNet);
                if (haveNet) {
                    isAvailable();
                    refreshCurrentTab();
                } else {
                    notAvailable();
                }
            }
        };
        IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        intentFilter.addAction(Constants.INTERNET_STATUS);
        brodcastManager.registerReceiver(baseReceiver, intentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //tripManager.removeChangeStatusListener(tripManagerListener);

        if (baseReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(baseReceiver);
            baseReceiver = null;
        }
    }


    private void setupViewPager(ViewPager viewPager) {
        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager());

        Fragment todayFragment = new HistoryFragment();
        Bundle todayArgs = new Bundle();
        todayArgs.putSerializable(Constants.HISTORY_INTERVAL_KEY, HistoryManager.HistoryInterval.TODAY);
        todayFragment.setArguments(todayArgs);
        adapter.addFragment(todayFragment, "сегодня");

        Fragment weekFragment = new HistoryFragment();
        Bundle weekArgs = new Bundle();
        weekArgs.putSerializable(Constants.HISTORY_INTERVAL_KEY, HistoryManager.HistoryInterval.THIS_WEEK);
        weekFragment.setArguments(weekArgs);
        adapter.addFragment(weekFragment, "неделя");

        Fragment monthFragment = new HistoryFragment();
        Bundle monthArgs = new Bundle();
        monthArgs.putSerializable(Constants.HISTORY_INTERVAL_KEY, HistoryManager.HistoryInterval.THIS_MONTH);
        monthFragment.setArguments(monthArgs);
        adapter.addFragment(monthFragment, "месяц");

        Fragment customRangeFragment = new HistoryFragment();
        Bundle customRangeArgs = new Bundle();
        customRangeArgs.putSerializable(Constants.HISTORY_INTERVAL_KEY, HistoryManager.HistoryInterval.CUSTOM_INTERVAL);
        customRangeFragment.setArguments(customRangeArgs);
        adapter.addFragment(customRangeFragment, "период");

        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                Log.d(TAG, "onPageSelected: "+i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    public void initToolbar() {
        Toolbar myToolbar =
                (Toolbar) findViewById(R.id.include);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_action_bar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView title = (TextView) findViewById(R.id.tvTitle);
        title.setText("История поездок");
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void needSwitchBack() {
        Log.d(TAG, "needSwitchBack: to " + lastTabIndex);
        binding.pager.setCurrentItem(lastTabIndex);
    }

    @Override
    public void notAvailable() {
        Log.d(TAG, "internetOff: ");
        binding.noInternetMessage.setVisibility(View.VISIBLE);
        binding.noInternetMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.startAnimation(AnimationUtils.loadAnimation(HistoryActivity.this,
                        R.anim.slid_down));
                view.setVisibility(View.GONE);
                refreshCurrentTab();
            }
        });
    }

    public void refreshCurrentTab() {
        Log.d(TAG, "refreshCurrentTab: ");
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.pager);
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.detach(currentFragment);
        fragmentTransaction.attach(currentFragment);
        fragmentTransaction.commit();
        //mb we have data from cache, check for internet second time

    }

    @Override
    public void isAvailable() {
        Log.d(TAG, "isAvailable: ");
        binding.noInternetMessage.setVisibility(View.GONE);
    }
}
