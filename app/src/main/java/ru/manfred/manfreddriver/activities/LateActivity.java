/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.activities;

import android.app.ActionBar;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import javax.inject.Inject;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.databinding.ActivityLateBinding;
import ru.manfred.manfreddriver.managers.ConnectionStatusManager;
import ru.manfred.manfreddriver.managers.ManfredConnectionStatusManager;

public class LateActivity extends AppCompatActivity {
    private static Integer TEN_MINUTE = 10;
    private static Integer FIFTEEN_MINUTE = 15;
    private static Integer TWENTY_MINUTE = 20;
    @Inject
    ManfredConnectionStatusManager connectionStatusManager;

    ConnectionStatusManager.ConnectStatusListener connectStatusListener;
    ActivityLateBinding activityLateBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityLateBinding = DataBindingUtil.setContentView(this, R.layout.activity_late);
        ManfredApplication.getTripComponent().inject(this);

        activityLateBinding.btnLate10m.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setLate(TEN_MINUTE);
            }
        });
        activityLateBinding.btnLate15m.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setLate(FIFTEEN_MINUTE);
            }
        });
        activityLateBinding.btnLate20m.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setLate(TWENTY_MINUTE);
            }
        });
        activityLateBinding.btnNotLate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setLate(null);
            }
        });

        initToolbar();
        connectStatusListener = new ConnectionStatusManager.ConnectStatusListener() {
            @Override
            public void netAvailable() {
                hideNoNetError();
            }

            @Override
            public void netUnAvailable() {
                noNetError();
            }

            @Override
            public void serverUnAvailable() {
                noNetError();
            }

            @Override
            public void tokenInvalid() {

            }
        };


    }

    void setLate(@Nullable Integer lateSize) {
        Intent intent = new Intent();
        if (lateSize == null) {
            setResult(RESULT_CANCELED);
            finish();
        } else {
            intent.putExtra("lateTime", lateSize);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    public void initToolbar() {
        Toolbar myToolbar =
                (Toolbar) findViewById(R.id.include);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_action_bar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView title = (TextView) findViewById(R.id.tvTitle);
        title.setText("В пути на заказ");
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        connectionStatusManager.addConnectStatusListener(connectStatusListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        connectionStatusManager.removeConnectStatusListener(connectStatusListener);
    }

    private void noNetError() {
        RelativeLayout noInet = findViewById(R.id.no_inet);
        noInet.setVisibility(View.VISIBLE);
        noInet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(LateActivity.this,
                        R.anim.slid_down));
                v.setVisibility(View.GONE);
                checkInetStatus();
            }
        });
        activityLateBinding.btnNotLate.setVisibility(View.GONE);
    }

    private void hideNoNetError() {
        RelativeLayout noInet = findViewById(R.id.no_inet);
        noInet.setVisibility(View.GONE);
        activityLateBinding.btnNotLate.setVisibility(View.VISIBLE);

    }

    private void checkInetStatus() {
        if (!connectionStatusManager.isNetAvailable()) {
            noNetError();
        }
    }

}
