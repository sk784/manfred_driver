/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.activities;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.graphics.Color;
import android.graphics.Rect;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.TouchDelegate;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.databinding.ActivityRideBinding;
import ru.manfred.manfreddriver.interactor.repository.RideRepository;
import ru.manfred.manfreddriver.managers.ManfredLocationManager;
import ru.manfred.manfreddriver.managers.SnackbarManager;
import ru.manfred.manfreddriver.managers.TripManager;
import ru.manfred.manfreddriver.managers.logging.LogTags;
import ru.manfred.manfreddriver.model.GoogleMaps.DirectionResults;
import ru.manfred.manfreddriver.model.api.GeoData;
import ru.manfred.manfreddriver.model.api.ManfredError;
import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.model.api.TimeInTripLiveData;
import ru.manfred.manfreddriver.utils.MapUtils;
import ru.manfred.manfreddriver.utils.PolylineDirectionCalculator;
import ru.manfred.manfreddriver.utils.RestoreTripInSession;
import ru.manfred.manfreddriver.utils.SharedPreferencesManager;
import ru.manfred.manfreddriver.utils.UIUtils;
import ru.manfred.manfreddriver.viewmodel.OrderViewModel;

import static ru.manfred.manfreddriver.utils.Constants.SAVED_SHOW_PHONE;
import static ru.manfred.manfreddriver.utils.MapUtils.fitToPolylineAndMarkers;

public class RideActivity extends BaseActivity implements OnMapReadyCallback {
    private static final String TAG = "RideActivity";
    private static final String SAVED_IS_TRANSFER_CANCELLED = "SAVED_IS_TRANSFER_CANCELLED";
    private static final int DEFAULT_ZOOM_LEVEL = 15;
    private final ObservableBoolean showPhone = new ObservableBoolean(false);
    long lastRefreshRoute = 0;
    private Polyline line1;
    private Polyline line2;
    private OrderViewModel orderVM;
    private Order order;
    private ActivityRideBinding binding;
    private ObservableBoolean showComment = new ObservableBoolean(true);
    @Inject
    ManfredLocationManager manfredLocationManager;
    private Location prevLocation;
    private TimeInTripLiveData timeInTripLiveData;
    private Marker destinationMarker;
    @Nullable private GoogleMap mMap;
    private Marker currentLocMarker;

    @Override
    TripManager.StatusState getStatusOfActivity() {
        return TripManager.StatusState.RIDE;
    }

    @Override
    void orderUpdated(Order order) {
        this.order = order;
        if (timeInTripLiveData == null) {
            startCountTime();
        } else {
            timeInTripLiveData.updateStartTime(order.getRide_start_time());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ManfredApplication.getTripComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_ride);
        super.onCreateDrawer();
        order = orderManager.getCurrentOrder();
        if (order == null) {
            restoreOrder();
        } else {
            orderVM = new OrderViewModel(order);
            binding.setOrder(orderVM);
            binding.ibAero.setEnabled(!order.isTo_airport_cancelled());
        }

        if (savedInstanceState != null) {
            showPhone.set(savedInstanceState.getBoolean(SAVED_SHOW_PHONE));
            binding.ibAero.setEnabled(savedInstanceState.getBoolean(SAVED_IS_TRANSFER_CANCELLED));
        }
        binding.setShowPhone(showPhone);
        initToolbar();
        initActivity();
    }

    @Override
    void showSnack(@NonNull SnackbarManager.Snack snack) {
        UIUtils.generateSnack(binding.clRoot, snack).show();
    }

    @Override
    String getActivityName() {
        return "ride";
    }

    private void initActivity() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.trip_map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        manfredLocationManager.getLocation().observe(this, new Observer<Location>() {
            @Override
            public void onChanged(@Nullable Location location) {
                showLocation(location, order);
            }
        });

        //increase click zone https://stackoverflow.com/questions/8176105/how-to-increase-hit-area-of-android-button-without-scaling-background
        final View parent = (View) binding.ibAero.getParent();  // button: the view you want to enlarge hit area
        parent.post(new Runnable() {
            public void run() {
                final Rect rect = new Rect();
                binding.ibAero.getHitRect(rect);
                final int extraSpace = 30;
                rect.top -= extraSpace;    // increase top hit area
                rect.left -= extraSpace;   // increase left hit area
                rect.bottom += extraSpace; // increase bottom hit area
                rect.right += extraSpace;  // increase right hit area
                parent.setTouchDelegate(new TouchDelegate(rect, binding.ibAero));
            }
        });

        binding.ibAero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showOffTransferDialog();
            }
        });

        binding.clPhonBlock.setOrder(orderVM);
        binding.clPhonBlock.callingAreaTap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: trying call");
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", orderVM.getCustomerNumber(), null));
                startActivity(intent);
            }
        });

        startCountTime();

        binding.btnNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loggingManager.addLog("RideActivity", "btnNavigation onClick", "navigator start", LogTags.OTHER);
                if (orderVM.getOrder().getDest_latitude().equals("")) {
                    MapUtils.startYandexNavigator(0, 0, getApplicationContext());
                } else {
                    MapUtils.startYandexNavigator(Double.parseDouble(orderVM.getOrder().getDest_latitude()),
                            Double.parseDouble(orderVM.getOrder().getDest_longitude()), getApplicationContext());
                }
            }
        });

        binding.moreLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showComment.set(!showComment.get());
            }
        });
        showComment.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                binding.tripRlComments.setVisibility(showComment.get() ? View.VISIBLE : View.GONE);
                binding.moreLess.setImageResource(showComment.get() ? R.drawable.less : R.drawable.more);
            }
        });

        binding.btnEndRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RideActivity.this, FinalActivity.class);
                startActivity(intent);
            }
        });

    }

    private void startCountTime() {
        //sheet happens =\
        if (order == null) {
            restoreOrder();
            return;
        }
        if (order.getRide_start_time() == null) {
            restoreOrder();
            return;
        }
        timeInTripLiveData = new TimeInTripLiveData(order.getRide_start_time());
        timeInTripLiveData.observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                if (integer != null) {
                    TextView duration = findViewById(R.id.action_bar_tv_time);
                    duration.setText(String.valueOf(integer));
                }
            }
        });
    }

    private void restoreOrder() {
        ProgressDialog progressDialog = new ProgressDialog(RideActivity.this);
        progressDialog.setTitle("Восстановление заказа...");
        progressDialog.show();
        RestoreTripInSession restoreTripInSession = new RestoreTripInSession();
        restoreTripInSession.restoreTrip(this);
    }

    private void initLocation(final OrderViewModel order, Location commonLocation) {
        if (order != null) {
            showLocation(commonLocation, order.getOrder());
        } else {
            restoreOrder();
        }
    }

    public void showLocation(@Nullable Location commonLocation, @Nullable Order order) {
        if (mMap == null) return;
        if (order == null) return;
        if (commonLocation == null) return;
        Location destinationLocation = null;
        if (!order.getDest_latitude().equals("")) {
            destinationLocation = new Location("");
            destinationLocation.setLatitude(Double.parseDouble(order.getDest_latitude()));
            destinationLocation.setLongitude(Double.parseDouble(order.getDest_longitude()));
        }

        if (destinationLocation == null) {
            LatLng latLng = new LatLng(commonLocation.getLatitude(), commonLocation.getLongitude());
            if (currentLocMarker == null) {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title("текущее местоположение");
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.my_location));
                markerOptions.flat(true);
                currentLocMarker = mMap.addMarker(markerOptions);
            } else {
                currentLocMarker.setPosition(latLng);
                if (prevLocation != null) {
                    float bearing = prevLocation.bearingTo(commonLocation);
                    currentLocMarker.setRotation(bearing);
                }
            }
            prevLocation = commonLocation;
            mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(commonLocation.getLatitude(), commonLocation.getLongitude())));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(DEFAULT_ZOOM_LEVEL));
        } else {
            destinationMarker = MapUtils.showDestinationMarkerOnMap(destinationLocation, commonLocation, destinationLocation,
                    mMap, getApplicationContext(), false);
            if (lastRefreshRoute == 0) {
                lastRefreshRoute = System.currentTimeMillis();
                showPathOnMap(commonLocation, destinationLocation);
            }
            long timeFromLastRefresh = System.currentTimeMillis() - lastRefreshRoute;
            if (timeFromLastRefresh > TripToPassengerActivity.ROUTE_REFRESH_TIME_MS) {
                lastRefreshRoute = System.currentTimeMillis();
                showPathOnMap(commonLocation, destinationLocation);
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.getUiSettings().setAllGesturesEnabled(false);
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if (manfredLocationManager.getCurrentLocation() != null) {
            initLocation(orderVM, manfredLocationManager.getCurrentLocation());
        }
    }

    public void initToolbar() {
        Toolbar myToolbar = findViewById(R.id.include);
        setSupportActionBar(myToolbar);
        final android.support.v7.app.ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            supportActionBar.setCustomView(R.layout.ride_action_bar);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }
        TextView title = findViewById(R.id.tvTitle);
        if (orderVM != null) {
            title.setText("Заказ " + orderVM.getOrder().getId());
        }
        TextView titleInTrip = findViewById(R.id.action_bar_tv_ride_title);
        titleInTrip.setText("длительность");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (orderVM == null) {
            if (order == null) {
                restoreOrder();
            } else {
                orderVM = new OrderViewModel(order);
                binding.setOrder(orderVM);
                binding.ibAero.setEnabled(!order.isTo_airport_cancelled());
            }
        }
    }

    @Override
    public void onBackPressed() {
    }

    public void showPathOnMap(Location currLocation, Location destLocation) {
        Log.d(TAG, "showPathOnMap: ");
        RideRepository rideRepository =
                ((ManfredApplication) getApplication()).getRepoProvider()
                        .getRideRepository(SharedPreferencesManager.getCredentials(getApplicationContext()).getToken());
        rideRepository.getDirection(new GeoData(currLocation), new GeoData(destLocation), new RideRepository.RepoAnswer<DirectionResults>() {
            @Override
            public void allOk(DirectionResults results) {
                PolylineDirectionCalculator polylineDirectionCalculator = new PolylineDirectionCalculator(results);
                PolylineOptions polylineOptions = polylineDirectionCalculator.getPolylineOptions();
                currentLocMarker = MapUtils.showMarkerOnMap(currentLocMarker, currLocation, mMap);
                if (line1 != null) {
                    line1.remove();
                    line2.remove();
                }
                polylineOptions.width(16);
                polylineOptions.color(Color.parseColor("#1976D2")).geodesic(true).zIndex(8);
                line1 = mMap.addPolyline(polylineOptions);
                polylineOptions.width(14);
                polylineOptions.color(Color.parseColor("#2196F3")).geodesic(true).zIndex(8);
                line2 = mMap.addPolyline(polylineOptions);
                List<Marker> markers = new ArrayList<>();
                markers.add(currentLocMarker);
                markers.add(destinationMarker);
                fitToPolylineAndMarkers(line1, mMap, getResources(), markers);
            }

            @Override
            public void error(String error) {

            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(SAVED_SHOW_PHONE, showPhone.get());
        savedInstanceState.putBoolean(SAVED_IS_TRANSFER_CANCELLED, binding.ibAero.isEnabled());
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void internetOff() {
        binding.noInternetMessage.setVisibility(View.VISIBLE);
        binding.btnEndRide.setVisibility(View.GONE);
        binding.btnNavigation.setVisibility(View.GONE);

    }

    @Override
    protected void internetOn() {
        binding.noInternetMessage.setVisibility(View.GONE);
        binding.btnEndRide.setVisibility(View.VISIBLE);
        binding.btnNavigation.setVisibility(View.VISIBLE);
    }

    @Override
    protected void setBusy() {

    }

    @Override
    protected void setFree() {

    }

    private void showOffTransferDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);

        builder.setTitle("Отключить трансфер");
        builder.setMessage("Стоимость поездки будет посчитана по обычному тарифу");
        builder.setPositiveButton("Подтверждаю", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                RideRepository rideRepository =
                        ((ManfredApplication) getApplication()).getRepoProvider().getRideRepository(SharedPreferencesManager.getCredentials(getApplicationContext()).getToken());
                rideRepository.cancelToAirport(new RideRepository.AcceptCallback() {
                    @Override
                    public void error(ManfredError manfredError) {
                        Toast.makeText(getApplicationContext(), manfredError.getText(), Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void accepted(@org.jetbrains.annotations.Nullable Order order) {
                        binding.ibAero.setEnabled(false);
                    }

                    @Override
                    public void debugMessage(String message) {
                        Log.d(TAG, "debugMessage: "+message);
                    }

                    @Override
                    public void serverNotAvailable() {
                        internetOff();
                    }
                });
            }
        });
        builder.setNegativeButton("Отмена", null);
        builder.show();
    }
}
