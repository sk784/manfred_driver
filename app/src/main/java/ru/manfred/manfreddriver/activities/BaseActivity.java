/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.activities;

import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.intercom.android.sdk.Intercom;
import io.intercom.com.bumptech.glide.Glide;
import ru.manfred.manfreddriver.BuildConfig;
import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.activities.history.HistoryActivity;
import ru.manfred.manfreddriver.activities.login.LoginActivity;
import ru.manfred.manfreddriver.activities.preorders.PreordersActivity;
import ru.manfred.manfreddriver.databinding.DrawerHeaderBinding;
import ru.manfred.manfreddriver.interactor.repository.DriverRepository;
import ru.manfred.manfreddriver.interactor.repository.ResponseCallback;
import ru.manfred.manfreddriver.interactor.repository.UserRepository;
import ru.manfred.manfreddriver.managers.ConnectionStatusManager;
import ru.manfred.manfreddriver.managers.ManfredActivityNotificationManager;
import ru.manfred.manfreddriver.managers.ManfredConnectionStatusManager;
import ru.manfred.manfreddriver.managers.ManfredOrderManager;
import ru.manfred.manfreddriver.managers.ManfredSnackbarManager;
import ru.manfred.manfreddriver.managers.network.ManfredNetworkManager;
import ru.manfred.manfreddriver.managers.sound.ManfredSoundManager;
import ru.manfred.manfreddriver.managers.ManfredTripManager;
import ru.manfred.manfreddriver.managers.ActivityNotificationManager;
import ru.manfred.manfreddriver.managers.SnackbarManager;
import ru.manfred.manfreddriver.managers.StatusManager;
import ru.manfred.manfreddriver.managers.TripManager;
import ru.manfred.manfreddriver.managers.history.ManfredHistoryCache;
import ru.manfred.manfreddriver.managers.logging.LogTags;
import ru.manfred.manfreddriver.managers.logging.ManfredLoggingManager;
import ru.manfred.manfreddriver.managers.preorder.ManfredPreOrderManager;
import ru.manfred.manfreddriver.managers.preorder.PreOrderEvent;
import ru.manfred.manfreddriver.managers.preorder.PreOrderManager;
import ru.manfred.manfreddriver.model.api.Credentials;
import ru.manfred.manfreddriver.model.api.Driver;
import ru.manfred.manfreddriver.model.api.ManfredError;
import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.model.api.PreOrderCounters;
import ru.manfred.manfreddriver.model.api.TimeToArrivalLiveDate;
import ru.manfred.manfreddriver.model.responces.DriverStatus;
import ru.manfred.manfreddriver.model.responces.IsFreeResponse;
import ru.manfred.manfreddriver.presenters.BasePresenter;
import ru.manfred.manfreddriver.presenters.MainPresenter;
import ru.manfred.manfreddriver.services.CheckEventService;
import ru.manfred.manfreddriver.ui.BadgeDrawerToggle;
import ru.manfred.manfreddriver.utils.Constants;
import ru.manfred.manfreddriver.utils.RestoreTrip;
import ru.manfred.manfreddriver.utils.RestoreTripInSession;
import ru.manfred.manfreddriver.utils.ServiceUtils;
import ru.manfred.manfreddriver.utils.SharedPreferencesManager;
import ru.manfred.manfreddriver.viewmodel.DriverViewModel;
import ru.manfred.manfreddriver.viewmodel.PreOrderVM;

import static ru.manfred.manfreddriver.utils.FCMUtils.invalidateFCMToken;

/**
 * Created by begemot on 15.08.17.
 * https://stackoverflow.com/questions/19451715/same-navigation-drawer-in-different-activities
 */

public abstract class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "BaseActivity";
    @Inject
    protected ManfredTripManager tripManager;
    @Inject
    protected ManfredOrderManager orderManager;
    @Inject
    protected ManfredSoundManager soundManager;
    protected boolean foreground;
    BadgeDrawerToggle mDrawerToggle;
    //LocalBroadcastManager brodcastManager;
    NavigationView navigationViewTop;
    AlertDialog preOrderSoonDialog;
    //private ObservableBoolean isPreOrderSoonDialogShowed = new ObservableBoolean(false);
    DrawerHeaderBinding headerBinding;
    @Inject
    ManfredPreOrderManager manfredPreOrderManager;
    @Inject
    ManfredLoggingManager loggingManager;
    @Inject
    ManfredHistoryCache manfredHistoryCache;
    @Inject
    ManfredConnectionStatusManager connectionStatusManager;
    @Inject
    ManfredActivityNotificationManager manfredNotificationManager;
    @Inject
    StatusManager manfredStatusManager;
    @Inject
    ManfredSnackbarManager manfredSnackbarManager;
    BasePresenter basePresenter;
    //TripManager.TripManagerListener tripManagerListener;
    PreOrderManager.PreOrdersListener basePreOrdersListener;
    ConnectionStatusManager.ConnectStatusListener connectStatusListener;

    abstract TripManager.StatusState getStatusOfActivity();

    abstract void orderUpdated(Order order);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: " + this);
        ManfredApplication.getTripComponent().inject(this);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        Intercom.client().handlePushMessage();

        manfredNotificationManager.getBaseMessages().observe(this, new Observer<ActivityNotificationManager.Message>() {
            @Override
            public void onChanged(@Nullable ActivityNotificationManager.Message message) {
                if (message != null) {
                    Log.d(TAG, "getBaseMessages: "+message);
                    new android.app.AlertDialog.Builder(BaseActivity.this, R.style.MyDialogTheme)
                            .setTitle(message.getTitle())
                            .setMessage(message.getText())
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //soundManager.stopPlaySound();
                                    //manfredNotificationManager.baseNotifShowed();
                                }
                            })
                            .show();
                }
            }
        });

/*        tripManagerListener = (status) -> {
            Log.d(TAG, "new status is " + status);
            changeState(status);
        };*/

        tripManager.getCurrentState().observe(this, new Observer<TripManager.Status>() {
            @Override
            public void onChanged(@Nullable TripManager.Status status) {
                Log.d(TAG, "new status is " + status);
                changeState(status);
            }
        });

        basePresenter = new BasePresenter();
        basePreOrdersListener = new PreOrderManager.PreOrdersListener() {
            @Override
            public void preOrderNewEvent(PreOrderEvent preOrderEvent) {
                //Log.d(TAG, "preOrderNewEvent: " + preOrderEvent.toString());
                String logsString = "new event is basePreOrdersListener " + preOrderEvent.toString();
                loggingManager.addLog("BaseActivity", "preOrderNewEvent", "", LogTags.OTHER);
                switch (preOrderEvent.getEventStatus()) {
                    case PRE_ORDER_START_SOON:
                        showPreOrderSoonDialog(preOrderEvent);
                        break;
                    case PRE_ORDER_TIME_OUT:
                        preOrderTimedOut(preOrderEvent);
                        break;
                }
            }
        };

        connectStatusListener = new ConnectionStatusManager.ConnectStatusListener() {
            @Override
            public void netAvailable() {
                internetOn();
            }

            @Override
            public void netUnAvailable() {
                internetOff();
            }

            @Override
            public void serverUnAvailable() {
                internetOff();
            }

            @Override
            public void tokenInvalid() {
                Log.d(TAG, "connectStatusListener tokenInvalid: ");
                Toast.makeText(BaseActivity.this, "Неверные авторизационные данные, возможно вошел кто-то другой, " +
                        "либо вы были отключены от системы. Попробуйте залогиниться еще раз", Toast.LENGTH_LONG).show();
                logoutNow(null);
            }
        };

        ServiceUtils.startService(getApplicationContext(), CheckEventService.class);


        if (((ManfredApplication) getApplication()).getRepoProvider().isWebSocket()) {
            final ManfredNetworkManager networkManger = ManfredApplication.getNetworkManger();
            networkManger.getIsConnected().observe(this, new Observer<Boolean>() {
                @Override
                public void onChanged(@Nullable Boolean isConnected) {
                    if (isConnected == null) return;
                    Log.d(TAG, "getIsConnected onChanged: " + isConnected);
                    if (isConnected) {
                        Toast.makeText(getApplicationContext(), "Связь с сервером установлена", Toast.LENGTH_SHORT).show();
                        checkBusyStatusFromServer();
                    } else {
                        manfredStatusManager.setBusy();
                        Toast.makeText(getApplicationContext(), "Связь с сервером потеряна", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            networkManger.getIsInMaintenance().observe(this, new Observer<Boolean>() {
                @Override
                public void onChanged(@Nullable Boolean aBoolean) {
                    if(aBoolean!=null){
                        if(aBoolean){
                            Intent maintenance = new Intent(BaseActivity.this, OutOfServiceActivity.class);
                            startActivity(maintenance);
                        }
                    }
                }
            });


        }

        manfredSnackbarManager.getSnack().observe(this, new Observer<SnackbarManager.Snack>() {
            @Override
            public void onChanged(@Nullable SnackbarManager.Snack snack) {
                if (snack != null) {
                    showSnack(snack);
                }
            }
        });



    }

    abstract void showSnack(@NonNull SnackbarManager.Snack snack);

    private String getCurrentActivityName() {
        return getActivityName();
    }

    abstract String getActivityName();

    public void changeState(@Nullable TripManager.Status incStatus) {
        if (incStatus == null) return;
        TripManager.StatusState status = incStatus.getStatusState();
        if (status == null) return;
        if(status== TripManager.StatusState.COMPLETED && getStatusOfActivity() == TripManager.StatusState.FREE){
            //because free and completed are same for mapsActivity
            return;
        }
        if (status.equals(getStatusOfActivity())) {
            Log.d(TAG, "incStatus is equal state of activity");
            orderUpdated(incStatus.getOrder());
            return;
        }
        loggingManager.addLog("BaseActivity", "changeState", "switching state to " + status, LogTags.OTHER);
        switch (status) {
            case OFFER_ADDITIONAL_PAYMENT_ERROR:
                Intent intent = new Intent(getApplicationContext(), NoMoneyActivity.class);
                //unsubscribeBase();
                if (incStatus.getOrder().getPayment_error_time() != null) {
                    Log.d(TAG, "tripManagerListener: Payment_error_time is null");
                    intent.putExtra(Constants.TIME_OF_PAYMENT_ERROR, incStatus.getOrder().getPayment_error_time().getTime());
                }
                //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            case CANCELLED:
                soundManager.stopNewOrderSound();
                //Activity activity = ManfredApplication.getLastShownActivity();
                if (!getCurrentActivityName().equals("map")) {
                    Log.d(TAG, "order cancelled, we foreground? " + foreground + ",it is activity " + this);
                    if (Objects.equals(incStatus.getReason().getType(), "cancelled_driver")) {
                        // TODO: 21.02.18 mb deprecated
                        soundManager.stopPlaySound();
                    } else {
                        endTrip(incStatus.getReason().getMessage());
                    }
                }
                break;
            case OFFER_TO_RIDE:
                final Intent intentNewOffer = new Intent(BaseActivity.this, NewOrderActivity.class);
                startActivity(intentNewOffer);
                break;
            case RIDE_TO_PASSENGER:
                final Intent intentToRideToPassenger = new Intent(BaseActivity.this, TripToPassengerActivity.class);
                //orderManager.setCurrentOrder(incStatus.getOrder());
                startActivity(intentToRideToPassenger);
                break;
            case WAIT_PASSENGER:
                Intent intentToWait = new Intent(BaseActivity.this, WaitActivity.class);
                startActivity(intentToWait);
                break;
            case RIDE:
                Intent intentToRide = new Intent(BaseActivity.this, RideActivity.class);
                startActivity(intentToRide);
                break;
            case COMPLETED:
                Intent intentToFinish = new Intent(BaseActivity.this, MapsActivity.class);
                startActivity(intentToFinish);
                break;
        }
    }

    private void logChangeCounters(PreOrderCounters preOrderCounters) {
        String lastPreOrderSizeString = mDrawerToggle.getBadgeText();
        int lastPreOrderSize = 0;
        if (lastPreOrderSizeString != null) {
            lastPreOrderSize = Integer.parseInt(mDrawerToggle.getBadgeText());
        }
        if (lastPreOrderSize != preOrderCounters.getIncPreOrders()) {
            String logString = "inc preOrders is " + preOrderCounters.getIncPreOrders();
            loggingManager.addLog("BaseActivity", "newCounters", logString, LogTags.PREORDER_COUNT_CHANGED);
        }
    }

    private void preOrderTimedOut(PreOrderEvent preOrderEvent) {
        Log.d(TAG, "preOrderTimeOut: ");
        if (preOrderSoonDialog != null) {
            if (preOrderSoonDialog.isShowing()) {
                Log.d(TAG, "preOrderTimeOut: preOrder dialog was showed, hiding");
                preOrderSoonDialog.dismiss();
                showPreOrderTimedOutDialog(preOrderEvent);
            } else {
                Log.d(TAG, "preOrderNewEvent: dialog not showed, ignoring");
            }
        } else {
            Log.d(TAG, "preOrderNewEvent: dialog is null, showing new");
            showPreOrderTimedOutDialog(preOrderEvent);
        }

    }

    protected void onCreateDrawer() {
        final DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new BadgeDrawerToggle(
                this,
                drawerLayout,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                if (getActionBar() != null) {
                    getActionBar().setTitle(R.string.navigation_drawer_open);
                }
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (getActionBar() != null) {
                    getActionBar().setTitle(R.string.navigation_drawer_close);
                }
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        drawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
        //mDrawerToggle.setBadgeEnabled(false);
        drawerLayout.addDrawerListener(mDrawerToggle);

        navigationViewTop = findViewById(R.id.navigation_drawer);
        navigationViewTop.setNavigationItemSelectedListener(this);

        NavigationView navigationViewBottom = findViewById(R.id.navigation_drawer_bottom);
        navigationViewBottom.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                //DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
                switch (item.getItemId()) {
                    case R.id.nav_exit:
                        logout(drawerLayout);
                        break;
                }
                return true;
            }
        });
        setVersionInNawigationDrawer(navigationViewBottom, R.id.nav_exit, BuildConfig.VERSION_NAME);

        headerBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.drawer_header, navigationViewTop, false);

        ((ManfredApplication) getApplication()).getCredentialsManager().getDriver().observe(this, new Observer<Driver>() {
            @Override
            public void onChanged(@Nullable Driver driver) {
                if (driver != null) {
                    DriverViewModel driverViewModel = new DriverViewModel(driver);
                    //Log.d(TAG, "onCreateDrawer: " + driverViewModel.getName());
                    setDriverInHeader(driverViewModel);
                }
            }
        });

        navigationViewTop.addHeaderView(headerBinding.getRoot());
        navigationViewTop.getHeaderView(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //DrawerLayout drawerLayout1 = (DrawerLayout) findViewById(R.id.drawer_layout);
                Intent intent = new Intent(BaseActivity.this, ProfileActivity.class);
                drawerLayout.closeDrawers();
                startActivity(intent);
            }
        });

        //manfredPreOrderManager.recalculateUnreadCounter();

    }

    private void setDriverInHeader(DriverViewModel driverViewModel) {
        if (driverViewModel != null) {
            headerBinding.setDriver(driverViewModel);
            headerBinding.ivAvatar.setClipToOutline(true);
            Glide
                    .with(this)
                    .load(driverViewModel.getAvatarUrl())
                    .into(headerBinding.ivAvatar);
        }
    }

    private void setNewPreOrdersCountInMenu(@IdRes int itemId, int notAcceptedPreOrders) {
        if (navigationViewTop != null) {
            View view = navigationViewTop.getMenu().findItem(itemId).getActionView();
            TextView allPreOrdersCounter = view.findViewById(R.id.menu_all_pre_orders);
            if (notAcceptedPreOrders == 0) {
                allPreOrdersCounter.setVisibility(View.GONE);
            } else {
                allPreOrdersCounter.setVisibility(View.VISIBLE);
                allPreOrdersCounter.setText(String.valueOf(notAcceptedPreOrders));
            }
        }
    }

    private void setMyPreOrdersCountInMenu(@IdRes int itemId, int myPreOrdersCount) {
        if (navigationViewTop != null) {
            View view = navigationViewTop.getMenu().findItem(itemId).getActionView();
            TextView myPreOrdersCounter = view.findViewById(R.id.menu_accepted_pre_orders);
            if (myPreOrdersCount == 0) {
                myPreOrdersCounter.setVisibility(View.GONE);
            } else {
                myPreOrdersCounter.setVisibility(View.VISIBLE);
                myPreOrdersCounter.setText(String.valueOf(myPreOrdersCount));
            }
        }
    }

    private void setVersionInNawigationDrawer(@Nullable NavigationView navigationView, @IdRes int itemId, String version) {
        if (navigationView != null) {
            View view = navigationView.getMenu().findItem(itemId).getActionView();
            TextView versionCaption = view.findViewById(R.id.tv_menu_version);
            version = "v." + version;
            versionCaption.setText(version);
        }
    }

    protected void logout(@Nullable DrawerLayout drawerLayout) {
        Log.d(TAG, "logout: ");
        loggingManager.addLogAndSendImmediately("BaseActivity", "logout",
                "trying for logout...", LogTags.OTHER);
        Credentials credentials = ((ManfredApplication) getApplication()).getCredentialsManager().getCredentials().getValue();
        if (credentials != null) {
            String token = credentials.getToken();
            UserRepository userRepository = ((ManfredApplication) getApplication()).getRepoProvider().getUserRepository(token);
            userRepository.logout(new UserRepository.AnswerCallback() {
                @Override
                public void success() {

                }

                @Override
                public void notSuccess(String error) {
                    Toast.makeText(getApplicationContext(), "Сервер сообщил об ошибке: " + error, Toast.LENGTH_LONG).show();

                }

                @Override
                public void authorisationRequired(String error) {

                }

                @Override
                public void serverNotAvailable() {

                }
            });
            logoutNow(drawerLayout);
        } else {
            Toast.makeText(getApplicationContext(), "Ошибка чтения токена", Toast.LENGTH_SHORT).show();
            logoutNow(drawerLayout);
        }

    }

    private void checkBusyStatusFromServer() {
        Credentials credentials = ((ManfredApplication) getApplication()).getCredentialsManager().getCredentials().getValue();
        if (credentials == null) return;
        DriverRepository driverRepository =
                ((ManfredApplication) getApplication()).getRepoProvider()
                        .getDriverRepository(SharedPreferencesManager.getCredentials(getApplicationContext()).getToken());
        driverRepository.getBusyStatus(new ResponseCallback<IsFreeResponse>() {
            @Override
            public void allOk(IsFreeResponse response) {
                boolean isFree = response.isFree();
                String logString = "statusIs: " + isFree;
                if (isFree) {
                    manfredStatusManager.setFree();
                } else {
                    manfredStatusManager.setBusy();
                }
                loggingManager.addLog("MapsActivity", "checkStatusFromServer", logString, LogTags.OTHER);
            }

            @Override
            public void error(ManfredError error) {
                Toast.makeText(getApplicationContext(), error.getText(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void logoutNow(@Nullable DrawerLayout drawerLayout) {
        Log.d(TAG, "logoutNow: ");
        Intercom.client().logout();
        Intent intent = new Intent(BaseActivity.this, LoginActivity.class);
        if (drawerLayout != null) {
            drawerLayout.closeDrawers();
        }
        stopService(new Intent(BaseActivity.this, CheckEventService.class));
        SharedPreferencesManager.removeCredentials(getApplicationContext());
        ((ManfredApplication) getApplication()).getCredentialsManager().setCredentials(null);
        manfredHistoryCache.invalidateCache();
        manfredPreOrderManager.invalidateCache();
        invalidateFCMToken();
        DrawerHeaderBinding headerBinding = DataBindingUtil.inflate(getLayoutInflater(),
                R.layout.drawer_header, navigationViewTop, false);
        headerBinding.setDriver(null);
        startActivity(intent);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Log.d(TAG, "onNavigationItemSelected: " + item.getItemId());
        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                switch (item.getItemId()) {
                    case R.id.nav_history:
                        Log.d("BaseActivity", "onOptionsItemSelected: need filteredHistory");
                        Intent intent = new Intent(BaseActivity.this, HistoryActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.nav_intercom:
                        Intercom.client().displayMessenger();
                        break;
                    case R.id.nav_pre_orders:
                        Log.d(TAG, "onNavigationItemSelected: PreordersActivity");
                        Intent preOrderIntent = new Intent(BaseActivity.this, PreordersActivity.class);
                        startActivity(preOrderIntent);
                        break;
                }
            }
        }, 200);//delay for smoose close animation
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item))
            return true;
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: " + this);
        loggingManager.addLog("BaseActivity", "onResume", this.toString(), LogTags.USER_ON_SCREEN);
        foreground = true;
        //ManfredCredentialsManager manfredCredentialsManager = ((ManfredApplication)getApplication()).getCredentialsManager();
        //tripManager.addChangeStatusListener(tripManagerListener);
        /*
        loggingManager.addLog("BaseActivity", "onResume",
                "subscribing to manfredPreOrderManager and connectionStatusManager", LogTags.OTHER);*/
        manfredPreOrderManager.addChangePreOrderListener(basePreOrdersListener);
        connectionStatusManager.addConnectStatusListener(connectStatusListener);
        manfredPreOrderManager.getNewPreOrders().observe(this, new Observer<List<Order>>() {
            @Override
            public void onChanged(@Nullable List<Order> orders) {
                if (orders == null) return;
                //PreOrderCounters preOrderCounters =
                // new PreOrderCounters(0,orders.size(),manfredPreOrderManager.getMyPreOrders().getValue().size());
                //Log.d(TAG, "newCounters: "+orders.toString());
                if (orders.isEmpty()) {
                    mDrawerToggle.setBadgeEnabled(false);
                    Log.d(TAG, "newCounters: hiding badge");
                } else {
                    Log.d(TAG, "newCounters: showing badge");
                    mDrawerToggle.setBadgeEnabled(true);
                    mDrawerToggle.setBadgeText(String.valueOf(orders.size()));
                    //loggingManager.addLog("BaseActivity", "newCounters in ", "", LogTags.OTHER);
                }
                setNewPreOrdersCountInMenu(R.id.nav_pre_orders, orders.size());
            }
        });
        manfredPreOrderManager.getMyPreOrders().observe(this, new Observer<List<Order>>() {
            @Override
            public void onChanged(@Nullable List<Order> orders) {
                if (orders != null) {
                    setMyPreOrdersCountInMenu(R.id.nav_pre_orders, orders.size());
                }
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: " + this);
        connectionStatusManager.removeConnectStatusListener(connectStatusListener);
        if (preOrderSoonDialog != null) {
            if (preOrderSoonDialog.isShowing()) {
                preOrderSoonDialog.dismiss();
            }
        }
    }

    protected void onPause() {
        String logString = "onPause: " + this;
        loggingManager.addLog("BaseActivity", "onPause", logString, LogTags.OTHER);
        foreground = false;
        super.onPause();
        //tripManager.removeChangeStatusListener(tripManagerListener);
        manfredPreOrderManager.removeChangePreOrderListener(basePreOrdersListener);
    }

    /*
    used if we need cancel trip and return to MapActivity
     */
    protected void endTrip(String reason) {
        Log.d(TAG, "endTrip: ");
        if (reason.isEmpty()) return;
        loggingManager.addLog("BaseActivity", "endTrip", reason, LogTags.OTHER);
        //soundManager.stopPlaySound();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        alertDialogBuilder.setMessage(reason)
                .setTitle("Ошибка")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        TimeToArrivalLiveDate.get(null).reset();
                        //soundManager.stopPlaySound();
                        /*if(TimeInTripLiveData.get(null)!=null) {
                            TimeInTripLiveData.get(null).reset();
                        }*/
                        //unsubscribeBase();
                        Intent intent = new Intent(BaseActivity.this, MapsActivity.class);
                        startActivity(intent);
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    protected abstract void internetOff();

    protected abstract void internetOn();

    protected abstract void setBusy();

    protected abstract void setFree();

    protected void showDlgForcedCancelOrder(final Order order, String title, final TripManager.Reason message) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this, R.style.MyDialogTheme);
        builder.setMessage(message.getMessage())
                .setTitle(title)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        tripManager.cancelRide(order, message);
                    }
                });
        android.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    protected void showPreOrderTimedOutDialog(PreOrderEvent preOrderEvent) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this, R.style.MyDialogTheme);
        Log.d(TAG, "showPreOrderTimedOutDialog: ");
        String message = "Вы не подтвердили заказ \nпо адресу " + preOrderEvent.getOrder().getAddress() +
                "\nи мы отдали его другому \nводителю.";
        builder.setMessage(message)
                .setTitle("Заказ отменен")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //tripManager.cancelRide(order,message);
                        soundManager.stopPlaySound();
                        //orderManager.setCurrentOrder(null);
                        //binding.setOrder(null);
                        String token = ((ManfredApplication) getApplication()).getCredentialsManager()
                                .getCredentials().getValue().getToken();
                        UserRepository userRepository =
                                ((ManfredApplication) getApplication()).getRepoProvider().getUserRepository(token);
                        userRepository.changeStatus(true, new UserRepository.AnswerCallback() {
                            @Override
                            public void success() {
                                setFree();
                                manfredPreOrderManager.eventProcessedSuccessfully(preOrderEvent);
                            }

                            @Override
                            public void notSuccess(String error) {
                                setBusy();
                            }

                            @Override
                            public void authorisationRequired(String error) {
                                connectionStatusManager.invalidToken();
                            }

                            @Override
                            public void serverNotAvailable() {
                                internetOff();
                            }
                        });
                    }
                });
        android.app.AlertDialog dialog = builder.create();
        dialog.show();
        loggingManager.addLog("BaseActivity", "showPreOrderTimedOutDialog",
                String.valueOf(preOrderEvent.getOrder().getId()), LogTags.OTHER);
        setBusy();
    }

    private void showPreOrderSoonDialog(PreOrderEvent preOrderEvent) {
        Order order = preOrderEvent.getOrder();
        if (order == null) return;
        if (preOrderSoonDialog != null && preOrderSoonDialog.isShowing()) {
            loggingManager.addLog("BaseActivity", "showPreOrderSoonDialog",
                    "not showing because dialog on screen", LogTags.OTHER);
            return;
        }
        Log.d(TAG, "preOrderStartSoon: on " + this.toString());
        AlertDialog.Builder builder = new AlertDialog.Builder(BaseActivity.this, R.style.MyDialogTheme);
        PreOrderVM preOrderVM = new PreOrderVM(order);
        String title = "Предзаказ в " + preOrderVM.getScheduledTime() + "\n";
        builder.setTitle(title);
        builder.setTitle(title +
                "Ответьте за XX мин");
        builder.setMessage("Забрать пассажира по адресу\n" +
                preOrderVM.getStartAddress());
        builder.setCancelable(false);
        builder.setPositiveButton("Подтверждаю", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String logString = "preOrder #" + preOrderEvent.getOrder().getId();
                loggingManager.addLog("BaseActivity", "positive",
                        logString, LogTags.PREORDER_CONFIRMING_CLICK);
                basePresenter.onConfirmPreOrder(BaseActivity.this, order, new MainPresenter.MainPresenterCallback() {
                    @Override
                    public void positive() {
                        //manfredPreOrderManager.soonOrderConfirmed(order);
                        manfredPreOrderManager.eventProcessedSuccessfully(preOrderEvent);
                    }

                    @Override
                    public void error(String text) {
                        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });
        builder.setNegativeButton("Не поеду", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //BasePresenter basePresenter=new BasePresenter();
                String logString = "preOrder #" + preOrderEvent.getOrder().getId();
                loggingManager.addLog("BaseActivity", "onClick", logString, LogTags.PREORDER_NOT_CONFIRMING_CLICK);
                ProgressDialog progress = showProgressBar(BaseActivity.this, order);
                if (progress == null) return;
                manfredPreOrderManager.cancelOrder(order, new PreOrderManager.ResultListener() {
                    @Override
                    public void ok(Order order) {
                        Log.d(TAG, "showPreOrderSoonDialog: cancelled");
                        manfredPreOrderManager.eventProcessedSuccessfully(preOrderEvent);
                        progress.dismiss();
                    }

                    @Override
                    public void error(String errorText) {
                        Toast.makeText(getApplicationContext(), errorText, Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

        preOrderSoonDialog = builder.create();

        if (!BaseActivity.this.isFinishing()) {
            preOrderSoonDialog.show();
        }


        String logString = "showing preOrder confirmer preOrder #" + preOrderEvent.getOrder().getId();
        loggingManager.addLog("BaseActivity", "showPreOrderSoonDialog", logString, LogTags.PREORDER_CONFIRMING_SHOW);

        //Log.d(TAG, "showPreOrderSoonDialog: calculating datas for dialog... PreOrder time is " + order.getCar_need_time().toString() + " and now " + new Date().toString());
        //Log.d(TAG, "showPreOrderSoonDialog: preorder_2_notification_minutes is " + order.getPreorder_2_notification_minutes());
        long toEndNotification = (TimeUnit.MILLISECONDS.toMinutes(order.getCar_need_time().getTime() - new Date().getTime())) - order.getPreorder_2_notification_minutes();
        //Log.d(TAG, "showPreOrderSoonDialog: toEndNotif is " + toEndNotification);
        toEndNotification = toEndNotification + 2;//because in start timer count -1 and we show current(BEFORE) minutes
        new CountDownTimer(TimeUnit.MINUTES.toMillis(toEndNotification), TimeUnit.MINUTES.toMillis(1)) {
            public void onTick(long millisUntilFinished) {
                //alertDialog.setMessage("00:"+ (millisUntilFinished/1000));
                Log.d(TAG, "onTick: new time " + TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished));
                String title2 = "Ответьте за " + TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) + " мин";
                preOrderSoonDialog.setTitle(title + title2);
            }

            public void onFinish() {
                //preOrderSoonDialog.setMessage("Time for order"+order.getId()+" is out");
            }
        }.start();
        //manfredPreOrderManager.preOrderSoonShowed(order);
    }

    private ProgressDialog showProgressBar(Context context, Order order) {
        if (order == null) {
            //Toast.makeText(context,"Ошибка: заказ пуст",Toast.LENGTH_LONG).show();
            return null;
        }
        ProgressDialog progress = new ProgressDialog(context);
        progress.setMessage("Идет обработка...");
        progress.setCancelable(false);
        progress.setCanceledOnTouchOutside(false);
        progress.show();
        return progress;
    }

    public void checkConnectionWithServer(View view, String statusOfScreen) {
        view.startAnimation(AnimationUtils.loadAnimation(this,
                R.anim.slid_down));
        view.setVisibility(View.GONE);
        DriverRepository driverRepository =
                ((ManfredApplication) getApplication()).getRepoProvider().getDriverRepository(SharedPreferencesManager.getCredentials(getApplicationContext()).getToken());
        Log.d(TAG, "checkConnectionWithServer: make response for restore state");
        driverRepository.getStatus(new ResponseCallback<DriverStatus>() {
            @Override
            public void allOk(DriverStatus response) {
                if (response.getDriver().is_free()) {
                    //when driver is free order is null, checking by free status in driver
                    if (statusOfScreen.equals("FREE")) {
                        internetOn();
                    } else {
                        RestoreTripInSession restoreTripInSession = new RestoreTripInSession();
                        restoreTripInSession.startActivityIfNeed(new RestoreTrip(), response, BaseActivity.this);
                    }
                } else {
                    if (response.getOrder() != null) {
                        if (response.getOrder().getStatus().equals(statusOfScreen)) {
                            //progress.dismiss();
                            internetOn();
                        } else {
                            //progress.dismiss();
                            RestoreTripInSession restoreTripInSession = new RestoreTripInSession();
                            restoreTripInSession.startActivityIfNeed(new RestoreTrip(), response, BaseActivity.this);
                        }
                    }
                }
            }

            @Override
            public void error(ManfredError error) {
                Log.d(TAG, "error: " + error.getType());
                if (error.getType() == ManfredError.ErrorStatus.SERVER_UNAVAILABLE_ERROR) {
                    //progress.dismiss();
                    internetOff();
                } else {
                    //progress.dismiss();
                    Toast.makeText(getApplicationContext(), "Связь установлена, но сервер вернул ошибку " + error.getText(), Toast.LENGTH_LONG).show();
                    internetOn();
                }
            }
        });
    }
}
