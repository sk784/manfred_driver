/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.LongSparseArray;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ebanx.swipebtn.OnStateChangeListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.databinding.ActivityMapsBinding;
import ru.manfred.manfreddriver.interactor.repository.DriverRepository;
import ru.manfred.manfreddriver.interactor.repository.ResponseCallback;
import ru.manfred.manfreddriver.interactor.repository.UserRepository;
import ru.manfred.manfreddriver.managers.ActivityNotificationManager;
import ru.manfred.manfreddriver.managers.CarsOnMapManager;
import ru.manfred.manfreddriver.managers.CurrLocationManager;
import ru.manfred.manfreddriver.managers.ManfredCarsOnMapManager;
import ru.manfred.manfreddriver.managers.ManfredLocationManager;
import ru.manfred.manfreddriver.managers.ManfredOrderManager;
import ru.manfred.manfreddriver.managers.SnackbarManager;
import ru.manfred.manfreddriver.managers.StatusManager;
import ru.manfred.manfreddriver.managers.TripManager;
import ru.manfred.manfreddriver.managers.logging.LogTags;
import ru.manfred.manfreddriver.managers.preorder.PreOrderEvent;
import ru.manfred.manfreddriver.managers.preorder.PreOrderManager;
import ru.manfred.manfreddriver.model.api.CarOnMap;
import ru.manfred.manfreddriver.model.api.Credentials;
import ru.manfred.manfreddriver.model.api.Driver;
import ru.manfred.manfreddriver.model.api.ManfredError;
import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.model.responces.IsFreeResponse;
import ru.manfred.manfreddriver.presenters.MainPresenter;
import ru.manfred.manfreddriver.services.CheckEventService;
import ru.manfred.manfreddriver.utils.MapUtils;
import ru.manfred.manfreddriver.utils.PermissionUtil;
import ru.manfred.manfreddriver.utils.ServiceUtils;
import ru.manfred.manfreddriver.utils.SharedPreferencesManager;
import ru.manfred.manfreddriver.utils.UIUtils;
import ru.manfred.manfreddriver.viewmodel.DriverViewModel;
import ru.manfred.manfreddriver.viewmodel.OrderViewModel;
import ru.manfred.manfreddriver.viewmodel.PreOrderMainScreen;

public class MapsActivity extends BaseActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnCameraMoveStartedListener {

    public static final long ORDER_ON_SCREEN_DELAY = TimeUnit.SECONDS.toMillis(80);
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private static final String TAG = "MapsActivity";
    private static final int DEFAULT_ZOOM_LEVEL = 15;
    //private static final int JOB_ID = 1;
    private static final int PERMISSION_IN_PREFERENCE_REQUEST_CODE = 20;
    LongSparseArray<Marker> carMarkers = new LongSparseArray<>();
    LocalBroadcastManager bManager;
    Location prevMarkerLocation;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    ObservableBoolean needShowCurrentLocation = new ObservableBoolean(true);
    //Credentials credentials;
    PreOrderManager.PreOrdersListener preOrdersListener;
    ActivityNotificationManager.NotificationManagerListener notificationManagerListener;
    CarsOnMapManager.CarPositionChangeListener carPositionChangeListener;
    @Inject
    ManfredOrderManager manfredOrderManager;
    @Inject
    ManfredLocationManager manfredLocationManager;
    @Inject
    ManfredCarsOnMapManager manfredCarsOnMapManager;
    @Inject
    StatusManager statusManager;
    CurrLocationManager.LocationChangedListener locationChangedListener;
    ProgressDialog progress;
    private boolean mapReady;
    private GoogleMap mMap;
    private ActivityMapsBinding binding;

    @Override
    TripManager.StatusState getStatusOfActivity() {
        return TripManager.StatusState.FREE;
    }

    @Override
    void orderUpdated(Order order) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_maps);
        Log.d(TAG, "onCreate: ");
        progress = new ProgressDialog(MapsActivity.this);
        super.onCreateDrawer();
        ManfredApplication.getTripComponent().inject(this);
        ((ManfredApplication) getApplication()).getCredentialsManager().getDriver().observe(this, new Observer<Driver>() {
            @Override
            public void onChanged(@Nullable Driver driver) {
                if (driver != null) {
                    binding.setDriver(new DriverViewModel(driver));
                }
            }
        });
        MainPresenter mainPresenter = new MainPresenter() {
            @Override
            public void changeStatus() {
                statusManager.setBusy();
            }

            @Override
            public void showAway() {

            }
        };
        binding.setPresenter(mainPresenter);

        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } else {
            Log.d(TAG, "onCreate: bar is null");
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        initButtons();
        checkForUpdate();
        statusManager.getIsFreeLiveData().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean isFree) {
                Log.d(TAG, "statusManager onChanged: status changed to " + isFree);
                if (isFree != null) {
                    if (!isFree) {
                        setBusy();
                    } else {
                        if (!isLocationPermissionGranted()) {
                            checkPermissionAndAskIfNeed();
                        } else {
                            if (!ManfredApplication.isNeedUpdate()) {
                                setFree();
                            } else {
                                Log.d(TAG, "onChanged: need update");
                            }
                        }
                    }
                }
            }
        });

        manfredLocationManager.getLocation().observe(this, new Observer<Location>() {
            @Override
            public void onChanged(@Nullable Location location) {
                updateLocation(location);
            }
        });

        moveCameraToCurrentLocation();

        mapReady = false;

        preOrdersListener = createPreOrdersListener();

        notificationManagerListener = new ActivityNotificationManager.NotificationManagerListener() {
            @Override
            public void showNotification(ActivityNotificationManager.Message message) {
                showAlert(message);
            }
        };

        carPositionChangeListener = new CarsOnMapManager.CarPositionChangeListener() {
            @Override
            public void carsChanged(List<CarOnMap> cars, List<Long> idsOfGoOfflineDrivers) {
                showCarsOnMap(cars, idsOfGoOfflineDrivers);
            }
        };

        binding.splashOnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.splashOnMap.setVisibility(View.GONE);
            }
        });

        if (ManfredApplication.isFirstStartAfterLogin()) {
            showWelcomeSplash();
        }

        needShowCurrentLocation.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                if (needShowCurrentLocation.get()) {
                    binding.myLocationButton.hide();
                } else {
                    binding.myLocationButton.show();
                }
            }
        });

        //first initialisation
        if (needShowCurrentLocation.get()) {
            binding.myLocationButton.hide();
        }

        manfredPreOrderManager.getOrderWaitToStart().observe(this, new Observer<Order>() {
            @Override
            public void onChanged(@Nullable Order order) {
                showWaitToStartFooter(order);
            }
        });

        /*
        if(BuildConfig.DEBUG) {
            requestPhoneStateStats();
            requestReadNetworkHistoryAccess();
        }*/
    }

    private void moveCameraToCurrentLocation() {
        Location location = manfredLocationManager.getCurrentLocation();
        if (location != null) {
            updateLocation(location);
        }
    }

    @Override
    void showSnack(@NonNull SnackbarManager.Snack snack) {
        UIUtils.generateSnack(binding.getRoot(), snack).show();
    }

    @Override
    String getActivityName() {
        return "map";
    }

    private void showLocationDisabledSnackbar() {
        Snackbar.make(binding.getRoot(), "Включите геолокацию в настройках приложения", Snackbar.LENGTH_LONG)
                .setAction("НАСТРОЙКИ", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openApplicationSettings();
                        Toast.makeText(getApplicationContext(),
                                "Откройте \"разрешения\" и включите \"геолокацию\"",
                                Toast.LENGTH_SHORT)
                                .show();
                    }
                })
                .show();
    }

    public void openApplicationSettings() {
        Intent appSettingsIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.parse("package:" + getPackageName()));
        startActivityForResult(appSettingsIntent, PERMISSION_IN_PREFERENCE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PERMISSION_IN_PREFERENCE_REQUEST_CODE) {
            if (isLocationPermissionGranted()) {
                manfredLocationManager.createManager();
            } else {
                checkPermissionAndAskIfNeed();
            }
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void busySwitchSetOffLine() {
        binding.busySwitch.setEnabled(true);
        binding.busySwitch.setButtonBackground(getDrawable(R.drawable.swype_gray_button));
        binding.busySwitch.setSlidingButtonBackground(getDrawable(R.drawable.swype_gray_background));
        binding.busySwitch.setText("Выйти на линию");
    }

    private void busySwitchSetOnLine() {
        binding.busySwitch.setEnabled(true);
        binding.busySwitch.setButtonBackground(getDrawable(R.drawable.swype_green_button));
        binding.busySwitch.setSlidingButtonBackground(getDrawable(R.drawable.swype_green_background));
        binding.busySwitch.setText("Уйти с линии");
    }

    private void checkForUpdate() {
        Log.d(TAG, "checkForUpdate: ");

        if (ManfredApplication.isNeedUpdate()) {
            Log.d(TAG, "checkForUpdate: need update");
            showNeedUpdateDialog();
            //isFree.set(true);
            statusManager.setBusy();
            /*
            setStatusBarOffLine();
            busySwitchSetOffLine();*/
            String token = ((ManfredApplication) getApplication()).getCredentialsManager().getCredentials().getValue().getToken();
            UserRepository userRepository = ((ManfredApplication) getApplication()).getRepoProvider().getUserRepository(token);
            userRepository.changeStatus(false, new UserRepository.AnswerCallback() {
                @Override
                public void success() {
                    setBusy();
                }

                @Override
                public void notSuccess(String error) {
                    setStatusBarOffLine();
                }

                @Override
                public void authorisationRequired(String error) {
                    connectionStatusManager.invalidToken();
                }

                @Override
                public void serverNotAvailable() {
                    internetOff();
                }
            });
        } else {
            //checkStatusFromServer();
            //setStatusBarOnLine();
        }
    }

    private void showWelcomeSplash() {
        ManfredApplication.setFirstStartAfterLogin(false);
        binding.splashOnMap.setVisibility(View.VISIBLE);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                binding.splashOnMap.setVisibility(View.GONE);
            }

        }, 5000);
    }

    @NonNull
    private PreOrderManager.PreOrdersListener createPreOrdersListener() {
        //manfredOrderManager.setCurrentOrder(null);
        binding.setOrder(null);
        binding.setNewPreorder(null);
        binding.preorderSoon.setVisibility(View.GONE);
        binding.busySwitch.setVisibility(View.VISIBLE);
        return new PreOrderManager.PreOrdersListener() {
            @Override
            public void preOrderNewEvent(PreOrderEvent preOrderEvent) {
                Log.d(TAG, "preOrderNewEvent: " + preOrderEvent.toString());
                switch (preOrderEvent.getEventStatus()) {
                    case PRE_ORDER_TIME_OUT_BEFORE_START:
                        hideWaitToStartFooter(preOrderEvent.getOrder());
                        showModalWindowStartPreOrderTimeOut("Заказ отменён", "Вы не выехали на заказ \nпо адресу " + preOrderEvent.getOrder() +
                                "\nи мы отдали его другому \nводителю.");
                        manfredPreOrderManager.eventProcessedSuccessfully(preOrderEvent);
                        break;
                    case PRE_ORDER_DECLINED:
                        hideWaitToStartFooter(preOrderEvent.getOrder());
                        if (!preOrderEvent.isSelfCancelled() && preOrderEvent.isMyPreOrder()) {
                            showAlert(new ActivityNotificationManager.Message("Отмена предзаказа",
                                    "Предзаказ " + preOrderEvent.getOrder().getId() + " был отменен"));
                        } else {
                            Log.d(TAG, "preOrderNewEvent: not our order");
                        }
                        manfredPreOrderManager.eventProcessedSuccessfully(preOrderEvent);
                        break;
                }
            }
        };
    }

    private void showNeedUpdateDialog() {
        //stopCheckOrderService();
        new AlertDialog.Builder(this, R.style.MyDialogTheme)
                .setTitle("Доступно обновление")
                .setMessage("Доступна новая версия приложения Manfred. Обновитесь, чтобы принимать заказы.")
                .setCancelable(false)
                .setPositiveButton("Обновить", (dialog, whichButton) -> {
                    /* User clicked OK so do some stuff */
                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object

                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id="
                                + appPackageName)));
                    }
                })
                .show();
    }

    private static final int READ_PHONE_STATE_REQUEST = 37;
    private void requestPhoneStateStats() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, READ_PHONE_STATE_REQUEST);
    }
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void requestReadNetworkHistoryAccess() {
        Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
        startActivity(intent);
    }

    private void checkStatusFromServer() {
        //ManfredCredentialsManager credentialsManager = ((ManfredApplication)getApplication()).getCredentialsManager();
        //Credentials credentials = credentialsManager.getCredentials().getValue();
        Log.d(TAG, "checkStatusFromServer: ");
        if (ManfredApplication.isNeedUpdate() || !isLocationPermissionGranted()) {
            Log.d(TAG, "checkStatusFromServer: need update or not have permissions");
            statusManager.setBusy();
            return;
        }
        ((ManfredApplication) getApplication()).getCredentialsManager().getCredentials().observe(this, credentials -> {
            if (credentials == null) {
                Log.d(TAG, "checkStatusFromServer: credentials is null");
            } else {
                //Log.d(TAG, "checkStatusFromServer: credentials is " + credentials.toString());
                DriverRepository driverRepository =((ManfredApplication)getApplication()).getRepoProvider().getDriverRepository(credentials.getToken());
                driverRepository.getBusyStatus(new ResponseCallback<IsFreeResponse>() {
                    @Override
                    public void allOk(IsFreeResponse response) {
                        boolean isFree = response.isFree();
                        String logString = "statusIs: " + isFree;
                        if (isFree) {
                            statusManager.setFree();
                        } else {
                            statusManager.setBusy();
                        }
                        loggingManager.addLog("MapsActivity", "checkStatusFromServer", logString, LogTags.OTHER);
                    }

                    @Override
                    public void error(ManfredError error) {
                        Toast.makeText(getApplicationContext(), error.getText(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

    }

    private void showAlert(ActivityNotificationManager.Message message) {
        Log.d(TAG, "showAlert: " + message.toString());
        if (!(this).isFinishing()) {
            new AlertDialog.Builder(this, R.style.MyDialogTheme)
                    .setTitle(message.getTitle())
                    .setMessage(message.getText())
                    .setCancelable(false)
                    .setPositiveButton("OK", null)
                    .show();
            manfredNotificationManager.messageShowed(message);
        }
    }

    private void showWaitToStartFooter(Order order) {
        if (order == null) {
            //Toast.makeText(getApplicationContext(),"сервер прислал пустой предзаказ",Toast.LENGTH_LONG).show();
            Log.d(TAG, "showWaitToStartFooter: null order, hiding");
            binding.preorderSoon.setVisibility(View.GONE);
            binding.busySwitch.setVisibility(View.VISIBLE);
            return;
        }
        //manfredOrderManager.setCurrentOrder(order);
        binding.setNewPreorder(new PreOrderMainScreen(order));
        OrderViewModel preOrderVM1 = new OrderViewModel(order);
        //Log.d(TAG, "new order id is " + order.getId());
        //Log.d(TAG, "newEvent: comment:"+order.getComments()+".");
        binding.setOrder(preOrderVM1);
        binding.preorderSoon.setVisibility(View.VISIBLE);
        binding.busySwitch.setVisibility(View.INVISIBLE);
        //binding.setPreOrderEvent(event);
        String logString = "preOrder #" + order.getId();
        loggingManager.addLog("MapsActivity", "showWaitToStartFooter",
                logString, LogTags.PREORDER_READY_TO_START_SHOWED);
    }

    private void hideWaitToStartFooter(Order order) {
        if (order == null) {
            Log.d(TAG, "showWaitToStartFooter: null order, ignoring");
            return;
        }
        if(binding.getNewPreorder()!=null) {
            if (binding.getNewPreorder().getId() != order.getId()) {
                return;
            }
        }
        binding.preorderSoon.setVisibility(View.GONE);
        binding.busySwitch.setVisibility(View.VISIBLE);
        binding.setOrder(null);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) binding.myLocationButton.getLayoutParams();
        params.addRule(RelativeLayout.ABOVE, R.id.busy_switch_container);
        binding.myLocationButton.setLayoutParams(params);
    }

    protected void showModalWindowStartPreOrderTimeOut(String title, final String message) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this, R.style.MyDialogTheme);
        builder.setMessage(message)
                .setTitle(title)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //tripManager.cancelRide(order,message);
                        //soundManager.stopPlaySound();
                        //manfredOrderManager.setCurrentOrder(null);
                        binding.setOrder(null);

                        UserRepository userRepository =
                                ((ManfredApplication) getApplication()).getRepoProvider()
                                        .getUserRepository(SharedPreferencesManager.getCredentials(getApplicationContext()).getToken());
                        userRepository.changeStatus(true, new UserRepository.AnswerCallback() {
                            @Override
                            public void success() {
                                setStatusBarOnLine();
                                statusManager.setFree();
                            }

                            @Override
                            public void notSuccess(String error) {
                                setStatusBarOffLine();
                            }

                            @Override
                            public void authorisationRequired(String error) {
                                connectionStatusManager.invalidToken();
                            }

                            @Override
                            public void serverNotAvailable() {
                                internetOff();
                            }
                        });
                    }
                });
        android.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void restoreState() {
        Log.d(TAG, "restoreState: ");
        //manfredPreOrderManager.recalculateUnreadCounter();
        if (tripManager.getCurrentStatus() != null) {
            TripManager.StatusState status = tripManager.getCurrentStatus().getStatusState();
            Log.d(TAG, "onCreate: restoring activity, trip manager status is " + status);
            changeState(new TripManager.Status(status, new TripManager.Reason("", ""), orderManager.getCurrentOrder()));
        }

        for (ActivityNotificationManager.Message message : new ArrayList<>(manfredNotificationManager.getNotShowedMessages())) {
            Log.d(TAG, "restoreState: showing alert " + message.toString());
            showAlert(message);
        }

        checkStatusFromServer();
    }

    private void initButtons() {
        binding.myLocationButton.setOnClickListener(view -> {
            if (mLastLocation != null) {
                mCurrLocationMarker = MapUtils.showMarkerOnMap(mCurrLocationMarker, mLastLocation, mMap);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude())));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(DEFAULT_ZOOM_LEVEL));
            }
            needShowCurrentLocation.set(true);
        });

        binding.busySwitch.setOnStateChangeListener(new OnStateChangeListener() {
            @Override
            public void onStateChange(boolean active) {
                String logString = "busySwitch swiped: ";
                loggingManager.addLogSilence("MapsActivity", "onStateChange", logString, LogTags.OTHER);
                if (active) {
                    //checkForUpdate();
                    binding.awayProgress.setVisibility(View.VISIBLE);
                    binding.busySwitch.setEnabled(false);
                    if (ManfredApplication.isNeedUpdate()) {
                        Toast.makeText(getApplicationContext(), "Обновите приложение перед выходом на линию", Toast.LENGTH_LONG).show();
                        binding.awayProgress.setVisibility(View.GONE);
                        binding.busySwitch.toggleState();
                        binding.busySwitch.setEnabled(true);
                        showNeedUpdateDialog();
                    } else {
                        changeBusyStatus();
                    }
                }
            }
        });
    }

    private void changeBusyStatus() {
        //isFree = !isFree;
        String logString = "isFree now is " + statusManager.isFree();
        loggingManager.addLog("MapsActivity", "changeBusyStatus", logString, LogTags.OTHER);
        Credentials credentials = SharedPreferencesManager.getCredentials(getApplicationContext());
        if (credentials == null) {
            Toast.makeText(getApplicationContext(), "Ошибка получение профиля, перезайдите в приложение", Toast.LENGTH_LONG).show();
            Log.d(TAG, "changeBusyStatus: credentials is null, logout");
            logout(null);
            return;
        }
        if (!isLocationPermissionGranted()) {
            checkPermissionAndAskIfNeed();
            setBusy();
        }
        changeStatusOnServer(credentials, !statusManager.isFree());
    }

    private void changeStatusOnServer(Credentials credentials, boolean isFree) {
        final UserRepository userRepository = ((ManfredApplication) getApplication()).getRepoProvider().getUserRepository(credentials.getToken());
        Log.d(TAG, "changeStatusOnServer: changing to " + isFree);
        userRepository.changeStatus(isFree, new UserRepository.AnswerCallback() {
            @Override
            public void success() {
                loggingManager.addLog("MapsActivity", "changeStatusOnServer success",
                        "to isFree? "+isFree, LogTags.OTHER);
                //isFree.set(!needStatusBusy);
                //Log.d(TAG, "changeStatusOnServer success: busy switch toggleState, current state is "+binding.busySwitch.isActive());
                binding.busySwitch.setEnabled(true);
                binding.busySwitch.toggleState();
                binding.awayProgress.setVisibility(View.GONE);
                if (isFree) {
                    Log.d(TAG, "changeStatusOnServer: send busy to manager");
                    statusManager.setFree();
                } else {
                    Log.d(TAG, "changeStatusOnServer: send free to manager");
                    statusManager.setBusy();
                }
            }

            @Override
            public void notSuccess(String error) {
                Log.d(TAG, "changeStatusOnServer: notSuccess ");
                //Toast.makeText(getApplicationContext(), "ошибка смены статуса", Toast.LENGTH_SHORT).show();
                binding.busySwitch.setEnabled(true);
                binding.awayProgress.setVisibility(View.GONE);
                binding.busySwitch.toggleState();
                showAlert(new ActivityNotificationManager.Message("Ошибка смены статуса",error));
                checkStatusFromServer();
            }

            @Override
            public void authorisationRequired(String error) {
                Log.d(TAG, "authorisationRequired: ");
                binding.awayProgress.setVisibility(View.GONE);
                connectionStatusManager.invalidToken();
            }

            @Override
            public void serverNotAvailable() {
                internetOff();
            }
        });
    }

    private void updateLocation(Location commonLocation) {
        //Log.d(TAG, "updateLocation: "+commonLocation);
        mLastLocation = commonLocation;
        binding.setLocation(commonLocation);
        if (mapReady) {
            //mCurrLocationMarker = MapUtils.showMarkerOnMap(mCurrLocationMarker, commonLocation, mMap);
            if (mCurrLocationMarker != null) {
                //currLocationMarker.remove();
                LatLng latLng = new LatLng(commonLocation.getLatitude(), commonLocation.getLongitude());
                mCurrLocationMarker.setPosition(latLng);
                if (prevMarkerLocation != null) {
                    mCurrLocationMarker.setRotation(prevMarkerLocation.bearingTo(commonLocation));
                }
                prevMarkerLocation = commonLocation;
                //Log.d(TAG, "showMarkerOnMap: bearing is "+location.getBearing());
            } else {
                prevMarkerLocation = commonLocation;
                LatLng latLng = new LatLng(commonLocation.getLatitude(), commonLocation.getLongitude());
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title("Current Position");
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.my_location));
                markerOptions.flat(true);
                mCurrLocationMarker = mMap.addMarker(markerOptions);
            }
            if (needShowCurrentLocation.get()) {
                //Log.d(TAG, "updateLocation: to location "+commonLocation);
                CameraUpdate center = CameraUpdateFactory.newLatLngZoom(new LatLng(commonLocation.getLatitude(),
                        commonLocation.getLongitude()), DEFAULT_ZOOM_LEVEL);
                //CameraUpdate zoom = CameraUpdateFactory.zoomTo(DEFAULT_ZOOM_LEVEL);
                mMap.moveCamera(center);
                //mMap.animateCamera(zoom);
            }
        }
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mapReady = true;
        mMap.setOnCameraMoveStartedListener(this);
        if(needShowCurrentLocation.get()) {
            moveCameraToCurrentLocation();
        }
    }

    @Override
    public void onCameraMoveStarted(int reason) {

        if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
            //The user gestured on the map
            needShowCurrentLocation.set(false);
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "onConnected: ");

        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "onConnected: start location updates");
            }
        } else {
            Log.d(TAG, "onConnected: start location updates");
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NotNull ConnectionResult connectionResult) {

    }

    public boolean isLocationPermissionGranted() {
        boolean havePermissions = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        String logString = "isLocationPermissionGranted: " + havePermissions;
        loggingManager.addLogSilence("MapsActivity", "isLocationPermissionGranted", logString, LogTags.OTHER);
        return havePermissions;
    }

    public void showRequestPermission() {
        loggingManager.addLog("MapsActivity", "showRequestPermission",
                "asking for location", LogTags.OTHER);
        // Asking user if explanation is needed
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            Log.d(TAG, "showRequestPermission: need show snack");

            // Show an explanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.

            //Prompt the user once explanation has been shown
            final String message = "Требуется разрешение на геолокацию для начала получения заказов";
            Snackbar.make(binding.getRoot(), message, Snackbar.LENGTH_LONG)
                    .setAction("разрешить", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ActivityCompat.requestPermissions(MapsActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    MY_PERMISSIONS_REQUEST_LOCATION);
                        }
                    })
                    .show();

        } else {
            // No explanation needed, we can request the permission.
            Log.d(TAG, "showRequestPermission: requestPermissions");
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        manfredLocationManager.createManager();
                    }

                } else {
                    // Permission denied, Disable the functionality that depends on this permission.
                    //Toast.makeText(this, "Вы не дали разрешения на геолокацию и будете сняты с линии", Toast.LENGTH_LONG).show();
                    setBusy();
                }
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: " + this);
        bManager = LocalBroadcastManager.getInstance(this);

        //preOrdersListener = createPreOrdersListener();
        manfredPreOrderManager.addChangePreOrderListener(preOrdersListener);
        manfredNotificationManager.addNotificationManagerListener(notificationManagerListener);
        manfredCarsOnMapManager.addCarPositionChangeListener(carPositionChangeListener);

        moveCameraToCurrentLocation();

        restoreState();
        checkPermissionAndAskIfNeed();
    }

    private void checkPermissionAndAskIfNeed() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PermissionUtil.checkPermission(this, Manifest.permission.ACCESS_FINE_LOCATION,
                    new PermissionUtil.PermissionAskListener() {
                        @Override
                        public void onNeedPermission() {
                            showRequestPermission();
                        }

                        @Override
                        public void onPermissionPreviouslyDenied() {
                            Log.d(TAG, "checkPermissionAndAskIfNeed onPermissionPreviouslyDenied: ");
                            showRequestPermission();
                            //showLocationDisabledSnackbar();
                        }

                        @Override
                        public void onPermissionDisabled() {
                            Log.d(TAG, "checkPermissionAndAskIfNeed onPermissionDisabled: ");
                            showLocationDisabledSnackbar();
                        }

                        @Override
                        public void onPermissionGranted() {
                            Log.d(TAG, "checkPermissionAndAskIfNeed onPermissionGranted: ");
                            manfredLocationManager.createManager();
                        }
                    });
        }
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause: go to background");
        manfredCarsOnMapManager.removeCarPositionChangeListener(carPositionChangeListener);
        manfredNotificationManager.removeNotificationManagerListener(notificationManagerListener);
        manfredPreOrderManager.removeChangePreOrderListener(preOrdersListener);
        //preOrdersListener = null;
        super.onPause();
    }

    protected void setStatusBarOnLine() {
        //Log.d(TAG, "setStatusBarOnLine: ");
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(getResources().getColor(R.color.new_order_statusbar_color));
        ActionBar bar = getSupportActionBar();

        if (bar != null) {
            if (!bar.isShowing()) {
                bar.show();
            }
            bar.setTitle("На линии");
            bar.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), R.color.online_actionbar_color)));
        }
    }

    protected void setStatusBarOffLine() {
        Log.d(TAG, "setStatusBarOffLine: ");
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(getResources().getColor(R.color.main_screen_gray_statusbar));
        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            if (!bar.isShowing()) {
                bar.show();
            }
            bar.setTitle("Занят");
            bar.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), R.color.busy_actionbar_color)));
        }
    }

    @Override
    protected void internetOff() {
        Log.d(TAG, "internetOff: ");
        binding.noInternetMessage.setVisibility(View.VISIBLE);
        binding.preorderSoon.setVisibility(View.GONE);
        binding.noInternetMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkConnectionWithServer(view, "FREE");
            }
        });
        manfredPreOrderManager.removeChangePreOrderListener(preOrdersListener);
        //preOrdersListener = null;
    }

    @Override
    protected void internetOn() {
        if (preOrdersListener == null) {
            Log.d(TAG, "internetOn: not have listener, adding");
            preOrdersListener = createPreOrdersListener();
            manfredPreOrderManager.addChangePreOrderListener(preOrdersListener);
        }
        binding.noInternetMessage.setVisibility(View.GONE);
    }

    @Override
    protected void setBusy() {
        Log.d(TAG, "setBusy: ");
        //statusManager.setBusy();
        //stopCheckOrderService();
        busySwitchSetOffLine();
        setStatusBarOffLine();
    }

    @Override
    protected void setFree() {
        Log.d(TAG, "setFree: ");
        //statusManager.setFree();
        startCheckOrderService();
        busySwitchSetOnLine();
        setStatusBarOnLine();
    }

    private void startCheckOrderService() {
        //Log.d(TAG, "startCheckOrderService: ");
        Credentials credentials = SharedPreferencesManager.getCredentials(this);
        if (credentials == null) {
            Toast.makeText(this, "Проблемы с авторизацией, попробуйте еще раз", Toast.LENGTH_LONG).show();
            Log.d(TAG, "startCheckOrderService: credentials is null, logout");
            logout(null);
        } else {
            ServiceUtils.startService(getApplicationContext(), CheckEventService.class);
        }
    }

    private void showCarsOnMap(List<CarOnMap> cars, List<Long> needToRemove) {
        if (!mapReady) return;
        //Log.d(TAG, "showCarsOnMap: need show "+cars.size()+" cars on map");
        for (CarOnMap carOnMap : cars) {
            Marker marker = carMarkers.get(carOnMap.getId(), null);
            if (marker == null) {
                marker = createCarMarker(carOnMap);
                if (marker == null) {
                    continue;
                }
                carMarkers.put(carOnMap.getId(), marker);
            }
            //Log.d(TAG, "showCarsOnMap: showning "+carOnMap.toString());
            //todo alpha for isFree drivers
            if (carOnMap.isBusy()) {
                //Log.d(TAG, "showCarsOnMap: it car isFree, setting alpha");
                marker.setAlpha(0.5f);
            } else {
                marker.setAlpha(1f);
            }
            marker.setPosition(new LatLng(carOnMap.getLocation().getLatitude(), carOnMap.getLocation().getLongitude()));
            marker.setRotation(carOnMap.getBearing());
            //MarkerAnimation.animateMarkerToICS(ourGlobalMarker, newLatLng, new LatLngInterpolator.Spherical());
        }
        //removing cars what go offline
        for (Long carId : needToRemove) {
            Marker currMark = carMarkers.get(carId);
            if (currMark != null) {
                currMark.remove();
            }
            carMarkers.remove(carId);
        }
    }

    private Marker createCarMarker(CarOnMap carOnMap) {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(new LatLng(carOnMap.getLocation().getLatitude(), carOnMap.getLocation().getLongitude()));
        //markerOptions.title(carOnMap.getId());
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(resizeBitmap(R.drawable.car, 20, 54)));
        Marker newMarker = null;
        if (mMap != null) {
            newMarker = mMap.addMarker(markerOptions);
            Log.d(TAG, "createCarMarker: " + carOnMap.toString());
        }
        return newMarker;
    }

    private Bitmap resizeBitmap(int drawableName, int width, int height) {
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(), drawableName);
        return Bitmap.createScaledBitmap(imageBitmap, ScreenUtils.dpToPx(width, getApplicationContext()),
                ScreenUtils.dpToPx(height, getApplicationContext()), false);
    }

}
