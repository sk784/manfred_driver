/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.activities;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.graphics.Color;
import android.graphics.Rect;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ebanx.swipebtn.OnStateChangeListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import javax.inject.Inject;

import io.intercom.android.sdk.Intercom;
import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.databinding.ActivityNewOrderBinding;
import ru.manfred.manfreddriver.interactor.repository.ConfirmationRepository;
import ru.manfred.manfreddriver.interactor.repository.RideRepository;
import ru.manfred.manfreddriver.managers.ConnectionStatusManager;
import ru.manfred.manfreddriver.managers.ManfredConnectionStatusManager;
import ru.manfred.manfreddriver.managers.ManfredLocationManager;
import ru.manfred.manfreddriver.managers.ManfredOrderManager;
import ru.manfred.manfreddriver.managers.ManfredSnackbarManager;
import ru.manfred.manfreddriver.managers.ManfredTripManager;
import ru.manfred.manfreddriver.managers.SnackbarManager;
import ru.manfred.manfreddriver.managers.TripManager;
import ru.manfred.manfreddriver.managers.logging.LogTags;
import ru.manfred.manfreddriver.managers.logging.ManfredLoggingManager;
import ru.manfred.manfreddriver.managers.notification.ManfredNotificationManager;
import ru.manfred.manfreddriver.managers.sound.ManfredSoundManager;
import ru.manfred.manfreddriver.model.GoogleMaps.DirectionResults;
import ru.manfred.manfreddriver.model.api.AcceptingOrder;
import ru.manfred.manfreddriver.model.api.GeoData;
import ru.manfred.manfreddriver.model.api.ManfredError;
import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.utils.ElapsedTimeLogger;
import ru.manfred.manfreddriver.utils.LocationUtils;
import ru.manfred.manfreddriver.utils.PolylineDirectionCalculator;
import ru.manfred.manfreddriver.utils.SharedPreferencesManager;
import ru.manfred.manfreddriver.utils.TimeCalculatorFromDirection;
import ru.manfred.manfreddriver.viewmodel.OrderViewModel;

public class NewOrderActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final String TAG = "NewOrderActivity";
    @Inject
    ManfredLocationManager manfredLocationManager;
    @Inject
    ManfredTripManager tripManager;
    @Inject
    ManfredSnackbarManager manfredSnackbarManager;
    @Inject
    protected ManfredSoundManager soundManager;
    @Inject
    ManfredOrderManager manfredOrderManager;
    @Inject
    ManfredConnectionStatusManager connectionStatusManager;
    @Inject
    ManfredLoggingManager loggingManager;
    @Inject
    ManfredNotificationManager notificationManager;

    //TripManager.TripManagerListener tripManagerListener;
    ConnectionStatusManager.ConnectStatusListener connectStatusListener;

    private ObservableBoolean isTimeToCustomerCalculated = new ObservableBoolean(false);
    @Nullable private DirectionResults directionResults;
    private int calculatedDurationInSec;
    private boolean isTryingToStart = false;

    private ActivityNewOrderBinding binding;
    @Nullable private GoogleMap mMap;
    private Order order;
    private OrderViewModel orderViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_new_order);
        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.my_statusbar_color));
        ManfredApplication.getTripComponent().inject(this);
        //NotificationHelper.removeAllNotificationImmediately((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE));
        notificationManager.hideAllNotification();
        Intercom.client().hideMessenger();

        order = manfredOrderManager.getCurrentOrder();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.trip_map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        //Log.d(TAG, "onCreate: order is " + (order != null ? order.toString() : " is null"));
        if (order == null) {
            Toast.makeText(getApplicationContext(), "Ошибка отображения заказа(возможно был отменен)",
                    Toast.LENGTH_SHORT).show();
            finish();
        } else {
            orderViewModel = new OrderViewModel(order);
            binding.setOrder(orderViewModel);
            Location destinationLocation = new Location("");
            destinationLocation.setLatitude(order.getLatitude());
            destinationLocation.setLongitude(order.getLongitude());
            calculateTimeToCustomer(manfredLocationManager.getCurrentLocation(), orderViewModel, destinationLocation);
        }

        binding.newOrderGoAway.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NewOrderActivity.this, AwayActivity.class);
                intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                finish();
            }
        });

        tripManager.getCurrentState().observe(this, new Observer<TripManager.Status>() {
            @Override
            public void onChanged(@Nullable TripManager.Status status) {
                if(status==null)return;
                loggingManager.addLog("NewOrderActivity", "newStatus", status.toString(), LogTags.OTHER);
                if (status.getStatusState() != null) {
                    switch (status.getStatusState()) {
                        case OFFER_SHOW_TIMEOUT:
                            manfredSnackbarManager.showSnack(SnackbarManager.Type.ORDER_MISSED);
                            finish();
                            break;
                        case CANCELLED:
                            cancelOrder(status.getReason());
                            break;
                        case RIDE_TO_PASSENGER:
                            final Intent intent = new Intent(NewOrderActivity.this, TripToPassengerActivity.class);
                            startActivity(intent);
                            break;
                        case WAIT_PASSENGER:
                            Intent intentToWait = new Intent(NewOrderActivity.this, WaitActivity.class);
                            startActivity(intentToWait);
                            break;
                        case RIDE:
                            Intent intentToRide = new Intent(NewOrderActivity.this, RideActivity.class);
                            startActivity(intentToRide);
                            break;
                    }
                }
            }
        });

/*        tripManagerListener = new TripManager.TripManagerListener() {
            @Override
            public void newStatus(TripManager.Status status) {
                loggingManager.addLog("NewOrderActivity", "newStatus", status.toString(), LogTags.OTHER);
                if (status.getStatusState() != null) {
                    switch (status.getStatusState()) {
                        case OFFER_SHOW_TIMEOUT:
                            manfredSnackbarManager.showSnack(SnackbarManager.Type.ORDER_MISSED);
                            finish();
                            break;
                        case CANCELLED:
                            cancelOrder(status.getReason());
                            break;
                        case RIDE_TO_PASSENGER:
                            final Intent intent = new Intent(NewOrderActivity.this, TripToPassengerActivity.class);
                            startActivity(intent);
                            break;
                        case WAIT_PASSENGER:
                            Intent intentToWait = new Intent(NewOrderActivity.this, WaitActivity.class);
                            startActivity(intentToWait);
                            break;
                        case RIDE:
                            Intent intentToRide = new Intent(NewOrderActivity.this, RideActivity.class);
                            startActivity(intentToRide);
                            break;
                    }
                }
            }
        };*/
        if (order != null) {
            ConfirmationRepository confirmationRepository =
                    ((ManfredApplication)getApplication()).getRepoProvider()
                            .getConfirmationRepository(SharedPreferencesManager.getCredentials(getApplicationContext()).getToken());
            confirmationRepository.orderShowed((int) order.getId());
        }

        binding.newOrderAccept.setOnStateChangeListener(new OnStateChangeListener() {
            @Override
            public void onStateChange(boolean active) {
                Log.d(TAG, "onStateChange: ");
                binding.newOrderAccept.setEnabled(false);
                binding.pbTakingOrder.setVisibility(View.VISIBLE);
                loggingManager.addLog("NewOrderActivity", "newOrderAccept onStateChange",
                        "accepting order", LogTags.OTHER);
                if (isTimeToCustomerCalculated.get()) {
                    acceptOrder(order);
                } else {
                    Toast.makeText(getApplicationContext(), "Вычисляем время до пассажира, подождите немного...",
                            Toast.LENGTH_SHORT).show();
                    isTryingToStart = true;
                }
            }
        });

        isTimeToCustomerCalculated.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                if (isTimeToCustomerCalculated.get() & isTryingToStart) {
                    acceptOrder(order);
                }
            }
        });

        connectStatusListener = new ConnectionStatusManager.ConnectStatusListener() {
            @Override
            public void netAvailable() {
                hideNoNetError();
            }

            @Override
            public void netUnAvailable() {
                noNetError();
            }

            @Override
            public void serverUnAvailable() {
                noNetError();
            }

            @Override
            public void tokenInvalid() {

            }
        };
    }

    private void acceptOrder(Order order) {
        soundManager.stopNewOrderSound();
        final RideRepository rideRepository =
                ((ManfredApplication)getApplication()).getRepoProvider()
                        .getRideRepository(SharedPreferencesManager.getCredentials(getApplicationContext()).getToken());
        AcceptingOrder accOrder = new AcceptingOrder(order.getId(),calculatedDurationInSec);
        ElapsedTimeLogger elapsedTimeLogger = new ElapsedTimeLogger(loggingManager,System.currentTimeMillis(), "accept_order");
        rideRepository.acceptOrder(accOrder, new RideRepository.AcceptCallback() {
            @Override
            public void error(ManfredError manfredError) {
                elapsedTimeLogger.finish();
                Log.d(TAG, "take order, error: "+manfredError.getText());
                String textError = "Ошибка" + manfredError.getText();
                manfredSnackbarManager.showSnack(SnackbarManager.Type.ORDER_TAKE_BY_OTHER_DRIVER);
                loggingManager.addLog("NewOrderActivity", "error", textError, LogTags.OTHER);
                tripManager.cancelRide(order, new TripManager.Reason("server_error", manfredError.getText()));
                binding.pbTakingOrder.setVisibility(View.GONE);
            }

            @Override
            public void accepted(Order accOrder) {
                elapsedTimeLogger.finish();
/*                NotificationHelper
                        .removeAllNotificationImmediately((NotificationManager)
                                getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE));*/
                notificationManager.hideAllNotification();
                //manfredOrderManager.setCurrentOrder(accOrder);
                loggingManager.addLogSilence("NewOrderActivity",
                        "accepted", String.valueOf(accOrder.getId()), LogTags.OTHER);
                tripManager.startRideToPassenger(accOrder);
            }

            @Override
            public void debugMessage(String message) {
                //Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
                loggingManager.addLog("NewOrderActivity", "acceptOrder debugMessage",
                        message, LogTags.OTHER);
            }

            @Override
            public void serverNotAvailable() {
                //Toast.makeText(getApplicationContext(), "Сервер недоступен", Toast.LENGTH_SHORT).show();
                elapsedTimeLogger.finish();
                manfredSnackbarManager.showSnack(SnackbarManager.Type.ORDER_TAKE_BY_OTHER_DRIVER);
                loggingManager.addLog("NewOrderActivity", "acceptOrder",
                        "serverNotAvailable", LogTags.OTHER);
                tripManager.cancelRide(order, new TripManager.Reason("server not available", "сервер недоступен"));
                finish();
            }
        });
    }

    private void cancelOrder(@Nullable TripManager.Reason reason) {
        soundManager.stopNewOrderSound();
        if(reason==null){
            finish();
            return;
        }
        Log.d(TAG, "cancelOrder: by reason "+reason);
        switch (reason.getType()) {
            case "cancelled_driver":
                //we need stop because it cancelled by away swyper
                Log.d(TAG, "it is cancelled by driver(away)");
                break;
            case "other_driver_take_order":
                manfredSnackbarManager.showSnack(SnackbarManager.Type.ORDER_TAKE_BY_OTHER_DRIVER);
                finish();
                break;
            case "order_cancelled_by_passenger":
                manfredSnackbarManager.showSnack(SnackbarManager.Type.ORDER_CANCELLED_BY_PASSENGER);
                finish();
                break;
            case "server_error":
                manfredSnackbarManager.showSnack(SnackbarManager.Type.ORDER_TAKE_BY_OTHER_DRIVER);
                finish();
                break;
            case "order_cancelled_by_operator":
                manfredSnackbarManager.showSnack(SnackbarManager.Type.ORDER_CANCEL_BY_OPERATOR);
                finish();
                break;
                default:
                    manfredSnackbarManager.showSnack(SnackbarManager.Type.ORDER_CANCELLED_BY_PASSENGER);
                    finish();
                    break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Order order = manfredOrderManager.getCurrentOrder();
        //tripManager.addChangeStatusListener(tripManagerListener);
        connectionStatusManager.addConnectStatusListener(connectStatusListener);
        loggingManager.addLog("NewOrderActivity", "onResume", "NewOrder showed " +
                (order != null ? order.getId() : "null"), LogTags.NEW_ORDER_SHOWED);

        Log.d(TAG, "onCreate: order is " + (order != null ? order.toString() : " is null"));
        if (order == null) {
            manfredSnackbarManager.showSnack(SnackbarManager.Type.ORDER_CANCELLED_BY_PASSENGER);
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        loggingManager.addLog("NewOrderActivity", "onPause", "", LogTags.USER_AWAY_FROM_SCREEN);
        //tripManager.removeChangeStatusListener(tripManagerListener);
        connectionStatusManager.removeConnectStatusListener(connectStatusListener);
    }

    private void noNetError() {
        RelativeLayout noInet = findViewById(R.id.no_inet);
        noInet.setVisibility(View.VISIBLE);
        noInet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(NewOrderActivity.this,
                        R.anim.slid_down));
                v.setVisibility(View.GONE);
                checkInetStatus();
            }
        });
    }

    private void hideNoNetError() {
        RelativeLayout noInet = findViewById(R.id.no_inet);
        noInet.setVisibility(View.GONE);

    }

    private void checkInetStatus() {
        if (!connectionStatusManager.isNetAvailable()) {
            noNetError();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.getUiSettings().setAllGesturesEnabled(false);
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                Rect rectf = new Rect();
                binding.newOrderAccept.getLocalVisibleRect(rectf);
                googleMap.setPadding(0,0,0,rectf.bottom);
                showMarkers(mMap,
                        manfredLocationManager.getCurrentLocation(),
                        order);
            }
        });
        if(isTimeToCustomerCalculated.get()) {
            Location destinationLocation = new Location("");
            destinationLocation.setLatitude(order.getLatitude());
            destinationLocation.setLongitude(order.getLongitude());
            drawDirectionOnMap(mMap, directionResults, manfredLocationManager.getCurrentLocation(), destinationLocation);
        }
    }

    private void showMarkers(@NonNull GoogleMap map, Location currLoc, @Nullable Order order) {
        Log.d(TAG, "showMarkers: map is "+map);
        //if (isTimeToCustomerCalculated.get()) return;
        if (currLoc == null) return;
        if(order==null){
            manfredSnackbarManager.showSnack(SnackbarManager.Type.ORDER_TAKE_BY_OTHER_DRIVER);
            finish();
            return;
        }

        MarkerOptions currMarkerOptions = new MarkerOptions();
        currMarkerOptions.position(LocationUtils.LocationToLatLng(currLoc));
        currMarkerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.my_location));
        map.addMarker(currMarkerOptions);

        double destLat=order.getLatitude();
        double destLong=order.getLongitude();
        LatLng destinationLatLng = new LatLng(destLat, destLong);

        MarkerOptions destinationMarkerOptions = new MarkerOptions();
        destinationMarkerOptions.position(destinationLatLng);
        destinationMarkerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.picup));
        map.addMarker(destinationMarkerOptions);

        //zoom to markers
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(destinationLatLng);
        builder.include(LocationUtils.LocationToLatLng(currLoc));
        LatLngBounds bounds = builder.build();

        Log.d(TAG, "showMarkers: bounds is "+bounds.toString());
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (height * 0.10); // offset from edges of the map in pixels
        Log.d(TAG, "showMarkers: paddings is "+padding);
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        map.moveCamera(cu);

    }

    private void calculateTimeToCustomer(@Nullable Location currLoc, OrderViewModel orderViewModel, Location destinationLocation) {
        if(currLoc==null){
            Toast.makeText(getApplicationContext(),"Ошибка расчета расстояния, текущая координата не определена ",Toast.LENGTH_LONG).show();
            isTimeToCustomerCalculated.set(true);
            return;
        }
        final RideRepository rideRepository =
                ((ManfredApplication)getApplication()).getRepoProvider()
                        .getRideRepository(SharedPreferencesManager.getCredentials(getApplicationContext()).getToken());
        rideRepository.getDirection(new GeoData(currLoc), new GeoData(destinationLocation),
                new RideRepository.RepoAnswer<DirectionResults>() {
            @Override
            public void allOk(DirectionResults answer) {
                if(mMap!=null) {
                    drawDirectionOnMap(mMap, answer, currLoc, destinationLocation);
                }
                showTimeToCustomer(answer, orderViewModel);
                isTimeToCustomerCalculated.set(true);
                directionResults=answer;
            }

            @Override
            public void error(String error) {
                Toast.makeText(getApplicationContext(),"Ошибка расчета расстояния: "+error,Toast.LENGTH_LONG).show();
                isTimeToCustomerCalculated.set(true);
            }
        });
    }

    private void showTimeToCustomer(DirectionResults answer, OrderViewModel orderViewModel) {
        TimeCalculatorFromDirection timeCalculatorFromDirection = new TimeCalculatorFromDirection(answer);
        int durationInMin = timeCalculatorFromDirection.getCalculatedTripProperties().getDurationInSec()/60;
        int distanceInMeters = timeCalculatorFromDirection.getCalculatedTripProperties().getDistanceInMeters();
        calculatedDurationInSec=timeCalculatorFromDirection.getCalculatedTripProperties().getDurationInSec();
        Log.d(TAG, "allOk: distanceInMeters "+distanceInMeters);
        Log.d(TAG, "showTimeToCustomer: timeToCostumer " + durationInMin);
        String calculatedLogString = "calculated: timeToCostumer " + durationInMin;
        loggingManager.addLogSilence(TAG, "calculated", calculatedLogString, LogTags.OTHER);
        orderViewModel.setDurationInMin(durationInMin);
        Order currOrder = manfredOrderManager.getCurrentOrder();
        if (currOrder != null) {
            currOrder.setTime_to_costumer(durationInMin * 60);
        }
        //manfredOrderManager.setCurrentOrder(currOrder);
        int distanceInKm = distanceInMeters / 1000;
        orderViewModel.distanceToCostumer.set("~" + distanceInKm + " км");

        SpannableString timeWithMinText = new SpannableString(durationInMin+" МИН");
        timeWithMinText.setSpan(new TextAppearanceSpan(NewOrderActivity.this, R.style.bigText),
                0, timeWithMinText.length()-4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        timeWithMinText.setSpan(new TextAppearanceSpan(NewOrderActivity.this, R.style.smallText),
                timeWithMinText.length()-3, timeWithMinText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        orderViewModel.timeInTripWithMin.set(timeWithMinText);
        SpannableString distanceWithMinText = new SpannableString(distanceInKm+" КМ");
        distanceWithMinText.setSpan(new TextAppearanceSpan(NewOrderActivity.this, R.style.bigText),
                0, distanceWithMinText.length()-4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        distanceWithMinText.setSpan(new TextAppearanceSpan(NewOrderActivity.this, R.style.smallText),
                distanceWithMinText.length()-3, distanceWithMinText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        orderViewModel.distanceInTripWithKm.set(distanceWithMinText);
    }

    private void drawDirectionOnMap(@NonNull GoogleMap map, DirectionResults answer, Location currLoc, Location destinationLocation) {
        Log.d(TAG, "drawDirectionOnMap: "+answer);
        PolylineDirectionCalculator polylineDirectionCalculator = new PolylineDirectionCalculator(answer);
        PolylineOptions polylineOptions = polylineDirectionCalculator.getPolylineOptions();
        polylineOptions.width(16);
        polylineOptions.color(Color.parseColor("#1976D2")).geodesic(true).zIndex(8);
        Polyline routeLine = map.addPolyline(polylineOptions);
        polylineOptions.width(14);
        polylineOptions.color(Color.parseColor("#2196F3")).geodesic(true).zIndex(8);
        map.addPolyline(polylineOptions);
        LatLng destinationLatLng = new LatLng(destinationLocation.getLatitude(),destinationLocation.getLongitude());
        fitToPolylineAndMarkers(routeLine, new LatLng(currLoc.getLatitude(),currLoc.getLongitude()),destinationLatLng);
    }

    private void fitToPolylineAndMarkers(@Nullable Polyline polyline,LatLng driverPoint, LatLng passengerPoint) {
        if (mMap == null) return;
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        if (polyline!=null && !polyline.getPoints().isEmpty()) {
            //Log.d(TAG, "fitToPolylineAndMarkers: line is "+polyline.getPoints());
            for (int i = 0; i < polyline.getPoints().size(); i++) {
                builder.include(polyline.getPoints().get(i));
            }
        }
        //Log.d(TAG, "fitToPolylineAndMarkers: driver is "+driverPoint.toString());
        //Log.d(TAG, "fitToPolylineAndMarkers: pasenger is "+passengerPoint.toString());
        builder.include(driverPoint);
        builder.include(passengerPoint);
        LatLngBounds bounds = builder.build();
        final int width = getResources().getDisplayMetrics().widthPixels;
        final int height = getResources().getDisplayMetrics().heightPixels;
        final int minMetric = Math.min(width, height);
        int padding = (int) (minMetric * 0.30); // offset from edges of the map in pixels

        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                mMap.moveCamera(cu);
            }
        });

    }

}
