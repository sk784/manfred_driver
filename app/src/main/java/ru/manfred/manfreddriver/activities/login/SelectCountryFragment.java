/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.activities.login;


import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.model.api.CountryModel;
import ru.manfred.manfreddriver.utils.CountryDiffutilCallback;
import ru.manfred.manfreddriver.utils.generators.country_generator.CountryGenerator;

/**
 * A simple {@link Fragment} subclass.
 */
public class SelectCountryFragment extends Fragment {
    private OnListFragmentInteractionListener mListener;

    public SelectCountryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_select_country, container, false);
        Context context = view.getContext();
        InputStream raw = getResources().openRawResource(R.raw.flags);
        Reader rd = new BufferedReader(new InputStreamReader(raw));
        final List<CountryModel> countryModels = CountryGenerator.generateCountries(rd, Resources.getSystem().getConfiguration().locale);
        RecyclerView recyclerView = view.findViewById(R.id.list);
        // Set the adapter
        final List<CountryModel> filteredCountries = new ArrayList<>(countryModels);
        final MyCountryItemRecyclerViewAdapter adapter = new MyCountryItemRecyclerViewAdapter(filteredCountries, mListener);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(adapter);

        ((ManfredApplication)getActivity().getApplication()).getManfredLoginManger().getFilteredString().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if (s == null) return;
                List<CountryModel> beforeChangerCountries = new ArrayList<>(filteredCountries);
                filteredCountries.clear();
                if (s.isEmpty()) {
                    filteredCountries.addAll(countryModels);
                } else {
                    for (CountryModel p : countryModels) {
                        if (p.getCountryName().toLowerCase().contains(s.toLowerCase())) {
                            filteredCountries.add(p);
                        }
                    }
                }
                //Log.d(TAG, "onChanged: after filtering we have " + filteredCountries.size() + " countries");
                CountryDiffutilCallback productDiffUtilCallback =
                        new CountryDiffutilCallback(beforeChangerCountries, filteredCountries);
                DiffUtil.DiffResult productDiffResult = DiffUtil.calculateDiff(productDiffUtilCallback);
                productDiffResult.dispatchUpdatesTo(adapter);
            }
        });

        return view;
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(CountryModel item);
    }

}
