/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import org.jetbrains.annotations.Nullable;

import javax.inject.Inject;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.managers.sound.ManfredSoundManager;
import ru.manfred.manfreddriver.utils.Constants;

/**
 * Created by Sergey on 12.07.2015.
 * Activity in front plan
 */

public class ModalActivity extends Activity {
    @Inject
    protected ManfredSoundManager soundManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ManfredApplication.getTripComponent().inject(this);
        //final Activity dialogActivity = this;

        Intent passedIntent = getIntent();
        @Nullable  String reason = passedIntent.getStringExtra(Constants.CANCEL_STRING);
        boolean cancelTrip = passedIntent.getBooleanExtra(Constants.NEED_CANCEL_TRIP, false);

        AlertDialog alertDialog = new AlertDialog.Builder(ModalActivity.this, R.style.MyDialogTheme).create();
        alertDialog.setTitle("Проблемы с заказом");
        alertDialog.setMessage(reason);
        alertDialog.setCancelable(false);
        alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                soundManager.stopPlaySound();
                if (cancelTrip) {
                    Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                    startActivity(intent);
                } else {
                    finish();
                }
            }
        });
        alertDialog.show();
    }
}
