/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.activities.login;


import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.databinding.FragmentEnterNumberBinding;
import ru.manfred.manfreddriver.managers.NewManfredLoginManger;
import ru.manfred.manfreddriver.model.responces.Phone;


/**
 * A simple {@link Fragment} subclass.
 */
public class EnterNumberFragment extends Fragment {
    NewManfredLoginManger manfredLoginManger;
    private static final String TAG = "EnterNumberFragment";

    public EnterNumberFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentEnterNumberBinding binding = FragmentEnterNumberBinding.inflate(getLayoutInflater());
        init(binding);
        return binding.getRoot();
    }

    private void init(FragmentEnterNumberBinding binding) {
        manfredLoginManger = ((ManfredApplication)getActivity().getApplication()).getManfredLoginManger();
        binding.registerPhoneNumberContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manfredLoginManger.selectRegionCode();
            }
        });

        manfredLoginManger.getIsNumberValid().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean!=null) {
                    binding.registerPhoneButtonContinue.setEnabled(aBoolean);
                }
            }
        });

        manfredLoginManger.getPhone().observe(this, new Observer<Phone>() {
            @Override
            public void onChanged(@Nullable Phone phone) {
                if(phone!=null) {
                    binding.setRegion(phone.getCountryModel());
                }
            }
        });

        binding.registerPhoneButtonContinue.setOnClickListener(v -> {
            Log.d(TAG, "register button click: ");
            manfredLoginManger.attemptLogin();
        });

        initEdit(binding.registerPhoneNumberEdit);
        binding.btnToManfred.setOnClickListener(v->{
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://driver.manfred.ru/"));
            startActivity(browserIntent);
        });
    }






    private void initEdit(final EditText registerPhoneNumberEdit) {
        //Log.d(TAG, "initEdit: ");
        registerPhoneNumberEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                manfredLoginManger.validateNumber(registerPhoneNumberEdit.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        registerPhoneNumberEdit.setTransformationMethod(new NumericKeyBoardTransformationMethod());
    }

    class NumericKeyBoardTransformationMethod extends PasswordTransformationMethod {
        @Override
        public CharSequence getTransformation(CharSequence source, View view) {
            return source;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(getView()!=null) {
            EditText numberInput = getView().findViewById(R.id.register_phone_number_edit);
            numberInput.requestFocus();
        }
    }
}
