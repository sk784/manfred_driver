/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.activities.history;

/**
 * Created by begemot on 27.03.18.
 */

public interface ServerAvailableStatus {
    void notAvailable();

    void isAvailable();
}
