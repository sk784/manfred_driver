/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.activities;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.ebanx.swipebtn.OnStateChangeListener;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.text.NumberFormat;
import java.util.Date;

import javax.inject.Inject;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.databinding.ActivityFinalBinding;
import ru.manfred.manfreddriver.interactor.repository.DriverRepository;
import ru.manfred.manfreddriver.interactor.repository.RideRepository;
import ru.manfred.manfreddriver.managers.ConnectionStatusManager;
import ru.manfred.manfreddriver.managers.ManfredConnectionStatusManager;
import ru.manfred.manfreddriver.managers.ManfredLocationManager;
import ru.manfred.manfreddriver.managers.ManfredOrderManager;
import ru.manfred.manfreddriver.managers.ManfredTripManager;
import ru.manfred.manfreddriver.managers.TripManager;
import ru.manfred.manfreddriver.managers.history.ManfredHistoryManager;
import ru.manfred.manfreddriver.managers.logging.LogTags;
import ru.manfred.manfreddriver.managers.logging.ManfredLoggingManager;
import ru.manfred.manfreddriver.managers.preorder.ManfredPreOrderManager;
import ru.manfred.manfreddriver.model.api.Credentials;
import ru.manfred.manfreddriver.model.api.GeoData;
import ru.manfred.manfreddriver.model.api.ManfredError;
import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.model.api.TimeToArrivalLiveDate;
import ru.manfred.manfreddriver.utils.Constants;
import ru.manfred.manfreddriver.utils.ElapsedTimeLogger;
import ru.manfred.manfreddriver.utils.MapUtils;
import ru.manfred.manfreddriver.utils.SharedPreferencesManager;
import ru.manfred.manfreddriver.viewmodel.OrderViewModel;

//https://github.com/ebanx/swipe-button/

public class FinalActivity extends AppCompatActivity {
    private static final String TAG = "FinalActivity";
    private boolean isCanBack=true;
    ActivityFinalBinding binding;
    @Nullable GeoData geoData;
    //Order order;
    //TripManager.TripManagerListener tripManagerListener;
    ConnectionStatusManager.ConnectStatusListener connectStatusListener;
    @Inject
    ManfredTripManager tripManager;
    @Inject
    protected ManfredOrderManager orderManager;
    @Inject
    ManfredPreOrderManager preOrderManager;
    @Inject
    ManfredConnectionStatusManager connectionStatusManager;
    @Inject
    ManfredLoggingManager loggingManager;
    @Inject
    ManfredLocationManager manfredLocationManager;


    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(FinalActivity.this, R.layout.activity_final);
        //Appsee.start("a100b7525aa44320aecef66730b4bf04");
        ManfredApplication.getTripComponent().inject(this);

        setStatusBarColor(this);
        Toolbar myToolbar =
                (Toolbar) findViewById(R.id.include2);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(R.color.app_bar_finish)));
        getSupportActionBar().setCustomView(R.layout.custom_action_bar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        myToolbar.setElevation(0);
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(shouldAllowBack()) {
                    finish();
                }
            }
        });

        TextView title = findViewById(R.id.tvTitle);
        if (orderManager.getCurrentOrder() != null) {
            Order currOrder = orderManager.getCurrentOrder();
            Log.d(TAG, "finishTrip: curr order is "+currOrder.toString());
            title.setTextColor(getResources().getColor(R.color.colorWhite));
            title.setText("Заказ " + currOrder.getId());
            String currSymbol = currOrder.getCurrency_symbol();
            Log.d(TAG, "curr order symbol is "+currSymbol);
            binding.tvCurrencySymbol.setText(currSymbol);
            binding.tvRoundedSymbol.setText(currOrder.getCurrency_symbol());
            OrderViewModel orderViewModel = new OrderViewModel(currOrder);
            binding.setOrder(orderViewModel);
        }else {
            Log.d(TAG, "finishTrip: curr order is null");
        }
        //LiveData<Location> liveData = new LocationLiveData(getApplicationContext());
        //order = orderManager.getCurrentOrder();


        binding.swipeBtn.setOnStateChangeListener(new OnStateChangeListener() {
            @Override
            public void onStateChange(boolean active) {
                loggingManager.addLog("FinalActivity", "onStateChange", "finalizzze order swiped",
                        LogTags.ORDER_FINISH_CLICK);
                finishTrip();
                binding.swipeBtn.setEnabled(false);
                binding.progressBar2.setVisibility(View.VISIBLE);
                isCanBack=false;
            }
        });


        RideRepository rideRepository = ((ManfredApplication)getApplication()).getRepoProvider().getRideRepository(SharedPreferencesManager
                .getCredentials(getApplicationContext()).getToken());
        rideRepository.getActualCost(new RideRepository.AcceptCallback() {
            @Override
            public void error(ManfredError manfredError) {
                Toast.makeText(getApplicationContext(),"невозможно получить текущую цену по причине "+manfredError.getText(),Toast.LENGTH_LONG).show();
            }

            @Override
            public void accepted(@org.jetbrains.annotations.Nullable Order order) {
                if(order!=null) {
                    NumberFormat nf = NumberFormat.getInstance();
                    nf.setMaximumFractionDigits(2);

                    Log.d(TAG, "onResponse: cost is " + order.getTotal_cost());
                    binding.tvDuration.setText(order.getTotal_cost().toString());
                }
            }

            @Override
            public void debugMessage(String message) {

            }

            @Override
            public void serverNotAvailable() {

            }
        });

        //Intent intent = getIntent();
        //geoData = (GeoData) intent.getSerializableExtra("location");
        if (manfredLocationManager.getCurrentLocation() != null) {
            geoData = new GeoData(manfredLocationManager.getCurrentLocation());
        }else {
            loggingManager.addLog("FinalActivity", "onCreate",
                    "we take null location from manfredLocationManager, truing take current from FusedLocationClient...",
                    LogTags.OTHER);
            FusedLocationProviderClient mFusedLocationClient;
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                geoData = new GeoData(location);
                                loggingManager.addLog("FinalActivity", "onSuccess",
                                        "we take location from FusedLocationClient "+location.toString(), LogTags.OTHER);
                            }else {
                                loggingManager.addLog("FinalActivity", "onSuccess",
                                        "FusedLocationClient return null location(", LogTags.OTHER);
                            }
                        }
                    });
        }

        tripManager.getCurrentState().observe(this, new Observer<TripManager.Status>() {
            @Override
            public void onChanged(@Nullable TripManager.Status status) {
                if(status!=null){
                    if (status.getStatusState() != null) {
                        switch (status.getStatusState()) {
                            case OFFER_ADDITIONAL_PAYMENT_ERROR:
                                Intent noMoneyIntent = new Intent(getApplicationContext(), NoMoneyActivity.class);
                                if (status.getOrder().getPayment_error_time() != null) {
                                    Log.d(TAG, "tripManagerListener: Payment_error_time is null");
                                    noMoneyIntent.putExtra(Constants.TIME_OF_PAYMENT_ERROR, status.getOrder().getPayment_error_time().getTime());
                                }
                                //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(noMoneyIntent);
                                break;
                            case COMPLETED:
                                Intent finishIntent = new Intent(FinalActivity.this, MapsActivity.class);
                                startActivity(finishIntent);
                                break;

                        }
                    }
                }
            }
        });

/*        tripManagerListener = (status) -> {
            if (status.getStatusState() != null) {
                switch (status.getStatusState()) {
                    case OFFER_ADDITIONAL_PAYMENT_ERROR:
                        Intent noMoneyIntent = new Intent(getApplicationContext(), NoMoneyActivity.class);
                        if (status.getOrder().getPayment_error_time() != null) {
                            Log.d(TAG, "tripManagerListener: Payment_error_time is null");
                            noMoneyIntent.putExtra(Constants.TIME_OF_PAYMENT_ERROR, status.getOrder().getPayment_error_time().getTime());
                        }
                        //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(noMoneyIntent);
                        break;
                    case COMPLETED:
                        Intent finishIntent = new Intent(FinalActivity.this, MapsActivity.class);
                        startActivity(finishIntent);
                        break;

                }
            }
        };*/

        connectStatusListener = new ConnectionStatusManager.ConnectStatusListener() {
            @Override
            public void netAvailable() {
                hideNoNetError();
            }

            @Override
            public void netUnAvailable() {
                noNetError();
            }

            @Override
            public void serverUnAvailable() {
                noNetError();
            }

            @Override
            public void tokenInvalid() {

            }
        };
    }

    @Override
    public void onBackPressed() {
        if (shouldAllowBack()) {
            super.onBackPressed();
        }
    }

    private boolean shouldAllowBack() {
        Log.d(TAG, "shouldAllowBack: "+isCanBack);
        return isCanBack;
    }

    public void setStatusBarColor(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.new_order_statusbar_color));
        }
    }

    @SuppressLint("MissingPermission")
    public void finishTrip() {
        //Log.d(TAG, "finishTrip: to server sended adress "+geoData.getAddress());
        binding.swipeBtn.setEnabled(false);
        Location currLocation = new Location("");
        if(geoData!=null) {
            currLocation.setLongitude(geoData.getLongitude());
            currLocation.setLatitude(geoData.getLatitude());
            Credentials credentials = SharedPreferencesManager.getCredentials(getApplicationContext());
            DriverRepository driverRepository =((ManfredApplication)getApplication()).getRepoProvider().getDriverRepository(credentials.getToken());
            MapUtils.getAddressFromLocation(currLocation, driverRepository, new MapUtils.LocationAddressListener() {
                @Override
                public void addressRecognised(String address) {
                    String logString = "finishing trip, address recognised: " + address;
                    loggingManager.addLog("FinalActivity", "finishTrip", logString, LogTags.OTHER);
                    geoData.setAddress(address);
                    sendFinishToServer(geoData, orderManager.getCurrentOrder());
                }

                @Override
                public void errorRecognising(String error) {
                    String logString = "set final adress to unknown";
                    loggingManager.addLog("FinalActivity", "errorRecognising " +error, logString, LogTags.OTHER);
                    geoData.setAddress("unknown");
                    sendFinishToServer(geoData, orderManager.getCurrentOrder());
                }
            });
        }else {
            sendFinishToServer(new GeoData(0,0,new Date().getTime(),"Ошибка GPS",0),
                    orderManager.getCurrentOrder());
        }
        TimeToArrivalLiveDate.get(null).reset();
    }

    private void sendFinishToServer(GeoData currGeoData, Order order) {
        String logString = "finishing trip, geodata is " + currGeoData.toString();
        loggingManager.addLog("FinalActivity", "sendFinishToServer", logString, LogTags.OTHER);
        Credentials credentials = SharedPreferencesManager.getCredentials(getApplicationContext());
        if (credentials != null) {
            String token = credentials.getToken();
            RideRepository rideRepository =
                    ((ManfredApplication)getApplication()).getRepoProvider().getRideRepository(token);
            ElapsedTimeLogger elapsedTimeLogger = new ElapsedTimeLogger(loggingManager,System.currentTimeMillis(), "finish_trip");
            rideRepository.finishTrip(geoData,new RideRepository.RepoAnswer() {
                @Override
                public void allOk(Object answer) {
                    elapsedTimeLogger.finish();
                    tripManager.finishRide(order, "finished by driver");
                    order.setStatus("COMPLETED");
                    preOrderManager.removePreOrder(order);
                    ManfredHistoryManager historyManager =
                            new ManfredHistoryManager(((ManfredApplication)getApplication()).getRepoProvider().getDriverRepository(token));
                    historyManager.invalidateCache();
                    //it is for showing orders was too close
                    preOrderManager.forceRefresh();
                }

                @Override
                public void error(String error) {
                    elapsedTimeLogger.finish();
                    Toast.makeText(getApplicationContext(), "Ошибка :"+error, Toast.LENGTH_SHORT).show();
                    binding.progressBar2.setVisibility(View.GONE);
                    isCanBack=true;
                }
            });
        } else {
            Toast.makeText(getApplicationContext(), "Ошибка чтения токена, перезапустите приложение", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //tripManager.addChangeStatusListener(tripManagerListener);
        connectionStatusManager.addConnectStatusListener(connectStatusListener);
        loggingManager.addLog("FinalActivity", "onResume", "", LogTags.USER_ON_SCREEN);
        if(orderManager.getCurrentOrder()==null) {
            Intent finishIntent = new Intent(FinalActivity.this, MapsActivity.class);
            startActivity(finishIntent);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        //tripManager.removeChangeStatusListener(tripManagerListener);
        connectionStatusManager.removeConnectStatusListener(connectStatusListener);
    }

    private void noNetError() {
        binding.noInternetMessage.setVisibility(View.VISIBLE);
        if (binding.swipeBtn.isActive()) {
            binding.swipeBtn.toggleState();
        }
        binding.swipeBtn.setEnabled(false);
        binding.noInternetMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(FinalActivity.this,
                        R.anim.slid_down));
                v.setVisibility(View.GONE);
                checkInetStatus();
            }
        });
    }

    private void hideNoNetError() {
        //RelativeLayout noInet=findViewById(R.id.no_inet);
        binding.swipeBtn.setEnabled(true);
        binding.noInternetMessage.setVisibility(View.GONE);

    }

    private void checkInetStatus() {
        if (!connectionStatusManager.isNetAvailable()) {
            noNetError();
        }
    }
}
