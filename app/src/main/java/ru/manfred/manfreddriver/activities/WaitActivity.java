/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.activities;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.databinding.ActivityWaitBinding;
import ru.manfred.manfreddriver.interactor.repository.RideRepository;
import ru.manfred.manfreddriver.managers.ManfredLocationManager;
import ru.manfred.manfreddriver.managers.SnackbarManager;
import ru.manfred.manfreddriver.managers.TripManager;
import ru.manfred.manfreddriver.managers.logging.LogTags;
import ru.manfred.manfreddriver.managers.logging.ManfredLoggingManager;
import ru.manfred.manfreddriver.model.GoogleMaps.DirectionResults;
import ru.manfred.manfreddriver.model.api.GeoData;
import ru.manfred.manfreddriver.model.api.ManfredError;
import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.model.api.TimeInTripLiveData;
import ru.manfred.manfreddriver.utils.ElapsedTimeLogger;
import ru.manfred.manfreddriver.utils.MapUtils;
import ru.manfred.manfreddriver.utils.PolylineDirectionCalculator;
import ru.manfred.manfreddriver.utils.RestoreTripInSession;
import ru.manfred.manfreddriver.utils.SharedPreferencesManager;
import ru.manfred.manfreddriver.utils.UIUtils;
import ru.manfred.manfreddriver.viewmodel.OrderViewModel;

import static ru.manfred.manfreddriver.utils.MapUtils.fitToPolylineAndMarkers;

public class WaitActivity extends BaseActivity implements OnMapReadyCallback {
    private static final String TAG = "WaitActivity";
    long lastRefreshRoute = 0;
    Polyline line1;
    Polyline line2;
    Order order;
    //int tripTime;
    ActivityWaitBinding binding;
    /*@Inject
    ManfredTripManager tripManager;*/
    ObservableBoolean showComment = new ObservableBoolean(true);
    @Inject
    ManfredLocationManager manfredLocationManager;
    @Inject
    ManfredLoggingManager loggingManager;
    Marker customerMaker;
    Marker destinationMarker;
    private GoogleMap mMap;
    private Marker currentLoc;

    @Override
    TripManager.StatusState getStatusOfActivity() {
        return TripManager.StatusState.WAIT_PASSENGER;
    }

    @Override
    void orderUpdated(Order order) {
        if(timeInTripLiveData==null) {
            timeInTripLiveData = new TimeInTripLiveData(order.getWait_passenger_time());
            timeInTripLiveData.observe(this, new Observer<Integer>() {
                @Override
                public void onChanged(@Nullable Integer integer) {
                    if (integer != null) {
                        TextView duration = findViewById(R.id.action_bar_tv_time);
                        duration.setText(String.valueOf(integer));
                    }
                }
            });
        }else {
            timeInTripLiveData.updateStartTime(order.getWait_passenger_time());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ManfredApplication.getTripComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_wait);
        super.onCreateDrawer();

        final OrderViewModel orderVM;
        order = orderManager.getCurrentOrder();
        Log.d(TAG, "onCreate: location manager is " + manfredLocationManager);
        if (order == null) {
            ProgressDialog progressDialog = new ProgressDialog(WaitActivity.this);
            progressDialog.setTitle("Восстановление заказа...");
            progressDialog.show();
            //unsubscribeBase();
            RestoreTripInSession restoreTripInSession = new RestoreTripInSession();
            restoreTripInSession.restoreTrip(this);
        } else {
            orderVM = new OrderViewModel(order);
            binding.setOrder(orderVM);
            initActivity(orderVM);
        }

        binding.moreLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showComment.set(!showComment.get());
            }
        });
        showComment.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                binding.comment.setVisibility(showComment.get() ? View.VISIBLE : View.GONE);
                binding.tvComments.setVisibility(showComment.get() ? View.VISIBLE : View.GONE);
                binding.view2.setVisibility(showComment.get() ? View.VISIBLE : View.GONE);
                binding.moreLess.setImageResource(showComment.get() ? R.drawable.less : R.drawable.more);
            }
        });

        manfredLocationManager.getLocation().observe(this, new Observer<Location>() {
            @Override
            public void onChanged(@Nullable Location location) {
                updateLocation(order, location);
            }
        });

    }

    @Override
    void showSnack(@NonNull SnackbarManager.Snack snack) {
        UIUtils.generateSnack(binding.getRoot(), snack).show();
    }

    @Override
    String getActivityName() {
        return "wait";
    }

    TimeInTripLiveData timeInTripLiveData;
    private void initActivity(OrderViewModel orderVM) {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.trip_map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        timeInTripLiveData = new TimeInTripLiveData(order.getWait_passenger_time());
        timeInTripLiveData.observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                if (integer != null) {
                    TextView duration = findViewById(R.id.action_bar_tv_time);
                    duration.setText(String.valueOf(integer));
                }
            }
        });

        binding.rlCallingArea.setOrder(orderVM);
        binding.rlCallingArea.callingAreaTap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //String phone = "+"+orderVM.getCustomerNumber();
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", orderVM.getCustomerNumber(), null));
                startActivity(intent);
            }
        });

        binding.btnLetsRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: btnLetsRide");
                String destAdress = order.getDest_address().isEmpty() ? "уточнить у пассажира" : order.getDest_address();
                new AlertDialog.Builder(WaitActivity.this, R.style.MyDialogTheme)
                        .setTitle("Подтвердите поездку")
                        .setMessage("Адрес пункта назначения - " + destAdress)
                        .setNegativeButton("Отмена", null)
                        .setPositiveButton("Подтверждаю", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                RideRepository rideRepository =
                                        ((ManfredApplication) getApplication()).getRepoProvider()
                                                .getRideRepository(SharedPreferencesManager.getCredentials(getApplicationContext()).getToken());
                                final ProgressDialog progress = new ProgressDialog(WaitActivity.this);
                                progress.setMessage("Начинаем поездку...");
                                progress.setCancelable(false);
                                progress.setCanceledOnTouchOutside(false);
                                progress.show();
                                ElapsedTimeLogger elapsedTimeLogger = new ElapsedTimeLogger(loggingManager, System.currentTimeMillis(), "start_ride");
                                loggingManager.addLog("WaitActivity", "onClick", "", LogTags.ORDER_START_RIDE_CLICK);
                                rideRepository.startTrip(new RideRepository.AcceptCallback() {
                                    @Override
                                    public void error(ManfredError manfredError) {
                                        elapsedTimeLogger.finish();
                                        if (manfredError.getResultCode().equals("invalid_token")) {
                                            progress.dismiss();
                                            showDlgForcedCancelOrder(order, "Возможно кто-то зашел под вашими учетными данными",
                                                    new TripManager.Reason("server_error", manfredError.getText()));
                                        } else {
                                            Toast.makeText(getApplicationContext(), manfredError.getText(), Toast.LENGTH_LONG).show();
                                            progress.dismiss();
                                        }
                                    }

                                    @Override
                                    public void accepted(Order order) {
                                        elapsedTimeLogger.finish();
                                        orderVM.setRide_start_time(new Date());
                                        progress.dismiss();
                                        tripManager.startRideToDestination(WaitActivity.this.order);
                                    }

                                    @Override
                                    public void debugMessage(String message) {

                                    }

                                    @Override
                                    public void serverNotAvailable() {
                                        elapsedTimeLogger.finish();
                                        progress.dismiss();
                                        internetOff();
                                    }
                                });
                            }
                        })
                        .show();
            }
        });

        initToolbar();

        binding.noInternetMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkConnectionWithServer(v, "WAIT_PASSENGER");
            }
        });
    }

    private void updateLocation(Order order, Location commonLocation) {
        if (commonLocation == null) return;
        if (order == null) return;
        Location customerLocation = new Location("");
        customerLocation.setLatitude(order.getLatitude());
        customerLocation.setLongitude(order.getLongitude());
        Location destinationLocation = null;
        if (!order.getDest_latitude().equals("")) {
            destinationLocation = new Location("");
            destinationLocation.setLatitude(Double.parseDouble(order.getDest_latitude()));
            destinationLocation.setLongitude(Double.parseDouble(order.getDest_longitude()));
        }

        currentLoc = MapUtils.showMarkerOnMap(currentLoc, commonLocation, mMap);
        customerMaker = MapUtils.showCustomerMarkerOnMap(null, commonLocation, customerLocation, mMap, getApplicationContext());
        destinationMarker = MapUtils.showDestinationMarkerOnMap(destinationLocation, commonLocation, destinationLocation, mMap, getApplicationContext(),
                false);

        if (lastRefreshRoute == 0) {
            lastRefreshRoute = System.currentTimeMillis();
            showPathOnMap(commonLocation, destinationLocation);
        }
        long timeFromLastRefresh = System.currentTimeMillis() - lastRefreshRoute;

        if (timeFromLastRefresh > TripToPassengerActivity.ROUTE_REFRESH_TIME_MS) {
            lastRefreshRoute = System.currentTimeMillis();
            showPathOnMap(commonLocation, destinationLocation);
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setAllGesturesEnabled(false);
        updateLocation(order, manfredLocationManager.getCurrentLocation());
    }

    public void initToolbar() {
        Toolbar myToolbar = findViewById(R.id.include);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.ride_action_bar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView title = findViewById(R.id.tvTitle);
        title.setText("Заказ " + order.getId());
        TextView titleInTrip = findViewById(R.id.action_bar_tv_ride_title);
        titleInTrip.setText("ожидание");
    }


    @Override
    public void onBackPressed() {
    }

    public void showPathOnMap(Location currLocation, Location destLocation) {
        if (destLocation == null) return;
        RideRepository rideRepository =
                ((ManfredApplication) getApplication()).getRepoProvider().getRideRepository(SharedPreferencesManager.getCredentials(getApplicationContext()).getToken());
        rideRepository.getDirection(new GeoData(currLocation), new GeoData(destLocation), new RideRepository.RepoAnswer<DirectionResults>() {
            @Override
            public void allOk(DirectionResults results) {
                PolylineDirectionCalculator polylineDirectionCalculator = new PolylineDirectionCalculator(results);
                PolylineOptions polylineOptions = polylineDirectionCalculator.getPolylineOptions();
                if (line1 != null) {
                    line1.remove();
                    line2.remove();
                }
                polylineOptions.width(16);
                polylineOptions.color(Color.parseColor("#1976D2")).geodesic(true).zIndex(8);
                line1 = mMap.addPolyline(polylineOptions);
                polylineOptions.width(14);
                polylineOptions.color(Color.parseColor("#2196F3")).geodesic(true).zIndex(8);
                line2 = mMap.addPolyline(polylineOptions);
                List<Marker> markers = new ArrayList<>();
                markers.add(customerMaker);
                markers.add(currentLoc);
                markers.add(destinationMarker);
                mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
                        fitToPolylineAndMarkers(line1, mMap, getResources(),markers);
                    }
                });

            }

            @Override
            public void error(String error) {

            }
        });
    }


    @Override
    protected void internetOff() {
        Log.d(TAG, "internetOff: ");
        binding.noInternetMessage.setVisibility(View.VISIBLE);
        binding.btnLetsRide.setVisibility(View.GONE);
    }

    @Override
    protected void internetOn() {
        Log.d(TAG, "internetOn: ");
        binding.noInternetMessage.setVisibility(View.GONE);
        binding.btnLetsRide.setVisibility(View.VISIBLE);
    }

    @Override
    protected void setBusy() {

    }

    @Override
    protected void setFree() {

    }

}
