/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.activities.preorders;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.activities.history.ServerAvailableStatus;
import ru.manfred.manfreddriver.adapters.PreOrderAdapter;
import ru.manfred.manfreddriver.managers.logging.LogTags;
import ru.manfred.manfreddriver.managers.logging.ManfredLoggingManager;
import ru.manfred.manfreddriver.managers.preorder.ManfredPreOrderManager;
import ru.manfred.manfreddriver.managers.preorder.PreOrderManager;
import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.utils.Constants;
import ru.manfred.manfreddriver.utils.ListItemDiffutilCallback;
import ru.manfred.manfreddriver.utils.PreOrdersDateSorter;
import ru.manfred.manfreddriver.viewmodel.ListItem;


public class PreorderFragment extends Fragment {
    List<ListItem> preOrders = new ArrayList<>();
    private static final String TAG = "PreorderFragment";
    @Inject
    ManfredPreOrderManager preOrderManager;
    @Inject
    ManfredLoggingManager loggingManager;
    PreOrderManager.PreOrdersListener preOrdersListener;
    //PreOrderManager.PreOrderListChangedListener preOrderListChangedListener;
    ServerAvailableStatus serverAvailableStatus;
    //private ProgressBar progressBar;

    public PreorderFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: ");
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_preorder, container, false);
        ManfredApplication.getTripComponent().inject(this);
        RecyclerView recyclerView = view.findViewById(R.id.preorder_recycler);
        ProgressBar progressBar = view.findViewById(R.id.preorder_progress_bar);

        final boolean isMy;
        isMy = getArguments() != null && getArguments().getBoolean(Constants.PREORDER_MY);


        /*
        LastAdapter adapter = new LastAdapter(preOrders, BR.preOrder)
                .map(PreOrderHolderVM.class, isMy ? R.layout.my_preorder_holder : R.layout.preorder_holder)
                .map(DateItemVM.class, R.layout.pre_order_date_holder)
                .into(recyclerView);*/
        //showNoPreorders(view);
        PreOrderAdapter adapter = new PreOrderAdapter(preOrders,isMy);
        Log.d(TAG, "onCreateView: preorder " + preOrders.size());

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.onSaveInstanceState();
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);

        preOrderManager.getIsRefreshing().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean!=null){
                    Log.d(TAG, "getIsRefreshing onChanged: "+aBoolean);
                    if(aBoolean){
                        progressBar.setVisibility(View.VISIBLE);
                    }else {
                        progressBar.setVisibility(View.GONE);
                    }
                }
            }
        });

        if(isMy){
            preOrderManager.getMyPreOrders().observe(this, new Observer<List<Order>>() {
                @Override
                public void onChanged(@Nullable List<Order> orders) {
                    if (orders != null) {
                        Log.d(TAG, "onChanged: MyPreOrders changed to "+orders.size());
                    }
                    PreOrdersDateSorter preOrdersDateSorter = new PreOrdersDateSorter(orders,isMy);
                    ListItemDiffutilCallback diffutilCallback = new ListItemDiffutilCallback(preOrders,preOrdersDateSorter.getItems());
                    DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffutilCallback);
                    preOrders=preOrdersDateSorter.getItems();
                    adapter.setItemsS(preOrdersDateSorter.getItems());
                    diffResult.dispatchUpdatesTo(adapter);
                    showNoPreorders(view);
                }
            });
        }else {
            preOrderManager.getNewPreOrders().observe(this, new Observer<List<Order>>() {
                @Override
                public void onChanged(@Nullable List<Order> orders) {
                    if (orders != null) {
                        Log.d(TAG, "onChanged: newPreOrders changed to "+orders.size());
                    }
                    PreOrdersDateSorter preOrdersDateSorter = new PreOrdersDateSorter(orders,isMy);
                    ListItemDiffutilCallback diffutilCallback = new ListItemDiffutilCallback(preOrders,preOrdersDateSorter.getItems());
                    DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffutilCallback,true);
                    adapter.setItemsS(preOrdersDateSorter.getItems());
                    diffResult.dispatchUpdatesTo(adapter);
                    preOrders=preOrdersDateSorter.getItems();
                    showNoPreorders(view);
                }
            });
        }

        preOrdersListener = preOrderEvent -> {
            PreOrdersDateSorter preOrdersDateSorter = new PreOrdersDateSorter(isMy ? preOrderManager.getMyPreOrders().getValue() :
                    preOrderManager.getNewPreOrders().getValue(), isMy);
            switch (preOrderEvent.getEventStatus()) {
                case NEW_PRE_ORDER:
                    //preOrders.clear();
                    //preOrders.addAll(preOrdersDateSorter.getItems());
                    loggingManager.addLog("PreorderFragment", "onCreateView", "preorder showed", LogTags.NEW_PREORDER_SHOWED);
                    showNoPreorders(view);
                    preOrderManager.eventProcessedSuccessfully(preOrderEvent);
                    break;
/*                case PRE_ORDER_ACCEPTED:
                    Log.d(TAG, "preOrderAccepted: ");
                    preOrders.clear();
                    preOrders.addAll(preOrdersDateSorter.getItems());
                    showNoPreorders(view);
                    preOrderManager.eventProcessedSuccessfully(preOrderEvent);
                    break;*/
                case PRE_ORDER_DECLINED:
                    Log.d(TAG, "preOrderDeclined: ");
                    preOrders.clear();
                    preOrders.addAll(preOrdersDateSorter.getItems());
                    showNoPreorders(view);
                    preOrderManager.eventProcessedSuccessfully(preOrderEvent);
                    break;
            }
        };

        return view;
    }

    private void showNoPreorders(View view) {
        //Log.d(TAG, "showNoPreorders: in view "+view);
        TextView tvNoPreOrders = view.findViewById(R.id.fragment_preorder_no_preorders);
        if (preOrders.isEmpty()) {
            //Log.d(TAG, "showNoPreorders: we have no preorders, show message");
            tvNoPreOrders.setVisibility(View.VISIBLE);
        } else {
            //Log.d(TAG, "showNoPreorders: hiding preorders");
            tvNoPreOrders.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity a;
        if (context instanceof Activity) {
            a = (Activity) context;
            try {
                serverAvailableStatus = (ServerAvailableStatus) a;
            } catch (ClassCastException e) {
                throw new ClassCastException(getParentFragment().toString() + " must implement " + ServerAvailableStatus.class.getName());
            }
        }
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume: ");
        super.onResume();
        preOrderManager.addChangePreOrderListener(preOrdersListener);
        //preOrderManager.addPreOrderListChangedListener(preOrderListChangedListener);
        //preOrderManager.recalculateAll();
        //preOrderManager.forceRefresh();

    }

    @Override
    public void onStop() {
        super.onStop();
        preOrderManager.removeChangePreOrderListener(preOrdersListener);
        //preOrderManager.removePreOrderListChangedListener(preOrderListChangedListener);
    }


}
