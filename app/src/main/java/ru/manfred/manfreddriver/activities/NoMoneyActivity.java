/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.ebanx.swipebtn.OnStateChangeListener;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.databinding.ActivityNoMoneyBinding;
import ru.manfred.manfreddriver.interactor.api.ManfredResultCallback;
import ru.manfred.manfreddriver.interactor.repository.DriverRepository;
import ru.manfred.manfreddriver.interactor.repository.RideRepository;
import ru.manfred.manfreddriver.managers.ManfredOrderManager;
import ru.manfred.manfreddriver.managers.ManfredSnackbarManager;
import ru.manfred.manfreddriver.managers.ManfredTripManager;
import ru.manfred.manfreddriver.managers.SnackbarManager;
import ru.manfred.manfreddriver.managers.TripManager;
import ru.manfred.manfreddriver.managers.logging.LogTags;
import ru.manfred.manfreddriver.managers.logging.ManfredLoggingManager;
import ru.manfred.manfreddriver.managers.preorder.ManfredPreOrderManager;
import ru.manfred.manfreddriver.model.api.Credentials;
import ru.manfred.manfreddriver.model.api.GeoData;
import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.utils.Constants;
import ru.manfred.manfreddriver.utils.MapUtils;
import ru.manfred.manfreddriver.utils.SharedPreferencesManager;
import ru.manfred.manfreddriver.utils.TimeDateUtils;

public class NoMoneyActivity extends AppCompatActivity {
    private static final String TAG = "NoMoneyActivity";
    private static final long COUNTDOWN_TIMER_VALUE = 10 * 60 * 1000;
    private static final String COUNTDOWN_START_AT = "countdown_state";
    private long timerStart = 0;
    @Inject
    ManfredSnackbarManager manfredSnackbarManager;

    @Inject
    ManfredTripManager tripManager;
    @Inject
    protected ManfredOrderManager orderManager;
    @Inject
    ManfredPreOrderManager preOrderManager;
    @Inject
    ManfredOrderManager manfredOrderManager;
    @Inject
    ManfredLoggingManager loggingManager;
    /*@Inject
    ManfredSoundManager manfredSoundManager;
    */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityNoMoneyBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_no_money);
        ManfredApplication.getTripComponent().inject(this);
        //  NotificationHelper.removeAllNotificationImmediately((ActivityNotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE));
        if (savedInstanceState == null) {
            Intent intent = getIntent();
            timerStart = intent.getLongExtra(Constants.TIME_OF_PAYMENT_ERROR, new Date().getTime());
            long dateDelta = new Date().getTime() - timerStart;
            initTimer(binding, COUNTDOWN_TIMER_VALUE - dateDelta);
        } else {
            Log.d(TAG, "onCreate: saved value of start " + savedInstanceState.getLong(COUNTDOWN_START_AT));
            timerStart = savedInstanceState.getLong(COUNTDOWN_START_AT);
            long dateDelta = new Date().getTime() - timerStart;
            Log.d(TAG, "onCreate: restoring state, delta is " + dateDelta + " ms");
            Log.d(TAG, "onCreate: restoring state, delta is " + TimeUnit.MILLISECONDS.toMinutes(dateDelta) + " minutes");
            initTimer(binding, COUNTDOWN_TIMER_VALUE - dateDelta);
        }

        loggingManager.addLog("NoMoneyActivity", "onCreate", "", LogTags.USER_ON_SCREEN);

        RideRepository rideRepository =
                ((ManfredApplication)getApplication()).getRepoProvider().getRideRepository(SharedPreferencesManager.getCredentials(getApplicationContext()).getToken());
        binding.noMoneyTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProgressDialog progressDialog = new ProgressDialog(NoMoneyActivity.this);
                progressDialog.setTitle("Проверяем оплату...");
                progressDialog.show();
                rideRepository.checkPayment(new ManfredResultCallback() {
                    @Override
                    public void error(String title, String message) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void allOk(Order order) {
                        progressDialog.dismiss();
                        //finish();
                        //проблема в том, что нужен предыдущий статус
                        tripManager.paymentSuccessful(order,
                                new TripManager.Reason("payment_successfull", "all nice after check"));
                        manfredSnackbarManager.showSnack(SnackbarManager.Type.PAYMENT_SUCCESSFUL);
                    }

                    @Override
                    public void debugMessage(String message) {
                        progressDialog.dismiss();
                        loggingManager.addLog("NoMoneyActivity", "noMoneyTryAgain", message, LogTags.OTHER);
                        //Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void serverNotAvailable() {
                        Toast.makeText(getApplicationContext(), "Сервер недоступен", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        binding.noMoneyCancelTrip.setOnStateChangeListener(new OnStateChangeListener() {
            @Override
            public void onStateChange(boolean active) {
                binding.noMoneyCancelTrip.setEnabled(false);
                FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(NoMoneyActivity.this);
                if (ActivityCompat.checkSelfPermission(NoMoneyActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    sendFinishToServer(new GeoData(0, 0, new Date(), 0));
                    return;
                }
                mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        GeoData geoData = new GeoData(location.getLongitude(), location.getLatitude(), new Date(), location.getAccuracy());
                        Credentials credentials = SharedPreferencesManager.getCredentials(getApplicationContext());
                        DriverRepository driverRepository =((ManfredApplication)getApplication()).getRepoProvider().getDriverRepository(credentials.getToken());
                        MapUtils.getAddressFromLocation(location, driverRepository, new MapUtils.LocationAddressListener() {
                            @Override
                            public void addressRecognised(String address) {
                                Log.d(TAG, "addressRecognised: " + address);
                                geoData.setAddress(address);
                                sendFinishToServer(geoData);
                            }

                            @Override
                            public void errorRecognising(String error) {
                                Log.d(TAG, "errorRecognising: "+error);
                                geoData.setAddress("unknown");
                                sendFinishToServer(geoData);
                            }
                        });
                    }
                });

            }
        });

        tripManager.getCurrentState().observe(this, new Observer<TripManager.Status>() {
            @Override
            public void onChanged(@Nullable TripManager.Status status) {
                if (status != null) {
                    if (status.getStatusState() != null) {
                        switch (status.getStatusState()) {
                            case CANCELLED:
                                Intent intent = new Intent(NoMoneyActivity.this, MapsActivity.class);
                                startActivity(intent);
                                break;
                        }
                    }
                    if (status.getReason() != null) {
                        if (status.getReason().getType().equals("payment_successfull")) {
                            manfredSnackbarManager.showSnack(SnackbarManager.Type.PAYMENT_SUCCESSFUL);
                            finish();
                        }
                    }
                }
            }
        });

/*        tripManager.addChangeStatusListener(new TripManager.TripManagerListener() {
            @Override
            public void newStatus(TripManager.Status status) {
                if (status.getStatusState() != null) {
                    switch (status.getStatusState()) {
                        case CANCELLED:
                            Intent intent = new Intent(NoMoneyActivity.this, MapsActivity.class);
                            startActivity(intent);
                            break;
                    }
                }
                if (status.getReason() != null) {
                    if (status.getReason().getType().equals("payment_successfull")) {
                        manfredSnackbarManager.showSnack(SnackbarManager.Type.PAYMENT_SUCCESSFUL);
                        finish();
                    }
                }
            }
        });*/
    }

    private void sendFinishToServer(GeoData currGeoData) {
        RideRepository rideRepository =
                ((ManfredApplication)getApplication()).getRepoProvider().getRideRepository(SharedPreferencesManager.getCredentials(getApplicationContext()).getToken());
        rideRepository.finishTrip(currGeoData, new RideRepository.RepoAnswer() {
            @Override
            public void allOk(Object answer) {
                Log.d(TAG, "onResponse: finish trip");
                //DriverRepository.clearCache();
                tripManager.finishRide(manfredOrderManager.getCurrentOrder(), "finish by driver");
                preOrderManager.removePreOrder(manfredOrderManager.getCurrentOrder());
                Intent intent = new Intent(NoMoneyActivity.this, MapsActivity.class);
                //it is for showing orders was too close
                preOrderManager.forceRefresh();
                startActivity(intent);
            }

            @Override
            public void error(String error) {
                Toast.makeText(getApplicationContext(),"ошибка: "+error,Toast.LENGTH_LONG).show();
            }
        });
        Log.d(TAG, "sendFinishToServer: finishing trip, geodata is " + currGeoData.toString());
    }

    private void initTimer(ActivityNoMoneyBinding binding, long countdownMillis) {
        Log.d(TAG, "initTimer: " + TimeUnit.MILLISECONDS.toMinutes(countdownMillis) + " minutes");
        if (timerStart == 0) {
            timerStart = new Date().getTime();
            Log.d(TAG, "initTimer: start at " + timerStart);
        }
        if (countdownMillis < 0) countdownMillis = 0;
        new CountDownTimer(countdownMillis, TimeUnit.SECONDS.toMillis(1)) {
            public void onTick(long millisUntilFinished) {
                binding.noMoneyTimer.setText(TimeDateUtils.msToHoursMins(millisUntilFinished));
            }

            public void onFinish() {

            }
        }.start();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        Log.d(TAG, "onSaveInstanceState: timer start at " + timerStart);
        savedInstanceState.putLong(COUNTDOWN_START_AT, timerStart);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
    }
}
