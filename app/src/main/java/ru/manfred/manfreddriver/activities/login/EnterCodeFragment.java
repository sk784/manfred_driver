/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.activities.login;


import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.iid.FirebaseInstanceId;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.databinding.FragmentEnterCodeBinding;
import ru.manfred.manfreddriver.managers.NewManfredLoginManger;
import ru.manfred.manfreddriver.model.responces.Phone;

/**
 * A simple {@link Fragment} subclass.
 */
public class EnterCodeFragment extends Fragment {
    NewManfredLoginManger manfredLoginManger;
    private static final String TAG = "EnterCodeFragment";

    public EnterCodeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentEnterCodeBinding binding = FragmentEnterCodeBinding.inflate(getLayoutInflater());
        init(binding);

        return binding.getRoot();
    }

    private void init(FragmentEnterCodeBinding binding) {
        manfredLoginManger = ((ManfredApplication)getActivity().getApplication()).getManfredLoginManger();
        manfredLoginManger.getPhone().observe(this, new Observer<Phone>() {
            @Override
            public void onChanged(@Nullable Phone phone) {
                binding.setPhone(phone);
            }
        });

        manfredLoginManger.getAuthState().observe(this, new Observer<NewManfredLoginManger.AuthState>() {
            @Override
            public void onChanged(@Nullable NewManfredLoginManger.AuthState authState) {
                if(authState!=null){
                    if(authState== NewManfredLoginManger.AuthState.FINISH){
                        //prevent memory lick
                        countDownTimer.cancel();
                        countDownTimer=null;
                    }
                }
            }
        });

        initEditText(binding);
        binding.resendSms.setOnClickListener(v->{
            startTimer(binding.resendSms);
            manfredLoginManger.reSendSMS();
        });

        startTimer(binding.resendSms);
    }

    private void initEditText(FragmentEnterCodeBinding binding) {

        View.OnKeyListener secondListener = new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==KeyEvent.KEYCODE_DEL){
                    Log.d(TAG, "secondDigit: KEYCODE_DEL ");
                    v.setOnKeyListener(null);
                    v.post(new Runnable() {
                        @Override
                        public void run() {
                            binding.firstDigit.requestFocus();
                        }
                    });

                }
                return false;
            }
        };

        View.OnKeyListener thirdListener = new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==KeyEvent.KEYCODE_DEL){
                    Log.d(TAG, "thirdDigit: KEYCODE_DEL ");
                    v.setOnKeyListener(null);
                    v.post(new Runnable() {
                        @Override
                        public void run() {
                            binding.secondDigit.requestFocus();
                        }
                    });
                }
                return false;
            }
        };

        View.OnKeyListener fourListener = new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==KeyEvent.KEYCODE_DEL){
                    Log.d(TAG, "fourDigit: KEYCODE_DEL ");
                    v.setOnKeyListener(null);
                    v.post(new Runnable() {
                        @Override
                        public void run() {
                            binding.thirdDigit.requestFocus();
                        }
                    });
                }
                return false;
            }
        };

        binding.firstDigit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count>0){
                    binding.secondDigit.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.secondDigit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    Log.d(TAG, "onFocusChange: v.setOnKeyListener(secondListener)");
                    v.setOnKeyListener(secondListener);
                }
            }
        });
        binding.thirdDigit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    Log.d(TAG, "onFocusChange: v.setOnKeyListener(thirdListener)");
                    v.setOnKeyListener(thirdListener);
                }
            }
        });
        binding.fourDigit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    Log.d(TAG, "onFocusChange: v.setOnKeyListener(fourListener)");
                    v.setOnKeyListener(fourListener);
                }
            }
        });

        binding.secondDigit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count >= 1) {
                    binding.secondDigit.setOnKeyListener(null);
                    binding.thirdDigit.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.thirdDigit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count >= 1) {
                    Log.d(TAG, "thirdDigit: binding.fourDigit.requestFocus()");
                    binding.fourDigit.requestFocus();
                    binding.thirdDigit.setOnKeyListener(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.fourDigit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count >= 1) {
                    Log.d(TAG, "onTextChanged: we have full code, sending");
                    FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(instanceIdResult -> {
                        String deviceToken = instanceIdResult.getToken();
                        // Do whatever you want with your setToken now
                        // i.e. store it on SharedPreferences or DB
                        // or directly send it to server
                        String pin = binding.firstDigit.getText().toString()+
                                binding.secondDigit.getText().toString()+
                                binding.thirdDigit.getText().toString()+
                                binding.fourDigit.getText().toString();
                        manfredLoginManger.sendSms(pin,deviceToken);
                        //sentSMSToSocket(pin);

                    });
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    CountDownTimer countDownTimer;
    private void startTimer(final Button resendSms) {
        resendSms.setEnabled(false);
        countDownTimer=new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                resendSms.setText("Отправить снова (" + millisUntilFinished / 1000 + " сек)");
            }

            public void onFinish() {
                resendSms.setText("Отправить снова");
                resendSms.setEnabled(true);
            }
        }.start();
    }

    @Override
    public void onResume(){
        super.onResume();
        View view = getView();
        if(view!=null) {
            EditText firstDigit = view.findViewById(R.id.first_digit);
            firstDigit.requestFocus();
            firstDigit.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(getActivity()!=null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        if (imm != null) {
                            imm.showSoftInput(firstDigit, 0);
                        }
                    }
                }
            },100);
        }
    }

}
