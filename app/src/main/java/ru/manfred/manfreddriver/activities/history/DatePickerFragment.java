/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.activities.history;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CalendarView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import ru.manfred.manfreddriver.R;

/**
 * Created by begemot on 13.03.18.
 */

public class DatePickerFragment extends DialogFragment {
    private NoticeDialogListener.TypeDatePicker typeDatePicker;
    private Date startDate;
    private Date selectedDate;
    private static final String TAG = "DatePickerFragment";

    public interface NoticeDialogListener {
        enum TypeDatePicker {
            START_DATE,
            END_DATE
        }

        void onDialogPositiveClick(TypeDatePicker typeDatePicker, Date date);

        void onDialogNegativeClick(TypeDatePicker typeDatePicker);
    }

    NoticeDialogListener mListener;

    public static DatePickerFragment newInstance(NoticeDialogListener.TypeDatePicker typeDatePicker, Date startDate) {
        DatePickerFragment frag = new DatePickerFragment();
        Bundle args = new Bundle();
        args.putSerializable("type", typeDatePicker);
        args.putSerializable("startDate", startDate);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle mArgs = getArguments();
        if (mArgs != null) {
            typeDatePicker = (NoticeDialogListener.TypeDatePicker) mArgs.getSerializable("type");
            startDate = (Date) mArgs.getSerializable("startDate");
        }

        try {
            mListener = (NoticeDialogListener) getParentFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException("Calling fragment must implement Callback interface");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.data_picker_custom_view, null);
        CalendarView calendarView = dialogView.findViewById(R.id.data_picker_calendar);
        TextView dateCaption = dialogView.findViewById(R.id.data_picker_tv_date);
        TextView caption = dialogView.findViewById(R.id.data_picker_tv_caption);
        String posButtonCap = "ok";
        String negButtonCap = "cancel";
        selectedDate = new Date();
        Log.d(TAG, "onCreateDialog: date initialized by " + selectedDate);
        Log.d(TAG, "onCreateDialog: start date is " + startDate + ", " + typeDatePicker);
        switch (typeDatePicker) {
            case START_DATE:
                caption.setText("Дата начала");
                posButtonCap = "далее";
                negButtonCap = "отмена";
                break;
            case END_DATE:
                caption.setText("Дата окончания");
                posButtonCap = "ok";
                negButtonCap = "назад";
                calendarView.setMinDate(startDate.getTime());
                break;
        }
        calendarView.setMaxDate(new Date().getTime());

        //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, MMM d",new Locale("ru","RU"));

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                Log.d(TAG, "onSelectedDayChange: " + year + ", " + month + ", " + dayOfMonth);
                //selectedDate = new Date(year,month,dayOfMonth);
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, month);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                selectedDate = cal.getTime();
                Log.d(TAG, "onSelectedDayChange: date is " + selectedDate);
                //dateCaption.setText(simpleDateFormat.format(selectedDate));
                dateCaption.setText(getTextOfDate(selectedDate));
            }
        });
        //dateCaption.setText(simpleDateFormat.format(calendarView.getDate()));
        dateCaption.setText(getTextOfDate(new Date(calendarView.getDate())));

        return new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.DatePickDialog))
                .setPositiveButton(posButtonCap, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d(TAG, "onClick: positive " + selectedDate);
                        mListener.onDialogPositiveClick(typeDatePicker, selectedDate);
                    }
                })
                .setNegativeButton(negButtonCap, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onDialogNegativeClick(typeDatePicker);
                    }
                })
                .setView(dialogView)
                .create();
    }

    private String getTextOfDate(Date date) {
        //this sex because we need capitalise first letter
        SimpleDateFormat dayOfWeek = new SimpleDateFormat("EEE", new Locale("ru", "RU"));
        SimpleDateFormat month = new SimpleDateFormat("MMMM", new Locale("ru", "RU"));
        SimpleDateFormat day = new SimpleDateFormat("d", new Locale("ru", "RU"));
        String begin = dayOfWeek.format(date);
        begin = begin.substring(0, 1).toUpperCase() + begin.substring(1);
        String middle = month.format(date);
        middle = middle.substring(0, 1).toUpperCase() + middle.substring(1);
        int dotIndex = middle.lastIndexOf(".");
        if (dotIndex != -1) {
            middle = middle.substring(0, dotIndex);
        }
        String end = day.format(date);
        return begin + ", " + middle + " " + end;
    }
}
