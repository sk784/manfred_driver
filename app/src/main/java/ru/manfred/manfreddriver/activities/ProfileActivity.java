/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.activities;


import android.app.ActionBar;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import javax.inject.Inject;

import io.intercom.com.bumptech.glide.Glide;
import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.databinding.ActivityProfileBinding;
import ru.manfred.manfreddriver.managers.ConnectionStatusManager;
import ru.manfred.manfreddriver.managers.ManfredConnectionStatusManager;
import ru.manfred.manfreddriver.managers.ManfredTripManager;
import ru.manfred.manfreddriver.managers.TripManager;
import ru.manfred.manfreddriver.managers.logging.LogTags;
import ru.manfred.manfreddriver.managers.logging.ManfredLoggingManager;
import ru.manfred.manfreddriver.model.api.Driver;
import ru.manfred.manfreddriver.viewmodel.DriverViewModel;

public class ProfileActivity extends AppCompatActivity {

    private static final String TAG = "ProfileActivity";
    @Inject
    ManfredTripManager tripManager;
    @Inject
    ManfredConnectionStatusManager connectionStatusManager;
    @Inject
    ManfredLoggingManager loggingManager;

    //TripManager.TripManagerListener tripManagerListener;
    ConnectionStatusManager.ConnectStatusListener connectStatusListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final ActivityProfileBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_profile);
        ManfredApplication.getTripComponent().inject(this);
        //((ManfredApplication) getApplication()).setLastShownActivity(this);
        ((ManfredApplication)getApplication()).getCredentialsManager().getDriver().observe(this, new Observer<Driver>() {
            @Override
            public void onChanged(@Nullable Driver driver) {
                if(driver!=null) {
                    DriverViewModel driverViewModel = new DriverViewModel(driver);
                    //Log.d(TAG, "onCreateDrawer: " + driverViewModel.getName());
                    binding.setDriver(driverViewModel);
                    binding.ivAvatar.setClipToOutline(true);
                    loggingManager.addLog("ProfileActivity", "onCreate", "", LogTags.USER_ON_SCREEN);
                    Glide
                            .with(ProfileActivity.this)
                            .load(driverViewModel.getAvatarUrl())
                            .into(binding.ivAvatar);

                }
            }
        });
        //DriverViewModel driverViewModel = new DriverViewModel(SharedPreferencesManager.getDriver(getApplicationContext()));
        initToolbar();

        tripManager.getCurrentState().observe(this, new Observer<TripManager.Status>() {
            @Override
            public void onChanged(@Nullable TripManager.Status status) {
                if (status != null && status.getStatusState() != null) {
                    switch (status.getStatusState()) {
                        case OFFER_TO_RIDE:
                            final Intent intentNewOrder = new Intent(ProfileActivity.this, NewOrderActivity.class);
                            startActivity(intentNewOrder);
                            break;
                    }
                }
            }
        });

/*        tripManagerListener = new TripManager.TripManagerListener() {
            @Override
            public void newStatus(TripManager.Status incStatus) {
                if (incStatus.getStatusState() != null) {
                    switch (incStatus.getStatusState()) {
                        case OFFER_TO_RIDE:
                            final Intent intentNewOrder = new Intent(ProfileActivity.this, NewOrderActivity.class);
                            startActivity(intentNewOrder);
                            break;
                    }
                }
            }
        };*/

        connectStatusListener = new ConnectionStatusManager.ConnectStatusListener() {
            @Override
            public void netAvailable() {
                hideNoNetError();
            }

            @Override
            public void netUnAvailable() {
                noNetError();
            }

            @Override
            public void serverUnAvailable() {
                noNetError();
            }

            @Override
            public void tokenInvalid() {

            }
        };
    }

    public void initToolbar() {
        Toolbar myToolbar =
                (Toolbar) findViewById(R.id.include);

        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_action_bar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView title = (TextView) findViewById(R.id.tvTitle);
        title.setText("Профиль");
    }

    @Override
    public boolean onNavigateUp() {
        finish();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected: ");
        switch (item.getItemId()) {
            case android.R.id.home:
                Log.d(TAG, "onOptionsItemSelected: back button");
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //tripManager.addChangeStatusListener(tripManagerListener);
        connectionStatusManager.addConnectStatusListener(connectStatusListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //tripManager.removeChangeStatusListener(tripManagerListener);
        connectionStatusManager.removeConnectStatusListener(connectStatusListener);
    }

    private void noNetError() {
        RelativeLayout noInet = findViewById(R.id.no_inet);
        noInet.setVisibility(View.VISIBLE);
        noInet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(ProfileActivity.this,
                        R.anim.slid_down));
                v.setVisibility(View.GONE);
                checkInetStatus();
            }
        });
    }

    private void hideNoNetError() {
        RelativeLayout noInet = findViewById(R.id.no_inet);
        noInet.setVisibility(View.GONE);

    }

    private void checkInetStatus() {
        if (!connectionStatusManager.isNetAvailable()) {
            noNetError();
        }
    }
}
