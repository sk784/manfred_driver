/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.activities;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.databinding.ActivityTripBinding;
import ru.manfred.manfreddriver.interactor.api.ManfredResultCallback;
import ru.manfred.manfreddriver.interactor.repository.RideRepository;
import ru.manfred.manfreddriver.managers.ManfredLocationManager;
import ru.manfred.manfreddriver.managers.SnackbarManager;
import ru.manfred.manfreddriver.managers.TripManager;
import ru.manfred.manfreddriver.managers.logging.LogTags;
import ru.manfred.manfreddriver.managers.logging.ManfredLoggingManager;
import ru.manfred.manfreddriver.model.GoogleMaps.DirectionResults;
import ru.manfred.manfreddriver.model.api.GeoData;
import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.model.api.TimeToArrivalLiveDate;
import ru.manfred.manfreddriver.utils.ElapsedTimeLogger;
import ru.manfred.manfreddriver.utils.MapUtils;
import ru.manfred.manfreddriver.utils.PolylineDirectionCalculator;
import ru.manfred.manfreddriver.utils.RestoreTripInSession;
import ru.manfred.manfreddriver.utils.SharedPreferencesManager;
import ru.manfred.manfreddriver.utils.UIUtils;
import ru.manfred.manfreddriver.viewmodel.OrderViewModel;

import static ru.manfred.manfreddriver.utils.MapUtils.fitToPolylineAndMarkers;

public class TripToPassengerActivity extends BaseActivity implements OnMapReadyCallback {

    public static final int ROUTE_REFRESH_TIME_MS = 60 * 1000;
    private static final String TAG = "TripToPassengerActivity";
    //ActionBarDrawerToggle mDrawerToggle;
    //CancelTripReceiver cancelTripReceiver;
    Order order;
    long lastRefreshRoute = 0;
    Polyline line1;
    //TripManager.TripManagerListener tripManagerListener;
    Polyline line2;
    ActivityTripBinding binding;
    //private boolean needShowCurrentLocation = true;
    ObservableBoolean showComment = new ObservableBoolean(true);
    @Inject
    ManfredLocationManager manfredLocationManager;
    @Inject
    ManfredLoggingManager loggingManager;
    //private boolean mapReady = false;
    @Nullable
    private GoogleMap mMap;
    private Marker currentPositionMarker;
    private Marker customerMarker;

    @Override
    TripManager.StatusState getStatusOfActivity() {
        return TripManager.StatusState.RIDE_TO_PASSENGER;
    }

    @Override
    void orderUpdated(Order order) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_trip);
        super.onCreateDrawer();
        ManfredApplication.getTripComponent().inject(this);

        final OrderViewModel orderVM;
        //LiveData<Location> liveData = new LocationLiveData(getApplicationContext());
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.trip_map);
        mapFragment.getMapAsync(this);

        Toolbar myToolbar =
                (Toolbar) findViewById(R.id.include);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.ride_action_bar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        order = orderManager.getCurrentOrder();
        if (order == null) {
            forceRestoreTrip();
        } else {
            orderVM = new OrderViewModel(order);
            binding.setOrder(orderVM);
            initActivity(orderVM);
            TextView title = findViewById(R.id.tvTitle);
            title.setText("Заказ " + order.getId());
            TextView timeTitle = findViewById(R.id.action_bar_tv_ride_title);
            timeTitle.setText("прибытие");
        }

        binding.moreLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showComment.set(!showComment.get());
            }
        });
        showComment.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                binding.comment.setVisibility(showComment.get() ? View.VISIBLE : View.GONE);
                binding.tvComments.setVisibility(showComment.get() ? View.VISIBLE : View.GONE);
                binding.moreLess.setImageResource(showComment.get() ? R.drawable.less : R.drawable.more);
            }
        });

        binding.noInternetMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkConnectionWithServer(v, "RIDE_TO_PASSENGER");
            }
        });

    }

    @Override
    void showSnack(@NonNull SnackbarManager.Snack snack) {
        UIUtils.generateSnack(binding.getRoot(), snack).show();
    }

    @Override
    String getActivityName() {
        return "trip_to_passenger";
    }

    private void forceRestoreTrip() {
        ProgressDialog progressDialog = new ProgressDialog(TripToPassengerActivity.this);
        progressDialog.setTitle("Восстановление заказа...");
        progressDialog.show();
        //unsubscribeBase();
        RestoreTripInSession restoreTripInSession = new RestoreTripInSession();
        restoreTripInSession.restoreTrip(this);
        binding.btnArriveToCustomer.setEnabled(false);
        binding.btnNavigation.setEnabled(false);
    }

    private void initActivity(OrderViewModel orderVM) {
        manfredLocationManager.getLocation().observe(this, new Observer<Location>() {
            @Override
            public void onChanged(@Nullable Location location) {
                drawPathOnMap(location, orderVM);
            }
        });

        //manfredLocationManager.addLocationListener(locationChangedListener);
        //Log.d(TAG, "onCreate: dest time is "+orderVM.destinationTime.get());
        int deltaInSeconds;
        if (order.getCar_need_time() != null) {
            //preOrder
            deltaInSeconds = (int) java.util.concurrent.TimeUnit.MILLISECONDS
                    .toSeconds(new Date().getTime() - order.getPreorder_start_time().getTime());
        } else {
            //order
            deltaInSeconds = (int) java.util.concurrent.TimeUnit.MILLISECONDS
                    .toSeconds(new Date().getTime() - order.getAccept_order_time().getTime());
            Log.d(TAG, "initActivity: delta(from start to current) from current to start is " + deltaInSeconds);
        }
        int destTime = (int) java.util.concurrent.TimeUnit.SECONDS.toMinutes(order.getTime_to_costumer() - deltaInSeconds);
        Log.d(TAG, "initActivity: destTime is " + destTime + "min");
        if (destTime < 0) destTime = 1;
        TimeToArrivalLiveDate.get(destTime).observe(TripToPassengerActivity.this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                if (integer != null) {
                    TextView duration = findViewById(R.id.action_bar_tv_time);
                    duration.setText(String.valueOf(integer));
                }
            }
        });


        binding.rlCallingArea.setOrder(orderVM);
        binding.rlCallingArea.callingAreaTap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callClient(orderVM);
            }
        });
        binding.btnArriveToCustomer.setEnabled(true);
        //binding.btnArriveToCustomer.setEnabled(true);
        binding.btnArriveToCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //orderVM.getOrder().setRide_start_time(new Date());
                new AlertDialog.Builder(TripToPassengerActivity.this, R.style.MyDialogTheme)
                        .setTitle("Подтвердите прибытие")
                        .setMessage("Адрес подачи - " + orderVM.getAddress())
                        .setNegativeButton("Отмена", null)
                        .setPositiveButton("Подтверждаю", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                loggingManager.addLogSilence("TripToPassengerActivity", "onClick",
                                        "arrive clicked", LogTags.ORDER_ON_PASSENGER_CLICK);
                                final ProgressDialog progress = new ProgressDialog(TripToPassengerActivity.this);
                                progress.setMessage("Меняем статус...");
                                progress.setCancelable(false);
                                progress.setCanceledOnTouchOutside(false);
                                progress.show();
                                ElapsedTimeLogger elapsedTimeLogger = new ElapsedTimeLogger(loggingManager, System.currentTimeMillis(), "arrive_to_passenger");
                                RideRepository rideRepository =
                                        ((ManfredApplication) getApplication()).getRepoProvider().getRideRepository(SharedPreferencesManager.getCredentials(getApplicationContext()).getToken());
                                rideRepository.onClient(new ManfredResultCallback() {
                                    @Override
                                    public void error(String title, String message) {
                                        elapsedTimeLogger.finish();
                                        if (!TripToPassengerActivity.this.isFinishing()) {
                                            progress.dismiss();
                                        }
                                        Toast.makeText(getApplicationContext(), title + " " + message, Toast.LENGTH_SHORT).show();
                                    }

                                    @Override
                                    public void allOk(Order order) {
                                        elapsedTimeLogger.finish();
                                        tripManager.waitingPassenger(orderVM.getOrder());
                                        progress.dismiss();
                                    }

                                    @Override
                                    public void debugMessage(String message) {
                                        Log.d(TAG, "debugMessage: " + message);
                                    }

                                    @Override
                                    public void serverNotAvailable() {
                                        elapsedTimeLogger.finish();
                                        internetOff();
                                        progress.dismiss();
                                    }
                                });
                            }
                        })
                        .show();

            }
        });
        binding.btnNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loggingManager.addLog("TripToPassengerActivity", "btnNavigation onClick", "navigator start", LogTags.OTHER);
                MapUtils.startYandexNavigator(orderVM.getOrder().getLatitude(), orderVM.getOrder().getLongitude(), getApplicationContext());
            }
        });
        binding.btnLateness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(TripToPassengerActivity.this, LateActivity.class);
                startActivityForResult(intent1, 1);
            }
        });
    }

    private void drawPathOnMap(Location commonLocation, OrderViewModel orderVM) {
        if (mMap == null) return;
        //Log.d(TAG, "drawPathOnMap: ");
        if (orderVM == null) {
            forceRestoreTrip();
        } else {
            //String logString = "position is "+commonLocation;
            //loggingManager.addLog("TripToPassengerActivity", "drawPathOnMap", logString, LogTags.OTHER);
            if (commonLocation == null) return;
            Location customerLocation = new Location("");
            customerLocation.setLatitude(orderVM.getOrder().getLatitude());
            customerLocation.setLongitude(orderVM.getOrder().getLongitude());

            currentPositionMarker = MapUtils.showMarkerOnMap(currentPositionMarker, commonLocation, mMap);
            //MapUtils.showCustomerMarkerOnMap(null, commonLocation, customerLocation, mMap, getApplicationContext(), false);

            if (lastRefreshRoute == 0) {
                lastRefreshRoute = System.currentTimeMillis();
                showPathOnMap(commonLocation, customerLocation);
            }
            long timeFromLastRefresh = System.currentTimeMillis() - lastRefreshRoute;
            //Log.d(TAG, "onChanged: time from last refresh: "+(timeFromLastRefresh/1000));
            if (timeFromLastRefresh > ROUTE_REFRESH_TIME_MS) {
                Log.d(TAG, "drawPathOnMap: from last drawing " + timeFromLastRefresh / 1000 + "s, redrawing");
                lastRefreshRoute = System.currentTimeMillis();
                showPathOnMap(commonLocation, customerLocation);
            }
        }
    }

    private void callClient(OrderViewModel orderVM) {
        String logString = "onClick: trying call";
        loggingManager.addLog("TripToPassengerActivity", "callClient", logString, LogTags.OTHER);
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", orderVM.getCustomerNumber(), null));
        startActivity(intent);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.getUiSettings().setAllGesturesEnabled(false);
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        Location curLoc = manfredLocationManager.getCurrentLocation();
        if (curLoc != null) {
            drawPathOnMap(curLoc, binding.getOrder());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_CANCELED) {
            return;
        }
        int lateTime = data.getIntExtra("lateTime", 0) * 60;
        String logString = Integer.toString(lateTime);
        loggingManager.addLog("TripToPassengerActivity", logString, "", LogTags.OTHER);
        RideRepository rideRepository =
                ((ManfredApplication) getApplication()).getRepoProvider().getRideRepository(SharedPreferencesManager.getCredentials(getApplicationContext()).getToken());

        rideRepository.delayOnOrder(new ManfredResultCallback() {
            @Override
            public void error(String title, String message) {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }

            @Override
            public void allOk(Order order) {
                TimeToArrivalLiveDate.get(null).addLate(data.getIntExtra("lateTime", 0));
            }

            @Override
            public void debugMessage(String message) {
            }

            @Override
            public void serverNotAvailable() {
            }
        }, lateTime);
    }

    public void showPathOnMap(Location currLocation, Location destLocation) {
        if (destLocation == null) return;
        if (mMap == null) return;
        RideRepository rideRepository =
                ((ManfredApplication) getApplication()).getRepoProvider().getRideRepository(SharedPreferencesManager.getCredentials(getApplicationContext()).getToken());
        rideRepository.getDirection(new GeoData(currLocation), new GeoData(destLocation), new RideRepository.RepoAnswer<DirectionResults>() {
            @Override
            public void allOk(DirectionResults results) {
                //we can be close and direction not created
                PolylineDirectionCalculator polylineDirectionCalculator = new PolylineDirectionCalculator(results);
                PolylineOptions polylineOptions = polylineDirectionCalculator.getPolylineOptions();
                if (line1 != null) {
                    line1.remove();
                    line2.remove();
                }
                Log.d(TAG, "polylineReady: drawing...");
                polylineOptions.width(16);
                // and add the color of your line and other properties
                polylineOptions.color(Color.parseColor("#1976D2")).geodesic(true).zIndex(8);
                // finally draw the lines on the map
                line1 = mMap.addPolyline(polylineOptions);
                // change the width and color
                polylineOptions.width(14);
                polylineOptions.color(Color.parseColor("#2196F3")).geodesic(true).zIndex(8);
                line2 = mMap.addPolyline(polylineOptions);
                showCustomerMarker();
                List<Marker> markers = new ArrayList<>();
                markers.add(currentPositionMarker);
                markers.add(customerMarker);
                mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
                        fitToPolylineAndMarkers(line1, mMap, getResources(), markers);
                    }
                });

            }

            @Override
            public void error(String error) {

            }
        });
    }

    private void showCustomerMarker() {
        if (customerMarker == null) {
            LatLng latLng = new LatLng(order.getLatitude(), order.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.picup));
            customerMarker = mMap.addMarker(markerOptions);
        }
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void internetOff() {
        //Log.d(TAG, "internetOff: ");
        binding.noInternetMessage.setVisibility(View.VISIBLE);
        binding.noInternetMessage.clearAnimation();
        binding.btnNavigation.setVisibility(View.GONE);
        binding.btnArriveToCustomer.setVisibility(View.GONE);
    }

    @Override
    protected void internetOn() {
        binding.noInternetMessage.setVisibility(View.GONE);
        binding.btnNavigation.setVisibility(View.VISIBLE);
        binding.btnArriveToCustomer.setVisibility(View.VISIBLE);
    }

    @Override
    protected void setBusy() {

    }

    @Override
    protected void setFree() {

    }

}
