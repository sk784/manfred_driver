/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.activities.preorders;

import ru.manfred.manfreddriver.model.api.Order;

public interface PreOrderStarter {
    void startOrder(Order order);
}
