/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.activities.login;

import android.arch.lifecycle.Observer;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.inject.Inject;

import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.UserAttributes;
import io.intercom.android.sdk.identity.Registration;
import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.activities.MapsActivity;
import ru.manfred.manfreddriver.databinding.ActivityLoginBinding;
import ru.manfred.manfreddriver.interactor.repository.DriverRepository;
import ru.manfred.manfreddriver.interactor.repository.HTTPDriverRepository;
import ru.manfred.manfreddriver.interactor.repository.ResponseCallback;
import ru.manfred.manfreddriver.managers.ConnectionStatusManager;
import ru.manfred.manfreddriver.managers.ManfredConnectionStatusManager;
import ru.manfred.manfreddriver.managers.ManfredCredentialsManager;
import ru.manfred.manfreddriver.managers.sound.ManfredSoundManager;
import ru.manfred.manfreddriver.managers.logging.AuthError;
import ru.manfred.manfreddriver.managers.logging.LogTags;
import ru.manfred.manfreddriver.managers.logging.ManfredLoggingManager;
import ru.manfred.manfreddriver.managers.NewManfredLoginManger;
import ru.manfred.manfreddriver.model.api.CountryModel;
import ru.manfred.manfreddriver.model.api.Credentials;
import ru.manfred.manfreddriver.model.api.Driver;
import ru.manfred.manfreddriver.model.api.ManfredError;
import ru.manfred.manfreddriver.model.responces.DriverStatus;
import ru.manfred.manfreddriver.services.CheckEventService;
import ru.manfred.manfreddriver.utils.NetworkUtil;
import ru.manfred.manfreddriver.utils.RestoreTrip;
import ru.manfred.manfreddriver.utils.ServiceUtils;
import ru.manfred.manfreddriver.utils.SharedPreferencesManager;

import static ru.manfred.manfreddriver.utils.FCMUtils.invalidateAndGetNewFCMToken;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity
        implements SelectCountryFragment.OnListFragmentInteractionListener, FragmentManager.OnBackStackChangedListener{

    private static final String TAG = "LoginActivity";
    ConnectionStatusManager.ConnectStatusListener connectStatusListener;
    ActivityLoginBinding binding;
    private ManfredCredentialsManager manfredCredentialsManager;

    @Inject
    ManfredConnectionStatusManager connectionStatusManager;
    @Inject
    ManfredLoggingManager loggingManager;
    @Inject
    ManfredSoundManager soundManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_login);

        ManfredApplication.getTripComponent().inject(this);

        manfredCredentialsManager = ((ManfredApplication)getApplication()).getCredentialsManager();
        if (NetworkUtil.getConnectivityStatus(getApplicationContext()) == NetworkUtil.TYPE_NOT_CONNECTED) {
            noNetError();
        }

        final Toolbar toolbar = findViewById(R.id.auth_bar);
        initEdit(binding.authBarSearchCountry,((ManfredApplication)getApplication()).getManfredLoginManger());
        setSupportActionBar(toolbar);
        getSupportFragmentManager().addOnBackStackChangedListener(this);

        //startCheckOrderService();

        NewManfredLoginManger manfredLoginManger = ((ManfredApplication)getApplication()).getManfredLoginManger();

        manfredLoginManger.getAuthState().observe(this, authState -> {
            if(authState==null)return;
            Log.d(TAG, "getAuthState: "+authState);
            switch (authState){
                case INPUT_PHONE:
                    swapFragment(new EnterNumberFragment(),false);
                    makeToolbarIntercomOnly(binding);
                    break;
                case INPUT_SMS:
                    swapFragment(new EnterCodeFragment(),true);
                    makeToolbarIntercomOnly(binding);
                    break;
                case INPUT_REGION_CODE:
                    swapFragment(new SelectCountryFragment(),true);
                    makeToolbaWithSearch(binding);
                    break;
                case FINISH:
                    RefreshProfileAndStart();
                    break;
            }
        });

        manfredLoginManger.getAuthError().observe(this, new Observer<AuthError>() {
            @Override
            public void onChanged(@Nullable AuthError authError) {
                if(authError!=null){
                    Log.d(TAG, "AuthError: "+authError);
                    switch (authError.getErrorStatus()){
                        case INVALID_NUMBER:
                            showErrorDialog(authError.getText());
                            break;
                        case NETWORK_ERROR:
                            showErrorDialog(authError.getText());
                            break;
                        case INVALID_SMS_CODE:
                            showInvalidSMSDialog(authError.getText(),manfredLoginManger);
                            break;
                        case DRIVER_ARCHIVED:
                            showBlockedDriverDialog(authError.getText(),manfredLoginManger);
                            break;
                            default:
                                showErrorDialog(authError.getText());
                    }
                }
            }
        });

        manfredLoginManger.getIsLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean!=null){
                    Log.d(TAG, "getIsNumberValid: "+aBoolean);
                    binding.pbLoginLoading.setVisibility(aBoolean ? View.VISIBLE : View.GONE);
                }
            }
        });


        initIntercomButton(binding.registerPhoneIntercomButton);
        connectStatusListener = new ConnectionStatusManager.ConnectStatusListener() {
            @Override
            public void netAvailable() {
                hideNoNetError();
            }

            @Override
            public void netUnAvailable() {
                noNetError();
            }

            @Override
            public void serverUnAvailable() {
                noNetError();
            }

            @Override
            public void tokenInvalid() {

            }
        };
        soundManager.stopPlaySound();
    }

    private void checkForUpdate(){
        DriverRepository driverRepository = new HTTPDriverRepository("");
        driverRepository.checkForUpdate(new DriverRepository.NetAnswerCallback<Boolean>() {
            @Override
            public void allFine(Boolean needUpdate) {
                if(needUpdate){
                    showNeedUpdateDialog();
                }
            }

            @Override
            public void error(String error) {

            }

            @Override
            public void serverNotAvailable() {

            }
        });
    }

    private void showNeedUpdateDialog() {
        //stopCheckOrderService();
        new android.app.AlertDialog.Builder(this, R.style.MyDialogTheme)
                .setTitle("Доступно обновление")
                .setMessage("Доступна новая версия приложения Manfred. Обновитесь, чтобы принимать заказы.")
                .setCancelable(false)
                .setPositiveButton("Обновить", (dialog, whichButton) -> {
                    /* User clicked OK so do some stuff */
                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id="
                                + appPackageName)));
                    }
                })
                .show();
    }

    private void showErrorDialog(String text){
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.MyDialogTheme);
        builder.setTitle("Ошибка");
        builder.setMessage(text);
        builder.setPositiveButton("OK",null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showBlockedDriverDialog(String text, NewManfredLoginManger manfredLoginManger) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.MyDialogTheme);
        builder.setTitle("Вы заблокированы");
        builder.setMessage(text);
        builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                manfredLoginManger.reEnterPhone();
            }
        });
        builder.setNegativeButton("Связаться с оператором", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                startIntercomAnonymous();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showInvalidSMSDialog(String text, NewManfredLoginManger manfredLoginManger) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.MyDialogTheme);
        builder.setTitle("Неверный код из СМС");
        builder.setMessage(text);
        builder.setPositiveButton("Изменить номер", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                manfredLoginManger.reEnterPhone();
            }
        });
        builder.setNegativeButton("Ввести код еще раз", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void RefreshProfileAndStart() {
        Log.d(TAG, "RefreshProfileAndStart: ");
        ManfredApplication.setFirstStartAfterLogin(true);
        String token;
        token = ((ManfredApplication)getApplication()).getManfredLoginManger().getToken();
        Credentials credentials = null;
        String phoneNumber = ((ManfredApplication)getApplication()).getManfredLoginManger().getPhone().getValue().getFullNumber();
        Crashlytics.setUserIdentifier(phoneNumber);
        if (token != null) {
            credentials = new Credentials(phoneNumber, token);
        }else {
            Log.d(TAG, "RefreshProfileAndStart: setToken is null");
            credentials = new Credentials(phoneNumber, "");
        }
        SharedPreferencesManager.setCredentials(getApplicationContext(), credentials);
        manfredCredentialsManager.setCredentials(credentials);
        //Refresh driver profile
        DriverRepository driverRepository = ((ManfredApplication)getApplication()).getRepoProvider().getDriverRepository(credentials.getToken());
        driverRepository.driverInfo(new DriverRepository.infoCallback() {
            @Override
            public void fineCallback(Driver driver) {
                //SharedPreferencesManager.setDriver(driver, getApplicationContext());
                Log.d(TAG, "onResponse: driver setted " + driver.toString());
                loggingManager.addLog("ManfredLoginManager", "correctLogin", "logging driver " + driver.getPhoneNumber(), LogTags.OTHER);
                invalidateAndGetNewFCMToken(getApplicationContext());
                Intercom.client().logout();
                Registration registration = Registration.create().withUserId(driver.getPhoneNumber());
                String driverName = driver.getName() + " " + driver.getSurname();
                Intercom.client().registerIdentifiedUser(registration);
                UserAttributes userAttributes = new UserAttributes.Builder()
                        .withCustomAttribute("user", "Driver")
                        .withPhone(driver.getPhoneNumber())
                        .withName(driverName)
                        .build();
                Intercom.client().updateUser(userAttributes);
                Crashlytics.setUserIdentifier(driver.getPhoneNumber());
                restoreStatus();
            }

            @Override
            public void errorCallback() {
                Toast.makeText(getApplicationContext(),"Ошибка получения профиля",Toast.LENGTH_LONG).show();
            }
        });
    }

    private void restoreStatus() {
        Log.d(TAG, "restoreStatus: ");
        //ManfredNetworkManager manfredNetworkManager = CheckEventService.getManfredNetworkManager();
        Credentials credentials = manfredCredentialsManager.getCredentials().getValue();
        if(credentials==null){
            Toast.makeText(getApplicationContext(),"Токен не установлен",Toast.LENGTH_LONG).show();
            return;
        }
        String token = credentials.getToken();
        DriverRepository driverRepository = new HTTPDriverRepository(token);
        Log.d(TAG, "restoreStatus: make response for restore state");
        driverRepository.getStatus(new ResponseCallback<DriverStatus>() {
            @Override
            public void allOk(DriverStatus driverStatus) {
                if(driverStatus.getFeatures().containsKey("websockets")){
                    boolean isSocket = driverStatus.getFeatures().get("websockets");
                    Log.d(TAG, "getStatus: it is socket? "+isSocket);
                    loggingManager.addLog("LoginActivity", "getStatus", "it is socket? "+isSocket, LogTags.OTHER);
                    if(isSocket){
                        ((ManfredApplication)getApplication()).getRepoProvider().setBySocket(ManfredApplication.getNetworkManger());
                    }else {
                        ((ManfredApplication)getApplication()).getRepoProvider().setByHTTP();
                    }

                }
                if (driverStatus.getOrder() == null) {
                    Log.d(TAG, "onResponse: don't need to restore trip, just starting...");
                    Intent intent = new Intent(LoginActivity.this, MapsActivity.class);
                    freeLoginManager();
                    startActivity(intent);
                } else {
                    Log.d(TAG, "onResponse: restoring trip..." + driverStatus.getOrder().getId());
                    RestoreTrip restoreTrip = new RestoreTrip();
                    Intent intent = restoreTrip.restoreTrip(driverStatus.getOrder(), getApplicationContext());
                    if (intent != null) {
                        startCheckOrderService();
                        //intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        freeLoginManager();
                        startActivity(intent);
                    } else {
                        intent = new Intent(getApplicationContext(), MapsActivity.class);
                        //intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        freeLoginManager();
                        startActivity(intent);
                    }
                }
            }

            @Override
            public void error(ManfredError error) {
                Toast.makeText(getApplicationContext(),"Ошибка восстановление статуса: "+error.getText(),Toast.LENGTH_LONG).show();
            }
        });
    }

    private void freeLoginManager(){
        ((ManfredApplication)getApplication()).freeLoginManager();
    }

    private void makeToolbaWithSearch(ActivityLoginBinding binding) {
        binding.authBarSearchCountry.setVisibility(View.VISIBLE);
        binding.registerPhoneIntercomButton.setVisibility(View.GONE);
        binding.authBar.setBackgroundColor(getResources().getColor(R.color.app_bar_finish));
        binding.authBar.setElevation(16);
    }

    private void makeToolbarIntercomOnly(ActivityLoginBinding binding) {
        binding.authBarSearchCountry.setVisibility(View.GONE);
        binding.registerPhoneIntercomButton.setVisibility(View.VISIBLE);
        binding.authBar.setElevation(0);
        binding.authBar.setBackgroundColor(Color.TRANSPARENT);
    }

    private void initIntercomButton(ImageButton supportChat) {
        List<String> items = new ArrayList<>();
        items.add("написать в поддержку");
        items.add("позвонить");
        //final CharSequence[] tags = items.toArray(new String[items.size()]);
        supportChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder supportDialog = new AlertDialog.Builder(LoginActivity.this, R.style.LoginSupportDialog);
                supportDialog.setTitle("Поддержка")
                        .setAdapter(new ArrayAdapter<String>(LoginActivity.this,
                                        R.layout.row_support, R.id.textView1, items),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which) {
                                            case 0:
                                                startIntercomAnonymous();
                                                break;
                                            case 1:
                                                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "+74951814777", null));
                                                startActivity(intent);
                                                break;
                                        }

                                    }
                                });
                supportDialog.setNegativeButton("Отмена", null); // dismisses by default
                supportDialog.create().show();
            }
        });
    }

    private void swapFragment(Fragment fragment, boolean needBackStacked) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.enter_from_right, R.animator.exit_to_left);
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        if (needBackStacked) {
            fragmentTransaction.addToBackStack(null);
        } else {
            while (fragmentManager.getBackStackEntryCount() > 0) {
                fragmentManager.popBackStackImmediate();
            }
        }
        fragmentTransaction.commit();
    }

    private void startIntercomAnonymous() {
        Registration registration = Registration.create().withUserId(UUID.randomUUID().toString());
        Intercom.client().registerIdentifiedUser(registration);
        UserAttributes userAttributes = new UserAttributes.Builder()
                .withCustomAttribute("user", "Driver")
                .build();
        Intercom.client().updateUser(userAttributes);
        Intercom.client().displayMessenger();
    }

    private void noNetError() {
        RelativeLayout noInet = findViewById(R.id.no_inet);
        noInet.setVisibility(View.VISIBLE);
        noInet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(LoginActivity.this,
                        R.anim.slid_down));
                v.setVisibility(View.GONE);
                checkInetStatus();
            }
        });
    }

    private void hideNoNetError() {
        RelativeLayout noInet = findViewById(R.id.no_inet);
        noInet.setVisibility(View.GONE);

    }

    private boolean isEmailValid(String email) {
        return true;
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }


    @Override
    protected void onResume() {
        super.onResume();
        checkForUpdate();
        connectionStatusManager.addConnectStatusListener(connectStatusListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        connectionStatusManager.removeConnectStatusListener(connectStatusListener);
    }

    private void checkInetStatus() {
        if (!connectionStatusManager.isNetAvailable()) {
            noNetError();
        }
    }

    private void initEdit(final EditText searchEdit, NewManfredLoginManger manfredLoginManger) {
        searchEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                manfredLoginManger.filterCountriesByString(searchEdit.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onBackStackChanged() {
        shouldDisplayHomeUp();
    }

    public void shouldDisplayHomeUp() {
        //Enable Up button only  if there are entries in the back stack
        boolean canback = getSupportFragmentManager().getBackStackEntryCount() > 0;
        Log.d(TAG, "shouldDisplayHomeUp: "+canback);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(canback);
    }

    @Override
    public void onListFragmentInteraction(CountryModel item) {
        ((ManfredApplication)getApplication()).getManfredLoginManger().enterRegionCode(item);
    }

    @Override
    public void onBackPressed() {
        // here remove code for your last fragment
        Log.d(TAG, "onBackPressed: ");
        makeToolbarIntercomOnly(binding);
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void startCheckOrderService() {
        ServiceUtils.startService(getApplicationContext(),CheckEventService.class);
    }

}

