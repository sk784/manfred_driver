/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.activities.history;


import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.activities.history.details.HistoryDetailsActivity;
import ru.manfred.manfreddriver.adapters.HistoryTripAdapter;
import ru.manfred.manfreddriver.managers.history.HistoryCustomRange;
import ru.manfred.manfreddriver.managers.history.HistoryManager;
import ru.manfred.manfreddriver.managers.history.ManfredHistoryManager;
import ru.manfred.manfreddriver.model.api.Credentials;
import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.utils.Constants;
import ru.manfred.manfreddriver.utils.HistoryDateSorter;
import ru.manfred.manfreddriver.utils.NetworkUtil;
import ru.manfred.manfreddriver.utils.TimeDateUtils;
import ru.manfred.manfreddriver.viewmodel.ListItem;
import ru.manfred.manfreddriver.viewmodel.OrderHistoryViewModel;

import static ru.manfred.manfreddriver.utils.NetworkUtil.TYPE_NOT_CONNECTED;

/**
 * fight with to soon showing picker:
 * https://stackoverflow.com/questions/10024739/how-to-determine-when-fragment-becomes-visible-in-viewpager
 */
public class HistoryFragment extends Fragment implements DatePickerFragment.NoticeDialogListener {
    protected boolean mIsVisibleToUser;
    //for dont reopen dialog on resume activity
    private boolean isDialogVisible = false;

    public interface HistoryFragmentCallback {
        public void needSwitchBack();
    }

    HistoryFragmentCallback historyFragmentCallback;
    ServerAvailableStatus serverAvailableStatus;

    private static final String TAG = "HistoryFragment";
    HistoryTripAdapter mAdapter;
    List<ListItem> filteredHistory = new ArrayList<>();
    //ArrayList<Order> allHistory = new ArrayList<>();
    HistoryManager.HistoryInterval historyInterval = null;
    ManfredHistoryManager historyManager;

    //for custom range fragment only
    Date startDate;
    Date endDate;

    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");
        mAdapter = new HistoryTripAdapter(filteredHistory, new HistoryTripAdapter.Callback() {
            @Override
            public void itemClicked(OrderHistoryViewModel item) {
                Intent intent = new Intent(getActivity(), HistoryDetailsActivity.class);
                //for scroll to in history details
                long clickedHistoryItemId = item.getOrder().getId();
                Log.d(TAG, "itemClicked: " + clickedHistoryItemId + ", in interval " + historyInterval);
                intent.putExtra(Constants.HISTORY_IN_LIST_ID, clickedHistoryItemId);
                intent.putExtra(Constants.HISTORY_INTERVAL_KEY, historyInterval);
                if (historyInterval == HistoryManager.HistoryInterval.CUSTOM_INTERVAL) {
                    intent.putExtra(Constants.HISTORY_START_DATE, startDate);
                    intent.putExtra(Constants.HISTORY_END_DATE, endDate);
                }
                startActivity(intent);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity a;
        if (context instanceof Activity) {
            a = (Activity) context;
            try {
                historyFragmentCallback = (HistoryFragmentCallback) a;
            } catch (ClassCastException e) {
                throw new ClassCastException(getParentFragment().toString() + " must implement " + HistoryFragmentCallback.class.getName());
            }

            try {
                serverAvailableStatus = (ServerAvailableStatus) a;
            } catch (ClassCastException e) {
                throw new ClassCastException(getParentFragment().toString() + " must implement " + ServerAvailableStatus.class.getName());
            }
        }
    }

    //mb create before showing(cache)
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        Bundle args = getArguments();
        final View view = inflater.inflate(R.layout.fragment_history, container, false);
        if (args != null) {
            historyInterval = (HistoryManager.HistoryInterval) args.getSerializable(Constants.HISTORY_INTERVAL_KEY);
            Log.d(TAG, "onCreateView: "+historyInterval);
        }
        RecyclerView recyclerView = view.findViewById(R.id.history_recycler);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.onSaveInstanceState();
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);
        //if true crashing after back from details when call cancel button in range dates dialog
        recyclerView.setSaveEnabled(false);

        ((ManfredApplication)getActivity().getApplication()).getCredentialsManager().getCredentials()
                .observe(this, new Observer<Credentials>() {
            @Override
            public void onChanged(@Nullable Credentials credentials) {
                if (credentials != null) {
                    historyManager = new ManfredHistoryManager(
                            ((ManfredApplication)getActivity().getApplication()).getRepoProvider().getDriverRepository(credentials.getToken()));
                    if (historyInterval != null) {
                        initShowingInterval(view);
                    }
                }
            }
        });

        return view;
    }

    private void initShowingInterval(View view) {
        Log.d(TAG, "initShowingInterval: "+historyInterval);
        switch (historyInterval) {
            case TODAY:
                showTodayHistory(view);
                break;
            case THIS_WEEK:
                showWeekHistory(view);
                break;
            case THIS_MONTH:
                showMonthHistory(view);
                break;
            case CUSTOM_INTERVAL:
                LinearLayout dateSelect = view.findViewById(R.id.select_dates);
                dateSelect.setVisibility(View.VISIBLE);
                dateSelect.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDatePicker(TypeDatePicker.START_DATE, null);
                    }
                });
                if(mIsVisibleToUser) {
                    showCustomDateHistory(getView());
                }
                break;
        }
    }

    /**
     * This method will call when viewpager create fragment and when we go to this fragment from
     * background or another activity, fragment
     * NOT call when we switch between each page in ViewPager
     */
    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: ");
/*        if (mIsVisibleToUser) {
            if (historyInterval == HistoryManager.HistoryInterval.CUSTOM_INTERVAL) {
                Log.d(TAG, "onStart: ");
                showCustomDateHistory(getView());
            }
        }*/
    }

    @Override
    public void onStop() {
        super.onStop();
        //Log.d(TAG, "onStop: " + historyInterval);
    }

    @Override
    public void onPause() {
        super.onPause();
        //Log.d(TAG, "onPause: ");
    }

    /**
     * This method will call at first time viewpager created and when we switch between each page
     * NOT called when we go to background or another activity (fragment) when we go back
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Log.d(TAG, "setUserVisibleHint: isVisibleToUser? "+isVisibleToUser);
        mIsVisibleToUser = isVisibleToUser;
        if (isResumed()) { // fragment have created
            if (mIsVisibleToUser) {
                if (historyInterval == HistoryManager.HistoryInterval.CUSTOM_INTERVAL) {
                    showCustomDateHistory(getView());
                }
            }
        }
    }

    private void showCustomDateHistory(View view) {
        HistoryCustomRange historyCustomRange = historyManager.getLastPeriod();
        if (historyCustomRange != null) {
            showIntervalHistory(historyCustomRange.getStartDate(), historyCustomRange.getEndDate());
            startDate = historyCustomRange.getStartDate();
            endDate = historyCustomRange.getEndDate();
        } else {
            showDatePicker(TypeDatePicker.START_DATE, null);
        }
    }

    private void showDatePicker(TypeDatePicker typeDatePicker, Date startDate) {
        Log.d(TAG, "showDatePicker: " + typeDatePicker + ", " + startDate);
        if (isDialogVisible) return;
        DialogFragment calendarPickerFragment = DatePickerFragment.newInstance(typeDatePicker, startDate);
        if (getFragmentManager() != null) {
            Log.d(TAG, "showCustomDateHistory: ");
            calendarPickerFragment.setCancelable(false);
            calendarPickerFragment.show(HistoryFragment.this.getChildFragmentManager(), "datePicker");
            isDialogVisible = true;
            this.startDate = startDate;
        }
    }

    private void showTodayHistory(View view) {
        Log.d(TAG, "showTodayHistory: ");
        historyManager.getTodayHistory(new HistoryManager.HistoryManagerCallback() {
            @Override
            public void success(List<Order> historyOrders) {
                Log.d(TAG, "showTodayHistory success: "+historyOrders.size());
                showHistoryInRecycler(historyOrders, view);
                serverAvailableStatus.isAvailable();
            }

            @Override
            public void error(String message) {
                Context context = getContext();
                if(context!=null) {
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                }
                hideProgressBar(view);
            }

            @Override
            public void serverNotAvailable() {
                servNotAvaibl();
            }
        });

    }

    private void checkInetStatusFr() {
        if (getContext() != null) {
            int inetStatus = NetworkUtil.getConnectivityStatus(getContext());
            if (inetStatus == TYPE_NOT_CONNECTED) {
                serverAvailableStatus.notAvailable();
            }
        }

    }

    private void servNotAvaibl() {
        serverAvailableStatus.notAvailable();
        hideProgressBar(Objects.requireNonNull(getView()));
        showNoOrders(getView());
    }

    private void showWeekHistory(View view) {
        historyManager.getWeekHistory(new HistoryManager.HistoryManagerCallback() {
            @Override
            public void success(List<Order> historyOrders) {
                serverAvailableStatus.isAvailable();
                showHistoryInRecycler(historyOrders, view);
            }

            @Override
            public void error(String message) {
                final FragmentActivity activity = getActivity();
                if(activity!=null) {
                    Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                }
                hideProgressBar(view);
            }

            @Override
            public void serverNotAvailable() {
                servNotAvaibl();
            }
        });
    }

    private void showMonthHistory(View view) {
        historyManager.getMonthHistory(new HistoryManager.HistoryManagerCallback() {
            @Override
            public void success(List<Order> historyOrders) {
                Log.d(TAG, "success: take " + historyOrders.size() + " in month");
                serverAvailableStatus.isAvailable();
                showHistoryInRecycler(historyOrders, view);
            }

            @Override
            public void error(String message) {
                final FragmentActivity activity = getActivity();
                if(activity!=null) {
                    Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                }
                hideProgressBar(view);
            }

            @Override
            public void serverNotAvailable() {
                servNotAvaibl();
            }
        });
    }

    private void showHistoryInRecycler(List<Order> historyOrders, View view) {
        filteredHistory.clear();
        filteredHistory.addAll(new HistoryDateSorter(historyOrders).getItems());
        hideProgressBar(view);
        mAdapter.notifyDataSetChanged();
        Log.d(TAG, "showHistoryInRecycler: interval is " + historyInterval + ", we have orders in history "
                + filteredHistory.size());
        if (filteredHistory.isEmpty()) {
            showNoOrders(view);
        } else {
            hideNoOrders(view);
        }
        checkInetStatusFr();
    }

    private void showProgressBar(View view) {
        ProgressBar progressBar = view.findViewById(R.id.pb_history);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar(View view) {
        ProgressBar progressBar = view.findViewById(R.id.pb_history);
        progressBar.setVisibility(View.GONE);
    }

    private void showNoOrders(View view) {
        //Log.d(TAG, "showNoOrders: ");
        TextView noOrders = view.findViewById(R.id.history_no_orders);
        noOrders.setVisibility(View.VISIBLE);
    }

    private void hideNoOrders(View view) {
        TextView noOrders = view.findViewById(R.id.history_no_orders);
        noOrders.setVisibility(View.GONE);
    }

    @Override
    public void onDialogPositiveClick(TypeDatePicker typeDatePicker, Date date) {
        Log.d(TAG, "onDialogPositiveClick: " + typeDatePicker + ", date is " + date);
        isDialogVisible = false;
        switch (typeDatePicker) {
            case START_DATE:
                startDate = date;
                showDatePicker(TypeDatePicker.END_DATE, startDate);
                break;
            case END_DATE:
                endDate = date;
                if (TimeDateUtils.isSameDay(startDate, endDate)) {
                    endDate = TimeDateUtils.addDays(endDate, 1);
                }
                showIntervalHistory(startDate, endDate);
                isDialogVisible = false;
                break;
        }
    }

    @Override
    public void onDialogNegativeClick(TypeDatePicker typeDatePicker) {
        isDialogVisible = false;
        switch (typeDatePicker) {
            case START_DATE:
                Log.d(TAG, "onDialogNegativeClick: ");
                if (startDate == null & endDate == null) {
                    historyFragmentCallback.needSwitchBack();
                }
                break;
            case END_DATE:
                showDatePicker(TypeDatePicker.START_DATE, null);
                break;
        }
    }

    private void showIntervalHistory(Date startDate, Date endDate) {
        filteredHistory.clear();
        mAdapter.notifyDataSetChanged();
        showProgressBar(getView());

        SimpleDateFormat onlyDayFormat = new SimpleDateFormat("d", new Locale("ru", "RU"));
        SimpleDateFormat dateAndMonth = new SimpleDateFormat("d MMMM", new Locale("ru", "RU"));
        Calendar startDayCal = Calendar.getInstance();
        Calendar endDayCal = Calendar.getInstance();
        startDayCal.setTime(startDate);
        endDayCal.setTime(endDate);
        boolean sameMonth = startDayCal.get(Calendar.YEAR) == endDayCal.get(Calendar.YEAR) &&
                startDayCal.get(Calendar.MONTH) == endDayCal.get(Calendar.MONTH);
        TextView interval = getView().findViewById(R.id.history_tv_date_range);
        String firstRangeDate;
        if (sameMonth) {
            firstRangeDate = onlyDayFormat.format(startDate);
        } else {
            firstRangeDate = dateAndMonth.format(startDate);
        }
        String rangeText = firstRangeDate + " — " + dateAndMonth.format(endDate);
        interval.setText(rangeText);
        historyManager.getPeriodHistory(startDate, endDate, new HistoryManager.HistoryManagerCallback() {
            @Override
            public void success(List<Order> historyOrders) {
                Log.d(TAG, "success: custom date positive, order size is " + historyOrders.size());
                serverAvailableStatus.isAvailable();
                showHistoryInRecycler(historyOrders, getView());
            }

            @Override
            public void error(String message) {
                Log.d(TAG, "error: " + message);
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void serverNotAvailable() {
                servNotAvaibl();
            }
        });
    }
}
