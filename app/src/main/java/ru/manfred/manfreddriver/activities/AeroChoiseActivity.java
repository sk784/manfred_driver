/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.activities;

import android.app.ActionBar;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.adapters.AirportAdapter;
import ru.manfred.manfreddriver.databinding.ActivityAeroChoiseBinding;
import ru.manfred.manfreddriver.interactor.repository.RideRepository;
import ru.manfred.manfreddriver.managers.ManfredOrderManager;
import ru.manfred.manfreddriver.model.api.Airport;
import ru.manfred.manfreddriver.utils.Constants;
import ru.manfred.manfreddriver.utils.SharedPreferencesManager;
@Deprecated
public class AeroChoiseActivity extends AppCompatActivity {

    private static final String TAG = "AeroChoiseActivity";
/*    @Inject
    ManfredOrderManager orderManager;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityAeroChoiseBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_aero_choise);

        //ManfredApplication.getTripComponent().inject(this);

        //AppDatabase database = Room.databaseBuilder(getApplicationContext(),AppDatabase.class,"manfred-base").allowMainThreadQueries().build();
        List<Object> airports = new ArrayList<>();
        airports.add(0, null);
        airports.addAll(airportList());
        //Log.d(TAG, "onCreate: airports from db with caption of list: "+airports.size());
        AirportAdapter airportAdapter = new AirportAdapter(airports) {
            @Override
            public void airportSelected(Airport airport) {
                RideRepository rideRepository =
                        ((ManfredApplication)getApplication()).getRepoProvider().getRideRepository(SharedPreferencesManager.getCredentials(getApplicationContext()).getToken());
                rideRepository.toAirport(airport);
                Intent intent = new Intent();
                intent.putExtra(Constants.AIRPORT, airport);
                setResult(RESULT_OK, intent);
                finish();
            }
        };

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.onSaveInstanceState();
        binding.aeroportsRecycler.setLayoutManager(mLayoutManager);
        binding.aeroportsRecycler.setAdapter(airportAdapter);
        initToolbar();

    }

    private List<Airport> airportList() {
        List<Airport> airports = new ArrayList<>();
        airports.add(new Airport("Внуково", "Москва", 55.600985, 37.277001));
        airports.add(new Airport("Домодедово", "Москва", 55.414348, 37.900525));
        airports.add(new Airport("Шереметьево", "Москва", 55.966786, 37.415685));
        airports.add(new Airport("Остафьево", "Москва", 55.502921, 37.510745));
        airports.add(new Airport("Жуковский", "Москва", 55.561976, 38.118087));
        return airports;
    }

    public void initToolbar() {
        Toolbar myToolbar =
                (Toolbar) findViewById(R.id.include);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_action_bar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView title = (TextView) findViewById(R.id.tvTitle);
        //title.setText(titleString);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
