/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.activities;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ebanx.swipebtn.OnStateChangeListener;
import com.ebanx.swipebtn.SwipeButton;

import javax.inject.Inject;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.interactor.repository.UserRepository;
import ru.manfred.manfreddriver.managers.ConnectionStatusManager;
import ru.manfred.manfreddriver.managers.ManfredConnectionStatusManager;
import ru.manfred.manfreddriver.managers.ManfredOrderManager;
import ru.manfred.manfreddriver.managers.ManfredSnackbarManager;
import ru.manfred.manfreddriver.managers.ManfredTripManager;
import ru.manfred.manfreddriver.managers.SnackbarManager;
import ru.manfred.manfreddriver.managers.TripManager;
import ru.manfred.manfreddriver.managers.logging.LogTags;
import ru.manfred.manfreddriver.managers.logging.ManfredLoggingManager;
import ru.manfred.manfreddriver.utils.Constants;
import ru.manfred.manfreddriver.utils.SharedPreferencesManager;

public class AwayActivity extends AppCompatActivity {
    @Inject
    ManfredOrderManager manfredOrderManager;
    @Inject
    ManfredTripManager manfredTripManager;
    @Inject
    ManfredConnectionStatusManager connectionStatusManager;
    @Inject
    ManfredLoggingManager loggingManager;
    @Inject
    ManfredSnackbarManager manfredSnackbarManager;
    //TripManager.TripManagerListener tripManagerListener;

    ConnectionStatusManager.ConnectStatusListener connectStatusListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_away);
        ManfredApplication.getTripComponent().inject(this);
        loggingManager.addLog("AwayActivity", "onCreate", "", LogTags.USER_ON_SCREEN);
        SwipeButton swipeButton = findViewById(R.id.go_away);
        swipeButton.setOnStateChangeListener(new OnStateChangeListener() {
            @Override
            public void onStateChange(boolean active) {
                Intent intent = new Intent();
                intent.putExtra(Constants.NEED_GO_AWAY, true);
                //setResult(RESULT_OK,intent);
                swipeButton.setEnabled(false);

                final UserRepository userRepository =
                        ((ManfredApplication)getApplication()).getRepoProvider().getUserRepository(SharedPreferencesManager.getCredentials(getApplicationContext()).getToken());
                userRepository.changeStatus(false, new UserRepository.AnswerCallback() {
                    @Override
                    public void success() {
                        manfredTripManager.cancelRide(manfredOrderManager.getCurrentOrder(),
                                new TripManager.Reason("cancelled_driver", "отменено водителем"));
                        //manfredOrderManager.setCurrentOrder(null);
                        goToMainScreen();
                    }

                    @Override
                    public void notSuccess(String error) {
                        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                        swipeButton.toggleState();
                        swipeButton.setEnabled(true);
                    }

                    @Override
                    public void authorisationRequired(String error) {
                        connectionStatusManager.invalidToken();
                        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                        finish();
                    }

                    @Override
                    public void serverNotAvailable() {
                        Toast.makeText(getApplicationContext(), "Сервер недоступен. Повторите попытку позже", Toast.LENGTH_SHORT).show();
                        swipeButton.toggleState();
                        swipeButton.setEnabled(true);
                    }
                });

                finish();
            }
        });

        manfredTripManager.getCurrentState().observe(this, new Observer<TripManager.Status>() {
            @Override
            public void onChanged(@Nullable TripManager.Status status) {
                if(status!=null){
                    if (status.getStatusState() != null) {
                        switch (status.getStatusState()){
                            case OFFER_SHOW_TIMEOUT:
                                goToMainScreen();
                                break;
                            case CANCELLED:
                                if(status.getReason()!=null) {
                                    orderCancelled(status.getReason().getType());
                                }else {
                                    goToMainScreen();
                                }
                                break;
                        }
                    }
                }
            }
        });

/*        tripManagerListener = new TripManager.TripManagerListener() {
            @Override
            public void newStatus(TripManager.Status status) {
                if (status.getStatusState() != null) {
                    switch (status.getStatusState()){
                        case OFFER_SHOW_TIMEOUT:
                            goToMainScreen();
                            break;
                        case CANCELLED:
                            if(status.getReason()!=null) {
                                orderCancelled(status.getReason().getType());
                            }else {
                                goToMainScreen();
                            }
                            break;
                    }
                }
            }
        };*/

        ImageButton imageButton = findViewById(R.id.away_back);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //not finish() because Xiaomi redraw last screen
                Intent intent = new Intent(AwayActivity.this, NewOrderActivity.class);
                intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                //finish();
            }
        });

        connectStatusListener = new ConnectionStatusManager.ConnectStatusListener() {
            @Override
            public void netAvailable() {
                hideNoNetError();
            }

            @Override
            public void netUnAvailable() {
                noNetError();
            }

            @Override
            public void serverUnAvailable() {
                noNetError();
            }

            @Override
            public void tokenInvalid() {

            }
        };
    }

    private void orderCancelled(String type) {
        switch (type) {
            case "other_driver_take_order":
                manfredSnackbarManager.showSnack(SnackbarManager.Type.ORDER_TAKE_BY_OTHER_DRIVER);
                goToMainScreen();
                break;
            case "order_cancelled_by_passenger":
                manfredSnackbarManager.showSnack(SnackbarManager.Type.ORDER_CANCELLED_BY_PASSENGER);
                goToMainScreen();
                break;
            case "server_error":
                //Toast.makeText(getApplicationContext(), reason.getMessage(), Toast.LENGTH_LONG).show();
                manfredSnackbarManager.showSnack(SnackbarManager.Type.ORDER_TAKE_BY_OTHER_DRIVER);
                goToMainScreen();
                break;
        }
    }

    public void goToMainScreen() {
        final Intent intent = new Intent(AwayActivity.this, MapsActivity.class);
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        connectionStatusManager.addConnectStatusListener(connectStatusListener);
        //manfredTripManager.addChangeStatusListener(tripManagerListener);
    }

    @Override
    protected void onPause() {
        connectionStatusManager.removeConnectStatusListener(connectStatusListener);
        //manfredTripManager.removeChangeStatusListener(tripManagerListener);
        super.onPause();
    }

    private void noNetError() {
        RelativeLayout noInet = findViewById(R.id.no_inet);
        noInet.setVisibility(View.VISIBLE);
        noInet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(AwayActivity.this,
                        R.anim.slid_down));
                v.setVisibility(View.GONE);
                checkInetStatus();
            }
        });
    }

    private void hideNoNetError() {
        RelativeLayout noInet = findViewById(R.id.no_inet);
        noInet.setVisibility(View.GONE);

    }

    private void checkInetStatus() {
        if (!connectionStatusManager.isNetAvailable()) {
            noNetError();
        }
    }
}
