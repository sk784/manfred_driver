/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.activities.preorders;

import android.app.ActionBar;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.activities.NewOrderActivity;
import ru.manfred.manfreddriver.activities.history.ServerAvailableStatus;
import ru.manfred.manfreddriver.adapters.PagerAdapter;
import ru.manfred.manfreddriver.databinding.ActivityPreordersBinding;
import ru.manfred.manfreddriver.managers.ManfredOrderManager;
import ru.manfred.manfreddriver.managers.ManfredSnackbarManager;
import ru.manfred.manfreddriver.managers.ManfredTripManager;
import ru.manfred.manfreddriver.managers.SnackbarManager;
import ru.manfred.manfreddriver.managers.TripManager;
import ru.manfred.manfreddriver.managers.preorder.ManfredPreOrderManager;
import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.utils.Constants;
import ru.manfred.manfreddriver.utils.NetworkUtil;
import ru.manfred.manfreddriver.utils.UIUtils;

import static ru.manfred.manfreddriver.utils.NetworkUtil.TYPE_NOT_CONNECTED;

public class PreordersActivity extends AppCompatActivity implements ServerAvailableStatus {
    public static final int MY_PREORDERS_TAB = 0;
    public static final int NEW_PREORDERS_TAB = 1;
    //TripManager.TripManagerListener tripManagerListener;
    //PreOrderManager.PreOrdersListener preOrdersListener;
    //PreOrderManager.PreOrderListChangedListener preOrderListChangedListener;
    //SnackbarManager.SnackbarManagerListener snackbarManagerListener;
    private LocalBroadcastManager brodcastManager;
    private BroadcastReceiver baseReceiver;

    @Inject
    ManfredPreOrderManager preOrderManager;
    @Inject
    ManfredTripManager tripManager;
    @Inject
    ManfredOrderManager manfredOrderManager;
    @Inject
    ManfredSnackbarManager manfredSnackbarManager;

    private static final String TAG = "PreordersActivity";
    ActivityPreordersBinding binding;
    //private static boolean foreground;
    //private static boolean unShovedSnack = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_preorders);
        //((ManfredApplication) getApplication()).setLastShownActivity(this);
        binding.preordersPager.setAdapter(new PagerAdapter(getSupportFragmentManager()));
        ManfredApplication.getTripComponent().inject(this);

        initToolbar();
        setupViewPager(binding.preordersPager, binding);
        binding.preordersTabLayout.setupWithViewPager(binding.preordersPager);


        tripManager.getCurrentState().observe(this, new Observer<TripManager.Status>() {
            @Override
            public void onChanged(@Nullable TripManager.Status status) {
                if (status != null && status.getStatusState() != null) {
                    switch (status.getStatusState()) {
                        case OFFER_TO_RIDE:
                            final Intent intentNewOrder = new Intent(PreordersActivity.this, NewOrderActivity.class);
                            startActivity(intentNewOrder);
                            break;
                    }
                }
            }
        });
/*        tripManagerListener = new TripManager.TripManagerListener() {
            @Override
            public void newStatus(TripManager.Status status) {
                if (status.getStatusState() != null) {
                    switch (status.getStatusState()) {
                        case OFFER_TO_RIDE:
                            final Intent intentNewOrder = new Intent(PreordersActivity.this, NewOrderActivity.class);
                            startActivity(intentNewOrder);
                            break;
                    }
                }
            }
        };*/


        manfredSnackbarManager.getSnack().observe(this, new Observer<SnackbarManager.Snack>() {
            @Override
            public void onChanged(@Nullable SnackbarManager.Snack snack) {
                if(snack!=null){
                    UIUtils.generateSnack(binding.aPreorder, snack).show();
                }
            }
        });

    }

    private void initTabTitleRefreshing(ActivityPreordersBinding binding) {
        preOrderManager.getNewPreOrders().observe(this, new Observer<List<Order>>() {
            @Override
            public void onChanged(@Nullable List<Order> orders) {
                if(orders!=null){
                    binding.preordersTabLayout.getTabAt(NEW_PREORDERS_TAB).setText(getIncPreOrdersTitle(orders.size()));
                }
            }
        });
        preOrderManager.getMyPreOrders().observe(this, new Observer<List<Order>>() {
            @Override
            public void onChanged(@Nullable List<Order> orders) {
                if(orders!=null){
                    binding.preordersTabLayout.getTabAt(MY_PREORDERS_TAB).setText(getAcceptedTitle(orders.size()));
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //foreground = true;
        //tripManager.addChangeStatusListener(tripManagerListener);
        //manfredSnackbarManager.addSnackShowerListener(snackbarManagerListener);

        brodcastManager = LocalBroadcastManager.getInstance(this);
        baseReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean haveNet = intent.getBooleanExtra(Constants.INTERNET_STATUS, true);
                Log.d(TAG, "onReceive: " + haveNet);
                if (haveNet) {
                    isAvailable();
                    //refreshCurrentTab();
                } else {
                    notAvailable();
                }
            }
        };
        IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        intentFilter.addAction(Constants.INTERNET_STATUS);
        brodcastManager.registerReceiver(baseReceiver, intentFilter);
        preOrderManager.forceRefresh();
    }

    protected void onPause() {
        Log.d(TAG, "onPause: go to background");
        //foreground = false;
        //tripManager.removeChangeStatusListener(tripManagerListener);
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (baseReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(baseReceiver);
            baseReceiver = null;
        }
    }

    private @NonNull List<Order> getCurrOrdersList(LiveData<List<Order>>orders) {
        List<Order>orderList = orders.getValue();
        if(orderList==null){
            orderList=new ArrayList<>();
        }
        return orderList;
    }

    private void setupViewPager(ViewPager viewPager, ActivityPreordersBinding binding) {
        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager());

        Fragment myPreOrders = new PreorderFragment();
        Bundle myArgs = new Bundle();
        myArgs.putBoolean(Constants.PREORDER_MY, true);
        myPreOrders.setArguments(myArgs);
        String myTitle;
        myTitle = getAcceptedTitle(getCurrOrdersList(preOrderManager.getMyPreOrders()).size());
        adapter.addFragment(myPreOrders, myTitle);

        Fragment allPreOrders = new PreorderFragment();
        Bundle newArgs = new Bundle();
        newArgs.putBoolean(Constants.PREORDER_MY, false);
        allPreOrders.setArguments(newArgs);
        String newTitle;
        newTitle = getIncPreOrdersTitle(getCurrOrdersList(preOrderManager.getNewPreOrders()).size());
        adapter.addFragment(allPreOrders, newTitle);

        viewPager.setAdapter(adapter);
        if(getCurrOrdersList(preOrderManager.getMyPreOrders()).size() == 0 && getCurrOrdersList(preOrderManager.getNewPreOrders()).size()!=0){
            viewPager.setCurrentItem(NEW_PREORDERS_TAB);
        }
        initTabTitleRefreshing(binding);

    }


    @NonNull
    private String getIncPreOrdersTitle(int size) {
        Log.d(TAG, "getIncPreOrdersTitle: we have " + size + " new preOrders");
        String newTitle;
        if (size == 0) {
            newTitle = "Новые";
        } else {
            newTitle = "Новые (" + size + ")";
        }
        Log.d(TAG, "getIncPreOrdersTitle: new title is " + newTitle);
        return newTitle;
    }

    @NonNull
    private String getAcceptedTitle(int size) {
        String myTitle;
        if (size == 0) {
            myTitle = "Принятые";
        } else {
            myTitle = "Принятые (" + size + ")";
        }
        return myTitle;
    }

    public void initToolbar() {
        Toolbar myToolbar = findViewById(R.id.include);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_action_bar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView title = findViewById(R.id.tvTitle);
        title.setText("Предзаказы");
        myToolbar.setNavigationOnClickListener(view -> finish());
    }

    @Override
    public void notAvailable() {
        Log.d(TAG, "internetOff: ");
        binding.noInternetMessage.setVisibility(View.VISIBLE);
        binding.noInternetMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.startAnimation(AnimationUtils.loadAnimation(PreordersActivity.this,
                        R.anim.slid_down));
                view.setVisibility(View.GONE);
                checkInetStatus();
            }
        });
    }

    @Override
    public void isAvailable() {
        binding.noInternetMessage.setVisibility(View.GONE);
    }

    private void checkInetStatus() {
        int inetStatus = NetworkUtil.getConnectivityStatus(getApplicationContext());
        Log.d(TAG, "onClick: inet status is " + inetStatus);
        if (inetStatus == TYPE_NOT_CONNECTED) {
            notAvailable();
        }
    }
}
