/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.activities.preorders.details;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.LongSparseArray;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nitrico.lastadapter.LastAdapter;
import com.github.nitrico.lastadapter.LayoutHandler;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ru.manfred.manfreddriver.BR;
import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.activities.NewOrderActivity;
import ru.manfred.manfreddriver.activities.RideActivity;
import ru.manfred.manfreddriver.activities.TripToPassengerActivity;
import ru.manfred.manfreddriver.activities.preorders.PreOrderStarter;
import ru.manfred.manfreddriver.databinding.ActivityPreordersDetailBinding;
import ru.manfred.manfreddriver.interactor.repository.ConfirmationRepository;
import ru.manfred.manfreddriver.interactor.repository.RideRepository;
import ru.manfred.manfreddriver.managers.ActivityNotificationManager;
import ru.manfred.manfreddriver.managers.ManfredCredentialsManager;
import ru.manfred.manfreddriver.managers.ManfredLocationManager;
import ru.manfred.manfreddriver.managers.ManfredOrderManager;
import ru.manfred.manfreddriver.managers.ManfredTripManager;
import ru.manfred.manfreddriver.managers.TripManager;
import ru.manfred.manfreddriver.managers.logging.LogTags;
import ru.manfred.manfreddriver.managers.logging.ManfredLoggingManager;
import ru.manfred.manfreddriver.managers.preorder.ManfredPreOrderManager;
import ru.manfred.manfreddriver.managers.preorder.PreOrderEvent;
import ru.manfred.manfreddriver.managers.preorder.PreOrderManager;
import ru.manfred.manfreddriver.model.GoogleMaps.DirectionResults;
import ru.manfred.manfreddriver.model.api.Credentials;
import ru.manfred.manfreddriver.model.api.GeoData;
import ru.manfred.manfreddriver.model.api.ManfredError;
import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.presenters.PreOrderPresenter;
import ru.manfred.manfreddriver.utils.Constants;
import ru.manfred.manfreddriver.utils.SharedPreferencesManager;
import ru.manfred.manfreddriver.utils.SnapHelperOneByOne;
import ru.manfred.manfreddriver.utils.TimeCalculatorFromDirection;
import ru.manfred.manfreddriver.viewmodel.PreOrderHolderVM;
import ru.manfred.manfreddriver.viewmodel.PreOrderVM;

public class PreordersDetailActivity extends AppCompatActivity {
    private static final String TAG = "PreordersDetailActivity";
    //TripManager.TripManagerListener tripManagerListener;
    @Inject
    ManfredPreOrderManager manfredPreOrderManager;
    @Inject
    ManfredTripManager tripManager;
    @Inject
    ManfredOrderManager manfredOrderManager;
    @Inject
    ManfredPreOrderManager preOrderManager;
    @Inject
    ManfredLoggingManager loggingManager;
    @Inject
    ManfredLocationManager manfredLocationManager;

    private List<PreOrderHolderVM> currPreOrders;
    private PreOrderManager.PreOrdersListener preOrdersListener;

    public void setCurrPreOrders(List<PreOrderHolderVM> currPreOrders) {
        this.currPreOrders = currPreOrders;
    }

    public List<PreOrderHolderVM> getCurrPreOrders() {
        return currPreOrders;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final ActivityPreordersDetailBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_preorders_detail);
        ManfredApplication.getTripComponent().inject(this);
        Intent intent = getIntent();
        long scrollToId = intent.getLongExtra(Constants.PREORDER_IN_LIST_ID, 0);
        boolean isMyOrder = intent.getBooleanExtra(Constants.PREORDER_MY, true);
        Log.d(TAG, "onCreate: its my prorders?" + isMyOrder + ", we need scroll to " + scrollToId);
        //ManfredPreOrderManager manfredPreOrderManager = new ManfredPreOrderManager();
        List<Order> allPreOrders;
        allPreOrders = getOrders(isMyOrder);
        //mapping Order to OrderHistoryViewModel
        LongSparseArray<PreOrderHolderVM> preOrderVMLongSparseArray = new LongSparseArray<>();
        PreOrderStarter preOrderStarter = createPreOrderStarter();
        ObservableList<PreOrderHolderVM> preOrderVMList = getPreOrderHolderVMs(isMyOrder, allPreOrders, preOrderStarter);
        for (PreOrderHolderVM preOrderVM : preOrderVMList) {
            Log.d(TAG, "onCreate: add order with id " + preOrderVM.getPreOrderVM().getOrder().getId());
            preOrderVMLongSparseArray.put(preOrderVM.getPreOrderVM().getOrder().getId(), preOrderVM);
        }

        //dont crash if cant scroll to id
        if (preOrderVMLongSparseArray.get(scrollToId) == null) {
            Toast.makeText(getApplicationContext(), "Предзаказ не найден", Toast.LENGTH_LONG).show();
            finish();
        }
        Log.d(TAG, "onCreate: we have preorders: " + preOrderVMList.size());

        setCurrPreOrders(preOrderVMList);
        ConfirmationRepository confirmationRepository = getConfirmationRepository();
        LastAdapter lastAdapter;
        LayoutHandler layoutHandler;
        if(isMyOrder){
        layoutHandler = new LayoutHandler() {
            @Override
            public int getItemLayout(@NotNull Object o, int i) {
                if(o instanceof PreOrderHolderVM){
                    if(confirmationRepository!=null) {
                        confirmationRepository.orderShowed(((PreOrderHolderVM) o).getId());
                    }
                }
                return R.layout.preorder_accepted_details_holder;
            }
        };}else {
            layoutHandler = new LayoutHandler() {
                @Override
                public int getItemLayout(@NotNull Object o, int i) {
                    if(o instanceof PreOrderHolderVM){
                        if(confirmationRepository!=null) {
                            confirmationRepository.orderShowed(((PreOrderHolderVM) o).getId());
                        }
                    }
                    return R.layout.preorder_details_holder;
                }
            };
        }
        if (isMyOrder) {
            lastAdapter=new LastAdapter(preOrderVMList, BR.preOrder)
                    .map(PreOrderHolderVM.class, R.layout.preorder_accepted_details_holder)
                    .handler(layoutHandler)
                    .into(binding.preorderDetailsRecycler);
        } else {
            lastAdapter=new LastAdapter(preOrderVMList, BR.preOrder)
                    .map(PreOrderHolderVM.class, R.layout.preorder_details_holder)
                    .handler(layoutHandler)
                    .into(binding.preorderDetailsRecycler);
        }

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mLayoutManager.onSaveInstanceState();
        binding.preorderDetailsRecycler.setLayoutManager(mLayoutManager);
        SnapHelperOneByOne snapHelperOneByOne = new SnapHelperOneByOne();
        snapHelperOneByOne.attachToRecyclerView(binding.preorderDetailsRecycler);
        Log.d(TAG, "onCreate: we have " + preOrderVMList.size() + " in adapter");
        int posInList = preOrderVMList.indexOf(preOrderVMLongSparseArray.get(scrollToId));
        Log.d(TAG, "onCreate: we find order with id " + scrollToId + ", position " + posInList);
        if (posInList == -1) {
            Log.d(TAG, "onCreate: bad position, quit");
            Toast.makeText(getApplicationContext(), "Ошибка, предзаказ был отменен или взят другим водителем", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        mLayoutManager.scrollToPosition(posInList);


        binding.preorderDetailsRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int position = mLayoutManager.findLastVisibleItemPosition();
                Log.d(TAG, "onScrollStateChanged: scrolled to " + position);
                long orderId = getCurrPreOrders().get(position).getPreOrderVM().getOrder().getId();
                setTitle("Предзаказ " + orderId);
            }
        });

        initToolbar("Предзаказ " + preOrderVMLongSparseArray.get(scrollToId).getPreOrderVM().getOrder().getId());

        tripManager.getCurrentState().observe(this, new Observer<TripManager.Status>() {
            @Override
            public void onChanged(@Nullable TripManager.Status status) {
                if (status != null) {
                    switch (status.getStatusState()) {
                        case OFFER_TO_RIDE:
                            final Intent intentNewOrder = new Intent(PreordersDetailActivity.this, NewOrderActivity.class);
                            startActivity(intentNewOrder);
                            break;
                    }
                }
            }
        });

/*        tripManagerListener = new TripManager.TripManagerListener() {
            @Override
            public void newStatus(TripManager.Status status) {
                if (status.getStatusState() != null) {
                    switch (status.getStatusState()) {
                        case OFFER_TO_RIDE:
                            final Intent intentNewOrder = new Intent(PreordersDetailActivity.this, NewOrderActivity.class);
                            startActivity(intentNewOrder);
                            break;
*//*                        case RIDE_TO_PASSENGER:
                            final Intent intent = new Intent(PreordersDetailActivity.this, TripToPassengerActivity.class);
                            startActivity(intent);
                            break;*//*
                    }
                }
            }
        };*/

        preOrdersListener = new PreOrderManager.PreOrdersListener() {
            @Override
            public void preOrderNewEvent(PreOrderEvent preOrderEvent) {
                Log.d(TAG, "preOrderNewEvent: "+preOrderEvent);
                switch (preOrderEvent.getEventStatus()) {
                    case NEW_PRE_ORDER:
                        PreOrderPresenter preOrderPresenter = new PreOrderPresenter(isMyOrder);
                        getCurrPreOrders().add(new PreOrderHolderVM(new PreOrderVM(preOrderEvent.getOrder()), preOrderPresenter, preOrderStarter));
                        break;
                    case PRE_ORDER_DECLINED:
                        if(preOrderEvent.getOrder()!=null) {
                            Log.d(TAG, "preOrderNewEvent: PRE_ORDER_DECLINED for preOrder "+preOrderEvent.getOrder().getId());
                            //preOrderVMList.remove(preOrderEvent.getOrder());
                            for (PreOrderHolderVM preOrderHolderVM : new ArrayList<>(preOrderVMList)){
                                if(preOrderHolderVM.getId()==preOrderEvent.getOrder().getId()){
                                    preOrderVMList.remove(preOrderHolderVM);
                                    if(preOrderEvent.isSelfCancelled()){
                                        showAlert(new ActivityNotificationManager.Message("Отмена предзаказа",
                                                "Вы отменили предзаказ " + preOrderEvent.getOrder().getId()),true);
                                    }else {
                                        boolean isNeedCloseAfterOK;
                                        isNeedCloseAfterOK = preOrderVMList.isEmpty();
                                        showAlert(new ActivityNotificationManager.Message("Отмена предзаказа",
                                                "Предзаказ " + preOrderEvent.getOrder().getId() + " был отменен"),isNeedCloseAfterOK);
                                    }
                                    manfredPreOrderManager.eventProcessedSuccessfully(preOrderEvent);
                                    int currShowedOrderPosition = mLayoutManager.findLastVisibleItemPosition();
                                    Log.d(TAG, "preOrderNewEvent: position of curr showing preOrder is "+currShowedOrderPosition);
                                    if(!getCurrPreOrders().isEmpty()){
                                        setTitle("Предзаказ " + getCurrPreOrders().get(currShowedOrderPosition).getPreOrderVM().getOrder().getId());
                                    }else {
                                        setTitle("Нет предзаказов");
                                    }
                                }
                            }
                        }
                        break;
                }
            }
        };

        manfredLocationManager.getLocation().observe(this, new Observer<Location>() {
            @Override
            public void onChanged(@Nullable Location location) {

            }
        });
    }

    @Nullable private ConfirmationRepository getConfirmationRepository() {
        ManfredCredentialsManager credentialsManager = ((ManfredApplication)getApplication()).getCredentialsManager();
        Credentials credentials = credentialsManager.getCredentials().getValue();
        if(credentials!=null){
            return ((ManfredApplication)getApplication()).getRepoProvider().getConfirmationRepository(credentials.getToken());
        }else {
            return null;
        }

    }

    @NonNull
    private PreOrderStarter createPreOrderStarter() {
        return new PreOrderStarter() {
            @Override
            public void startOrder(Order order) {
                Location currLocation = manfredLocationManager.getCurrentLocation();
                if(currLocation==null){
                    Toast.makeText(getApplicationContext(),"Текущее местоположение не определено, старт поездки невозможен",Toast.LENGTH_LONG);
                    return;
                }
                ProgressDialog progress = showProgressBar(PreordersDetailActivity.this);
                String token = SharedPreferencesManager.getCredentials(getApplicationContext()).getToken();
                final RideRepository rideRepository = ((ManfredApplication)getApplication()).getRepoProvider().getRideRepository(token);
                Location passengerLocation = new Location("");
                passengerLocation.setLongitude(order.getLongitude());
                passengerLocation.setLatitude(order.getLatitude());
                rideRepository.getDirection(new GeoData(currLocation),
                        new GeoData(passengerLocation), new RideRepository.RepoAnswer<DirectionResults>() {
                    @Override
                    public void allOk(DirectionResults answer) {
                        TimeCalculatorFromDirection timeCalculatorFromDirection = new TimeCalculatorFromDirection(answer);
                        rideRepository.startPreOrder(order, timeCalculatorFromDirection.getCalculatedTripProperties().getDurationInSec(),
                                new RideRepository.AcceptCallback() {
                                    @Override
                                    public void error(ManfredError manfredError) {
                                        progress.dismiss();
                                        String logString = "error: " + manfredError.getText();
                                        loggingManager.addLog("MainPresenter", "startPreOrder", logString, LogTags.OTHER);
                                        Toast.makeText(getApplicationContext(), manfredError.getText(), Toast.LENGTH_SHORT).show();
                                        preOrderManager.removePreOrder(order);
                                    }

                                    @Override
                                    public void accepted(Order accOrder) {
                                        Log.d(TAG, "onStartPreOrder: preorder " + order.getId());
                                        //manfredOrderManager.setCurrentOrder(accOrder);
                                        //tripManager.setCurrOrder(accOrder);
                                        progress.dismiss();
                                        tripManager.startRideToPassenger(accOrder);
                                        preOrderManager.preOrderGo(accOrder);
                                        final Intent intentRide = new Intent(PreordersDetailActivity.this, TripToPassengerActivity.class);
                                        startActivity(intentRide);
                                    }

                                    @Override
                                    public void debugMessage(String message) {
                                        Log.d(TAG, "debugMessage: " + message);
                                    }

                                    @Override
                                    public void serverNotAvailable() {
                                        Intent intent = new Intent(Constants.INTERNET_STATUS);
                                        intent.putExtra(Constants.INTERNET_STATUS, false);
                                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                                    }
                                });
                    }

                    @Override
                    public void error(String error) {

                    }
                });
                String logString = "preOrder #" + order.getId();
                loggingManager.addLog("MainPresenter", "onStartPreOrder", logString, LogTags.PREORDER_READY_TO_START_CLICK_START);
            }
        };
    }

    private void showAlert(ActivityNotificationManager.Message message, boolean isNeedCloseAfterOK) {
        Log.d(TAG, "showAlert: " + message.toString()+" need close? "+isNeedCloseAfterOK);
        if(!(this).isFinishing()) {
            DialogInterface.OnClickListener okClicked = null;

            if(isNeedCloseAfterOK){
                okClicked=new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                };
            }
            new AlertDialog.Builder(this, R.style.MyDialogTheme)
                    .setTitle(message.getTitle())
                    .setMessage(message.getText())
                    .setCancelable(false)
                    .setPositiveButton("OK", okClicked)
                    .show();
        }
    }

    @NonNull
    private ObservableList<PreOrderHolderVM> getPreOrderHolderVMs(boolean isMyOrder, List<Order> allPreOrders, PreOrderStarter starter) {
        ObservableList<PreOrderHolderVM> preOrderVMList = new ObservableArrayList<>();
        PreOrderPresenter preOrderPresenter = new PreOrderPresenter(isMyOrder);
        for (Order order : allPreOrders) {
            preOrderVMList.add(new PreOrderHolderVM(new PreOrderVM(order), preOrderPresenter, starter));
        }
        return preOrderVMList;
    }

    private List<Order> getOrders(boolean isMyOrder) {
        List<Order> allPreOrders;
        if (isMyOrder) {
            allPreOrders = manfredPreOrderManager.getMyPreOrders().getValue();
        } else {
            allPreOrders = manfredPreOrderManager.getNewPreOrders().getValue();
        }
        if(allPreOrders==null){
            allPreOrders=new ArrayList<>();
        }
        return allPreOrders;
    }

    public void initToolbar(String title) {
        Toolbar myToolbar = findViewById(R.id.include);
        setSupportActionBar(myToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        }
        getSupportActionBar().setCustomView(R.layout.custom_action_bar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setTextColor(getResources().getColor(R.color.black_text));
        tvTitle.setText(title);
        myToolbar.setNavigationOnClickListener(view -> finish());
    }

    private void setTitle(String title) {
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(title);
    }

    private ProgressDialog showProgressBar(Context context) {
        ProgressDialog progress = new ProgressDialog(context);
        progress.setMessage("Берем заказ...");
        progress.setCancelable(false);
        progress.setCanceledOnTouchOutside(false);
        progress.show();
        return progress;
    }

    @Override
    protected void onResume() {
        super.onResume();
        //tripManager.addChangeStatusListener(tripManagerListener);
        manfredPreOrderManager.addChangePreOrderListener(preOrdersListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //tripManager.removeChangeStatusListener(tripManagerListener);
        manfredPreOrderManager.removeChangePreOrderListener(preOrdersListener);
    }
}
