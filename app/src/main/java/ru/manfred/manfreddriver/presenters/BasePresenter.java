/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.presenters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.interactor.repository.RideRepository;
import ru.manfred.manfreddriver.model.api.ManfredError;
import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.utils.Constants;
import ru.manfred.manfreddriver.utils.SharedPreferencesManager;

/**
 * Created by begemot on 18.01.18.
 * presenter for BaseActivity
 */

public class BasePresenter {
    private static final String TAG = "BasePresenter";

    public void onConfirmPreOrder(Context context, Order order, MainPresenter.MainPresenterCallback mainPresenterCallback) {
        ProgressDialog progress = showProgressBar(context, order);
        if (progress == null) return;
        final RideRepository rideRepository = ((ManfredApplication)context.getApplicationContext()).getRepoProvider()
                .getRideRepository(SharedPreferencesManager.getCredentials(context).getToken());
        rideRepository.confirmPreOrder(order, new RideRepository.AcceptCallback() {
            @Override
            public void error(ManfredError manfredError) {
                progress.dismiss();
                mainPresenterCallback.error(manfredError.getText());
            }

            @Override
            public void accepted(Order order) {
                progress.dismiss();
                mainPresenterCallback.positive();
            }

            @Override
            public void debugMessage(String message) {
                Log.d(TAG, "debugMessage: " + message);
            }

            @Override
            public void serverNotAvailable() {
                Intent intent = new Intent(Constants.INTERNET_STATUS);
                intent.putExtra(Constants.INTERNET_STATUS, false);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        });
    }

    public void onDeclinePreOrder(Context context, Order order, MainPresenter.MainPresenterCallback mainPresenterCallback) {
        ProgressDialog progress = showProgressBar(context, order);
        if (progress == null) return;
        RideRepository rideRepository = ((ManfredApplication)context.getApplicationContext()).getRepoProvider().getRideRepository(SharedPreferencesManager
                .getCredentials(context.getApplicationContext()).getToken());
        rideRepository.declinePreOrder(order, new RideRepository.AcceptCallback() {
            @Override
            public void error(ManfredError manfredError) {
                Log.d(TAG, "error: ");
                progress.dismiss();
                mainPresenterCallback.error(manfredError.getText());
            }

            @Override
            public void accepted(Order order) {
                progress.dismiss();
                mainPresenterCallback.positive();
            }

            @Override
            public void debugMessage(String message) {
                Log.d(TAG, "debugMessage: " + message);
            }

            @Override
            public void serverNotAvailable() {
                Intent intent = new Intent(Constants.INTERNET_STATUS);
                intent.putExtra(Constants.INTERNET_STATUS, false);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        });
    }

    @Nullable
    private ProgressDialog showProgressBar(Context context, Order order) {
        if (order == null) {
            //Toast.makeText(context,"Ошибка: заказ пуст",Toast.LENGTH_LONG).show();
            return null;
        }
        ProgressDialog progress = new ProgressDialog(context);
        progress.setMessage("Подтверждаем заказ...");
        progress.setCancelable(false);
        progress.setCanceledOnTouchOutside(false);
        progress.show();
        return progress;
    }
}
