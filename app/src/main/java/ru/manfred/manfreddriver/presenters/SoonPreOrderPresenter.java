/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.presenters;

import ru.manfred.manfreddriver.model.api.Order;

public class SoonPreOrderPresenter {
    private interface PreOrderView {
        void PreOrderSoonDialog(Order preOrder);

        void ReadyToStart(Order order);
    }

    void showPreOrderSoonDialog() {

    }

    void showReadyToStart() {

    }
}
