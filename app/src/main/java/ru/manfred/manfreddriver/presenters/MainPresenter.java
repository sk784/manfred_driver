/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.presenters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import javax.inject.Inject;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.activities.preorders.details.PreordersDetailActivity;
import ru.manfred.manfreddriver.interactor.repository.DriverRepository;
import ru.manfred.manfreddriver.interactor.repository.RideRepository;
import ru.manfred.manfreddriver.managers.ManfredOrderManager;
import ru.manfred.manfreddriver.managers.ManfredTripManager;
import ru.manfred.manfreddriver.managers.StatusManager;
import ru.manfred.manfreddriver.managers.logging.LogTags;
import ru.manfred.manfreddriver.managers.logging.ManfredLoggingManager;
import ru.manfred.manfreddriver.managers.preorder.ManfredPreOrderManager;
import ru.manfred.manfreddriver.model.GoogleMaps.DirectionResults;
import ru.manfred.manfreddriver.model.api.GeoData;
import ru.manfred.manfreddriver.model.api.ManfredError;
import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.utils.Constants;
import ru.manfred.manfreddriver.utils.SharedPreferencesManager;
import ru.manfred.manfreddriver.utils.TimeCalculatorFromDirection;
import ru.manfred.manfreddriver.viewmodel.OrderViewModel;

/**
 * Created by begemot on 08.12.17.
 */

abstract public class MainPresenter {
    private static final String TAG = "MainPresenter";
    @Inject
    protected ManfredTripManager tripManager;
    @Inject
    protected ManfredOrderManager orderManager;
    @Inject
    protected ManfredPreOrderManager preOrderManager;
    @Inject
    protected ManfredLoggingManager loggingManager;
    @Inject
    StatusManager statusManager;

    public interface MainPresenterCallback {
        void positive();

        void error(String text);
    }


    @Nullable
    private ProgressDialog showProgressBar(Context context, OrderViewModel order) {
        if (order == null) {
            Toast.makeText(context, "Ошибка: заказ пуст", Toast.LENGTH_LONG).show();
            return null;
        }
        ProgressDialog progress = new ProgressDialog(context);
        progress.setMessage("Берем заказ...");
        progress.setCancelable(false);
        progress.setCanceledOnTouchOutside(false);
        progress.show();
        return progress;
    }

    @Nullable
    private ProgressDialog showProgressBar(Context context, Order order) {
        if (order == null) {
            Toast.makeText(context, "Ошибка: заказ пуст", Toast.LENGTH_LONG).show();
            return null;
        }
        ProgressDialog progress = new ProgressDialog(context);
        progress.setMessage("Берем заказ...");
        progress.setCancelable(false);
        progress.setCanceledOnTouchOutside(false);
        progress.show();
        return progress;
    }


    /**
     * called when preorder start
     *
     * @param order pre-order
     */
    public void onStartPreOrder(Context context, OrderViewModel order, @Nullable Location location) {
        if(location==null){
            Toast.makeText(context, "Ваше местоположение не определено. Старт поездки невозможен. Проверьте разрешения геолокации",
                    Toast.LENGTH_LONG).show();
            preOrderManager.forceRefresh();
            loggingManager.addLog("MainPresenter", "onStartPreOrder", "not have location", LogTags.OTHER);
            return;
        }
        ManfredApplication.getTripComponent().inject(this);
        ProgressDialog progress = showProgressBar(context, order);
        if (progress == null) return;
        String token = SharedPreferencesManager.getCredentials(context).getToken();
        final RideRepository rideRepository = ((ManfredApplication)context.getApplicationContext()).getRepoProvider().getRideRepository(token);
        Location passengerLocation = new Location("");
        passengerLocation.setLongitude(order.getOrder().getLongitude());
        passengerLocation.setLatitude(order.getOrder().getLatitude());
        rideRepository.getDirection(new GeoData(location), new GeoData(passengerLocation), new RideRepository.RepoAnswer<DirectionResults>() {
            @Override
            public void allOk(DirectionResults answer) {
                TimeCalculatorFromDirection timeCalculatorFromDirection = new TimeCalculatorFromDirection(answer);
                rideRepository.startPreOrder(order.getOrder(), timeCalculatorFromDirection.getCalculatedTripProperties().getDurationInSec(),
                        new RideRepository.AcceptCallback() {
                    @Override
                    public void error(ManfredError manfredError) {
                        progress.dismiss();
                        loggingManager.addLog("MainPresenter",
                                "onStartPreOrder error", manfredError.toString(), LogTags.OTHER);
                        Toast.makeText(context, manfredError.getText(), Toast.LENGTH_SHORT).show();
                        //preOrderManager.removePreOrder(order.getOrder());
                        preOrderManager.forceRefresh();
                        if(manfredError.getResultCode().equals("invalid_order")){
                            preOrderManager.removePreOrder(order.getOrder());

                        }
                    }

                    @Override
                    public void accepted(Order accOrder) {
                        Log.d(TAG, "onStartPreOrder: preOrder " + order.getOrder().getId());
                        progress.dismiss();
                        tripManager.startRideToPassenger(accOrder);
                        preOrderManager.preOrderGo(accOrder);

                    }

                    @Override
                    public void debugMessage(String message) {
                        Log.d(TAG, "debugMessage: " + message);
                    }

                    @Override
                    public void serverNotAvailable() {
                        Intent intent = new Intent(Constants.INTERNET_STATUS);
                        intent.putExtra(Constants.INTERNET_STATUS, false);
                        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                    }
                });
            }

            @Override
            public void error(String error) {

            }
        });
        String logString = "preOrder #" + order.getId();
        loggingManager.addLog("MainPresenter", "onStartPreOrder", logString, LogTags.PREORDER_READY_TO_START_CLICK_START);

    }

    public void showPreOrder(Context context, OrderViewModel orderViewModel) {
        Log.d(TAG, "showPreOrder: ");
        //for scroll to history details
        Intent intent = new Intent(context, PreordersDetailActivity.class);
        //Log.d(TAG, "itemClicked: "+clickedHistoryItemId);
        intent.putExtra(Constants.PREORDER_IN_LIST_ID, orderViewModel.getOrder().getId());
        intent.putExtra(Constants.PREORDER_MY, true);
        context.startActivity(intent);
    }


    public abstract void changeStatus();

    public abstract void showAway();
}
