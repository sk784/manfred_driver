/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.presenters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Objects;

import javax.inject.Inject;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.activities.preorders.details.PreordersDetailActivity;
import ru.manfred.manfreddriver.managers.ManfredOrderManager;
import ru.manfred.manfreddriver.managers.ManfredSnackbarManager;
import ru.manfred.manfreddriver.managers.ManfredTripManager;
import ru.manfred.manfreddriver.managers.SnackbarManager;
import ru.manfred.manfreddriver.managers.logging.LogTags;
import ru.manfred.manfreddriver.managers.logging.ManfredLoggingManager;
import ru.manfred.manfreddriver.managers.preorder.ManfredPreOrderManager;
import ru.manfred.manfreddriver.managers.preorder.PreOrderManager;
import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.utils.Constants;
import ru.manfred.manfreddriver.viewmodel.PreOrderVM;

/**
 * Created by begemot on 18.12.17.
 * presenter for preOrder screen
 */

public class PreOrderPresenter {
    private boolean isMyPreOrders;
    private static final String TAG = "PreOrderPresenter";
    @Inject
    ManfredPreOrderManager preOrderManager;
    @Inject
    ManfredOrderManager orderManager;
    @Inject
    ManfredTripManager tripManager;
    @Inject
    ManfredSnackbarManager manfredSnackbarManager;
    @Inject
    ManfredLoggingManager loggingManager;

    public PreOrderPresenter(boolean isMyPreOrders) {
        this.isMyPreOrders = isMyPreOrders;
        ManfredApplication.getTripComponent().inject(this);
    }

    public void showDetails(Context context, PreOrderVM item) {
        Intent intent = new Intent(context, PreordersDetailActivity.class);
        //for scroll to history details
        long clickedHistoryItemId = item.getOrder().getId();
        Log.d(TAG, "itemClicked: " + clickedHistoryItemId);
        intent.putExtra(Constants.PREORDER_IN_LIST_ID, clickedHistoryItemId);
        intent.putExtra(Constants.PREORDER_MY, isMyPreOrders);
        //intent.putParcelableArrayListExtra(Constants.ALL_HISTORY, allHistory);
        context.startActivity(intent);
    }

    public void confirmPreOrder(View view, Context context, PreOrderVM preOrderVM) {
        String logString = "confirmPreOrder: we take pre-order with id " + preOrderVM.getOrder().getId();
        loggingManager.addLog("PreOrderPresenter", "confirmPreOrder",
                logString, LogTags.NEW_PREORDER_ACCEPTING_CLICK);
        view.setEnabled(false);
        ((Button)view).setText("");
        preOrderVM.startAccepting();
        preOrderManager.acceptPreOrder(preOrderVM.getOrder(), new PreOrderManager.ResultListener() {
            @Override
            public void ok(Order order) {
                Log.d(TAG, "ok: status of accepted preOrder is " + order.getStatus());
                if (Objects.equals(order.getStatus(), "RIDE_TO_PASSENGER")) {
                    //orderManager.setCurrentOrder(order);
                    //tripManager.setCurrOrder(order);
                    tripManager.startRideToPassenger(order);
                } else {
                    ((Activity) context).finish();
                    manfredSnackbarManager.showSnack(SnackbarManager.Type.PREORDER_ACCEPTED);
                }
            }

            @Override
            public void error(String errorText) {
                ((Activity) context).finish();
                manfredSnackbarManager.showSnack(SnackbarManager.Type.PREORDER_TAKE_OTHER_DRIVER);
            }
        });

    }

    private void orderConfirmed(Context context, PreOrderVM preOrderVM) {
        Log.d(TAG, "orderConfirmed: ");
        //orderManager.setCurrentOrder(preOrderVM.getOrder());
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context, R.style.MyDialogTheme);
        String title = "Предзаказ принят";
        String message = "Заберите пассажира " + preOrderVM.getOrder().getCustomerName() + " " + preOrderVM.getScheduledTimeWithDate() + " по адресу " + preOrderVM.getStartAddress();
        builder.setMessage(message)
                .setTitle(title)
                .setCancelable(false)
                .setPositiveButton("OK", (dialogInterface, i) -> ((Activity) context).finish());
        android.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void cancelPreOrder(PreOrderVM preOrderVM, Context context) {
        String log = "confirmPreOrder: we cancel pre-order with id " + preOrderVM.getOrder().getId();
        loggingManager.addLog("PreOrderPresenter", "cancelPreOrder", log, LogTags.OTHER);
        preOrderManager.cancelOrder(preOrderVM.getOrder(), new PreOrderManager.ResultListener() {
            @Override
            public void ok(Order order) {
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context, R.style.MyDialogTheme);
                String title = "Предзаказ отменен";
                String message = "Вы отменили предказ номер " + preOrderVM.getOrder().getId();
                builder.setMessage(message)
                        .setTitle(title)
                        .setCancelable(false)
                        .setPositiveButton("OK", (dialogInterface, i) -> {
                            ((Activity) context).finish();
                        });
                android.app.AlertDialog dialog = builder.create();
                dialog.show();
            }

            @Override
            public void error(String errorText) {
                Log.d(TAG, "error: "+errorText);
                Toast.makeText(context, errorText, Toast.LENGTH_LONG).show();
            }
        });
    }
}
