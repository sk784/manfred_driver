/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import ru.manfred.manfreddriver.db.dao.LocationsDao;
import ru.manfred.manfreddriver.db.dao.LogDao;
import ru.manfred.manfreddriver.db.entity.GeoDateEntity;
import ru.manfred.manfreddriver.db.entity.LogDB;

/**
 * Created by begemot on 03.10.17.
 */

@Database(entities = {LogDB.class, GeoDateEntity.class}, version = 3, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    //public abstract OrderDao orderDao();
    public abstract LogDao logDao();
    public abstract LocationsDao locationsDao();

/*    private static AppDatabase INSTANCE;


    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "user-database")
                                    .fallbackToDestructiveMigration()
                                    .build();
                }
            }
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }*/
}
