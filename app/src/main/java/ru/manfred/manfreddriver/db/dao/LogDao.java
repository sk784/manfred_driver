/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import ru.manfred.manfreddriver.db.entity.LogDB;

@Dao
public interface LogDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(LogDB log);

    @Query("SELECT * FROM LogDB")
    List<LogDB> getAllLogs();

    @Query("SELECT * FROM LogDB WHERE driverLogin = :driverName")
    LiveData<List<LogDB>> getDriverLogs(String driverName);

    @Query("SELECT * FROM LogDB WHERE driverLogin = :driverName")
    List<LogDB> getDriverLogsSync(String driverName);

    @Delete
    void deleteLogs(List<LogDB> logs);
}
