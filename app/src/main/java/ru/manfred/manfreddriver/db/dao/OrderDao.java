/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import ru.manfred.manfreddriver.db.entity.OrderDb;

/**
 * Created by begemot on 03.10.17.
 */
@Dao
public interface OrderDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(OrderDb order);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<OrderDb> orderDbs);

    @Delete
    void delete(OrderDb order);

    @Query("SELECT * FROM OrderDb")
    List<OrderDb> getAllOrders();

    @Query("SELECT * FROM OrderDb WHERE declined")
    List<OrderDb> getDeclinedOrders();

    @Query("SELECT * FROM OrderDb WHERE id == :id")
    OrderDb getOrderById(long id);

    @Query("SELECT id FROM OrderDb WHERE isPreOrder AND readied")
    List<Long> getReadiedPreOrdersIDs();
}
