/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.db.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.Nullable;

import java.util.Date;

import ru.manfred.manfreddriver.model.api.Order;

/**
 * Created by begemot on 03.10.17.
 */
@Entity
public class OrderDb {
    @PrimaryKey
    private long id;
    private double longitude;
    private double latitude;
    private String dest_longitude;
    private String dest_address;
    private String dest_latitude;
    private String address;
    private Date orderTime;
    private Date ride_start_time;
    private Date ride_finish_time;
    private Date created;
    private Boolean needAssistant;
    private String customerName;
    private String comments;
    private String customerNumber;
    private String status;
    private String final_address;
    private boolean declined = false;
    private boolean isPreOrder;
    private boolean readied;
    private @Nullable
    Date car_need_time;
    private String payment_type;

    public OrderDb() {
    }

    public OrderDb(Order order) {
        id = order.getId();
        longitude = order.getLongitude();
        latitude = order.getLatitude();
        dest_longitude = order.getDest_longitude();
        dest_address = order.getDest_address();
        dest_latitude = order.getDest_latitude();
        address = order.getAddress();
        orderTime = order.getOrderTime();
        ride_start_time = order.getRide_start_time();
        ride_finish_time = order.getRide_finish_time();
        created = order.getCreated();
        needAssistant = order.getNeedAssistant();
        customerName = order.getCustomerName();
        comments = order.getComments();
        customerName = order.getCustomerName();
        customerNumber = order.getCustomerNumber();
        status = order.getStatus();
        final_address = order.getFinal_address();
        car_need_time = order.getCar_need_time();
        if (car_need_time != null) {
            isPreOrder = true;
        }
        readied = false;
        payment_type = order.getPayment_type();
    }

    public boolean isPreOrder() {
        return isPreOrder;
    }

    public void setPreOrder(boolean preOrder) {
        isPreOrder = preOrder;
    }

    public boolean isReadied() {
        return readied;
    }

    public void setReadied(boolean readied) {
        this.readied = readied;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getDest_longitude() {
        return dest_longitude;
    }

    public void setDest_longitude(String dest_longitude) {
        this.dest_longitude = dest_longitude;
    }

    public String getDest_address() {
        return dest_address;
    }

    public void setDest_address(String dest_address) {
        this.dest_address = dest_address;
    }

    public String getDest_latitude() {
        return dest_latitude;
    }

    public void setDest_latitude(String dest_latitude) {
        this.dest_latitude = dest_latitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public Date getRide_start_time() {
        return ride_start_time;
    }

    public void setRide_start_time(Date ride_start_time) {
        this.ride_start_time = ride_start_time;
    }

    public Date getRide_finish_time() {
        return ride_finish_time;
    }

    public void setRide_finish_time(Date ride_finish_time) {
        this.ride_finish_time = ride_finish_time;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Boolean getNeedAssistant() {
        return needAssistant;
    }

    public void setNeedAssistant(Boolean needAssistant) {
        this.needAssistant = needAssistant;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFinal_address() {
        return final_address;
    }

    public void setFinal_address(String final_address) {
        this.final_address = final_address;
    }

    public boolean isDeclined() {
        return declined;
    }

    public void setDeclined(boolean declined) {
        this.declined = declined;
    }

    @Nullable
    public Date getCar_need_time() {
        return car_need_time;
    }

    public void setCar_need_time(@Nullable Date car_need_time) {
        this.car_need_time = car_need_time;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }
}
