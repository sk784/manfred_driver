/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import ru.manfred.manfreddriver.db.entity.GeoDateEntity;

@Dao
public interface LocationsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(GeoDateEntity geoDateEntity);

    @Query("SELECT * FROM GeoDateEntity")
    List<GeoDateEntity> getAllGeos();

    @Query("SELECT * FROM GeoDateEntity WHERE driverLogin = :driverName")
    LiveData<List<GeoDateEntity>> getDriverLocations(String driverName);

    @Query("SELECT * FROM GeoDateEntity WHERE driverLogin = :driverName")
    List<GeoDateEntity> getDriverLocationsSync(String driverName);

    @Delete
    public void deleteLocations(List<GeoDateEntity> locations);
}
