/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.db.entity;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import ru.manfred.manfreddriver.model.api.LogNet;

@Entity
public class LogDB {
    @PrimaryKey(autoGenerate = true)
    public long id;
    public String driverLogin;
    @Embedded
    public LogNet logNet;
}
