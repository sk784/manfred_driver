/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.di.modules;

import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.manfred.manfreddriver.managers.ManfredSnackbarManager;
import ru.manfred.manfreddriver.managers.SnackbarManager;

/**
 * Created by begemot on 20.02.18.
 */
@Module
public class SnackbarModule {
    @Provides
    @NonNull
    @Singleton
    public SnackbarManager provideSnackbarManager() {
        return new ManfredSnackbarManager();
    }
}
