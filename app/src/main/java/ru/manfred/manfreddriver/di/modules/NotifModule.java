/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.di.modules;

import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.manfred.manfreddriver.managers.ManfredActivityNotificationManager;
import ru.manfred.manfreddriver.managers.ActivityNotificationManager;

/**
 * Created by begemot on 22.02.18.
 */
@Module
public class NotifModule {
    @Provides
    @NonNull
    @Singleton
    public ActivityNotificationManager provideNotificationManager() {
        return new ManfredActivityNotificationManager();
    }
}
