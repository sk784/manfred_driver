/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.di.modules;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.manfred.manfreddriver.managers.preorder.ManfredPreOrderManager;
import ru.manfred.manfreddriver.managers.preorder.PreOrderManager;

/**
 * Created by begemot on 14.12.17.
 */
@Module
public class PreOrderModule {
    @Provides
    @NonNull
    @Singleton
    public PreOrderManager providePreOrderManager(Context context) {
        Log.d("PreOrderModule", "providePreOrderManager: ");
        return new ManfredPreOrderManager(context);
    }

}
