/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.di.modules;

import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.manfred.manfreddriver.managers.logging.ManfredLoggingManager;

/**
 * Created by begemot on 16.03.18.
 */
@Module
public class LogModule {
    @Provides
    @NonNull
    @Singleton
    public ManfredLoggingManager provideLoggingManager(Context context) {
        return new ManfredLoggingManager(context);
    }
}
