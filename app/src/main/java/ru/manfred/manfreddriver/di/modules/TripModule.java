/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.di.modules;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.manfred.manfreddriver.managers.ManfredTripManager;
import ru.manfred.manfreddriver.managers.TripManager;

/**
 * Created by begemot on 15.10.17.
 */
@Module
public class TripModule {
    @Provides
    @NonNull
    @Singleton
    public TripManager provideTripManage(Context context) {
        Log.d("TripModule", "provideTripManage: ");
        return new ManfredTripManager(context);
    }
}
