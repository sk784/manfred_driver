package ru.manfred.manfreddriver.di.modules;

import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.manfred.manfreddriver.managers.notification.ManfredNotificationManager;
import ru.manfred.manfreddriver.managers.notification.StatusBarNotification;

@Module
public class StatusBarNotificationModule {
    @Provides
    @NonNull
    @Singleton
    public ManfredNotificationManager provideNotificationManager(Context context){
        return new ManfredNotificationManager(context);
    }
}
