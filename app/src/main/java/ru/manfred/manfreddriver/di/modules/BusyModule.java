/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.di.modules;

import android.support.annotation.NonNull;
import android.util.Log;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.manfred.manfreddriver.managers.StatusManager;

@Module
public class BusyModule {
    @Provides
    @NonNull
    @Singleton
    public StatusManager provideStatusManager(){
        Log.d("BusyModule", "provideStatusManager: ");
        return new StatusManager();
    }
}
