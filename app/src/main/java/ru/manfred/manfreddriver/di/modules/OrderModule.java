/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.di.modules;

import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.manfred.manfreddriver.managers.ManfredOrderManager;
import ru.manfred.manfreddriver.managers.OrderManager;

/**
 * Created by begemot on 25.10.17.
 */
@Module
public class OrderModule {
    @Provides
    @NonNull
    @Singleton
    public OrderManager provideOrderManager(Context context) {
        return new ManfredOrderManager(context);
    }
}
