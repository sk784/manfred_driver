/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.di.modules;

import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.manfred.manfreddriver.managers.CarsOnMapManager;
import ru.manfred.manfreddriver.managers.ManfredCarsOnMapManager;

@Module
public class CarOnMapModule {
    @Provides
    @NonNull
    @Singleton
    public CarsOnMapManager provideCarsOnMap() {
        return new ManfredCarsOnMapManager();
    }
}
