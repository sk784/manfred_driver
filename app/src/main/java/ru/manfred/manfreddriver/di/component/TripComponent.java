/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.di.component;

import javax.inject.Singleton;

import dagger.Component;
import ru.manfred.manfreddriver.activities.AeroChoiseActivity;
import ru.manfred.manfreddriver.activities.AwayActivity;
import ru.manfred.manfreddriver.activities.BaseActivity;
import ru.manfred.manfreddriver.activities.FinalActivity;
import ru.manfred.manfreddriver.activities.LateActivity;
import ru.manfred.manfreddriver.activities.MapsActivity;
import ru.manfred.manfreddriver.activities.ModalActivity;
import ru.manfred.manfreddriver.activities.NewOrderActivity;
import ru.manfred.manfreddriver.activities.NoMoneyActivity;
import ru.manfred.manfreddriver.activities.ProfileActivity;
import ru.manfred.manfreddriver.activities.RideActivity;
import ru.manfred.manfreddriver.activities.SplashActivity;
import ru.manfred.manfreddriver.activities.TripToPassengerActivity;
import ru.manfred.manfreddriver.activities.WaitActivity;
import ru.manfred.manfreddriver.activities.history.HistoryActivity;
import ru.manfred.manfreddriver.activities.history.details.HistoryDetailsActivity;
import ru.manfred.manfreddriver.activities.login.LoginActivity;
import ru.manfred.manfreddriver.activities.preorders.PreorderFragment;
import ru.manfred.manfreddriver.activities.preorders.PreordersActivity;
import ru.manfred.manfreddriver.activities.preorders.details.PreordersDetailActivity;
import ru.manfred.manfreddriver.di.modules.AppModule;
import ru.manfred.manfreddriver.di.modules.BusyModule;
import ru.manfred.manfreddriver.di.modules.CarOnMapModule;
import ru.manfred.manfreddriver.di.modules.CheckNetModule;
import ru.manfred.manfreddriver.di.modules.LocationModule;
import ru.manfred.manfreddriver.di.modules.LogModule;
import ru.manfred.manfreddriver.di.modules.NotifModule;
import ru.manfred.manfreddriver.di.modules.OrderModule;
import ru.manfred.manfreddriver.di.modules.PreOrderModule;
import ru.manfred.manfreddriver.di.modules.SnackbarModule;
import ru.manfred.manfreddriver.di.modules.SoundModule;
import ru.manfred.manfreddriver.di.modules.StatusBarNotificationModule;
import ru.manfred.manfreddriver.di.modules.TripModule;
import ru.manfred.manfreddriver.interactor.api.websocket.WebSocketClient;
import ru.manfred.manfreddriver.managers.ManfredConnectionStatusManager;
import ru.manfred.manfreddriver.managers.ManfredEventManager;
import ru.manfred.manfreddriver.managers.ManfredLocationManager;
import ru.manfred.manfreddriver.managers.ManfredOrderManager;
import ru.manfred.manfreddriver.managers.sound.ManfredSoundManager;
import ru.manfred.manfreddriver.managers.logging.ManfredLoggingManager;
import ru.manfred.manfreddriver.managers.preorder.ManfredPreOrderManager;
import ru.manfred.manfreddriver.presenters.MainPresenter;
import ru.manfred.manfreddriver.presenters.PreOrderPresenter;
import ru.manfred.manfreddriver.services.CheckEventService;
import ru.manfred.manfreddriver.utils.PreOrdersDateSorter;
import ru.manfred.manfreddriver.utils.RestoreTrip;

/**
 * Created by begemot on 15.10.17.
 */

@Component(modules = {AppModule.class, TripModule.class, OrderModule.class, SoundModule.class, PreOrderModule.class, SnackbarModule.class,
        NotifModule.class, LogModule.class, LocationModule.class, CarOnMapModule.class, CheckNetModule.class, BusyModule.class, StatusBarNotificationModule.class})
@Singleton
public interface TripComponent {
    void inject(ManfredEventManager manfredEventManager);

    void inject(ManfredOrderManager orderManager);

    void inject(BaseActivity activity);

    void inject(MapsActivity activity);

    void inject(CheckEventService service);

    void inject(FinalActivity activity);

    void inject(ManfredSoundManager soundManager);

    void inject(TripToPassengerActivity activity);

    void inject(RestoreTrip restoreTrip);

    void inject(ModalActivity modalActivity);

    void inject(MainPresenter mainPresenter);

    void inject(PreorderFragment preorderFragment);

    void inject(PreordersActivity preordersActivity);

    void inject(PreordersDetailActivity preordersDetailActivity);

    void inject(PreOrderPresenter preOrderPresenter);

    void inject(AeroChoiseActivity aeroChoiseActivity);

    void inject(NoMoneyActivity noMoneyActivity);

    void inject(ManfredLoggingManager manfredLoggingManager);

    void inject(ManfredPreOrderManager preOrderManager);

    void inject(PreOrdersDateSorter preOrdersDateSorter);

    void inject(NewOrderActivity newOrderActivity);

    void inject(AwayActivity awayActivity);

    void inject(HistoryActivity historyActivity);

    void inject(HistoryDetailsActivity historyDetailsActivity);

    void inject(ProfileActivity profileActivity);

    void inject(WaitActivity waitActivity);

    void inject(RideActivity rideActivity);

    void inject(LoginActivity loginActivity);

    void inject(LateActivity lateActivity);
    void inject(ManfredLocationManager manfredLocationManager);

    void inject(ManfredConnectionStatusManager manfredConnectionStatusManager);

    void inject(SplashActivity splashActivity);

    void inject(WebSocketClient webSocketClient);
}
