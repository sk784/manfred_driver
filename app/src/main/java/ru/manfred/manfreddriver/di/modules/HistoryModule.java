/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.di.modules;

import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.manfred.manfreddriver.managers.history.HistoryCache;
import ru.manfred.manfreddriver.managers.history.ManfredHistoryCache;

/**
 * Created by begemot on 13.03.18.
 */
@Module
public class HistoryModule {
    @Provides
    @NonNull
    @Singleton
    public HistoryCache provideHistoryCache() {
        //Log.d("TripModule", "provideTripManage: ");
        return new ManfredHistoryCache();
    }
}
