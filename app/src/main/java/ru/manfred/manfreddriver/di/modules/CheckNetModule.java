/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.di.modules;

import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.manfred.manfreddriver.managers.ConnectionStatusManager;
import ru.manfred.manfreddriver.managers.ManfredConnectionStatusManager;

@Module
public class CheckNetModule {
    @Provides
    @NonNull
    @Singleton
    public ConnectionStatusManager provideConnectionStatusManager(Context context) {
        return new ManfredConnectionStatusManager(context);
    }
}
