/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.di.component;

import javax.inject.Singleton;

import dagger.Component;
import ru.manfred.manfreddriver.di.modules.HistoryModule;
import ru.manfred.manfreddriver.managers.history.ManfredHistoryManager;

/**
 * Created by begemot on 13.03.18.
 */
@Component(modules = {HistoryModule.class})
@Singleton
public interface HistoryComponent {
    void inject(ManfredHistoryManager historyManager);
}
