/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.di.modules;

import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.manfred.manfreddriver.managers.CurrLocationManager;
import ru.manfred.manfreddriver.managers.ManfredLocationManager;

/**
 * Created by begemot on 21.03.18.
 */
@Module
public class LocationModule {
    @Provides
    @NonNull
    @Singleton
    public CurrLocationManager provideLocationManager(Context context) {
        return new ManfredLocationManager(context);
    }
}
