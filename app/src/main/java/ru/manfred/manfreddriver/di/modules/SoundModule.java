/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.di.modules;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.manfred.manfreddriver.managers.sound.ManfredSoundManager;
import ru.manfred.manfreddriver.managers.ManfredTripManager;
import ru.manfred.manfreddriver.managers.sound.SoundManager;

/**
 * Created by begemot on 01.11.17.
 */
@Module
public class SoundModule {
    @Provides
    @NonNull
    @Singleton
    public SoundManager provideSoundManager(Context context, ManfredTripManager tripManager) {
        Log.d("SoundModule", "provideSoundManager: ");
        return new ManfredSoundManager(context, tripManager);
    }
}
