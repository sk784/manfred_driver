/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.services;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import io.intercom.android.sdk.push.IntercomPushClient;

/**
 * Created by begemot on 01.12.17.
 */

public class ManfredFCMIDService extends FirebaseInstanceIdService {
    private final IntercomPushClient intercomPushClient = new IntercomPushClient();

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        intercomPushClient.sendTokenToIntercom(getApplication(), refreshedToken);
        //DO HOST LOGIC HERE
    }
}
