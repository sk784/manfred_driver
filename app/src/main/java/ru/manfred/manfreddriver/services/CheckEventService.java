/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.services;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.arch.lifecycle.LifecycleService;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.activities.SplashActivity;
import ru.manfred.manfreddriver.db.entity.GeoDateEntity;
import ru.manfred.manfreddriver.interactor.api.ApiFactory;
import ru.manfred.manfreddriver.interactor.repository.DBRepository;
import ru.manfred.manfreddriver.interactor.repository.DriverRepository;
import ru.manfred.manfreddriver.interactor.repository.ResponseCallback;
import ru.manfred.manfreddriver.interactor.repository.RideRepository;
import ru.manfred.manfreddriver.managers.ManfredCarsOnMapManager;
import ru.manfred.manfreddriver.managers.ManfredConnectionStatusManager;
import ru.manfred.manfreddriver.managers.ManfredCredentialsManager;
import ru.manfred.manfreddriver.managers.ManfredEventManager;
import ru.manfred.manfreddriver.managers.ManfredLocationManager;
import ru.manfred.manfreddriver.managers.ManfredOrderManager;
import ru.manfred.manfreddriver.managers.ManfredTripManager;
import ru.manfred.manfreddriver.managers.StatusManager;
import ru.manfred.manfreddriver.managers.logging.LogTags;
import ru.manfred.manfreddriver.managers.logging.ManfredLoggingManager;
import ru.manfred.manfreddriver.managers.network.EventProcessedAnswer;
import ru.manfred.manfreddriver.managers.network.ManfredEventNetworkReceiver;
import ru.manfred.manfreddriver.managers.network.NewEventAnswer;
import ru.manfred.manfreddriver.managers.network.NewEventCallback;
import ru.manfred.manfreddriver.model.api.Car;
import ru.manfred.manfreddriver.model.api.Credentials;
import ru.manfred.manfreddriver.model.api.Driver;
import ru.manfred.manfreddriver.model.api.GeoData;
import ru.manfred.manfreddriver.model.api.ManfredError;
import ru.manfred.manfreddriver.model.api.ManfredEventResponse;
import ru.manfred.manfreddriver.model.responces.CarList;
import ru.manfred.manfreddriver.model.responces.IsFreeResponse;
import ru.manfred.manfreddriver.utils.ConcerrencyUtils;
import ru.manfred.manfreddriver.utils.SharedPreferencesManager;

public class CheckEventService extends LifecycleService {
    private static final long DELAY_SENDING_CORDS_MILLS = TimeUnit.SECONDS.toMillis(15);
    private static final long DELAY_SENDING_LOGS_MILLIS = TimeUnit.SECONDS.toMillis(120);
    //private static final
    private final String TAG = this.getClass().getName();
    @Nullable
    List<GeoDateEntity> notSendedGeo;
    @Inject
    ManfredOrderManager manfredOrderManager;
    @Inject
    ManfredLocationManager manfredLocationManager;
    //private ScheduledExecutorService reSendCoordsTask;
    /*
    private AtomicBoolean isFilteredCoordsReceived = new AtomicBoolean(true);
    private ScheduledExecutorService sendFilteredCoordsTask;*/
    @Inject
    ManfredCarsOnMapManager carsOnMapManager;
    @Inject
    ManfredConnectionStatusManager connectionStatusManager;
    @Inject
    ManfredTripManager tripManager;
    @Inject
    ManfredLoggingManager loggingManager;
    @Inject
    StatusManager statusManager;
    DBRepository dbRepository;
    ManfredCredentialsManager manfredCredentialsManager;
    ForegroundServiceStateResolver foregroundServiceStateResolver = new ForegroundServiceStateResolver();
    private @Nullable
    ScheduledExecutorService executorService;
    private AtomicBoolean isCarsChecked = new AtomicBoolean(true);
    private AtomicBoolean isReSendReceived = new AtomicBoolean(true);
    private AtomicBoolean isEventReceived = new AtomicBoolean(true);
    private AtomicBoolean isLocationReceived = new AtomicBoolean(true);

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ManfredApplication.getTripComponent().inject(this);
        loggingManager.addLog("CheckEventService", "onStartCommand", "", LogTags.OTHER);
        manfredCredentialsManager = ((ManfredApplication) getApplication()).getCredentialsManager();
        dbRepository = ((ManfredApplication) getApplication()).getDbRepository();
        initTasks(manfredCredentialsManager);
        //checkForBusy(manfredCredentialsManager.getCredentials().getValue());
        return super.onStartCommand(intent, flags, startId);
    }

    private void initWebSocketReceiver() {
        Log.d(TAG, "initWebSocketReceiver: ");
        ManfredEventManager manfredEventManager =
                new ManfredEventManager(manfredCredentialsManager, ((ManfredApplication) getApplication()).getRepoProvider());
        ManfredApplication.getNetworkManger().addRequestReceiver(createEventListener(manfredEventManager));
    }

    private void startSyncForegroundState() {
        statusManager.getIsFreeLiveData().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean != null) {
                    foregroundServiceStateResolver.setBusy(!aBoolean);
                }
            }
        });
        tripManager.getIsRiding().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean != null) {
                    foregroundServiceStateResolver.setRiding(aBoolean);
                }
            }
        });
    }

    /**
     * we do it because in Xiaomi service is restart and we must know actual busy status
     */
    private void checkForBusy(Credentials credentials) {
        if (credentials == null) return;
        Log.d(TAG, "checkForBusy: ");
        //Log.d(TAG, "checkForBusy: credentials is " + credentials.toString());
        DriverRepository driverRepository = ((ManfredApplication) getApplication()).getRepoProvider().getDriverRepository(credentials.getToken());
        driverRepository.getBusyStatus(new ResponseCallback<IsFreeResponse>() {
            @Override
            public void allOk(IsFreeResponse response) {
                boolean isFree = response.isFree();
                String logString = "statusIs: " + isFree;
                if (isFree) {
                    statusManager.setFree();
                } else {
                    statusManager.setBusy();
                }
                loggingManager.addLogAndSendImmediately("CheckEventService", "checkForBusy",
                        logString, LogTags.OTHER);
            }

            @Override
            public void error(ManfredError error) {
                Toast.makeText(getApplicationContext(), error.getText(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private ManfredEventNetworkReceiver createEventListener(ManfredEventManager manfredEventManager) {
        return new ManfredEventNetworkReceiver(new NewEventCallback() {
            @Override
            public void newEvent(NewEventAnswer event) {
                //Log.d(TAG, "eventListener: id of event " + event.getId());
                manfredEventManager.newEvent(event.getPayload());
                ManfredApplication.getNetworkManger().sentEventAcceptedRequest(new EventProcessedAnswer(event.getPayload().getId()));
                connectionStatusManager.serverAvailable();
            }
        });
    }

    private boolean isServiceRunningInForeground(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (manager != null) {
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    if (service.foreground) {
                        return true;
                    }

                }
            }
        }
        return false;
    }

    private void toForeground() {
        if (isServiceRunningInForeground(getApplicationContext(), CheckEventService.class)) return;
        String channelId;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channelId = createNotificationChannel();
        } else {
            // If earlier version channel ID is not used
            channelId = "";
        }
        Intent notificationIntent = new Intent(this, SplashActivity.class);
        notificationIntent.setAction(Intent.ACTION_MAIN);
        notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext(), channelId)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentTitle("Manfred Driver")
                .setContentText("Передача координат")
                .setContentIntent(pendingIntent);
        Notification notification = notificationBuilder.build();
        startForeground(1234, notification);
    }

    //https://stackoverflow.com/questions/47531742/startforeground-fail-after-upgrade-to-android-8-1
    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel() {
        String channelId = "my_service";
        String channelName = "Сервис manfred";
        NotificationChannel chan = new NotificationChannel(channelId,
                channelName, NotificationManager.IMPORTANCE_HIGH);
        NotificationManager service = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (service != null) {
            service.createNotificationChannel(chan);
        }
        return channelId;
    }

    private void initTasks(ManfredCredentialsManager credentialsManager) {
        Log.d(TAG, "initTasks: ");
        loggingManager.addLog("CheckEventService", "initTasks", "", LogTags.OTHER);

        /*
        if(credentials==null){
            loggingManager.addLog("CheckEventService", "initTasks",
                    "credtials is null, return", LogTags.OTHER);
            return;
        }*/

        executorService = Executors.newScheduledThreadPool(ConcerrencyUtils.recommendedThreadCount());
        //scheduleRegularTask(credentials, executorService);
        //scheduleReSendCoordsTask(executorService, credentials);
        credentialsManager.getCredentials().observe(this, new Observer<Credentials>() {
            @Override
            public void onChanged(@Nullable Credentials credentials) {
                if (credentials != null) {
                    scheduleTask(credentials);
                } else {
                    loggingManager.addLog("CheckEventService", "initTasks",
                            "credtials now is null, return", LogTags.OTHER);
                    stopAllTasks("unlogged");
                }
            }
        });
        startSyncForegroundState();
    }

    private void scheduleTask(@NonNull Credentials credentials) {
        stopAllTasks("stoping before schedulling new");
        foregroundServiceStateResolver.getIsNeedRegularTask().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean isForeground) {
                if(isForeground==null)return;
                Log.d(TAG, "foregroundServiceStateResolver onChanged: "
                        + (isForeground ? "need foreground" : "need background"));
                if(isForeground){
                    toForeground();
                    executorService = createServiceIfNeed(executorService);
                    scheduleRegularTask(credentials, executorService);
                }else {
                    stopForeground(true);
                    stopAllTasks("go to offline");
                }

                if(((ManfredApplication) getApplication()).getRepoProvider().isWebSocket()){
                    initWebSocketReceiver();
                }else {
                    if (isForeground) {
                        startEventTask(credentials);
                    }
                }
            }
        });

    }

    private void startEventTask(@NonNull Credentials credentials) {
        ManfredEventManager manfredEventManager =
                new ManfredEventManager(manfredCredentialsManager,
                        ((ManfredApplication) getApplication()).getRepoProvider());
        scheduleEventTask(executorService, manfredEventManager, credentials.getToken());
    }

    private ScheduledExecutorService createServiceIfNeed(ScheduledExecutorService scheduledExecutorService) {
        if (scheduledExecutorService == null) {
            return Executors.newScheduledThreadPool(ConcerrencyUtils.recommendedThreadCount());
        } else {
            if (scheduledExecutorService.isShutdown() || scheduledExecutorService.isTerminated()) {
                Log.d(TAG, "ScheduledExecutorService: executorService.isTerminated(), creating new");
                return Executors.newScheduledThreadPool(ConcerrencyUtils.recommendedThreadCount());
            } else {
                return scheduledExecutorService;
            }
        }
    }

    private void scheduleRegularTask(Credentials credentials, ScheduledExecutorService scheduledExecutorService) {
        Log.d(TAG, "scheduleRegularTask: ");
        loggingManager.addLog("CheckEventService", "scheduleRegularTask", "", LogTags.OTHER);
        if (scheduledExecutorService != null) {
            scheduleSendLocationTask(scheduledExecutorService);
            schedulleSendLogTask(scheduledExecutorService);
            scheduleCheckCarsTask(scheduledExecutorService, credentials);
        }
    }

    private void scheduleEventTask(ScheduledExecutorService executorService1, ManfredEventManager manfredEventManager, String token) {
        loggingManager.addLog("CheckEventService", "scheduleEventTask", "", LogTags.OTHER);
        executorService1.scheduleWithFixedDelay(new Runnable() { // Определяем задачу
            @Override
            public void run() {
                //Log.d(TAG, "run: check events");
                //loggingManager.addString(TAG, "check events");
                if (!isEventReceived.get()) {
                    loggingManager.addLog("CheckEventService", "run",
                            "events not getted because previous query not completed", LogTags.OTHER);
                    return;
                }
                isEventReceived.set(false);
                ApiFactory.getDriverService().getEvents(token).enqueue(new Callback<ManfredEventResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<ManfredEventResponse> call, @NonNull Response<ManfredEventResponse> response) {
                        ManfredEventResponse manfredEventResponse = response.body();
                        isEventReceived.set(true);
                        if (manfredEventResponse != null) {
                            switch (manfredEventResponse.getStatus()) {
                                case "success":
                                    manfredEventManager.newEvents(manfredEventResponse.getData().getEvents());
                                    connectionStatusManager.serverAvailable();
                                    break;
                                case "error":
                                    if (manfredEventResponse.getResult_code().equals("invalid_token")) {
                                        connectionStatusManager.invalidToken();
                                    }
                                    break;
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ManfredEventResponse> call, @NonNull Throwable t) {
                        isEventReceived.set(true);
                        if (t instanceof SocketTimeoutException || t instanceof java.net.UnknownHostException) {
                            connectionStatusManager.serverUnAvailable();
                        }
                    }
                });

            }
        }, 0L, 3000L, TimeUnit.MILLISECONDS);
    }

    private void schedulleSendLogTask(ScheduledExecutorService service) {
        service.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                //Log.d(TAG, "run: sendLogsTask");
                loggingManager.sendLogs();
            }
        }, 0L, DELAY_SENDING_LOGS_MILLIS, TimeUnit.MILLISECONDS);
    }

    private void scheduleCheckCarsTask(ScheduledExecutorService executorService, Credentials credentials) {
        executorService.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                //Log.d(TAG, "run: checkCars");
                if (!isCarsChecked.get()) {
/*                    loggingManager.addLog("CheckEventService", "run",
                            "carlist not checking because previous query not processed", LogTags.OTHER);*/
                    return;
                }
                isCarsChecked.set(false);
                DriverRepository driverRepository =
                        ((ManfredApplication) getApplication()).getRepoProvider().getDriverRepository(credentials.getToken());
                driverRepository.getCarsForMap(new DriverRepository.NetAnswerCallback<CarList>() {
                    @Override
                    public void allFine(CarList answer) {
                        isCarsChecked.set(true);
                        List<Driver> drivers = new ArrayList<>();
                        for (Car car : answer.getDrivers()) {
                            if (car.getDriver() != null) {
                                drivers.add(car.getDriver());
                            }
                        }
                        //Log.d(TAG, "allFine: we take new drivers coords");
                        carsOnMapManager.setCarsOnMap(drivers);
                    }

                    @Override
                    public void error(String error) {
                        isCarsChecked.set(true);
                    }

                    @Override
                    public void serverNotAvailable() {
                        isCarsChecked.set(true);
                    }
                });
            }
        }, 0L, 1L, TimeUnit.MINUTES);
    }

    private void scheduleReSendCoordsTask(ScheduledExecutorService executorService, Credentials credentials) {
        executorService.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                //Log.d(TAG, "run: scheduleReSendCoordsTask");
                if (notSendedGeo != null && !notSendedGeo.isEmpty()) {
                    String logText = "run: trying send " + notSendedGeo.size() + " locations";
                    loggingManager.addLog("CheckEventService", "run", logText, LogTags.OTHER);
                    if (!isReSendReceived.get()) {
                        loggingManager.addLog("CheckEventService", "run",
                                "not resending coords, because previus not processed", LogTags.OTHER);
                        return;
                    }
                    isReSendReceived.set(false);
/*                    List<GeoData> notSendedGeoDB = DBConvertorsUtils.geoDataDBToGeo(notSendedGeo);
                    rideService.sendNotAcceptedLocations(credentials.getToken(), new SendLocations(notSendedGeoDB)).enqueue(new Callback<ManfredResponse>() {
                        @Override
                        public void onResponse(@NonNull Call<ManfredResponse> call, @NonNull Response<ManfredResponse> response) {
                            ManfredResponse manfredResponse = response.body();
                            isReSendReceived.set(true);
                            if (manfredResponse != null) {
                                if (manfredResponse.getStatus().equals("success")) {
                                    loggingManager.addLog("CheckEventService", "sendLocationTask",
                                            "resended location success, clearing list", LogTags.OTHER);
                                    //notSendedGeo.clear();
                                    dbRepository.removeSendedLocations(notSendedGeo,credentials.getPhoneNumber());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<ManfredResponse> call, Throwable t) {
                            isReSendReceived.set(true);
                            String logText = "error with resending locations: " + t.getMessage();
                            loggingManager.addLog("CheckEventService", "sendLocationTask", logText, LogTags.OTHER);
                        }
                    });*/
                }
            }
        }, 1500L, 30000L, TimeUnit.MILLISECONDS);
    }

    private void scheduleSendLocationTask(ScheduledExecutorService executorService) {
        loggingManager.addLog("CheckEventService", "scheduleSendLocationTask", "", LogTags.OTHER);
        //re-init
        isLocationReceived.set(true);
        executorService.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                Location location = manfredLocationManager.getCurrentLocation();
                if (location == null) {
                    loggingManager.addLog("CheckEventService", "run",
                            "run: cords is null, not sending", LogTags.OTHER);
                    isLocationReceived.set(true);
                    return;
                }
                GeoData geoData = new GeoData(location.getLongitude(), location.getLatitude(), location.getTime(), location.getAccuracy());
                if (!isLocationReceived.get()) {
                    loggingManager.addLogSilence("CheckEventService", "run",
                            "not sending coords because previous query not completed", LogTags.OTHER);
                    addLocToNotSended(geoData);
                } else {
                    sendLocation(geoData);
                }
            }
        }, 500L, DELAY_SENDING_CORDS_MILLS, TimeUnit.MILLISECONDS);
    }

    private void sendLocation(GeoData geoData) {
        RideRepository rideRepository =
                ((ManfredApplication) getApplication()).getRepoProvider()
                        .getRideRepository(SharedPreferencesManager.getCredentials(getApplicationContext()).getToken());
        rideRepository.sendLocation(geoData, new ResponseCallback() {
            @Override
            public void allOk(Object response) {
                isLocationReceived.set(true);
            }

            @Override
            public void error(ManfredError error) {
                String logText = "onFailure: location not sended because " + error;
                loggingManager.addLog("CheckEventService", "sendLocationTask", logText, LogTags.OTHER);
                isLocationReceived.set(true);
                addLocToNotSended(geoData);
                if (error.getType() == ManfredError.ErrorStatus.NEED_AUTORISATION) {
                    Log.d(TAG, "error: need autorisation");
                    connectionStatusManager.invalidToken();
                }
            }
        });
        isLocationReceived.set(false);
    }

    private void addLocToNotSended(GeoData geoData) {
        /*
        loggingManager.addLog("CheckEventService", "addLocToNotSended", geoData.toString(), LogTags.OTHER);
        //notSendedGeo.add(geoData);
        Credentials credentials = manfredCredentialsManager.getCredentials().getValue();
        if (credentials != null) {
            dbRepository.addLocationToNotSended(geoData,credentials.getPhoneNumber());
        }*/
    }

    @Override
    public void onDestroy() {
        // The service is no longer used and is being destroyed
        Log.d(TAG, "destroy service");
        //loggingManager.addString(TAG, "destroy service");
        stopAllTasks("service destroyed");
        super.onDestroy();
    }

    private void stopAllTasks(String reason) {
        if (loggingManager != null) {
            loggingManager.addLogAndSendImmediately(TAG, "stopAllTasks",
                    reason, LogTags.OTHER);
        }
        if (executorService != null) {
            shutdownAndAwaitTermination(executorService);
        }
        isEventReceived.set(true);
        isCarsChecked.set(true);
        isLocationReceived.set(true);
        isReSendReceived.set(true);
    }

    private void shutdownAndAwaitTermination(ScheduledExecutorService pool) {
        pool.shutdown(); // Disable new tasks from being submitted
        Log.d(TAG, "shutdownAndAwaitTermination: pool.shutdown()");
        try {
            // Wait a while for existing tasks to terminate
            if (!pool.awaitTermination(2, TimeUnit.SECONDS)) {
                pool.shutdownNow(); // Cancel currently executing tasks
                // Wait a while for tasks to respond to being cancelled
                //executorService=null;
                loggingManager.addLogAndSendImmediately(TAG, "stopAllTasks",
                            "shutdownAndAwaitTermination: Pool did not terminate", LogTags.OTHER);
                Log.d(TAG, "shutdownAndAwaitTermination: Pool did not terminate");
            }
        } catch (InterruptedException ie) {
            // (Re-)Cancel if current thread also interrupted
            loggingManager.addLogAndSendImmediately(TAG, "stopAllTasks",
                    "shutdownAndAwaitTermination: pool.shutdownNow() because exception " + ie.getMessage(), LogTags.OTHER);
            pool.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
            //executorService=null;
            Log.d(TAG, "shutdownAndAwaitTermination: pool.shutdownNow() because exception " + ie.getMessage());
        }
    }

}
