/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.services;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

/**
 * strategy for select service state
 */
public class ForegroundServiceStateResolver {
    private MutableLiveData<Boolean> isForeground = new MutableLiveData<>();
    private boolean isRiding = false;
    private boolean isBusy = false;
    private static final String TAG = "ForegroundServiceStateR";

    public ForegroundServiceStateResolver() {
        isForeground.postValue(false);
    }

    public void setRiding(boolean riding) {
        Log.d(TAG, "setRiding: "+riding);
        isRiding = riding;
        calculateForeground();
    }

    public void setBusy(boolean busy) {
        Log.d(TAG, "setBusy: "+busy);
        isBusy = busy;
        calculateForeground();
    }

    private void calculateForeground(){
        Log.d(TAG, "calculateForeground: busy? "+isBusy+", riding? "+isRiding);
        boolean calculatedForeground;
        if(!isBusy){
            calculatedForeground=true;
        }else {
            calculatedForeground = isRiding;
        }

        if(isForeground.getValue()!=null) {
            boolean currForeground = isForeground.getValue();
            if(currForeground!=calculatedForeground){
                isForeground.postValue(calculatedForeground);
            }
        }else {
            isForeground.postValue(calculatedForeground);
        }
    }

    LiveData<Boolean> getIsNeedRegularTask(){
        return isForeground;
    }
}
