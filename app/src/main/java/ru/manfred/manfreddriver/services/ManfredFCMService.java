/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.services;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import java.util.Map;

import io.intercom.android.sdk.push.IntercomPushClient;
import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.managers.ManfredCredentialsManager;
import ru.manfred.manfreddriver.managers.ManfredEventManager;
import ru.manfred.manfreddriver.model.api.Event;

/**
 * Created by begemot on 01.12.17.
 */

public class ManfredFCMService extends FirebaseMessagingService {
    private final IntercomPushClient intercomPushClient = new IntercomPushClient();
    private static final String TAG = "ManfredFCMService";

    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d("ManfredFCMService", "onMessageReceived: " + remoteMessage.getData());
        /*
        original
        Map message = remoteMessage.getData();
        */
        Map<String, String> message = remoteMessage.getData();
        if (intercomPushClient.isIntercomPush(message)) {
            intercomPushClient.handlePush(getApplication(), message);
        } else {
            Gson gson = new Gson();
            //Event incEvent = gson.fromJson(gson.toJsonTree(message), ManfredPushEvent.class).getEvent();
            Event incEvent = gson.fromJson(message.get("event"), Event.class);
            Log.d(TAG, "onMessageReceived: event is " + incEvent.toString());
            ManfredCredentialsManager credentialsManager = ((ManfredApplication)getApplication()).getCredentialsManager();
            ManfredEventManager manfredEventManager =
                    new ManfredEventManager(credentialsManager,((ManfredApplication)getApplication()).getRepoProvider());
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    manfredEventManager.newEvent(incEvent);
                }
            });

        }
    }
}
