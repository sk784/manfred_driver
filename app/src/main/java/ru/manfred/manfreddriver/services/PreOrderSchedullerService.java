/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.services;

import android.app.job.JobParameters;
import android.app.job.JobService;

import ru.manfred.manfreddriver.utils.Constants;

/**
 * Created by begemot on 26.12.17.
 * notification about scheduled orders
 */

@Deprecated
public class PreOrderSchedullerService extends JobService {
    //@Inject
    //ManfredPreOrderManager preOrderManager;

    @Override
    public void onCreate() {
        super.onCreate();
        //ManfredApplication.getTripComponent().inject(this);
    }

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        long order = jobParameters.getExtras().getLong(Constants.PREORDER_KEY);
        //preOrderManager.orderSoon(order);
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        return false;
    }
}
