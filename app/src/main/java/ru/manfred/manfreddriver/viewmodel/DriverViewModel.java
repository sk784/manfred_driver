/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.viewmodel;

import android.support.annotation.NonNull;

import ru.manfred.manfreddriver.BuildConfig;
import ru.manfred.manfreddriver.model.api.Driver;

public class DriverViewModel {
    private String name;
    private String fullname;
    private String carNumber;
    private String carType;
    //private String birthday;
    private String license;
    private String phoneNumber;
    private String avatarUrl;
    private String tariffs;
    private String id;
    private String helloCaption;
    private boolean isActive;

    public DriverViewModel(@NonNull Driver driver) {
        name = driver.getName();
        setHelloCaption("Здравствуйте, " + driver.getName());
        setFullname(createFullName(driver));
        carNumber = driver.getCarNumber();
        carType = driver.getCarType();
        /*
        if(getBirthday()!=null) {
            birthday = TimeDateUtils.DateToString(driver.getBirthday());
        }else {
            birthday="-";
        }*/
        license = driver.getDriverLicense();
        phoneNumber = driver.getPhoneNumber();
        setAvatarUrl(removeLastChar(BuildConfig.BASE_URL, 4) + driver.getImage());

        StringBuilder sb = new StringBuilder();
        for (String line : driver.getTariff_names()) {
            sb.append(line);
            sb.append("\n");
        }
        tariffs = sb.toString();
        id = "ID " + driver.getId();
        if(driver.getCar_status()!=null) {
            isActive = driver.getCar_status().equals("ACTIVE");
        }else {
            isActive=true;
        }
    }

    private String createFullName(Driver driver) {
        String fullName = "";
        if (driver.getName() != null) fullName = fullName + driver.getName();
        if (driver.getPatronymic() != null) fullName = fullName + " " + driver.getPatronymic();
        if (driver.getSurname() != null) fullName = fullName + " " + driver.getSurname();
        return fullName;
    }

    private String removeLastChar(String str, int size) {
        return str.substring(0, str.length() - size);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getTariffs() {
        return tariffs;
    }

    public void setTariffs(String tariffs) {
        this.tariffs = tariffs;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getHelloCaption() {
        return helloCaption;
    }

    public void setHelloCaption(String helloCaption) {
        this.helloCaption = helloCaption;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
