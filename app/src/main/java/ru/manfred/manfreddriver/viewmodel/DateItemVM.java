/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.viewmodel;

import android.text.format.DateUtils;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
public class DateItemVM extends ListItem {
    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public DateItemVM(String date) {
        this.date = date;
    }

    public DateItemVM(@NotNull Date date) {
        SimpleDateFormat sdfr = new SimpleDateFormat("d MMMM", new Locale("ru"));
        this.date = (DateUtils.isToday(date.getTime())) ? "Сегодня" : sdfr.format(date);
    }

    @Override
    public int getType() {
        return TYPE_DATE;
    }
}
