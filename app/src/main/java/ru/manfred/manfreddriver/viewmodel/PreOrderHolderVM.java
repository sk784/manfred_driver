/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.viewmodel;

import org.jetbrains.annotations.Nullable;

import ru.manfred.manfreddriver.activities.preorders.PreOrderStarter;
import ru.manfred.manfreddriver.activities.preorders.details.PreordersDetailActivity;
import ru.manfred.manfreddriver.presenters.PreOrderPresenter;

/**
 * Created by begemot on 18.12.17.
 * holder for adapter objects
 */

public class PreOrderHolderVM extends ListItem {
    private PreOrderVM preOrderVM;
    private PreOrderPresenter preOrderPresenter;
    @Nullable private PreOrderStarter starter;
    private int id;

    public PreOrderHolderVM(PreOrderVM preOrderVM, PreOrderPresenter preOrderPresenter, @Nullable PreOrderStarter starter) {
        this.preOrderVM = preOrderVM;
        this.preOrderPresenter = preOrderPresenter;
        this.id=(int)preOrderVM.getOrder().getId();
        this.starter = starter;
    }

    public PreOrderVM getPreOrderVM() {
        return preOrderVM;
    }

    public PreOrderPresenter getPreOrderPresenter() {
        return preOrderPresenter;
    }

    public int getId() {
        return id;
    }

    public void start(){
        if (starter != null) {
            starter.startOrder(preOrderVM.getOrder());
        }
    }

    @Override
    public int getType() {
        return TYPE_GENERAL;
    }
}
