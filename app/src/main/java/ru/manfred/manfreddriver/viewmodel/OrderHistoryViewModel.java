/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.viewmodel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import ru.manfred.manfreddriver.model.api.Order;

/**
 * Created by begemot on 15.08.17.
 */

public class OrderHistoryViewModel extends ListItem {
    private static final String TAG = "OrderHistoryViewModel";
    private String id;
    private String startAddress;
    private String destinationAddress;
    private String startTime;
    private String finishTime;
    private String costumerName;
    private String timeInTrip;
    private String final_address;
    private String cost;
    private String dateTimeInterval;
    private String timeInterval;
    private Order order;
    private String carType;
    private String carNumber;
    private String tariffs;
    private String payment_type;

    public OrderHistoryViewModel(final Order order) {
        SimpleDateFormat sdfr = new SimpleDateFormat("HH:mm", new Locale("ru"));
        //Log.d(TAG, "OrderHistoryViewModel: creating id "+order.getId());
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM", new Locale("ru"));
        this.order = order;

        this.id = "ЗАКАЗ " + String.valueOf(order.getId());
        this.startAddress = order.getAddress();
        this.destinationAddress = order.getDest_address();
        if (order.getFinal_address() == null || order.getFinal_address().equals("unknown")) {
            this.final_address = order.getDest_address();
        } else {
            this.final_address = order.getFinal_address();
        }
        String orderFinishDate = "";
        if (order.getRide_finish_time() != null) {
            //Log.d(TAG, "OrderHistoryViewModel: id is "+order.getId());
            //Log.d(TAG, "OrderHistoryViewModel: start time is "+order.getRide_start_time());
            this.startTime = sdfr.format(order.getRide_start_time());
            orderFinishDate = dateFormat.format(order.getRide_start_time());
            //Log.d(TAG, "OrderHistoryViewModel: formatted startTime is "+orderFinishDate);
        } else {
            if(order.getCancel_time()!=null) {
                orderFinishDate = dateFormat.format(order.getCancel_time());
            }
            //Log.d(TAG, "OrderHistoryViewModel: we have nul startTime in order "+order.getId());
        }
        if (order.getRide_finish_time() != null) {
            this.finishTime = sdfr.format(order.getRide_finish_time());
        }
        this.costumerName = order.getCustomerName();
        this.timeInTrip = String.valueOf(getDateDiff(order.getRide_start_time(), order.getRide_finish_time(), TimeUnit.MINUTES)) + " мин";
        setCost(order.getTotal_cost() + " "+order.getCurrency_symbol());

        String stringInterval = orderFinishDate;
        if(getStartTime()!=null){
            stringInterval = stringInterval +", "+getStartTime();
        }
        if(getFinishTime()!=null){
            stringInterval = stringInterval + " – " + getFinishTime();
        }
        setDateTimeInterval(stringInterval);

        if(getStartTime()==null){
            setTimeInterval( sdfr.format(order.getAccept_order_time()));
        }else {
            setTimeInterval(getStartTime() + " – " + getFinishTime());
        }
        if (order.getCar() != null) {
            //Log.d(TAG, "OrderHistoryViewModel: car of order "+order.getId()+" is"+order.getCar().toString());
            setCarType(order.getCar().getCarType().getBrand() + "-" + order.getCar().getCarType().getName());
            setCarNumber(order.getCar().getCar_number());
        }
        setTariffs(order.getTariff_title());

        payment_type = order.getPayment_type();
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    public String getCostumerName() {
        return costumerName;
    }

    public void setCostumerName(String costumerName) {
        this.costumerName = costumerName;
    }

    public String getTimeInTrip() {
        return timeInTrip;
    }

    public void setTimeInTrip(String timeInTrip) {
        this.timeInTrip = timeInTrip;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFinal_address() {
        return final_address;
    }

    public String getTariffs() {
        return tariffs;
    }

    public void setTariffs(String tariffs) {
        this.tariffs = tariffs;
    }

    public void setFinal_address(String final_address) {
        this.final_address = final_address;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    /**
     * Get a diff between two dates
     *
     * @param date1    the oldest date
     * @param date2    the newest date
     * @param timeUnit the unit in which you want the diff
     * @return the diff value, in the provided unit
     */
    private long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        if (date1 == null || date2 == null) return 0;
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getDateTimeInterval() {
        return dateTimeInterval;
    }

    public void setDateTimeInterval(String dateTimeInterval) {
        this.dateTimeInterval = dateTimeInterval;
        //Log.d(TAG, "setDateTimeInterval: "+dateTimeInterval);
    }

    public Order getOrder() {
        return order;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(String timeInterval) {
        this.timeInterval = timeInterval;
    }

    @Override
    public int getType() {
        return TYPE_GENERAL;
    }
}
