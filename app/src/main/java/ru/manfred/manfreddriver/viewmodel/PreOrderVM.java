/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.viewmodel;

import android.databinding.ObservableBoolean;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import ru.manfred.manfreddriver.model.api.Order;

/**
 * Created by begemot on 14.12.17.
 */

public class PreOrderVM {
    private Order order;
    private String startAddress;
    private String destinationAddress;
    private String scheduledTime;
    private String scheduledTimeWithDate;
    private Boolean needAssistant;
    private String comments;
    private String preorderNumber;
    private String assistantName;
    private String tariffName;
    private ObservableBoolean isAccepting;
    //true if less 2hour to start

    public PreOrderVM(Order order) {
        Calendar cal = Calendar.getInstance();
        //TimeZone tz = cal.getTimeZone();
        TimeZone tz = TimeZone.getTimeZone("UTC");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", new Locale("ru"));
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM", new Locale("ru"));
        Log.d("Time zone: ", tz.getDisplayName());
        //timeFormat.setTimeZone(tz);
        //dateFormat.setTimeZone(tz);

        setOrder(order);
        setStartAddress(order.getAddress());
        setDestinationAddress(order.getDest_address());
        setScheduledTime(timeFormat.format(order.getCar_need_time()));
        setScheduledTimeWithDate(dateFormat.format(order.getCar_need_time()) + ", " + timeFormat.format(order.getCar_need_time()));
        setNeedAssistant(order.getNeedAssistant());
        setComments(order.getComments());
        setPreorderNumber(order.getTariff_title() + " #" + order.getId());
        /*if(order.getStatus().equals("SCHEDULED_ACCEPTED")){
            setPreorderNumber("Заказ " + order.getId());
        }else {
            setPreorderNumber(order.getTariff_title()+" #" + order.getId());
        }*/
        if (getNeedAssistant()) {
            setAssistantName("! АССИСТЕНТ !");
        }
        setTariffName(order.getTariff_title());
        /* remove when we have asistante name
        String assistantString = order.getAssist()!=null ? "Ассистент - " + order.getAssist().getName() : "! АССИСТЕНТ !";
        setAssistantName(assistantString);
        */
        isAccepting=new ObservableBoolean(false);
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        if (destinationAddress.isEmpty()) {
            this.destinationAddress = "Сообщит пассажир";
        } else {
            this.destinationAddress = destinationAddress;
        }
    }

    public String getScheduledTime() {
        return scheduledTime;
    }

    public void setScheduledTime(String scheduledTime) {
        this.scheduledTime = scheduledTime;
    }

    public String getScheduledTimeWithDate() {
        return scheduledTimeWithDate;
    }

    public void setScheduledTimeWithDate(String scheduledTimeWithDate) {
        this.scheduledTimeWithDate = scheduledTimeWithDate;
    }

    public Boolean getNeedAssistant() {
        return needAssistant;
    }

    public void setNeedAssistant(Boolean needAssistant) {
        this.needAssistant = needAssistant;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getPreorderNumber() {
        return preorderNumber;
    }

    public void setPreorderNumber(String preorderNumber) {
        this.preorderNumber = preorderNumber;
    }

    public String getAssistantName() {
        return assistantName;
    }

    public void setAssistantName(String assistantName) {
        this.assistantName = assistantName;
    }

    public String getTariffName() {
        return tariffName;
    }

    public void setTariffName(String tariffName) {
        this.tariffName = tariffName;
    }

    public boolean cancellable() {
        long timeToStart = order.getCar_need_time().getTime() - new Date().getTime();
        return order.getPreorder_2_notification_minutes() > TimeUnit.MILLISECONDS.toMinutes(timeToStart);
    }

    public void startAccepting(){
        isAccepting.set(true);
    }

    public ObservableBoolean getIsAccepting() {
        return isAccepting;
    }
}
