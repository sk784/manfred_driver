/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.viewmodel;

import android.databinding.ObservableField;
import android.os.CountDownTimer;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import ru.manfred.manfreddriver.model.api.Order;

/**
 * Created by begemot on 10.01.18.
 */

public class PreOrderMainScreen {
    private long id;
    private String firstString; //Заказ 205 через
    private ObservableField<String> secondString = new ObservableField<>(); //104 минуты

    public PreOrderMainScreen(final Order order) {
        if (order == null) {
            return;
        }
        id=order.getId();
        firstString = "Заказ " + order.getId() + " через";
        long timeToStart = (order.getCar_need_time().getTime()-TimeUnit.MINUTES.toMillis(order.getPreorder_start_timeout_minutes())) - new Date().getTime();
        new CountDownTimer(timeToStart, TimeUnit.MINUTES.toMillis(1)) {
            public void onTick(long millisUntilFinished) {
                secondString.set(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) + " мин");
            }

            public void onFinish() {

            }
        }.start();
    }

    public long getId() {
        return id;
    }

    public String getFirstString() {
        return firstString;
    }

    public ObservableField<String> getSecondString() {
        return secondString;
    }
}
