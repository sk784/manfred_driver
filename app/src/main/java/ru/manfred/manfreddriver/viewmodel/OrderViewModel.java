/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableField;
import android.text.SpannableString;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.Serializable;
import java.util.Date;

import ru.manfred.manfreddriver.model.api.Order;

/**
 * Created by begemot on 18.07.17.
 */

public class OrderViewModel extends BaseObservable implements Serializable {
    private static final String TAG = "OrderViewModel";
    //private final long id;
    private String address;
    private String destinationAdress;
    private Boolean needAssistant;
    private String customerName;
    private String comments;
    private Order order;
    private String customerNumber;
    private Date ride_start_time;
    private String tariffName;
    private String payment_type;

    private String id;
    //for new order on map screen
    @Bindable
    public ObservableField<String> timeToDistWithEnding = new ObservableField<>();
    @Bindable
    public ObservableField<String> distanceToCostumer = new ObservableField<>();
    @Bindable
    public final ObservableField<String> destinationTime = new ObservableField<>();
    //hh:mm
    @Bindable
    public final ObservableField<String> timeInTrip = new ObservableField<>();
    //MM
    @Bindable
    public final ObservableField<String> timeInTripMin = new ObservableField<>();
    //20 min
    @Bindable
    public final ObservableField<SpannableString> timeInTripWithMin = new ObservableField<>();
    //10 km
    @Bindable
    public final ObservableField<SpannableString> distanceInTripWithKm = new ObservableField<>();
    @Bindable
    public final ObservableField<String> timeEnding = new ObservableField<>();
    private final String currencySymbol;
    private int durationInMin;
    private String preorderNumber;

    public String getPreorderNumber() {
        return preorderNumber;
    }

    public void setPreorderNumber(String preorderNumber) {
        this.preorderNumber = preorderNumber;
    }

    public String getTariffName() {
        return tariffName;
    }

    public OrderViewModel(Order order) {
        this.order = order;
        this.customerName = order.getCustomerName();
        this.address = order.getAddress();
        //Log.d(TAG, "OrderViewModel: dest adress "+order.getDest_address());
        this.destinationAdress = order.getDest_address();
        this.needAssistant = order.getNeedAssistant();
        this.comments = order.getComments();
        this.ride_start_time = order.getRide_start_time();
        this.tariffName = order.getTariff_title();

        //duration_calculated=false;
        setPreorderNumber(order.getTariff_title() + " #" + order.getId());
        //Log.d(TAG, "OrderViewModel: number is "+order.getCustomerNumber());
        this.customerNumber = "+" + order.getCustomerNumber();
        //currentLocation=location;
        //Log.d(TAG, "OrderViewModel: time is "+order.getTime_to_costumer());
        if (order.getTime_to_costumer() != 0) {
            setDurationInMin(order.getTime_to_costumer() / 60);
        }
        setId("Заказ " + order.getId());

        currencySymbol = order.getCurrency_symbol();

        payment_type = order.getPayment_type();
    }

    public String getDestinationCaption(){
        String caption = "Забрать пассажира";
        if(order.getCar_need_time()!=null){
            DateTimeFormatter dateTimeFormat = DateTimeFormat.forPattern("HH:mm");
            caption=caption+" в "+dateTimeFormat.print(new DateTime(order.getCar_need_time()));
        }
        return caption;
    }

    public int getDurationInMin() {
        return durationInMin;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setDurationInMin(int durationInMin) {
        //Log.d(TAG, "setDurationInMin: "+durationInMin);
        //duration_calculated=true;
        this.durationInMin = durationInMin;
        destinationTime.set(String.valueOf(durationInMin));
        timeEnding.set(ending(durationInMin));
        timeToDistWithEnding.set(String.valueOf(durationInMin) + " " + ending(durationInMin));

    }

    private String ending(int time) {
        Integer min = plurals((long) time);
        String ending = "";
        switch (min) {
            case 0:
                ending = "минут";
                break;
            case 1:
                ending = "минута";
                break;
            case 2:
                ending = "минуты";
                break;
            case 5:
                ending = "минут";
                break;
        }
        return ending;
    }

    private Integer plurals(Long n) {
        if (n == 0) return 0;
        n = Math.abs(n) % 100;
        Long n1 = n % 10;
        if (n > 10 && n < 20) return 5;
        if (n1 > 1 && n1 < 5) return 2;
        if (n1 == 1) return 1;
        return 5;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getNeedAssistant() {
        return needAssistant;
    }

    public void setNeedAssistant(Boolean needAssistant) {
        this.needAssistant = needAssistant;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setTripTime(int minutes) {
        String startTime = "00:00";
        int h = minutes / 60 + Integer.parseInt(startTime.substring(0, 1));
        int m = minutes % 60 + Integer.parseInt(startTime.substring(3, 4));
        String H = "";
        String M = "";
        if (h < 10) H = "0";
        if (m < 10) M = "0";
        String newtime = H + h + ":" + M + m;
        timeInTrip.set(newtime);
        timeInTripMin.set(String.valueOf(minutes));
        timeEnding.set(ending(minutes));
    }


    public String getDestinationAdress() {
        return destinationAdress;
    }

    public void setDestinationAdress(String destinationAdress) {
        this.destinationAdress = destinationAdress;
    }

    public Date getRide_start_time() {
        return ride_start_time;
    }

    public void setRide_start_time(Date ride_start_time) {
        this.ride_start_time = ride_start_time;
        this.order.setRide_start_time(ride_start_time);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    @Override
    public String toString() {
        return "OrderViewModel{" +
                "address='" + address + '\'' +
                ", destinationAdress='" + destinationAdress + '\'' +
                ", needAssistant=" + needAssistant +
                ", customerName='" + customerName + '\'' +
                ", comments='" + comments + '\'' +
                ", order=" + order +
                ", customerNumber='" + customerNumber + '\'' +
                ", ride_start_time=" + ride_start_time +
                ", id='" + id + '\'' +
                ", timeToDistWithEnding=" + timeToDistWithEnding +
                ", distanceToCostumer=" + distanceToCostumer +
                ", destinationTime=" + destinationTime +
                ", timeInTrip=" + timeInTrip +
                ", timeInTripMin=" + timeInTripMin +
                ", timeEnding=" + timeEnding +
                ", durationInMin=" + durationInMin +
                '}';
    }
}
