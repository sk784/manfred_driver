/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.responces;

import java.util.List;

import ru.manfred.manfreddriver.model.api.LogNet;

public class ListLog {
    private final List<LogNet> logs;

    public ListLog(List<LogNet> logs) {
        this.logs = logs;
    }
}
