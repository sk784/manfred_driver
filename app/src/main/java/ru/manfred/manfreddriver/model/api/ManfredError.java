/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

import org.jetbrains.annotations.NotNull;

/**
 * Created by begemot on 26.11.17.
 */

public class ManfredError {
    private final String text;
    private final String resultCode;
    private final ErrorStatus type;

    public enum ErrorStatus {
        NET_ERROR,
        SERVER_UNAVAILABLE_ERROR,
        NEED_AUTORISATION,
        SERVER_MAINTENANCE //технические работы
    }

    public ManfredError(String text, ErrorStatus type) {
        this.text = text;
        this.type = type;
        this.resultCode = "";
    }

    public String getResultCode() {
        return resultCode;
    }

    public ManfredError(String text, String resultCode, ErrorStatus type) {
        this.text = text;
        this.resultCode = resultCode;
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public ErrorStatus getType() {
        return type;
    }

    @NotNull
    @Override
    public String toString() {
        return "ManfredError{" +
                "text='" + text + '\'' +
                ", resultCode='" + resultCode + '\'' +
                ", type=" + type +
                '}';
    }
}
