/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

import android.support.annotation.Nullable;

import org.jetbrains.annotations.NotNull;

import java.util.Date;

/**
 * Created by begemot on 01.10.17.
 */

public class Event {
    private String type;
    @Nullable private Order order;
    private String message;
    private Date created;
    private long id;
    @Nullable private Boolean is_free;

    public Event(String type, Order order) {
        this.type = type;
        this.order = order;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @org.jetbrains.annotations.Nullable
    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Nullable
    public Boolean getIs_free() {
        return is_free;
    }

    public void setIs_free(@Nullable Boolean is_free) {
        this.is_free = is_free;
    }

    @NotNull
    @Override
    public String toString() {
        return "Event{" +
                "type='" + type + '\'' +
                ", message='" + message + '\'' +
                ", created=" + created +
                ", id=" + id +
                '}';
    }
}
