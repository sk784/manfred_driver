/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by begemot on 18.12.17.
 */

public class Assistant implements Parcelable {
    private long id;
    private String name;
    private String phone;
    private String image;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.name);
        dest.writeString(this.phone);
        dest.writeString(this.image);
    }

    public Assistant() {
    }

    protected Assistant(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.phone = in.readString();
        this.image = in.readString();
    }

    public static final Parcelable.Creator<Assistant> CREATOR = new Parcelable.Creator<Assistant>() {
        @Override
        public Assistant createFromParcel(Parcel source) {
            return new Assistant(source);
        }

        @Override
        public Assistant[] newArray(int size) {
            return new Assistant[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
