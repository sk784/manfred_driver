/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

/**
 * Created by begemot on 15.01.18.
 */

public class AcceptingPreOrder {
    private long id;
    private boolean confirmed;

    public AcceptingPreOrder(long id, boolean confirmed) {
        this.id = id;
        this.confirmed = confirmed;
    }

    public long getId() {
        return id;
    }

    public boolean isConfirmed() {
        return confirmed;
    }
}
