/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.responces;

import ru.manfred.manfreddriver.model.api.Order;

/**
 * Created by begemot on 24.01.18.
 */

public class OrderResponce {
    Order order;

    public OrderResponce(Order order) {
        this.order = order;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}
