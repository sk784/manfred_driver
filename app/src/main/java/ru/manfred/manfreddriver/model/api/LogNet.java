/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

public class LogNet {
    private final String tag;
    private final String subsystem;
    private final String text;
    private final long date;

    public LogNet(String tag, String subsystem, String text, long date) {
        this.tag = tag;
        this.subsystem = subsystem;
        this.text = text;
        this.date = date;
    }

    private LogNet(Builder builder) {
        tag = builder.tag;
        subsystem = builder.subsystem;
        text = builder.text;
        date = builder.date;
    }


    public String getTag() {
        return tag;
    }

    public String getSubsystem() {
        return subsystem;
    }

    public String getText() {
        return text;
    }

    public long getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "LogNet{" +
                "tag='" + tag + '\'' +
                ", subsystem='" + subsystem + '\'' +
                ", text='" + text + '\'' +
                ", date=" + date +
                '}';
    }

    public static final class Builder {
        private String tag;
        private String subsystem;
        private String text;
        private long date;

        public Builder() {
        }

        public Builder tag(String val) {
            tag = val;
            return this;
        }

        public Builder subsystem(String val) {
            subsystem = val;
            return this;
        }

        public Builder text(String val) {
            text = val;
            return this;
        }

        public Builder date(long val) {
            date = val;
            return this;
        }

        public LogNet build() {
            return new LogNet(this);
        }

        @Override
        public String toString() {
            return "Builder{" +
                    "tag='" + tag + '\'' +
                    ", subsystem='" + subsystem + '\'' +
                    ", text='" + text + '\'' +
                    ", date=" + date +
                    '}';
        }
    }
}
