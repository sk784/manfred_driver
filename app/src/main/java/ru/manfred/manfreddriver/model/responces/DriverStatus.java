/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.responces;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

import ru.manfred.manfreddriver.model.api.Driver;
import ru.manfred.manfreddriver.model.api.Order;

/**
 * Created by begemot on 29.08.17.
 */

public class DriverStatus {
    @SerializedName("profile")
    private Driver driver;
    @Nullable
    @SerializedName("order")
    private Order order;
    @SerializedName("features")
    private Map<String, Boolean> features;

    public DriverStatus(Driver driver, @Nullable Order order, Map<String, Boolean> features) {
        this.driver = driver;
        this.order = order;
        this.features = features;
    }

    public Driver getDriver() {
        return driver;
    }

    @Nullable
    public Order getOrder() {
        return order;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public void setOrder(@Nullable Order order) {
        this.order = order;
    }

    public Map<String, Boolean> getFeatures() {
        return features;
    }

    @NotNull
    @Override
    public String toString() {
        return "DriverStatus{" +
                "driver=" + driver +
                ", order=" + order +
                ", features=" + features +
                '}';
    }
}
