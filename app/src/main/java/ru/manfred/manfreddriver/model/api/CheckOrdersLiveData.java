/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

import android.arch.lifecycle.LiveData;
import android.os.Handler;

/**
 * Created by begemot on 02.08.17.
 */

public class CheckOrdersLiveData extends LiveData<Order> {
    private Runnable updater;
    private static final String TAG = "CheckOrdersLiveData";
    private Handler timerHandler;

    private void updateTime() {
        timerHandler = new Handler();

        updater = new Runnable() {
            @Override
            public void run() {
                if (getValue() == null) {
                    timerHandler.removeCallbacks(updater);
                    return;
                }
                timerHandler.postDelayed(updater, 1000);

            }
        };
        timerHandler.post(updater);
    }
}
