/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

/**
 * Created by begemot on 10.07.17.
 */

public class ManfredResponse<T> {
    private final String message;
    private final T data;
    private final String result_code;
    private final String status;

    public ManfredResponse(String message, T data, String result_code, String status) {
        this.message = message;
        this.data = data;
        this.result_code = result_code;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public T getData() {
        return (T) data;
    }

    public String getResult_code() {
        return result_code;
    }

    public String getStatus() {
        return status;
    }
}
