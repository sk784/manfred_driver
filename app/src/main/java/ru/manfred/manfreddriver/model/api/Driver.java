/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by begemot on 04.07.17.
 */

public class Driver {
    private long id;
    private String name;
    private String surname;
    private String patronymic;
    private String driverLicense;
    private String medicalCertificate;
    private String phoneNumber;
    private String carNumber;
    private String carType;
    private List<String> tariff_names;
    /**
     * if ACTIVE all ok, can take orders
     */
    @SerializedName("car_status")
    private String car_status;
    //URL to image
    private String image;
    /*
    Is driver busy?
     */
    private boolean is_free;
    @SerializedName("location")
    private GeoData location;
/*
    protected Driver(Parcel in) {
        id = in.readLong();
        name = in.readString();
        surname = in.readString();
        patronymic = in.readString();
        driverLicense = in.readString();
        medicalCertificate = in.readString();
        phoneNumber = in.readString();
        carNumber = in.readString();
        carType = in.readString();
        tariff_names = in.createStringArrayList();
        image = in.readString();
        is_free = in.readByte() != 0;
    }*/

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public boolean is_free() {
        return is_free;
    }

    public void setIs_free(boolean is_free) {
        this.is_free = is_free;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getDriverLicense() {
        return driverLicense;
    }

    public void setDriverLicense(String driverLicense) {
        this.driverLicense = driverLicense;
    }

    public String getMedicalCertificate() {
        return medicalCertificate;
    }

    public void setMedicalCertificate(String medicalCertificate) {
        this.medicalCertificate = medicalCertificate;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    @Nullable
    public String getCar_status() {
        return car_status;
    }

    public void setCar_status(String car_status) {
        this.car_status = car_status;
    }

    @Override
    public String toString() {
        return "Driver{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", driverLicense='" + driverLicense + '\'' +
                ", medicalCertificate='" + medicalCertificate + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", carNumber='" + carNumber + '\'' +
                ", carType='" + carType + '\'' +
                ", tariff_names=" + tariff_names +
                ", image='" + image + '\'' +
                ", is_free=" + is_free +
                ", location=" + location +
                '}';
    }

    public Driver() {
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<String> getTariff_names() {
        return tariff_names;
    }

    public void setTariff_names(List<String> tariff_names) {
        this.tariff_names = tariff_names;
    }

    public boolean isIs_free() {
        return is_free;
    }

    public GeoData getLocation() {
        return location;
    }

    public void setLocation(GeoData location) {
        this.location = location;
    }
}
