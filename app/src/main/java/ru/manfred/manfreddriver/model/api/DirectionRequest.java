/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

public class DirectionRequest {
    private final GeoData from;
    private final GeoData to;

    public DirectionRequest(GeoData from, GeoData to) {
        this.from = from;
        this.to = to;
    }

    public GeoData getFrom() {
        return from;
    }

    public GeoData getTo() {
        return to;
    }
}
