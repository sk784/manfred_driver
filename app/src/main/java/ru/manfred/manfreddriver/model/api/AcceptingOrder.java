/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

import ru.manfred.manfreddriver.viewmodel.OrderViewModel;

/**
 * Created by begemot on 25.07.17.
 */

public class AcceptingOrder {
    private final long id;
    private final int timeToCostumer;

    public AcceptingOrder(OrderViewModel order) {
        id = order.getOrder().getId();
        timeToCostumer = order.getDurationInMin() * 60;
        //Log.d(TAG, "AcceptingOrder: time to costumer is "+timeToCostumer);
    }

    public AcceptingOrder(long id, int timeToCostumer) {
        this.id = id;
        this.timeToCostumer = timeToCostumer;
    }

    public AcceptingOrder(Order order) {
        this.id = order.getId();
        this.timeToCostumer = order.getTime_to_costumer();
    }

    public AcceptingOrder(int id) {
        this.id = id;
        timeToCostumer = 0;
        //Log.d(TAG, "AcceptingOrder: time to costumer is "+timeToCostumer);
    }

    public long getId() {
        return id;
    }

    public int getTimeToCostumer() {
        return timeToCostumer;
    }

    @Override
    public String toString() {
        return "AcceptingOrder{" +
                "id=" + id +
                ", timeToCostumer=" + timeToCostumer +
                '}';
    }
}
