/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.responces;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import ru.manfred.manfreddriver.model.api.OrderList;

/**
 * Created by begemot on 15.08.17.
 */

public class ManfredResponseOrderHistory {
    private final String message;
    @SerializedName("data")
    @Nullable
    private final OrderList data;
    private final String result_code;
    private final String status;

    public ManfredResponseOrderHistory(String message, @Nullable OrderList data, String result_code, String status) {
        this.message = message;
        this.data = data;
        this.result_code = result_code;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    @Nullable
    public OrderList getData() {
        return data;
    }

    public String getResult_code() {
        return result_code;
    }

    public String getStatus() {
        return status;
    }
}
