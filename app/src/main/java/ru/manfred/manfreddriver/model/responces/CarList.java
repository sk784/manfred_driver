/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.responces;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import ru.manfred.manfreddriver.model.api.Car;

public class CarList {
    @SerializedName("cars")
    private List<Car> cars;

    public CarList(List<Car> cars) {
        this.cars = cars;
    }

    public List<Car> getDrivers() {
        return cars;
    }

    public void setDrivers(List<Car> drivers) {
        this.cars = drivers;
    }
}
