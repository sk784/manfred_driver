/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.responces;

/**
 * Created by begemot on 21.02.18.
 */

public class ManfredResponseWithGenerics<T> {
    private final String message;
    private final Data<T> data;
    private final String result_code;

    public ManfredResponseWithGenerics(String message, Data<T> data, String result_code) {
        this.message = message;
        this.data = data;
        this.result_code = result_code;
    }

    public String getMessage() {
        return message;
    }

    public Data<T> getData() {
        return data;
    }

    public String getResult_code() {
        return result_code;
    }
}
