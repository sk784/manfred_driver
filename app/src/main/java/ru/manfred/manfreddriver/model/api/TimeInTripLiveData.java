/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

import android.arch.lifecycle.LiveData;
import android.os.Handler;
import android.util.Log;

import java.util.Date;

import ru.manfred.manfreddriver.utils.TimeDateUtils;

/**
 * Created by begemot on 28.07.17.
 */

public class TimeInTripLiveData extends LiveData<Integer> {
    //private static TimeInTripLiveData sInstance;
    private Date startTrip;
    private Runnable updater;
    private static final String TAG = "TimeInTripLiveData";
    private Handler timerHandler;

    public TimeInTripLiveData(Date startTrip) {
        Log.d(TAG, "TimeInTripLiveData: time is " + startTrip);
        this.startTrip = startTrip;
        int timeInTrip = TimeDateUtils.TimeDiffInMin(startTrip);
        Log.d(TAG, "TimeInTripLiveData: calculated "+timeInTrip);
        setValue(timeInTrip);
    }

    public void updateStartTime(Date date){
        this.startTrip = date;
        int timeInTrip = TimeDateUtils.TimeDiffInMin(startTrip);
        Log.d(TAG, "updateStartTime: from "+date+", calculated "+timeInTrip);
        setValue(timeInTrip);
    }

    private void reset() {
        timerHandler.removeCallbacksAndMessages(updater);
        startTrip = null;
    }

    @Override
    protected void onActive() {
        updateTime();
    }

    @Override
    protected void onInactive() {
        stopUpdateTime();
    }

    private void stopUpdateTime(){
        timerHandler.removeCallbacksAndMessages(updater);
        timerHandler=null;
    }

    private void updateTime() {
        timerHandler = new Handler();
        updater = new Runnable() {
            @Override
            public void run() {
                if(timerHandler==null){
                    return;
                }
                if (getValue() == null) {
                    timerHandler.removeCallbacks(updater);
                    return;
                }
                timerHandler.postDelayed(updater, 60000);
                int newTime = TimeDateUtils.TimeDiffInMin(startTrip);
                Log.d(TAG, "updateTime: start tick time "+startTrip+", new time is "+newTime);
                setValue(newTime);
            }
        };
        timerHandler.post(updater);
    }
}
