/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.responces;

import com.google.gson.annotations.SerializedName;

import ru.manfred.manfreddriver.model.api.CountryModel;

public class Phone {
    //private String country_code;
    private static final String TAG = "Phone";
    private transient CountryModel countryModel;
    private final String phone;
    @SerializedName("country_code")
    private final String countryCode;

    public Phone(CountryModel countryModel, String phone) {
        this.countryModel = countryModel;
        this.countryCode=countryModel.getCountryCode();
        this.phone = phone;
    }

    public String getCountry_code() {
        return countryCode;
    }

    public String getPhone() {
        return phone;
    }

    public String getFullNumber() {
        return countryModel.getCountryCode() + phone;
    }

    public CountryModel getCountryModel() {
        return countryModel;
    }

    public String getPhoneDescr(){
        return "Мы отправим смс с кодом на номер \n" + getFullNumber();
    }

    @Override
    public String toString() {
        return "Phone{" +
                "countryModel=" + countryModel +
                ", phone='" + phone + '\'' +
                ", countryCode='" + countryCode + '\'' +
                '}';
    }
}
