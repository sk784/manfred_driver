/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.GoogleMaps;

import com.google.gson.annotations.SerializedName;

/**
 * Created by begemot on 18.07.17.
 */

public class Duration {
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @SerializedName("text")
    private String text;

    @SerializedName("value")
    private int value;
}
