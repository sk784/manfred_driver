/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

/**
 * Created by begemot on 06.09.17.
 */

public class Busy {
    private boolean is_free;

    public Busy(boolean is_free) {
        this.is_free = is_free;
    }

    public boolean is_free() {
        return is_free;
    }

    public void setIs_free(boolean is_free) {
        this.is_free = is_free;
    }
}
