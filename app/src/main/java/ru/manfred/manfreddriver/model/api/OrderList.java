/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by begemot on 15.08.17.
 */

public class OrderList {
    @SerializedName("orders")
    private List<Order> orders;

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
}
