/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

public class AppVersion {
    private final String version;

    public AppVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }
}
