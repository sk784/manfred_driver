/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

import android.support.annotation.Nullable;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by begemot on 04.07.17.
 * DTO for order(used: new order, restoring orders, pre-orders)
 */
@SuppressWarnings("unused")
public class Order implements Serializable {
    private static final String TAG = "Order";
    private long id;
    private double longitude;
    private double latitude;
    private String dest_longitude;
    private String dest_address;
    private String dest_latitude;
    private String address;
    private Date orderTime;
    private Date ride_start_time;
    private Date ride_finish_time;
    private Date preorder_start_time;

    public int getPreorder_start_timeout_minutes() {
        return preorder_start_timeout_minutes;
    }

    public void setPreorder_start_timeout_minutes(int preorder_start_timeout_minutes) {
        this.preorder_start_timeout_minutes = preorder_start_timeout_minutes;
    }

    /**
     * start of waiting
     */
    private Date wait_passenger_time;
    private Date created;
    private Date cancel_time;
    private Date accept_order_time;
    /**
     * when preOrder cancelled if not start
     */
    private int preorder_start_timeout_minutes;
    private Boolean needAssistant;
    private String customerName;
    private String comments;
    private String customerNumber;
    //in seconds
    private int time_to_costumer;
    private Float total_cost;
    private Float holded_amount;
    private String tariff_title;
    private boolean to_airport_cancelled;
    private String currency;
    private String currency_symbol;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency_symbol() {
        return currency_symbol;
    }

    public void setCurrency_symbol(String currency_symbol) {
        this.currency_symbol = currency_symbol;
    }

    @Nullable
    private Car car;

    public Date getCancel_time() {
        return cancel_time;
    }

    public void setCancel_time(Date cancel_time) {
        this.cancel_time = cancel_time;
    }

    @Nullable
    private Driver driver;
    @Nullable
    private Date car_need_time;
    private boolean has_debt;
    @Nullable
    private Date payment_error_time;
    /*
    Число минут до старта предзаказа для вывода второго уведомления
    */
    private int preorder_2_notification_minutes;

    /*
        can be:
        "SEARCH_DRIVER";
        "RIDE_TO_PASSENGER";
        "WAIT_PASSENGER";
        "RIDE";
        "COMPLETED";
        "CANCELLED";
        SCHEDULED
        SCHEDULED_ACEPTED
        */
    private String status;

    private String final_address;


    private String payment_type;

    /*
    @Deprecated
    private Boolean isLoaded;
    */


    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    /*
            История движения водителя с пассажиром
             */
    private List<GeoData> trackList;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public List<GeoData> getTrackList() {
        return trackList;
    }

    public void setTrackList(List<GeoData> trackList) {
        this.trackList = trackList;
    }


    public String getAddress() {
        return address;
    }

    public Date getPreorder_start_time() {
        return preorder_start_time;
    }

    public void setPreorder_start_time(Date preorder_start_time) {
        this.preorder_start_time = preorder_start_time;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getNeedAssistant() {
        return needAssistant;
    }

    public void setNeedAssistant(Boolean needAssistant) {
        this.needAssistant = needAssistant;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getDest_longitude() {
        return dest_longitude;
    }

    public void setDest_longitude(String dest_longitude) {
        this.dest_longitude = dest_longitude;
    }

    public String getDest_latitude() {
        return dest_latitude;
    }

    public void setDest_latitude(String dest_latitude) {
        this.dest_latitude = dest_latitude;
    }

    public String getDest_address() {
        return dest_address;
    }

    public void setDest_address(String dest_address) {
        this.dest_address = dest_address;
    }

    public Date getRide_start_time() {
        return ride_start_time;
    }

    public void setRide_start_time(Date ride_start_time) {
        this.ride_start_time = ride_start_time;
    }

    public Date getRide_finish_time() {
        return ride_finish_time;
    }

    public void setRide_finish_time(Date ride_finish_time) {
        this.ride_finish_time = ride_finish_time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFinal_address() {
        return final_address;
    }

    public void setFinal_address(String final_address) {
        this.final_address = final_address;
    }


    public Order() {
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getAccept_order_time() {
        return accept_order_time;
    }

    public void setAccept_order_time(Date accept_order_time) {
        this.accept_order_time = accept_order_time;
    }

    public Float getTotal_cost() {
        return total_cost;
    }

    public void setTotal_cost(Float total_cost) {
        this.total_cost = total_cost;
    }

    public int getTime_to_costumer() {
        return time_to_costumer;
    }

    public void setTime_to_costumer(int time_to_costumer) {
        this.time_to_costumer = time_to_costumer;
    }

    @Nullable
    public Car getCar() {
        return car;
    }

    public void setCar(@Nullable Car car) {
        this.car = car;
    }

    @Nullable
    public Assistant getAssist() {
        return assist;
    }

    public void setAssist(@Nullable Assistant assist) {
        this.assist = assist;
    }


    @Nullable
    public Driver getDriver() {
        return driver;
    }

    public void setDriver(@Nullable Driver driver) {
        this.driver = driver;
    }

    public String getTariff_title() {
        return tariff_title;
    }

    public void setTariff_title(String tariff_title) {
        this.tariff_title = tariff_title;
    }

    @Nullable
    private Assistant assist;

    public void setCar_need_time(@Nullable Date car_need_time) {
        this.car_need_time = car_need_time;
    }

    @Nullable
    public Date getCar_need_time() {
        return car_need_time;
    }

    public int getPreorder_2_notification_minutes() {
        return preorder_2_notification_minutes;
    }

    public void setPreorder_2_notification_minutes(int preorder_2_notification_minutes) {
        this.preorder_2_notification_minutes = preorder_2_notification_minutes;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                ", dest_longitude='" + dest_longitude + '\'' +
                ", dest_address='" + dest_address + '\'' +
                ", dest_latitude='" + dest_latitude + '\'' +
                ", address='" + address + '\'' +
                ", orderTime=" + orderTime +
                ", ride_start_time=" + ride_start_time +
                ", ride_finish_time=" + ride_finish_time +
                ", preorder_start_time=" + preorder_start_time +
                ", wait_passenger_time=" + wait_passenger_time +
                ", created=" + created +
                ", cancel_time=" + cancel_time +
                ", accept_order_time=" + accept_order_time +
                ", needAssistant=" + needAssistant +
                ", customerName='" + customerName + '\'' +
                ", comments='" + comments + '\'' +
                ", customerNumber='" + customerNumber + '\'' +
                ", time_to_costumer=" + time_to_costumer +
                ", total_cost=" + total_cost +
                ", holded_amount=" + holded_amount +
                ", tariff_title='" + tariff_title + '\'' +
                ", to_airport_cancelled=" + to_airport_cancelled +
                ", currency='" + currency + '\'' +
                ", currency_symbol='" + currency_symbol + '\'' +
                ", car=" + car +
                ", driver=" + driver +
                ", car_need_time=" + car_need_time +
                ", has_debt=" + has_debt +
                ", payment_error_time=" + payment_error_time +
                ", preorder_2_notification_minutes=" + preorder_2_notification_minutes +
                ", status='" + status + '\'' +
                ", final_address='" + final_address + '\'' +
                ", trackList=" + trackList +
                ", assist=" + assist +
                ", payment+type=" + payment_type +
                '}';
    }



    public boolean isHas_debt() {
        return has_debt;
    }

    public void setHas_debt(boolean has_debt) {
        this.has_debt = has_debt;
    }

    @Nullable
    public Date getPayment_error_time() {
        return payment_error_time;
    }

    public void setPayment_error_time(@Nullable Date payment_error_time) {
        this.payment_error_time = payment_error_time;
    }

    public Float getHolded_amount() {
        return holded_amount;
    }

    public void setHolded_amount(Float holded_amount) {
        this.holded_amount = holded_amount;
    }

    public Date getWait_passenger_time() {
        return wait_passenger_time;
    }

    public void setWait_passenger_time(Date wait_passenger_time) {
        this.wait_passenger_time = wait_passenger_time;
    }

    public boolean isTo_airport_cancelled() {
        return to_airport_cancelled;
    }

    public void setTo_airport_cancelled(boolean to_airport_cancelled) {
        this.to_airport_cancelled = to_airport_cancelled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final Order order = (Order) o;

        if (id != order.id) return false;
        if (Double.compare(order.longitude, longitude) != 0) return false;
        if (Double.compare(order.latitude, latitude) != 0) return false;
        if (preorder_start_timeout_minutes != order.preorder_start_timeout_minutes) return false;
        if (time_to_costumer != order.time_to_costumer) return false;
        if (to_airport_cancelled != order.to_airport_cancelled) return false;
        if (has_debt != order.has_debt) return false;
        if (preorder_2_notification_minutes != order.preorder_2_notification_minutes) return false;
        if (dest_longitude != null ? !dest_longitude.equals(order.dest_longitude) : order.dest_longitude != null)
            return false;
        if (dest_address != null ? !dest_address.equals(order.dest_address) : order.dest_address != null)
            return false;
        if (dest_latitude != null ? !dest_latitude.equals(order.dest_latitude) : order.dest_latitude != null)
            return false;
        if (!address.equals(order.address)) return false;
        if (orderTime != null ? !orderTime.equals(order.orderTime) : order.orderTime != null)
            return false;
        if (ride_start_time != null ? !ride_start_time.equals(order.ride_start_time) : order.ride_start_time != null)
            return false;
        if (ride_finish_time != null ? !ride_finish_time.equals(order.ride_finish_time) : order.ride_finish_time != null)
            return false;
        if (preorder_start_time != null ? !preorder_start_time.equals(order.preorder_start_time) : order.preorder_start_time != null)
            return false;
        if (wait_passenger_time != null ? !wait_passenger_time.equals(order.wait_passenger_time) : order.wait_passenger_time != null)
            return false;
        if (!created.equals(order.created)) return false;
        if (cancel_time != null ? !cancel_time.equals(order.cancel_time) : order.cancel_time != null)
            return false;
        if (accept_order_time != null ? !accept_order_time.equals(order.accept_order_time) : order.accept_order_time != null)
            return false;
        if (!needAssistant.equals(order.needAssistant)) return false;
        if (customerName != null ? !customerName.equals(order.customerName) : order.customerName != null)
            return false;
        if (comments != null ? !comments.equals(order.comments) : order.comments != null)
            return false;
        if (customerNumber != null ? !customerNumber.equals(order.customerNumber) : order.customerNumber != null)
            return false;
        if (total_cost != null ? !total_cost.equals(order.total_cost) : order.total_cost != null)
            return false;
        if (holded_amount != null ? !holded_amount.equals(order.holded_amount) : order.holded_amount != null)
            return false;
        if (tariff_title != null ? !tariff_title.equals(order.tariff_title) : order.tariff_title != null)
            return false;
        if (currency != null ? !currency.equals(order.currency) : order.currency != null)
            return false;
        if (currency_symbol != null ? !currency_symbol.equals(order.currency_symbol) : order.currency_symbol != null)
            return false;
        if (car != null ? !car.equals(order.car) : order.car != null) return false;
        if (driver != null ? !driver.equals(order.driver) : order.driver != null) return false;
        if (car_need_time != null ? !car_need_time.equals(order.car_need_time) : order.car_need_time != null)
            return false;
        if (payment_error_time != null ? !payment_error_time.equals(order.payment_error_time) : order.payment_error_time != null)
            return false;
        if (!status.equals(order.status)) return false;
        if (final_address != null ? !final_address.equals(order.final_address) : order.final_address != null)
            return false;
        if (trackList != null ? !trackList.equals(order.trackList) : order.trackList != null)
            return false;
        if (payment_type != null ? !payment_type.equals(order.payment_type) : order.payment_type != null)
            return false;
        return assist != null ? assist.equals(order.assist) : order.assist == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (int) (id ^ (id >>> 32));
        temp = Double.doubleToLongBits(longitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(latitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (dest_longitude != null ? dest_longitude.hashCode() : 0);
        result = 31 * result + (dest_address != null ? dest_address.hashCode() : 0);
        result = 31 * result + (dest_latitude != null ? dest_latitude.hashCode() : 0);
        result = 31 * result + address.hashCode();
        result = 31 * result + (orderTime != null ? orderTime.hashCode() : 0);
        result = 31 * result + (ride_start_time != null ? ride_start_time.hashCode() : 0);
        result = 31 * result + (ride_finish_time != null ? ride_finish_time.hashCode() : 0);
        result = 31 * result + (preorder_start_time != null ? preorder_start_time.hashCode() : 0);
        result = 31 * result + (wait_passenger_time != null ? wait_passenger_time.hashCode() : 0);
        result = 31 * result + created.hashCode();
        result = 31 * result + (cancel_time != null ? cancel_time.hashCode() : 0);
        result = 31 * result + (accept_order_time != null ? accept_order_time.hashCode() : 0);
        result = 31 * result + preorder_start_timeout_minutes;
        result = 31 * result + needAssistant.hashCode();
        result = 31 * result + (customerName != null ? customerName.hashCode() : 0);
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        result = 31 * result + (customerNumber != null ? customerNumber.hashCode() : 0);
        result = 31 * result + time_to_costumer;
        result = 31 * result + (total_cost != null ? total_cost.hashCode() : 0);
        result = 31 * result + (holded_amount != null ? holded_amount.hashCode() : 0);
        result = 31 * result + (tariff_title != null ? tariff_title.hashCode() : 0);
        result = 31 * result + (to_airport_cancelled ? 1 : 0);
        result = 31 * result + (currency != null ? currency.hashCode() : 0);
        result = 31 * result + (currency_symbol != null ? currency_symbol.hashCode() : 0);
        result = 31 * result + (car != null ? car.hashCode() : 0);
        result = 31 * result + (driver != null ? driver.hashCode() : 0);
        result = 31 * result + (car_need_time != null ? car_need_time.hashCode() : 0);
        result = 31 * result + (has_debt ? 1 : 0);
        result = 31 * result + (payment_error_time != null ? payment_error_time.hashCode() : 0);
        result = 31 * result + preorder_2_notification_minutes;
        result = 31 * result + status.hashCode();
        result = 31 * result + (final_address != null ? final_address.hashCode() : 0);
        result = 31 * result + (assist != null ? assist.hashCode() : 0);
        return result;
    }
}
