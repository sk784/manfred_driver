/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.responces;

import com.google.gson.annotations.SerializedName;

public class SMSCodeRequest {
    @SerializedName("phone")
    private final String phone;
    @SerializedName("sms_code")
    private final String smsCode;
    @SerializedName("country_code")
    private final String countryCode;
    @SerializedName("token")
    private final String token;
    @SerializedName("platform")
    private final String platform;
    @SerializedName("app_version")
    private final String appVersion;

    public SMSCodeRequest(String phone, String smsCode, String countryCode, String token, String platform, String appVersion) {
        this.phone = phone.replaceAll("[^0-9]+", "");
        this.smsCode = smsCode;
        this.countryCode = countryCode.replaceAll("[^0-9]+", "");
        this.token = token;
        this.platform = platform;
        this.appVersion = appVersion;
    }

    public String getPhone() {
        return phone;
    }

    public String getSmsCode() {
        return smsCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getToken() {
        return token;
    }

    public String getPlatform() {
        return platform;
    }

    public String getAppVersion() {
        return appVersion;
    }
}
