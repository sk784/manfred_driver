/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by begemot on 10.07.17.
 */

public class ResponseAuth implements Serializable {
    @SerializedName("token")
    private String token;
    private boolean need_update;

    public ResponseAuth(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isNeed_update() {
        return need_update;
    }

    public void setNeed_update(boolean need_update) {
        this.need_update = need_update;
    }

    @Override
    public String toString() {
        return "ResponseAuth{" +
                "token='" + token + '\'' +
                ", need_update=" + need_update +
                '}';
    }
}
