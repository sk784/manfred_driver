/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by begemot on 13.12.17.
 */

public class CarType implements Parcelable {
    private long id;
    private String brand;
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.brand);
        dest.writeString(this.name);
    }

    public CarType() {
    }

    protected CarType(Parcel in) {
        this.id = in.readLong();
        this.brand = in.readString();
        this.name = in.readString();
    }

    public static final Parcelable.Creator<CarType> CREATOR = new Parcelable.Creator<CarType>() {
        @Override
        public CarType createFromParcel(Parcel source) {
            return new CarType(source);
        }

        @Override
        public CarType[] newArray(int size) {
            return new CarType[size];
        }
    };
}
