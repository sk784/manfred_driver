/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;


import android.location.Location;

public class CarOnMap {
    private float bearing;
    private long id;
    private Location location;
    private boolean busy;
    private static final String TAG = "CarOnMap";

    public CarOnMap(long id, Location location, boolean busy) {
        //Log.d(TAG, "CarOnMap: creating with "+id+", "+location.toString());
        this.id = id;
        this.location = location;
        this.bearing = 0;
        this.busy = busy;
    }

    public void setLocation(Location location) {
        float newBearing = this.location.bearingTo(location);
        if (newBearing != 0) {
            setBearing(newBearing);
        }
        this.location = location;
    }

    private void setBearing(float bearing) {
        //Log.d(TAG, "setBearing: to "+bearing);
        this.bearing = bearing;
    }

    public float getBearing() {
        return bearing;
    }

    public long getId() {
        return id;
    }

    public Location getLocation() {
        return location;
    }

    public boolean isBusy() {
        return busy;
    }

    public void setBusy(boolean busy) {
        this.busy = busy;
    }

    @Override
    public String toString() {
        return "CarOnMap{" +
                "bearing=" + bearing +
                ", id=" + id +
                ", location=" + location +
                '}';
    }
}
