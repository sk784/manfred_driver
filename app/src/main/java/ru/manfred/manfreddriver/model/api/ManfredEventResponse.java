/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

/**
 * Created by begemot on 01.10.17.
 */

public class ManfredEventResponse {
    private final String message;
    private final Events data;
    private final String result_code;
    private final String status;

    public ManfredEventResponse(String message, Events data, String result_code, String status) {
        this.message = message;
        this.data = data;
        this.result_code = result_code;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public Events getData() {
        return data;
    }

    public String getResult_code() {
        return result_code;
    }

    public String getStatus() {
        return status;
    }
}
