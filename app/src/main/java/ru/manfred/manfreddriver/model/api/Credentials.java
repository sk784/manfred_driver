/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by begemot on 05.07.17.
 */
public class Credentials {
    private String phoneNumber;
    private String token;
    @SerializedName("platform")
    private final String platform = "android";

    public Credentials(String phoneNumber, @NonNull String token) {
        this.phoneNumber = phoneNumber;
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "Credentials{" +
                "phoneNumber='" + phoneNumber + '\'' +
                ", setToken='" + token + '\'' +
                ", platform='" + platform + '\'' +
                '}';
    }

    public String getPlatform() {
        return platform;
    }
}
