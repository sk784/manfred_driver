/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.responces;

import com.google.gson.annotations.SerializedName;

/**
 * Created by begemot on 29.08.17.
 */

public class ManfredResponceDriverStatus {
    private final String message;
    @SerializedName("data")
    private final DriverStatus driverStatus;
    private final String result_code;
    private final String status;

    public ManfredResponceDriverStatus(String message, DriverStatus driverStatus, String result_code, String status) {
        this.message = message;
        this.driverStatus = driverStatus;
        this.result_code = result_code;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public DriverStatus getDriverStatus() {
        return driverStatus;
    }

    public String getResult_code() {
        return result_code;
    }

    public String getStatus() {
        return status;
    }
}
