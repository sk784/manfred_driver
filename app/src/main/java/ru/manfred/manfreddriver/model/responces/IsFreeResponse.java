/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.responces;

import com.google.gson.annotations.SerializedName;

public class IsFreeResponse {
    @SerializedName("is_free")
    private final boolean isFree;

    public IsFreeResponse(boolean isFree) {
        this.isFree = isFree;
    }

    public boolean isFree() {
        return isFree;
    }
}
