/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

import android.arch.persistence.room.Ignore;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by begemot on 04.07.17.
 */

public class GeoData implements Serializable {
    private double longitude;
    private double latitude;
    private long date;
    private String address;
    private float accuracy;

    public GeoData(android.location.Location location){
        this.longitude=location.getLongitude();
        this.latitude=location.getLatitude();
        this.date = location.getTime();
        this.address = "unknown place";
        this.accuracy = location.getAccuracy();
    }

    //for room(full matching with field constructor)
    public GeoData(double longitude, double latitude, long date, String address, float accuracy) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.date = date;
        this.address = address;
        this.accuracy = accuracy;
    }

    public GeoData(double longitude, double latitude, Date date, float accuracy) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.date = date.getTime() / 1000;
        //java.util.Date utilDate = date;
        //java.sql.Timestamp sq = new java.sql.Timestamp(utilDate.getTime());
        this.accuracy = accuracy;
    }

    @Ignore //- it is for room ignoring constructor
    public GeoData(double longitude, double latitude, long date, float accuracy) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.date = date / 1000;
        //java.util.Date utilDate = date;
        //java.sql.Timestamp sq = new java.sql.Timestamp(utilDate.getTime());
        this.accuracy = accuracy;
    }

    public GeoData(double longitude, double latitude, Date date, String address, float accuracy) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.date = date.getTime() / 1000;
        this.address = address;
        this.accuracy = accuracy;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setAccuracy(float accuracy) {
        this.accuracy = accuracy;
    }

    @Override
    public String toString() {
        return "GeoData{" +
                "longitude=" + longitude +
                ", latitude=" + latitude +
                ", date=" + date +
                ", address='" + address + '\'' +
                '}';
    }

    public float getAccuracy() {
        return accuracy;
    }
}
