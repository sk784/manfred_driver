/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.responces;

import com.google.gson.annotations.SerializedName;

public class ManfredResponseNeedUpdate {
    private final String message;
    @SerializedName("data")
    private final NeedUpdate data;
    private final String result_code;
    private final String status;

    public ManfredResponseNeedUpdate(String message, NeedUpdate data, String result_code, String status) {
        this.message = message;
        this.data = data;
        this.result_code = result_code;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public NeedUpdate getData() {
        return data;
    }

    public String getResult_code() {
        return result_code;
    }

    public String getStatus() {
        return status;
    }
}
