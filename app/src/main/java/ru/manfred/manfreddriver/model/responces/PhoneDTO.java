/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.responces;

import android.util.Log;

public class PhoneDTO {
    private static final String TAG = "PhoneDTO";
    private final String country_code;
    private final String phone;

    public PhoneDTO(Phone phone) {
        this.country_code = phone.getCountry_code().replaceAll("[^0-9]+", "");
        this.phone = phone.getPhone().replaceAll("[^0-9]+", "");
        Log.d(TAG, "PhoneDTO: "+this.country_code+ " "+ this.phone);
    }

    public PhoneDTO(String country_code, String phone) {
        this.country_code = country_code;
        this.phone = phone;
    }

    public String getCountry_code() {
        return country_code;
    }

    @Override
    public String toString() {
        return "PhoneDTO{" +
                "country_code='" + country_code + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }

    public String getPhone() {
        return phone;
    }
}
