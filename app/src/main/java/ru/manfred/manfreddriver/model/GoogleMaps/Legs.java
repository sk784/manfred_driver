/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.GoogleMaps;

import java.util.List;

public class Legs {
    private List<Steps> steps;
    private Duration duration;
    private Distance distance;
    private Distance duration_in_traffic;

    public Distance getDuration_in_traffic() {
        return duration_in_traffic;
    }

    public void setDuration_in_traffic(Distance duration_in_traffic) {
        this.duration_in_traffic = duration_in_traffic;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    public List<Steps> getSteps() {
        return steps;
    }
}
