/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

import android.arch.persistence.room.Ignore;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by begemot on 29.09.17.
 */
public class Airport implements Parcelable {
    private String name;
    private String terminal;
    private double latitude;
    private double longitude;

    @Ignore
    public Airport() {

    }

    public Airport(String name, String terminal, double latitude, double longitude) {
        this.name = name;
        this.terminal = terminal;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.terminal);
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
    }

    protected Airport(Parcel in) {
        this.name = in.readString();
        this.terminal = in.readString();
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
    }

    public static final Parcelable.Creator<Airport> CREATOR = new Parcelable.Creator<Airport>() {
        @Override
        public Airport createFromParcel(Parcel source) {
            return new Airport(source);
        }

        @Override
        public Airport[] newArray(int size) {
            return new Airport[size];
        }
    };
}
