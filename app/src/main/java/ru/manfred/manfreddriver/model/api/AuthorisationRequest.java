/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

import com.google.gson.annotations.SerializedName;

/**
 * Created by begemot on 11.10.17.
 */

public class AuthorisationRequest {
    private String login;
    private String password;
    @SerializedName("setToken")
    private String pushToken;
    @SerializedName("platform")
    private final String platform = "android";
    private String app_version;

    public AuthorisationRequest(String login, String password, String pushToken, String app_version) {
        this.login = login;
        this.password = password;
        this.pushToken = pushToken;
        this.app_version = app_version;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPushToken() {
        return pushToken;
    }

    public void setPushToken(String pushToken) {
        this.pushToken = pushToken;
    }

    public String getPlatform() {
        return platform;
    }

    public String getApp_version() {
        return app_version;
    }

    public void setApp_version(String app_version) {
        this.app_version = app_version;
    }
}
