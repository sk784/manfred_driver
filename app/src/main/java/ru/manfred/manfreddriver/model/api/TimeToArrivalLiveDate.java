/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

import android.arch.lifecycle.LiveData;
import android.os.Handler;
import android.support.annotation.MainThread;
import android.support.annotation.Nullable;

/**
 * Created by begemot on 27.07.17.
 */

public class TimeToArrivalLiveDate extends LiveData<Integer> {
    private static TimeToArrivalLiveDate sInstance;
    private Runnable updater;
    private static final String TAG = "TimeToArrivalLiveDate";
    private Handler timerHandler;

    @MainThread
    public static TimeToArrivalLiveDate get(@Nullable Integer arrivalTime) {
        //Log.d(TAG, "get: time is "+arrivalTime);
        if (sInstance == null && arrivalTime != null) {
            sInstance = new TimeToArrivalLiveDate(arrivalTime);
        }
        if (arrivalTime == null && sInstance == null) return new TimeToArrivalLiveDate(0);
        return sInstance;
    }

    private TimeToArrivalLiveDate(Integer arrivalTime) {
        setValue(arrivalTime);
        updateTime();
    }

    public void addLate(int lateMins) {
        if (getValue() != null) {
            setValue(getValue() + lateMins);
        }
    }

    @Override
    protected void onActive() {

    }

    public void reset() {
        if (timerHandler != null) {
            timerHandler.removeCallbacks(updater);
        }
        sInstance = null;
    }

    @Override
    protected void onInactive() {

    }

    private void updateTime() {
        timerHandler = new Handler();

        updater = new Runnable() {
            @Override
            public void run() {
                if (getValue() == null) {
                    timerHandler.removeCallbacks(updater);
                    return;
                }
                timerHandler.postDelayed(updater, 60000);
                if (getValue() > 0) {
                    setValue(getValue() - 1);
                }
            }
        };
        timerHandler.post(updater);
    }
}
