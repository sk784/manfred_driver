/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

import com.google.gson.annotations.SerializedName;

public class HistoryRequestDTO{
    @SerializedName("dateFrom")
    private final String dateFrom;
    @SerializedName("dateTo")
    private final String dateTo;

    public HistoryRequestDTO(String dateFrom, String dateTo) {
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    @Override
    public String toString() {
        return "HistoryRequestDTO{" +
                "dateFrom='" + dateFrom + '\'' +
                ", dateTo='" + dateTo + '\'' +
                '}';
    }
}