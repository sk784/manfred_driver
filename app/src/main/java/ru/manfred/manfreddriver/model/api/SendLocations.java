/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

import java.util.List;

public class SendLocations {
    private final List<GeoData> locations;

    public SendLocations(List<GeoData> locations) {
        this.locations = locations;
    }

    public List<GeoData> getLocations() {
        return locations;
    }
}
