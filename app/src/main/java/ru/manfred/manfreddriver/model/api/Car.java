/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

import com.google.gson.annotations.SerializedName;

/**
 * Created by begemot on 04.07.17.
 */

public class Car {
    @SerializedName("color")
    private String color;

    @SerializedName("driver")
    private Driver driver;

    @SerializedName("car_number")
    private String carNumber;

    @SerializedName("assist")
    private Assist assist;

    @SerializedName("id")
    private long id;

    @SerializedName("car_type")
    private CarType carType;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCar_number() {
        return carNumber;
    }

    public void setCar_number(String car_number) {
        this.carNumber = car_number;
    }

    public CarType getCarType() {
        return carType;
    }

    public void setCarType(CarType carType) {
        this.carType = carType;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", car_number='" + carNumber + '\'' +
                ", carType=" + carType +
                ", color='" + color + '\'' +
                '}';
    }

    public CarType getCar_type() {
        return carType;
    }

    public void setCar_type(CarType car_type) {
        this.carType = car_type;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }
}
