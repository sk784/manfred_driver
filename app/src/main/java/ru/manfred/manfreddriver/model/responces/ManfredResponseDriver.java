/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.responces;

import com.google.gson.annotations.SerializedName;

import ru.manfred.manfreddriver.model.api.Driver;

/**
 * Created by begemot on 22.08.17.
 */

public class ManfredResponseDriver {
    private final String message;
    @SerializedName("data")
    private final Driver data;
    private final String result_code;
    private final String status;

    public ManfredResponseDriver(String message, Driver data, String result_code, String status) {
        this.message = message;
        this.data = data;
        this.result_code = result_code;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public Driver getData() {
        return data;
    }

    public String getResult_code() {
        return result_code;
    }

    public String getStatus() {
        return status;
    }
}
