/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.responces;

/**
 * Created by begemot on 24.01.18.
 */

public class ManfredResponceAcceptingOrder {
    private final String message;
    private final OrderResponce data;
    private final String result_code;

    public ManfredResponceAcceptingOrder(String message, OrderResponce data, String result_code) {
        this.message = message;
        this.data = data;
        this.result_code = result_code;
    }

    public String getMessage() {
        return message;
    }

    public OrderResponce getData() {
        return data;
    }

    public String getResult_code() {
        return result_code;
    }
}
