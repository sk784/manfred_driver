/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by begemot on 12.03.18.
 */
@Deprecated
public class PathDate {
    private static final ThreadLocal<DateFormat> DF = new ThreadLocal<DateFormat>() {
        @Override
        public DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd");
        }
    };
    private static final String TAG = "PathDate";

    private final Date date;

    public PathDate(Date date) {
        this.date = date;
    }

    public String getDate(){
        return DF.get().format(date);
    }

    @Override
    public String toString() {
        return DF.get().format(date);
    }
}
