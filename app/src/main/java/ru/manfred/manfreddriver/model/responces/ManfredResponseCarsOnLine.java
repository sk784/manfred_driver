/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.responces;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class ManfredResponseCarsOnLine {
    private final String message;
    @SerializedName("data")
    private CarList data;
    private final String result_code;
    private final String status;

    public ManfredResponseCarsOnLine(String message, CarList data, String result_code, String status) {
        this.message = message;
        this.data = data;
        this.result_code = result_code;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    @Nullable
    public CarList getData() {
        return data;
    }

    public String getResult_code() {
        return result_code;
    }

    public String getStatus() {
        return status;
    }
}
