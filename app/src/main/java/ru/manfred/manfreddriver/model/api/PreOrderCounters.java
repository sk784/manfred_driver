/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

public class PreOrderCounters {
    private final int unread;
    private final int incPreOrders;
    private final int myPreOrders;

    public PreOrderCounters(int unread, int incPreOrders, int myPreOrders) {
        this.unread = unread;
        this.incPreOrders = incPreOrders;
        this.myPreOrders = myPreOrders;
    }

    public int getUnread() {
        return unread;
    }

    public int getIncPreOrders() {
        return incPreOrders;
    }

    public int getMyPreOrders() {
        return myPreOrders;
    }

    @Override
    public String toString() {
        return "PreOrderCounters{" +
                "unread=" + unread +
                ", incPreOrders=" + incPreOrders +
                ", myPreOrders=" + myPreOrders +
                '}';
    }
}
