/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.model.api;

import com.google.gson.annotations.SerializedName;

public class DelaySize{
    @SerializedName("delay_time")
    private final int DelayTimeInSec;

    public DelaySize(int delayTimeInSec) {
        DelayTimeInSec = delayTimeInSec;
    }

    public int getDelayTimeInSec() {
        return DelayTimeInSec;
    }
}