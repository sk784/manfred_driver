/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers.history;

import java.util.Date;
import java.util.List;

import ru.manfred.manfreddriver.model.api.Order;

/**
 * Created by begemot on 06.02.18.
 */

public interface HistoryManager {
    enum HistoryInterval {
        TODAY,
        THIS_WEEK,
        THIS_MONTH,
        CUSTOM_INTERVAL
    }

    public interface HistoryManagerCallback {
        void success(List<Order> historyOrders);

        void error(String message);

        void serverNotAvailable();
    }

    void getTodayHistory(HistoryManagerCallback historyCallback);

    void getWeekHistory(HistoryManagerCallback historyCallback);

    void getMonthHistory(HistoryManagerCallback historyCallback);

    void getPeriodHistory(Date startDate, Date endDate, HistoryManagerCallback historyCallback);

    HistoryCustomRange getLastPeriod();

    void invalidateCache();

}
