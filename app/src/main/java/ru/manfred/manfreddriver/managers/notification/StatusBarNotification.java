package ru.manfred.manfreddriver.managers.notification;

import android.support.annotation.Nullable;

import java.util.UUID;

public class StatusBarNotification {
    private final int id;
    @Nullable private final String title;
    private final String text;

    public int getId() {
        return id;
    }

    public StatusBarNotification(@Nullable String title, String text) {
        this.id = UUID.randomUUID().hashCode();
        this.title = title;
        this.text = text;
    }

    @Nullable public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final StatusBarNotification that = (StatusBarNotification) o;

        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        return text != null ? text.equals(that.text) : that.text == null;
    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (text != null ? text.hashCode() : 0);
        return result;
    }
}
