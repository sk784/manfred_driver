/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers;

import java.util.List;

import ru.manfred.manfreddriver.model.api.Event;
import ru.manfred.manfreddriver.model.api.Order;

/**
 * Created by begemot on 15.10.17.
 */

public interface EventManager {
    interface EventManagerListener {
        void newEvent(Order order);
    }

    void newEvent(Event event);
    @Deprecated
    void newEvents(List<Event> events);
}
