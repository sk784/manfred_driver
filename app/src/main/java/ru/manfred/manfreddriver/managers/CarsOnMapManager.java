/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers;

import java.util.List;

import ru.manfred.manfreddriver.model.api.CarOnMap;
import ru.manfred.manfreddriver.model.api.Driver;

public interface CarsOnMapManager {
    interface CarPositionChangeListener {
        void carsChanged(List<CarOnMap> cars, List<Long> idsOfGoOfflineDrivers);
    }

    void setCarsOnMap(List<Driver> carsOnMap);

    void addCarPositionChangeListener(CarPositionChangeListener carPositionChangeListener);

    void removeCarPositionChangeListener(CarPositionChangeListener carPositionChangeListener);
}
