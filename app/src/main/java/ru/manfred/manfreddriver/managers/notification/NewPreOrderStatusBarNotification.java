package ru.manfred.manfreddriver.managers.notification;

class NewPreOrderStatusBarNotification extends StatusBarNotification {
    public NewPreOrderStatusBarNotification(String title, String text) {
        super(title, text);
    }
}
