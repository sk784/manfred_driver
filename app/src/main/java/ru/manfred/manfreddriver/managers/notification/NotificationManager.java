package ru.manfred.manfreddriver.managers.notification;

import java.util.UUID;

public interface NotificationManager {
    void showNotification(StatusBarNotification notification);
    void hideNotification(StatusBarNotification notification);
    void showNotificationIfNotOnScreen(StatusBarNotification notification);
    void hideAllNotification();
}
