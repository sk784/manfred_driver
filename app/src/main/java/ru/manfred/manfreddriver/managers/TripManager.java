/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers;

import android.arch.lifecycle.LiveData;
import android.support.annotation.Nullable;

import ru.manfred.manfreddriver.model.api.Order;

/**
 * Created by begemot on 15.10.17.
 */

public interface TripManager {
    class Status {
        private final StatusState statusState;
        private final Reason reason;
        @Nullable private final Order order;

        public Status(StatusState statusState, Reason reason, @Nullable Order order) {
            this.statusState = statusState;
            this.reason = reason;
            this.order = order;
        }

        @Nullable
        public StatusState getStatusState() {
            return statusState;
        }

        public Reason getReason() {
            return reason;
        }

        @Nullable
        public Order getOrder() {
            return order;
        }

        @Override
        public String toString() {
            return "Status{" +
                    "statusState=" + statusState +
                    ", reason=" + reason +
                    ", order=" + order +
                    '}';
        }
    }
    enum StatusState {
        FREE,
        OFFER_TO_RIDE,
        OFFER_SHOW_TIMEOUT,
        RIDE_TO_PASSENGER,
        OFFER_ADDITIONAL_PAYMENT_ERROR,
        WAIT_PASSENGER,
        RIDE,
        COMPLETED,
        CANCELLED
    }

    class Reason {
        String type;
        String message;

        public Reason(String type, String message) {
            this.type = type;
            this.message = message;
        }

        public String getType() {
            return type;
        }

        public String getMessage() {
            return message;
        }

        @Override
        public String toString() {
            return "Reason{" +
                    "type='" + type + '\'' +
                    ", message='" + message + '\'' +
                    '}';
        }
    }

    interface TripManagerListener {
        void newStatus(Status status);
    }

    void offerRide(Order order);

    void cancelRide(Order order, Reason reason);

    void startRideToDestination(Order order);

    void finishRide(Order order, String text);

    void waitingPassenger(Order order);

    void startRideToPassenger(Order order);

    void offerOnScreenTimeOut(Order order, Reason reason);

    void offerAdditionalPaymentError(Order order, Reason reason);

    void offerCostResult(Order order, Reason reason);

    Status getCurrentStatus();

    void paymentSuccessful(Order order, Reason reason);

    void orderUpdated(Order order, Reason reason);

    LiveData<Status> getCurrentState();


    /*
    void addChangeStatusListener(TripManagerListener listener);
    void removeChangeStatusListener(TripManagerListener listener);*/
}
