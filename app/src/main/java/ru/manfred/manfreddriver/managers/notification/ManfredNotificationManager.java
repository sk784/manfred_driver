package ru.manfred.manfreddriver.managers.notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.activities.NewOrderActivity;
import ru.manfred.manfreddriver.activities.SplashActivity;
import ru.manfred.manfreddriver.activities.preorders.PreordersActivity;
@Singleton
public class ManfredNotificationManager implements NotificationManager {
    private static final String TAG = "ManfredNotificationMana";
    private Context context;

    @Inject
    public ManfredNotificationManager(Context context) {
        this.context = context;
    }

    @Override
    public void showNotification(StatusBarNotification notification) {
        PendingIntent pendingIntent;
        if(notification instanceof NewOrderStatusBarNotification){
            Intent notifyIntent = new Intent(context, NewOrderActivity.class);
            pendingIntent = PendingIntent.getActivity(context, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        }else if (notification instanceof NewPreOrderStatusBarNotification){
            Intent preOrderIntent = new Intent(context, PreordersActivity.class);
            pendingIntent = PendingIntent.getActivity(context, 0, preOrderIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        } else{
            Intent resultIntent = new Intent(context, SplashActivity.class);
            pendingIntent = PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        }
        showNotification(notification.getText(),notification.getTitle(), notification.getId(), pendingIntent);
    }

    @Override
    public void hideNotification(StatusBarNotification notification) {
        android.app.NotificationManager notificationManager = (android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(notification.getId());
    }

    @Override
    public void showNotificationIfNotOnScreen(StatusBarNotification notification) {
        if(!ManfredApplication.isForeground()){
            showNotification(notification);
        }
    }

    @Override
    public void hideAllNotification() {
        android.app.NotificationManager notificationManager = (android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    private void showNotification(String messageBody, @Nullable String title, int id, PendingIntent resultPendingIntent) {
        Log.d(TAG, "showNotification: " + messageBody);
        String channelId = "manfred_channel";
        android.app.NotificationManager notificationManager = (android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChanel(channelId, notificationManager, "Manfred events", "События manfred");
        }

        RemoteViews mRemoteViews;

        if(title!=null && title.isEmpty()){
            mRemoteViews = new RemoteViews(context.getPackageName(), R.layout.notification_without_title);
            mRemoteViews.setTextViewText(R.id.notif_content, messageBody);
        }else {
            mRemoteViews = new RemoteViews(context.getPackageName(), R.layout.custom_notification_small);
            mRemoteViews.setTextViewText(R.id.notif_content, messageBody);
            mRemoteViews.setTextViewText(R.id.notif_title, title);
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContent(mRemoteViews)
                .setAutoCancel(true)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC) //to show content on lock screen
                .setContentIntent(resultPendingIntent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder
                    .setPriority(android.app.NotificationManager.IMPORTANCE_HIGH);
        }

        if (notificationManager != null) {
            Log.d(TAG, "showNotification: notifing...");
            notificationManager.notify(id, notificationBuilder.build());
        }

        //flash screen
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        flashScreen(pm);
    }

    @RequiresApi(26)
    private void createNotificationChanel(String channelId, android.app.NotificationManager notificationManager, String s, String s2) {
        NotificationChannel notificationChannel = new NotificationChannel(channelId, s,
                android.app.NotificationManager.IMPORTANCE_HIGH);
        // Configure the notification channel.
        notificationChannel.setDescription(s2);
        notificationChannel.enableLights(true);
        notificationChannel.setLightColor(Color.RED);
        notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
        notificationChannel.setImportance(android.app.NotificationManager.IMPORTANCE_HIGH);
        notificationChannel.enableVibration(true);
        notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }

    private void flashScreen(PowerManager pm) {
        if (pm != null) {
            boolean isScreenOn = pm.isInteractive();
            Log.e(TAG, "isScreenOn? " + isScreenOn);
            if (!isScreenOn) {
                PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP
                        | PowerManager.ON_AFTER_RELEASE, "manfred::MyLock");
                wl.acquire(10000);

                PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "manfred::MyLock");
                wl_cpu.acquire(10000);
            }
        }
    }
}
