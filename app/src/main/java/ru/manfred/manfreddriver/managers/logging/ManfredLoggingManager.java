

/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */
package ru.manfred.manfreddriver.managers.logging;

import android.arch.lifecycle.Observer;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.db.entity.LogDB;
import ru.manfred.manfreddriver.interactor.repository.DBRepository;
import ru.manfred.manfreddriver.interactor.repository.DriverRepository;
import ru.manfred.manfreddriver.managers.LoggingManager;
import ru.manfred.manfreddriver.managers.ManfredCredentialsManager;
import ru.manfred.manfreddriver.managers.network.LogListener;
import ru.manfred.manfreddriver.managers.network.ManfredNetworkManager;
import ru.manfred.manfreddriver.model.api.Credentials;
import ru.manfred.manfreddriver.model.api.LogNet;
import ru.manfred.manfreddriver.utils.DBConvertorsUtils;

/**
 * Created by begemot on 16.03.18.
 */
@Singleton
public class ManfredLoggingManager implements LoggingManager {
    private static final String TAG = "ManfredLoggingManager";
    private AtomicBoolean isLogsReceived = new AtomicBoolean(true);
    @Nullable private List<LogDB> logs = new ArrayList<>();
    @Nullable private Credentials credentials;
    private DBRepository dbRepository;
    private ManfredNetworkManager networkManager;
    private DriverRepository driverRepository;

    @Inject
    public ManfredLoggingManager(Context context) {
        //ManfredApplication.getTripComponent().inject(this);
        //Log.d(TAG, "ManfredLoggingManager: creating...");
        final ManfredCredentialsManager manfredCredentialsManager = ((ManfredApplication) context.getApplicationContext()).getCredentialsManager();

        dbRepository = ((ManfredApplication)context.getApplicationContext()).getDbRepository();
        manfredCredentialsManager.getCredentials().observeForever(new Observer<Credentials>() {
            @Override
            public void onChanged(@Nullable Credentials cred) {
                //Log.d(TAG, "Credentials changed: ");
                if (cred != null) {
                    credentials = cred;
                    driverRepository = ((ManfredApplication) context.getApplicationContext()).getRepoProvider().getDriverRepository(cred.getToken());
                    Log.d(TAG, "start observing database ");
                    dbRepository.getStroredLogs(credentials.getPhoneNumber()).observeForever(new Observer<List<LogDB>>() {
                        @Override
                        public void onChanged(@Nullable List<LogDB> logDBS) {
                            logs=logDBS;
                        }
                    });
                }
            }
        });
        ((ManfredApplication)context.getApplicationContext()).getRepoProvider().getIsSocketLiveData().observeForever(new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean!=null){
                    if(aBoolean){
                        networkManager = ManfredApplication.getNetworkManger();
                        networkManager.setLogOfWebsocketListener(new LogListener() {
                            @Override
                            public void addLog(String className, String methodName, String text, LogTags logTag) {
                                //Log.d(TAG, "addLog: we have log from websocket");
                                addLogSilence(className, methodName, text, logTag);
                            }
                        });
                    }
                }
            }
        });
    }

    @Override
    public void addLog(String className, String methodName, String text, LogTags logTag) {
        //Log.d(TAG, "addLog: "+text);
        Log.d(className, "LogManager - "+methodName + ": " + text);
        addLogSilence(className, methodName, text, logTag);
    }

    @Override
    public void addLogSilence(String className, String methodName, String text, LogTags logTag) {
        long currentDate = System.currentTimeMillis() / 1000;
        LogNet.Builder builder = new LogNet.Builder();
        LogNet logNet = builder.date(currentDate)
                .subsystem(className)
                .text(methodName + ": " + text)
                .tag(logTag.toString())
                .build();
        if (credentials != null) {
            dbRepository.addLog(logNet,credentials.getPhoneNumber());
        }
    }

    @Override
    public void addLogAndSendImmediately(String className, String methodName, String text, LogTags logTag) {
        Log.d(className, methodName + ": " + text);
        addLogSilence(className, methodName, text, logTag);
        sendLogs();
    }

    @Override
    public void sendLogs() {
        if (credentials == null) {
            Log.d(TAG, "sendLogs: not sended log, creditional is null");
            return;
        }
        if (!isLogsReceived.get()) {
            Log.d(TAG, "sendLogs: not sended because previous not completed");
            return;
        }
        if (credentials != null) {
            if (logs != null && !logs.isEmpty()) {
                //Log.d(TAG, "sendLogs: log size is " + logs.size());
                isLogsReceived.set(false);
                List<LogNet>logsForSend=DBConvertorsUtils.logsDBToLogs(logs);
/*                if (logsForSend != null) {
                    Log.d(TAG, "sendLogs: we need send "+logsForSend.size());
                }else {
                    Log.d(TAG, "sendLogs: not have logs for sending");
                }*/
                driverRepository.sendLogs(logsForSend, new DriverRepository.NetAnswerCallbackWithoutPayload() {
                    @Override
                    public void allFine() {
                        dbRepository.removeSendedLogs(logs,credentials.getPhoneNumber());
                        if(logsForSend!=null) {
                            Log.d(TAG, "allFine: " + logsForSend.size() + " log succefully sended");
                        }
                        isLogsReceived.set(true);
                        //logs.clear();
                        //Log.d(TAG, "allFine: sended successfully");
                    }

                    @Override
                    public void error(String error) {
                        Log.d(TAG, "error: " + error);
                        isLogsReceived.set(true);
                    }

                    @Override
                    public void serverNotAvailable() {
                        Log.d(TAG, "serverNotAvailable: ");
                        isLogsReceived.set(true);
                    }
                });
            }
        }
    }
}
