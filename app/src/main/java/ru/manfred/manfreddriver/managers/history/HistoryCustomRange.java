/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers.history;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.utils.TimeDateUtils;

/**
 * Created by begemot on 13.03.18.
 */

public class HistoryCustomRange {
    private Date startDate;
    private Date endDate;
    private List<Order> orders;
    private static final String TAG = "HistoryCustomRange";

    public HistoryCustomRange(Date startDate, Date endDate, List<Order> orders) {
        //Log.d(TAG, "HistoryCustomRange: setting cache for dates "+startDate+", "+endDate);
        this.startDate = startDate;
        this.endDate = endDate;
        this.orders = orders;
    }

    public List<Order> getOrdersHistory(Date startDate, Date endDate) {
        //Log.d(TAG, "getOrdersHistory: trying to take "+startDate+", "+endDate);
        if (TimeDateUtils.isSameDay(this.startDate, startDate) & TimeDateUtils.isSameDay(this.endDate, endDate)) {
            //Log.d(TAG, "getOrdersHistory: we have this in cache, returning");
            return orders;
        } else {
            return new ArrayList<>();
        }
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }
}
