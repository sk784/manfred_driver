/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers.network;

import android.support.annotation.UiThread;

public interface NetworkReceiver {
    String getTypeOfRequest();
    @UiThread
    void receiveResponse(String response);
    @UiThread
    void errorWithReceive(String message);
}
