/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.manfred.manfreddriver.utils.SingleLiveEvent;

/**
 * Created by begemot on 22.02.18.
 */
@Singleton
public class ManfredActivityNotificationManager implements ActivityNotificationManager {
    private List<NotificationManagerListener> listeners = new ArrayList<>();
    private static final String TAG = "ManfredNotificationMana";
    private List<Message> messageQuery = new ArrayList<>();
    private SingleLiveEvent<Message> baseMessages = new SingleLiveEvent<>();


    @Inject
    public ManfredActivityNotificationManager() {
    }

    public LiveData<Message> getBaseMessages() {
        return baseMessages;
    }

    @Override
    public void showNotification(Message message) {
        Log.d(TAG, "showNotification: " + message.toString());
        notifyListeners(message);

    }

    public void showBaseNotification(Message message){
        baseMessages.postValue(message);
    }

/*    public void baseNotifShowed(){
        baseMessages.postValue(null);
    }*/

    @Override
    public void showFinishCostNotification(float totalCost, float debit) {
        Log.d(TAG, "showFinishCostNotification: ");
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(0);
        String messageText = "Оплачено — " + String.valueOf(nf.format(totalCost)) + " ₽ \n" +
                "Долг пассажира — " + String.valueOf(nf.format(debit)) + " ₽";
        Message message = new Message("Заказ завершен", messageText);
        notifyListeners(message);
    }

    @Override
    public void showSystemNotification(Message message, int id) {

    }

    @Override
    public void addNotificationManagerListener(NotificationManagerListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeNotificationManagerListener(NotificationManagerListener listener) {
        listeners.remove(listener);
    }

    @Override
    public void messageShowed(Message message) {
        messageQuery.remove(message);
    }

    @Override
    public List<Message> getNotShowedMessages() {
        Log.d(TAG, "getNotShowedMessages: returning " + messageQuery.size() + " messages");
        return messageQuery;
    }

    private void notifyListeners(Message message) {
        Log.d(TAG, "notifyListeners: ");
        messageQuery.add(message);
        for (NotificationManagerListener listener : listeners) {
            listener.showNotification(message);
        }
    }

    @Override
    public String toString() {
        return "ManfredActivityNotificationManager{" +
                "listeners=" + listeners +
                ", messageQuery=" + messageQuery +
                '}';
    }
}
