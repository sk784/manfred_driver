/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers.history;

import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.manfred.manfreddriver.model.api.Order;

/**
 * Created by begemot on 13.03.18.
 */
@Singleton
public class ManfredHistoryCache implements HistoryCache {
    private static final String TAG = "ManfredHistoryCache";
    private List<Order> todayHistory;
    private List<Order> weekHistory;
    private List<Order> monthHistory;
    @Nullable
    private HistoryCustomRange historyCustomRange;

    @Inject
    public ManfredHistoryCache() {
        Log.d(TAG, "ManfredHistoryCache: ");
    }

    @Override
    public void setTodayHistory(List<Order> orders) {
        todayHistory = orders;
    }

    @Override
    public void setWeekHistory(List<Order> orders) {
        weekHistory = orders;
    }

    @Override
    public void setMonthHistory(List<Order> orders) {
        monthHistory = orders;
    }

    @Override
    public void setRangeHistory(Date startDate, Date endDate, List<Order> orders) {
        historyCustomRange = new HistoryCustomRange(startDate, endDate, orders);
    }

    @Override
    public void invalidateCache() {
        todayHistory = null;
        weekHistory = null;
        monthHistory = null;
    }

    @Override
    public List<Order> getTodayHistory() {
        return todayHistory;
    }

    @Override
    public List<Order> getWeekHistory() {
        return weekHistory;
    }

    @Override
    public List<Order> getMonthHistory() {
        return monthHistory;
    }

    @Nullable
    @Override
    public List<Order> getRangeHistory(Date startDate, Date endDate) {
        if (historyCustomRange == null || historyCustomRange.getOrdersHistory(startDate, endDate).isEmpty()) {
            return null;
        } else {
            return historyCustomRange.getOrdersHistory(startDate, endDate);
        }
    }

    @Nullable
    public HistoryCustomRange getHistoryCustomRange() {
        return historyCustomRange;
    }
}
