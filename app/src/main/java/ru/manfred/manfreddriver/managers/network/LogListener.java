/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers.network;

import ru.manfred.manfreddriver.managers.logging.LogTags;

public interface LogListener{
    void addLog(String className, String methodName, String text, LogTags logTag);
}
