/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers.preorder;

import ru.manfred.manfreddriver.model.api.Order;

public class CurrentPreOrderState {
    public enum SoonPreOrderStatus {
        PRE_ORDER_START_SOON,
        PRE_ORDER_WAIT_TO_START,
        PRE_ORDER_TIME_OUT
    }

    private Order currPreOrder;
    private SoonPreOrderStatus soonPreOrderStatus;

    CurrentPreOrderState(Order currPreOrder, SoonPreOrderStatus soonPreOrderStatus) {
        this.currPreOrder = currPreOrder;
        this.soonPreOrderStatus = soonPreOrderStatus;
    }

    public long getPreOrderId() {
        return currPreOrder.getId();
    }

    public void setOrderTimedOut(Order preOrder) {
        if (currPreOrder.getId() == preOrder.getId()) {
            soonPreOrderStatus = SoonPreOrderStatus.PRE_ORDER_TIME_OUT;
        }
    }

    public Order getCurrPreOrder() {
        return currPreOrder;
    }

    public SoonPreOrderStatus getSoonPreOrderStatus() {
        return soonPreOrderStatus;
    }
}
