/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers;

import android.annotation.SuppressLint;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.LongSparseArray;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.interactor.repository.ConfirmationRepository;
import ru.manfred.manfreddriver.interactor.repository.DriverRepository;
import ru.manfred.manfreddriver.managers.history.ManfredHistoryManager;
import ru.manfred.manfreddriver.managers.logging.LogTags;
import ru.manfred.manfreddriver.managers.logging.ManfredLoggingManager;
import ru.manfred.manfreddriver.managers.notification.ManfredNotificationManager;
import ru.manfred.manfreddriver.managers.notification.NewOrderStatusBarNotification;
import ru.manfred.manfreddriver.managers.notification.StatusBarNotification;
import ru.manfred.manfreddriver.managers.preorder.ManfredPreOrderManager;
import ru.manfred.manfreddriver.managers.sound.ManfredSoundManager;
import ru.manfred.manfreddriver.model.api.Driver;
import ru.manfred.manfreddriver.model.api.Event;
import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.utils.RepositoryProvider;

/**
 * Created by begemot on 15.10.17.
 */

public class ManfredEventManager implements EventManager {

    private static final String TAG = "ManfredEventManager";
    @Inject
    ManfredTripManager tripManager;
    @Inject
    ManfredPreOrderManager manfredPreOrderManager;
    @Inject
    ManfredActivityNotificationManager notificationManager;
    @Inject
    ManfredNotificationManager manfredNotificationManager;
    @Inject
    ManfredLoggingManager loggingManager;
    @Inject
    ManfredOrderManager orderManager;
    @Inject
    ManfredSoundManager soundManager;
    @Inject
    StatusManager statusManager;
    private DriverRepository driverRepository;
    @Nullable private ConfirmationRepository confirmationRepository;//init asynchronic
    //private RepositoryProvider repositoryProvider;
    private String currToken;
/*    @Inject
    HistoryCache historyCache;*/
    private Driver driver;

    private static final String RIDE_TO_PASSENGER = "RIDE_TO_PASSENGER";
    private static final String WAIT_PASSENGER = "WAIT_PASSENGER";
    private static final String RIDE = "RIDE";


    //ConfirmationRepository testConfirmationRepository;

    public ManfredEventManager(ManfredCredentialsManager credentialsManager, RepositoryProvider repositoryProvider) {
        //this.networkManager = networkManager;
        //this.driver = driver;
        credentialsManager.getDriver().observeForever(savedDriver -> driver = savedDriver);
        ManfredApplication.getTripComponent().inject(this);
        credentialsManager.getCredentials().observeForever(credentials -> {
            if (credentials != null) {
                currToken=credentials.getToken();
                driverRepository=repositoryProvider.getDriverRepository(currToken);
                confirmationRepository=repositoryProvider.getConfirmationRepository(currToken);
            }
        });
    }

    //for test
    ManfredEventManager(ManfredTripManager manfredTripManager) {
        tripManager = manfredTripManager;
    }

    @Override
    public void newEvent(Event event) {
        Log.d(TAG, "newEvent: ");
        processEvent(event);
    }

    @Override
    public void newEvents(List<Event> events) {
        //Log.d(TAG, "newEvents: ");
        //loggingManager.addLog("ManfredEventManager", "newEvents", "checking for events", LogTags.OTHER);

        if (!events.isEmpty()) {
            //Log.d(TAG, "newEvents: "+events.size());

            //logEvents(events);
            List<Event> filteredEvents = filterEvents(events);

            //may be empty after removing orders
            if (!filteredEvents.isEmpty()) {
                Log.d(TAG, "newEvents: first event created in "
                        + (events.get(0).getCreated() != null ? events.get(0).getCreated() : "null")
                        + " last event - " + (events.get(events.size() - 1).getCreated() != null ?
                        events.get(events.size() - 1).getCreated() : "null"));

            }


            for (Event event : filteredEvents) {
                processEvent(event);
            }
        }
    }

    //@AddTrace(name = "filterEventsTrace")
    private List<Event> filterEvents(List<Event> events) {
        List<Event> filteredEvents = new ArrayList<>(events);
        Collections.sort(filteredEvents, (o1, o2) -> {
            if (o1.getCreated() == null || o2.getCreated() == null)
                return 0;
            return o1.getCreated().compareTo(o2.getCreated());
        });
            /*
            remove cancelled offers - troubles with sound
            store object of events what consist new order for
            matching and deleting cancelled by object
            in events map newOrders<order id, event object>
            */
        LongSparseArray<Event> newOrders = new LongSparseArray<>();

        List<Event> eventsForRemove = new ArrayList<>();
        for (Event event : filteredEvents) {
            if (event.getType().equals("new_order")
                    || event.getType().equals("preorder_reminder")
                    || event.getType().equals("preorder_wait_to_start")) {
                if (event.getOrder().getStatus().equals("CANCELLED")) {
                    Log.d(TAG, "newEvents: status of order " + event.getOrder().getId() + " cancelled, removing...");
                    //loggingManager.addString(TAG, "newEvents: status of order "+event.getOrder().getId()+" cancelled, removing...");
                    eventsForRemove.add(event);
                } else {
                    newOrders.put(event.getOrder().getId(), event);
                }
            }
        }

        for (Event event : filteredEvents) {
            if (event.getType().equals("order_cancelled_by_passenger") ||
                    event.getType().equals("order_cancelled_by_operator")) {
                Log.d(TAG, "newEvents: order " + event.getOrder().getId() + " need to removed(was cancelled)");
                //loggingManager.addString(TAG, "newEvents: order "+event.getOrder().getId()+" need to removed(was cancelled)");
                eventsForRemove.add(newOrders.get(event.getOrder().getId()));
            }

            if (event.getType().equals("preorder_wait_to_start") || event.getType().equals("preorder_reminder")) {
                if (event.getOrder().getCar_need_time() != null) {
                    if (event.getOrder().getCar_need_time().before(new Date())) {
                        Log.d(TAG, "newEvents: order " + event.getOrder().getId() +
                                " need to removed(old notification about pre order)");
                        //loggingManager.addString(TAG, "newEvents: order "+event.getOrder().getId()
                        // +" need to removed(old notification about pre order), Car_need_time =
                        // "+event.getOrder().getCar_need_time());
                        eventsForRemove.add(newOrders.get(event.getOrder().getId()));
                    }
                }
            }
        }

        //Log.d(TAG, "newEvents: events size before removing is "+events.size());
        //Log.d(TAG, "newEvents: we need to remove "+eventsForRemove.size()+" orders, it is: ");
        //logEvents(eventsForRemove);

        filteredEvents.removeAll(eventsForRemove);
        //Log.d(TAG, "newEvents: events size after removing cancelled orders is "+events.size());

        //Log.d(TAG, "newEvents: we have it events after removing: ");
        logEvents(filteredEvents);
        return filteredEvents;
    }

    private void processEvent(Event event) {
        final String type = event.getType();
        String logStringNewEvent = "processing event: " + event.toString() +
                ". App state is " + (ManfredApplication.isForeground() ? "foreground" : "background");
        loggingManager.addLog("ManfredEventManager", "processEvent", logStringNewEvent, LogTags.OTHER);
        //ConfirmationRepository confirmationRepository = new ConfirmationRepository(token);
        switch (type) {
            case "order_cancelled_by_passenger":
                soundManager.stopNewOrderSound();
                soundManager.cancelOrder();
                manfredPreOrderManager.preOrderCancelledByPassenger(event.getOrder());
                tripManager.cancelRide(event.getOrder(), new TripManager.Reason(type,
                        "Пассажир отменил заказ.\nНичего страшного, будут и другие.\nТакое случается."));
                ManfredHistoryManager historyManager =
                        new ManfredHistoryManager(driverRepository);
                historyManager.invalidateCache();
                break;
            case "order_cancelled_by_operator":
                manfredPreOrderManager.preOrderCancelledByOperator(event.getOrder());
                tripManager.cancelRide(event.getOrder(), new TripManager.Reason(type, event.getMessage()));
                break;
            case "driver_changed":
                soundManager.stopNewOrderSound();
                manfredPreOrderManager.preOrderCancelledByOperator(event.getOrder());
                tripManager.cancelRide(event.getOrder(), new TripManager.Reason(type, event.getMessage()));
                notificationManager.showBaseNotification
                        (new ActivityNotificationManager.Message("",event.getMessage()));
                break;
            case "assigned_to_order":
                if (event.getOrder() != null) {
                    forcedStartTrip(event.getOrder());
                }
                break;
            case "order_accepted_by_operator":
                if (event.getOrder() != null) {
                    forcedStartTrip(event.getOrder());
                }
                break;
            case "confirm_preorder_timeout":
                manfredPreOrderManager.preOrderTimedOut(event.getOrder());
                break;
            case "driver_status_updated":
                if(event.getIs_free()!=null) {
                    if (event.getIs_free()) {
                        statusManager.setFree();
                    } else {
                        statusManager.setBusy();
                    }
                }
                break;
            case "start_ride":
                tripManager.startRideToDestination(event.getOrder());
                break;
            case "start_ride_to_passenger":
                tripManager.startRideToPassenger(event.getOrder());
                break;
            case "order_finished":
                tripManager.finishRide(event.getOrder(), event.getMessage());
                //notificationManager.showBaseNotification(new ActivityNotificationManager.Message("Завершено оператором", event.getMessage()));
                notificationManager.showNotification(new ActivityNotificationManager.Message("Завершено оператором", event.getMessage()));
                break;
            case "search_driver_timeout":
                soundManager.stopNewOrderSound();
                tripManager.offerOnScreenTimeOut(event.getOrder(), new TripManager.Reason(type, event.getMessage()));
                break;
            case "prepayment_error":
                manfredPreOrderManager.removePreOrder(event.getOrder());
                tripManager.cancelRide(event.getOrder(), new TripManager.Reason(type, event.getMessage()));
                break;
            case "next_order_payment_error":
                if (tripManager.getCurrentStatus()!=null &&
                        tripManager.getCurrentStatus().getStatusState() ==
                                TripManager.StatusState.OFFER_ADDITIONAL_PAYMENT_ERROR) {
                    Log.d(TAG, "we have this status, ignoring");
                } else {
                    tripManager.offerAdditionalPaymentError(event.getOrder(), new TripManager.Reason(type, event.getMessage()));
                    String text = "Оплата по заказу"+event.getOrder().getId()+"не прошла";
                    manfredNotificationManager.showNotificationIfNotOnScreen(new NewOrderStatusBarNotification("Оплата не прошла", text));
                }
                break;
            case "new_order":
                processNewOrder(event);
                break;
            case "payment_successfull":
                tripManager.paymentSuccessful(event.getOrder(), new TripManager.Reason(type, event.getMessage()));
                break;
            case "new_scheduled_order":
                manfredPreOrderManager.addPreOrder(event.getOrder());
                if((event.getOrder()!=null)) {
                    if (confirmationRepository != null) {
                        confirmationRepository.orderReceived((int) event.getOrder().getId());
                    }
                }
                break;
            case "preorder_reminder":
                manfredPreOrderManager.orderSoon(event.getOrder().getId());
                break;
            case "preorder_wait_to_start":
                manfredPreOrderManager.waitForStart(event.getOrder());
                break;
            case "preorder_wait_to_start_timeout":
                manfredPreOrderManager.preOrderTimedOutBeforeStart(event.getOrder());
                break;
            case "preorder_reminder_notification":
                //point D
                manfredPreOrderManager.orderReminder(event.getOrder());
                break;
            case "order_additional_payment_timeout":
                tripManager.cancelRide(event.getOrder(), new TripManager.Reason(type, event.getMessage()));
                break;
            case "order_updated":
                tripManager.orderUpdated(event.getOrder(), new TripManager.Reason(type, event.getMessage()));
                break;
            case "order_accepted":
                if (isPreorder(event.getOrder())) {
                    Log.d(TAG, "newEvents: look like preorder, processing...");
                    manfredPreOrderManager.preOrderAccepted(event.getOrder());
                } else {
                    Log.d(TAG, "newEvents: not have car_need_time in order, it's just accepted order, processing...");
                    if (isMyOrder(event.getOrder())) {
                        Log.d(TAG, "newEvents: it look my order, ignoring");
                        break;
                    }
                    TripManager.Reason cancelReason = new TripManager.Reason("order_accepted", event.getMessage());
                    if (Objects.equals(event.getMessage(), "Водитель взял заказ")) {
                        cancelReason = new TripManager.Reason("other_driver_take_order",
                                "Другой водитель взял заказ");
                        soundManager.stopNewOrderSound();
                    }
                    tripManager.cancelRide(event.getOrder(), cancelReason);
                }
                break;
            case "finish_order_result":
                float debit = event.getOrder().getTotal_cost() - event.getOrder().getHolded_amount();
                Log.d(TAG, "finish_order_result: total_cost is " + event.getOrder().getTotal_cost() +
                        ", holded_amount is " + event.getOrder().getHolded_amount());
                notificationManager.showFinishCostNotification(event.getOrder().getHolded_amount(), debit);
                break;
        }
    }

    private void processNewOrder(Event event) {
        int orderId = (int) event.getOrder().getId();
        if (confirmationRepository != null) {
            confirmationRepository.orderReceived(orderId);
        }
        orderManager.newOrder(event.getOrder());
        String logString;
        if (ManfredApplication.isForeground()) {
            logString = "new order #" + orderId + ", app is foreground";
        } else {
            logString = "new order #" + orderId + ", app is background";
        }
        loggingManager.addLog("ManfredEventManager", "processEvent", logString, LogTags.NEW_ORDER_RECEIVED);
    }

    private void forcedStartTrip(Order order) {
        soundManager.stopNewOrderSound();
        soundManager.forcedStartTrip();
        //orderManager.setCurrentOrder(order);
        //tripManager.setCurrOrder(order);
        switch (order.getStatus()) {
            case RIDE_TO_PASSENGER:
                tripManager.startRideToPassenger(order);
                break;
            case WAIT_PASSENGER:
                tripManager.waitingPassenger(order);
                break;
            case RIDE:
                tripManager.startRideToDestination(order);
                break;
        }
        manfredPreOrderManager.forceRefresh();
        notificationManager.showBaseNotification
                (new ActivityNotificationManager.Message("","Вы были назначены на заказ оператором"));
        manfredNotificationManager.hideAllNotification();
        manfredNotificationManager.showNotification(new StatusBarNotification(null,"Вы были назначены на заказ оператором"));

    }

    /*
    matching by driver id
    */
    private boolean isMyOrder(Order order) {
        boolean result;
        if (order.getDriver() == null) {
            Log.d(TAG, "isMyOrder: no information about driver in order, not my order");
            return false;
        }

        result = driver != null && driver.getId() == order.getDriver().getId();
        Log.d(TAG, "isMyOrder: " + order.getId() + "? - " + result);
        return result;
    }

    private boolean isPreorder(Order order) {
        boolean preorder = false;
        if (order.getCar_need_time() != null) {
            preorder = true;
        }
        Log.d(TAG, "isPreorder: testing for preorder, it is " + preorder);

        Log.d(TAG, "isPreorder: test for this order not in trip now ");
        return preorder;
    }

    private void logEvents(List<Event> events) {
        Log.d(TAG, "logEvents: we have " + events.size() + " events, it is:");

        for (Event event : events) {
            Log.d(TAG, event.toString());
        }
    }




}
