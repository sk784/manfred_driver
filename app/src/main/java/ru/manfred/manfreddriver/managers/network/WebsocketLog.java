/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers.network;

import ru.manfred.manfreddriver.managers.logging.LogTags;

public class WebsocketLog{
    private final String className;
    private final String methodName;
    private final String text;
    private final LogTags logTag;

    public WebsocketLog(String className, String methodName, String text, LogTags logTag) {
        this.className = className;
        this.methodName = methodName;
        this.text = text;
        this.logTag = logTag;
    }

    public String getClassName() {
        return className;
    }

    public String getMethodName() {
        return methodName;
    }

    public String getText() {
        return text;
    }

    public LogTags getLogTag() {
        return logTag;
    }
}