/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class StatusManager {
    private static final String TAG = "StatusManager";
    private boolean isFree =true;
    private MutableLiveData<Boolean>isBusyLiveData=new MutableLiveData<>();

    @Inject
    public StatusManager() {
        Log.d(TAG, "StatusManager: creating");
    }

    public void setBusy(){
        Log.d(TAG, "setBusy: ");
        isFree =false;
        isBusyLiveData.postValue(false);
    }
    public void setFree(){
        Log.d(TAG, "setFree: ");
        isFree =true;
        isBusyLiveData.postValue(true);
    }

    public LiveData<Boolean> getIsFreeLiveData() {
        return isBusyLiveData;
    }

    public boolean isFree() {
        return isFree;
    }
}
