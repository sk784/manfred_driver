/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers;

import android.location.Location;
import android.util.Log;
import android.util.LongSparseArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.manfred.manfreddriver.model.api.CarOnMap;
import ru.manfred.manfreddriver.model.api.Driver;

@Singleton
public class ManfredCarsOnMapManager implements CarsOnMapManager {
    private HashMap<Long, CarOnMap> carsOnMap = new HashMap<Long, CarOnMap>();
    private List<CarPositionChangeListener> carPositionChangeListeners = new ArrayList<>();
    private static final String TAG = "ManfredCarsOnMapManager";
    private static final int TWO_MINUTES = 1000 * 60 * 2;
    private static final int CORDS_TIME_OUT = 5000;

    @Inject
    public ManfredCarsOnMapManager() {
    }

    @Override
    public void setCarsOnMap(List<Driver> carsOnMap) {
        notifyListeners(recalculateCars(carsOnMap));
    }

    private void notifyListeners(List<Long> offlinedCars) {
        for (CarPositionChangeListener listener : carPositionChangeListeners) {
            listener.carsChanged(new ArrayList<>(carsOnMap.values()), offlinedCars);
        }
    }

    @Override
    public void addCarPositionChangeListener(CarPositionChangeListener carPositionChangeListener) {
        carPositionChangeListeners.add(carPositionChangeListener);
    }

    @Override
    public void removeCarPositionChangeListener(CarPositionChangeListener carPositionChangeListener) {
        carPositionChangeListeners.remove(carPositionChangeListener);
    }

    private List<Long> recalculateCars(List<Driver> cars) {
        //Log.d(TAG, "recalculateCars: count of cars is "+cars.size());
        //int i=0;
        for (Driver car : cars) {
            //Log.d(TAG, "recalculateCars: step "+i);
            //Log.d(TAG, "recalculateCars: processing driver "+car.getName());
            //carsOnMap.get(car.getId(),new CarOnMap(car.getId(),createLocation(car),!car.is_free())).setLocation(createLocation(car));
            CarOnMap currCar = carsOnMap.get(car.getId());
            if (currCar == null) {
                //Log.d(TAG, "recalculateCars: new driver, add");
                carsOnMap.put(car.getId(), new CarOnMap(car.getId(), createLocation(car), !car.is_free()));
            } else {
                if (isBetterLocation(currCar.getLocation(), createLocation(car))) {
                    //Log.d(TAG, "recalculateCars: new location is better, refreshing");
                    currCar.setLocation(createLocation(car));
                    currCar.setBusy(!car.is_free());
                    carsOnMap.put(car.getId(), currCar);
                } else {
                    //Log.d(TAG, "recalculateCars: new location not better, ignoring");
                }
            }
            //i++;
        }
        return removeOfflineDriversFromMap(cars);
    }

    private List<Long> removeOfflineDriversFromMap(List<Driver> drivers) {
        List<Long> offlineIds = new ArrayList<>();
        ArrayList<Long> idCarsOnMap = new ArrayList<>(carsOnMap.keySet());
        ArrayList<Long> idCarsFromServer = new ArrayList<>();
        for (Driver driver : drivers) {
            idCarsFromServer.add(driver.getId());
        }
        for (Long carIdOnMap : idCarsOnMap) {
            if (!idCarsFromServer.contains(carIdOnMap)) {
                Log.d(TAG, "removeOfflineDriversFromMap: not have car with id " + carIdOnMap + ", removing");
                offlineIds.add(carIdOnMap);
                carsOnMap.remove(carIdOnMap);
            }
        }
        return offlineIds;
    }

    private Location createLocation(Driver driver) {
        Location location = new Location("");
        //Log.d(TAG, "createLocation: "+driver.toString());
        if (driver.getLocation() == null) return location;
        location.setLatitude(driver.getLocation().getLatitude());
        location.setLongitude(driver.getLocation().getLongitude());
        return location;
    }

    private <C> List<C> asList(LongSparseArray<C> sparseArray) {
        if (sparseArray == null) return null;
        List<C> arrayList = new ArrayList<C>(sparseArray.size());
        for (int i = 0; i < sparseArray.size(); i++)
            arrayList.add(sparseArray.valueAt(i));
        return arrayList;
    }

    private boolean isBetterLocation(Location location, Location currentBestLocation) {
        boolean isBetter = false;
        float newLocAccuracy = location.getAccuracy();
        float locDelta = currentBestLocation.distanceTo(location);
        if (locDelta > newLocAccuracy) isBetter = true;
        return isBetter;
    }

}
