/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers;

import ru.manfred.manfreddriver.managers.logging.LogTags;

/**
 * Created by begemot on 16.03.18.
 */

public interface LoggingManager {
    void addLog(String className, String methodName, String text, LogTags logTag);

    void addLogSilence(String className, String methodName, String text, LogTags logTag);

    void addLogAndSendImmediately(String className, String methodName, String text, LogTags logTag);

    void sendLogs();
}
