package ru.manfred.manfreddriver.managers.sound.sounds;

import ru.manfred.manfreddriver.R;

public class NoMoneySound implements DriverSound {
    @Override
    public int getSound() {
        return R.raw.notification;
    }

    @Override
    public boolean isPlayLooped() {
        return false;
    }

    @Override
    public int getPriority() {
        return 3;
    }
}
