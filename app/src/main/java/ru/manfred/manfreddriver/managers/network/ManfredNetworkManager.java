/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers.network;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import ru.manfred.manfreddriver.BuildConfig;
import ru.manfred.manfreddriver.interactor.api.websocket.NvWebSocketClient;
import ru.manfred.manfreddriver.interactor.api.websocket.OKHttpWebSocketClient;
import ru.manfred.manfreddriver.interactor.api.websocket.WebSocketClient;
import ru.manfred.manfreddriver.managers.logging.LogTags;

public class ManfredNetworkManager implements NetworkManager {
    private static final int TIMEOUT_OF_REQUEST_IN_SECONDS = 20;
    private static final int TIME_OF_CHECK_TIMEOUT = 5;
    private List<NetworkReceiver>receivers = new ArrayList<>();
    private final WebSocketClient webSocketClient;
    private MutableLiveData<Boolean>isConnected=new MutableLiveData<>();
    private static final String TAG = "ManfredNetworkManager";
    private LinkedBlockingQueue<Runnable> processingEventQueue = new LinkedBlockingQueue<>();
    private ThreadPoolExecutor eventExecutor = new ThreadPoolExecutor(1, 1,
            0L, TimeUnit.MILLISECONDS, processingEventQueue);
    private MutableLiveData<Boolean> isInMaintenance = new MutableLiveData<>();

    /*
    @Nullable private LogListener logListener;

    public void setLogListener(@NonNull LogListener logListener) {
        this.logListener = logListener;
    }*/

    public void setLogOfWebsocketListener(LogListener logListener){
        webSocketClient.setLogListener(logListener);
    }

    public ManfredNetworkManager() {
        Log.d(TAG, "ManfredNetworkManager: creating");
        webSocketClient = new OKHttpWebSocketClient(createMessageListener(), BuildConfig.CONNECTION_HOSTS);
        webSocketClient.connect();
        webSocketClient.setSocketConnectionListener(createSocketConnectionListener());
        initTimeOutChecker();
    }

    private void initTimeOutChecker() {
        final ScheduledExecutorService timeOutChecker = Executors.newScheduledThreadPool(1);
        timeOutChecker.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                if(receivers.isEmpty()){
                    Log.d(TAG, "timeOutChecker: receivers is empty, skip");
                    return;
                }
                //Log.d(TAG, "timeOutChecker: checking for timed out request in "+receivers);
                for (NetworkReceiver networkReceiver : new ArrayList<>(receivers)){
                    if(networkReceiver instanceof ManfredNetworkReceiver){
                        try {
                            if(isRequestTimedOut(((ManfredNetworkReceiver) networkReceiver))){
                                removeTimedOutReceiver(networkReceiver);
                                Log.d(TAG, "timeOutChecker: "+networkReceiver.getTypeOfRequest()
                                        +" with date of creation "+((ManfredNetworkReceiver) networkReceiver).getDateOfSendingRequest()+" is timed out, removed");
                            }
                        }catch (Throwable t){
                            Log.e(TAG, "exception in timeOutChecker",t);
                        }

                    }
                }
                //Log.d(TAG, "run: ___");
            }
        },30, TIME_OF_CHECK_TIMEOUT,TimeUnit.SECONDS);
    }

    private boolean isRequestTimedOut(ManfredNetworkReceiver manfredNetworkReceiver){
        Date dateOfRequest = manfredNetworkReceiver.getDateOfSendingRequest();
        int timeFromRequestSec = (int)Math.abs(new Date().getTime()-dateOfRequest.getTime())/1000;
        Log.d(TAG, "timeOutChecker: we have query "+manfredNetworkReceiver.getTypeOfRequest()
                +" was sended "+timeFromRequestSec+" sec ago");
        return timeFromRequestSec > TIMEOUT_OF_REQUEST_IN_SECONDS;
    }

    private void removeTimedOutReceiver(NetworkReceiver networkReceiver) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                networkReceiver.errorWithReceive("превышено время ожидания ответа");
            }
        });
        receivers.remove(networkReceiver);

    }

    @NonNull
    private WebSocketClient.SocketConnectionListener createSocketConnectionListener() {
        Log.d(TAG, "createSocketConnectionListener: ");
        return new WebSocketClient.SocketConnectionListener() {
            @Override
            public void onConnectionChange(boolean isConnect) {
                if(isConnected.getValue()!=null) {
                    Log.d(TAG, "onConnectionChange: "+isConnect);
                    if (isConnect != isConnected.getValue()) {
                        isConnected.postValue(isConnect);
                    }
                }
                if(!isConnect){
                    sendToAllReceiversLinkToServerAborted();
                }
            }
        };
    }

    public void disconnect(){
        webSocketClient.close();
    }

    @NonNull
    private WebSocketClient.MessageListener createMessageListener() {
        return new WebSocketClient.MessageListener() {
            @Override
            public void onSocketMessage(String answer) {
                //Log.d(TAG, "onSocketMessage: "+answer);
                //Log.d(TAG, "onSocketMessage: in thread "+Thread.currentThread().getId());
                //Log.d(TAG, "onSocketMessage: size of queue now "+processingEventQueue.size()+" adding new task to queue");
                eventExecutor.submit(new Runnable() {
                    @Override
                    public void run() {
                        //Log.d(TAG, "onSocketMessage: processing answer in thread"+Thread.currentThread().getId());
                        try {
                            processMessage(answer);
                        }catch (Exception e){
                            Log.e(TAG, "run: onSocketMessage", e);
                        }
                    }
                });
            }
        };
    }

    private void sendToAllReceiversLinkToServerAborted() {
        for (NetworkReceiver networkReceiver : new ArrayList<>(receivers)){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    networkReceiver.errorWithReceive("Связь с сервером прервана");
                }
            });
            if(networkReceiver instanceof ManfredNetworkReceiver){
                receivers.remove(networkReceiver);
            }
        }

    }

    public LiveData<Boolean> getIsConnected() {
        return isConnected;
    }

    public LiveData<Boolean> getIsInMaintenance(){
        return isInMaintenance;
    }

    private void processMessage(String answer) {
        Gson gson = new GsonBuilder().create();
        WebSocketAnswer socketAnswer = gson.fromJson(answer,WebSocketAnswer.class);
        setForMaintenance(socketAnswer);
        //Log.d(TAG, "processMessage: processing "+socketAnswer.getType()+" with id "+socketAnswer.getId());
        for (NetworkReceiver networkReceiver : new ArrayList<>(receivers)){
            /*
            Log.d(TAG, "onSocketMessage: we try receiver "+networkReceiver.getTypeOfRequest()+"for message "+socketAnswer.getId()+
                    " with type "+socketAnswer.getType());*/
            if(networkReceiver.getTypeOfRequest().equals(socketAnswer.getType())){
                noticeReceiver(answer, networkReceiver);
                //need remove receiver if it single-shot request
                removeReceiver(socketAnswer, networkReceiver);
            }
        }
    }

    private void setForMaintenance(WebSocketAnswer socketAnswer) {
        if(socketAnswer.getResultCode()==null)return;
        if(socketAnswer.getResultCode().equals("server_maintance")){
            isInMaintenance.postValue(true);
        }else {
            isInMaintenance.postValue(false);
        }
    }

    private void noticeReceiver(String answer, NetworkReceiver networkReceiver) {
        //Log.d(TAG, "onSocketMessage: sending "+answer +"to "+networkReceiver.getTypeOfRequest());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                networkReceiver.receiveResponse(answer);
            }
        });
    }

    private void removeReceiver(WebSocketAnswer socketAnswer, NetworkReceiver networkReceiver) {
        if(networkReceiver instanceof ManfredNetworkReceiver) {
            Long requestId = ((ManfredNetworkReceiver)networkReceiver).getIdOfRequest();
            if (requestId != null) {
                if (requestId == socketAnswer.getIdOfRequest()) {
                    receivers.remove(networkReceiver);
                }
            }
        }
    }

    private void runOnUiThread(Runnable action) {
        new Handler(Looper.getMainLooper()).post(action);
    }

    @Override
    public void sendRequest(WebSocketRequest request) {
        //Log.d(TAG, "sendRequest: "+request.getType());
        webSocketClient.sendRequest(request);
    }

    @Override
    public void sentEventAcceptedRequest(EventProcessedAnswer eventProcessedAnswer) {
        //Log.d(TAG, "sentEventAcceptedRequest: "+eventProcessedAnswer.toString());
        webSocketClient.sendEventReceived(eventProcessedAnswer);
    }

    @Override
    public void addRequestReceiver(NetworkReceiver networkReceiver) {
        //Log.d(TAG, "addRequestReceiver: "+networkReceiver);
        receivers.add(networkReceiver);
    }

    @Override
    public void addQuery(NetworkQuery networkQuery) {
        //Log.d(TAG, "addQuery: "+networkQuery.getWebSocketRequest().getType());
        NetworkReceiver addingReceiver = networkQuery.getNetworkReceiver();
        ((ManfredNetworkReceiver) addingReceiver).setDateOfSendingRequest(new Date());
        receivers.add(addingReceiver);
        sendRequest(networkQuery.getWebSocketRequest());
    }

    @Override
    public void removeRequestReceiver(ManfredNetworkReceiver networkReceiver) {
        Log.d(TAG, "removeRequestReceiver: "+networkReceiver);
        receivers.remove(networkReceiver);
    }

    @Override
    public void internetOff() {
        sendToAllReceiversLinkToServerAborted();
        webSocketClient.inetOff();
    }

    @Override
    public void internetOn() {
        webSocketClient.inetOn();
    }
}
