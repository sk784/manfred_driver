/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers;

import android.Manifest;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.managers.logging.LogTags;
import ru.manfred.manfreddriver.managers.logging.ManfredLoggingManager;

/**
 * Created by begemot on 21.03.18.
 * provide location information
 */
@Singleton
public class ManfredLocationManager implements CurrLocationManager {
    private static final String TAG = "ManfredLocationManager";
    private Context context;
    private MutableLiveData<Location>location=new MutableLiveData<>();
    private FusedLocationProviderClient fusedLocationProviderClient = null;
    private static final long MIN_UPDATE_INTERVAL = 5000;
    private static final long MAX_UPDATE_INTERVAL = 10000;
    private boolean isCreate = false;

    @Inject
    ManfredLoggingManager loggingManager;


    @Inject
    public ManfredLocationManager(Context context) {
        //Log.d(TAG, "ManfredLocationManager: creating");
        this.context = context;
        ManfredApplication.getTripComponent().inject(this);
        createManager();
        //createFilteringSystem();

    }

    public void createManager() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            loggingManager.addLog("ManfredLocationManager", "createManager",
                    "not have permissions, not creating", LogTags.OTHER);
            return;
        }
        if(isCreate){
            loggingManager.addLog("ManfredLocationManager", "createManager",
                    "already created, not need to create", LogTags.OTHER);
            return;
        }
        FusedLocationProviderClient locationProviderClient = getFusedLocationProviderClient();
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(MAX_UPDATE_INTERVAL);
        locationRequest.setFastestInterval(MIN_UPDATE_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        Looper looper = Looper.myLooper();
        LocationCallback locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                //Log.d(TAG, "onLocationResult: ");
                location.postValue(locationResult.getLastLocation());
            }
        };
        locationProviderClient.requestLocationUpdates(locationRequest, locationCallback, looper);
        isCreate=true;
        loggingManager.addLog("ManfredLocationManager", "createManager", "created", LogTags.OTHER);
    }

    private FusedLocationProviderClient getFusedLocationProviderClient() {
        if (fusedLocationProviderClient == null) {
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context);
        }
        return fusedLocationProviderClient;
    }

    @Override
    public LiveData<Location> getLocation() {
        return location;
    }

    @Override
    public Location getCurrentLocation() {
        return location.getValue();
    }

}
