/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers.network;

import com.google.gson.Gson;

public class ManfredEventNetworkReceiver implements NetworkReceiver {
    private NewEventCallback newEventCallback;

    public ManfredEventNetworkReceiver(NewEventCallback newEventCallback) {
        this.newEventCallback = newEventCallback;
    }

    @Override
    public String getTypeOfRequest() {
        return "new_event";
    }

    @Override
    public void receiveResponse(String response) {
        Gson gson = new Gson();
        NewEventAnswer event = gson.fromJson(response,NewEventAnswer.class);
        newEventCallback.newEvent(event);
    }

    @Override
    public void errorWithReceive(String message) {

    }

}
