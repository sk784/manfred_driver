/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers;

import android.arch.lifecycle.LiveData;
import android.location.Location;
import android.support.annotation.Nullable;

/**
 * Created by begemot on 21.03.18.
 */

public interface CurrLocationManager {
    interface LocationChangedListener {
        void locationChanged(Location location);
    }

    LiveData<Location> getLocation();

    @Nullable
    Location getCurrentLocation();
}
