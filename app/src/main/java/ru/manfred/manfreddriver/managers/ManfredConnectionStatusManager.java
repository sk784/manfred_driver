/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.managers.logging.LogTags;
import ru.manfred.manfreddriver.managers.logging.ManfredLoggingManager;
import ru.manfred.manfreddriver.utils.Constants;
import ru.manfred.manfreddriver.utils.NetworkUtil;

import static ru.manfred.manfreddriver.utils.NetworkUtil.TYPE_NOT_CONNECTED;

@Singleton
public class ManfredConnectionStatusManager implements ConnectionStatusManager {
    private List<ConnectStatusListener> listeners = new ArrayList<>();
    private Context context;
    private boolean isServerAvailable = true;
    private static final String TAG = "ManfredConnectionStatus";
    @Inject
    ManfredLoggingManager loggingManager;

    @Inject
    public ManfredConnectionStatusManager(Context context) {
        this.context = context;
        LocalBroadcastManager brodcastManager = LocalBroadcastManager.getInstance(context);
        ManfredApplication.getTripComponent().inject(this);
        BroadcastReceiver baseReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean haveNet = intent.getBooleanExtra(Constants.INTERNET_STATUS, true);
                //Log.d(TAG, "onReceive: "+haveNet);
                if (haveNet) {
                    haveInet();
                } else {
                    noInet();
                }
            }
        };
        IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        intentFilter.addAction(Constants.INTERNET_STATUS);
        brodcastManager.registerReceiver(baseReceiver, intentFilter);
    }

    private void noInet() {
        loggingManager.addLog("ManfredConnectionStatusManager", "noInet", "", LogTags.OTHER);
        for (ConnectStatusListener connectStatusListener : listeners) {
            connectStatusListener.netUnAvailable();
        }
        if(((ManfredApplication)context.getApplicationContext()).getRepoProvider().isWebSocket()) {
            ManfredApplication.getNetworkManger().internetOff();
        }
    }

    private void haveInet() {
        loggingManager.addLog("ManfredConnectionStatusManager", "haveInet", "", LogTags.OTHER);
        for (ConnectStatusListener connectStatusListener : listeners) {
            connectStatusListener.netAvailable();
        }
        if(((ManfredApplication)context.getApplicationContext()).getRepoProvider().isWebSocket()) {
            ManfredApplication.getNetworkManger().internetOn();
        }
    }

    private void noServerLink() {
        for (ConnectStatusListener connectStatusListener : listeners) {
            connectStatusListener.serverUnAvailable();
        }
    }

    private void haveServerLink() {
        for (ConnectStatusListener connectStatusListener : listeners) {
            connectStatusListener.netAvailable();
        }
    }

    public void invalidToken(){
        Log.d(TAG, "invalidToken: ");
        loggingManager.addLog("ManfredConnectionStatusManager", "invalidToken", "", LogTags.OTHER);
        for (ConnectStatusListener connectStatusListener : listeners) {
            connectStatusListener.tokenInvalid();
        }
    }

    @Override
    public void serverUnAvailable() {
        loggingManager.addLog("ManfredConnectionStatusManager", "serverUnAvailable", "", LogTags.OTHER);
        isServerAvailable = false;
        noServerLink();
    }

    @Override
    public void serverAvailable() {
        //loggingManager.addLog("ManfredConnectionStatusManager", "serverAvailable", "", LogTags.OTHER);
        isServerAvailable = true;
        haveServerLink();
    }

    @Override
    public void addConnectStatusListener(ConnectStatusListener connectStatusListener) {
        listeners.add(connectStatusListener);
    }

    @Override
    public void removeConnectStatusListener(ConnectStatusListener connectStatusListener) {
        listeners.remove(connectStatusListener);
    }

    @Override
    public boolean isNetAvailable() {
        return !(NetworkUtil.getConnectivityStatus(context) == TYPE_NOT_CONNECTED);
    }
}
