/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.Nullable;
import android.util.Log;

import ru.manfred.manfreddriver.interactor.repository.DriverRepository;
import ru.manfred.manfreddriver.model.api.Credentials;
import ru.manfred.manfreddriver.model.api.Driver;
import ru.manfred.manfreddriver.utils.RepositoryProvider;

public class ManfredCredentialsManager {
    private static final String TAG = "ManfredCredentialsManag";
    private MutableLiveData<Credentials> credentials;
    private MutableLiveData<Driver> driver;
    private RepositoryProvider repositoryProvider;

    public ManfredCredentialsManager(RepositoryProvider repositoryProvider) {
        this.repositoryProvider = repositoryProvider;
        credentials = new MutableLiveData<>();
        driver = new MutableLiveData<>();
    }

    public LiveData<Credentials> getCredentials() {
        return credentials;
    }

    public LiveData<Driver> getDriver() {
        return driver;
    }

    public void setCredentials(@Nullable Credentials incCredentials) {
        this.credentials.postValue(incCredentials);
        if (incCredentials==null){
            Log.d(TAG, "setCredentials: to null");
            return;
        }
        Log.d(TAG, "setCredentials: "+incCredentials.toString());
        DriverRepository driverRepository =repositoryProvider.getDriverRepository(incCredentials.getToken());
        driverRepository.driverInfo(new DriverRepository.infoCallback() {
            @Override
            public void fineCallback(Driver netDriver) {
                driver.postValue(netDriver);
            }

            @Override
            public void errorCallback() {

            }
        });
    }
}
