package ru.manfred.manfreddriver.managers.notification;

public class NewOrderStatusBarNotification extends StatusBarNotification {
    public NewOrderStatusBarNotification(String title, String text) {
        super(title, text);
    }
}
