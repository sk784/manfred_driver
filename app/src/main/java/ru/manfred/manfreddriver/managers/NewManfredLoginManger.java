/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import ru.manfred.manfreddriver.BuildConfig;
import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.interactor.repository.UserRepository;
import ru.manfred.manfreddriver.managers.logging.AuthError;
import ru.manfred.manfreddriver.managers.network.ManfredNetworkManager;
import ru.manfred.manfreddriver.managers.network.NetworkQuery;
import ru.manfred.manfreddriver.managers.network.WebSocketAnswer;
import ru.manfred.manfreddriver.managers.network.WebSocketAnswerCallback;
import ru.manfred.manfreddriver.model.api.CountryModel;
import ru.manfred.manfreddriver.model.api.ManfredError;
import ru.manfred.manfreddriver.model.api.ResponseAuth;
import ru.manfred.manfreddriver.model.responces.Phone;
import ru.manfred.manfreddriver.model.responces.PhoneDTO;
import ru.manfred.manfreddriver.model.responces.SMSCodeRequest;
import ru.manfred.manfreddriver.utils.RepositoryProvider;

public class NewManfredLoginManger{
    private static final String TAG = "NewManfredLoginManger";
    private final MutableLiveData<AuthState> authState;
    private final MutableLiveData<AuthError> authError;
    private final MutableLiveData<Phone> phone;
    private final MutableLiveData<Boolean> isNumberValid;
    private final MutableLiveData<String> filteredString;
    private String token;
    private final MutableLiveData<Boolean> isLoading;
    private final RepositoryProvider repositoryProvider;

    public enum AuthState {
        INPUT_PHONE,
        INPUT_SMS,
        INPUT_REGION_CODE,
        FINISH
    }

    public NewManfredLoginManger(RepositoryProvider repositoryProvider) {
        this.repositoryProvider = repositoryProvider;
        authState = new MutableLiveData<>();
        authState.setValue(AuthState.INPUT_PHONE);
        authError = new MutableLiveData<>();
        phone = new MutableLiveData<>();
        phone.postValue(new Phone(new CountryModel("🇷🇺", "Russia", "+7"),""));
        isNumberValid = new MutableLiveData<>();
        isNumberValid.postValue(false);
        filteredString = new MutableLiveData<>();
        isLoading = new MutableLiveData<>();
        isLoading.postValue(false);
    }

    public void attemptLogin() {
        if(phone.getValue()==null)return;
        Log.d(TAG, "attemptLogin: "+phone);
        isLoading.postValue(true);
        repositoryProvider.getUserRepository("").attemptLogin(new PhoneDTO(phone.getValue()), new UserRepository.AuthorisationCallback() {
            @Override
            public void success(@Nullable Object answer) {
                isLoading.postValue(false);
                authState.postValue(AuthState.INPUT_SMS);
            }

            @Override
            public void error(AuthError error) {
                isLoading.postValue(false);
                Log.d(TAG, "error: "+error);
                authError.postValue(error);
            }
        });
    }

    public String getToken() {
        return token;
    }

    public void sendSms(String pin, String pushToken) {
        Phone phone = getPhone().getValue();
        Log.d(TAG, "sendSms: for "+phone);
        isLoading.postValue(true);

        if(phone!=null) {
            SMSCodeRequest smsCodeRequest = new SMSCodeRequest(phone.getPhone(), pin,
                    phone.getCountry_code(), pushToken, "android", BuildConfig.VERSION_NAME);
            //ManfredNetworkManager manfredNetworkManager = CheckEventService.getManfredNetworkManager();
            repositoryProvider.getUserRepository("").sendCode(smsCodeRequest, new UserRepository.AuthorisationCallback<String>() {
                @Override
                public void success(@Nullable String answer) {
                    isLoading.postValue(false);
                    //Log.d(TAG, "sendSms messageReceived: result code is " + message.getResultCode());
                    //Log.d(TAG, "sendSms: auth_success "+message.toString());
                    //setToken.postValue(resp.getData().getToken());
                    //Log.d(TAG, "sendSms: auth_success, token is "+message.getPayload().getToken());
                    token=answer;
                    authState.postValue(AuthState.FINISH);
                }

                @Override
                public void error(AuthError error) {
                    isLoading.postValue(false);
                    authError.postValue(error);
                }
            });
        }
    }

    public MutableLiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    public void reSendSMS(){
        attemptLogin();
    }

    public void selectRegionCode() {
        authState.postValue(AuthState.INPUT_REGION_CODE);
    }

    public void enterRegionCode(CountryModel countryModel) {
        if (phone.getValue() != null) {
            phone.postValue(new Phone(countryModel,phone.getValue().getPhone()));
        } else {
            phone.postValue(new Phone(countryModel, ""));
        }
        authState.postValue(AuthState.INPUT_PHONE);
    }

    public void reEnterPhone(){
        authState.setValue(AuthState.INPUT_PHONE);
    }

    public void validateNumber(String number){
        if(phone.getValue()==null)return;
        CountryModel countryModel = phone.getValue().getCountryModel();
        //number = countryModel.getCountryCode() + number;
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        try {
            Phonenumber.PhoneNumber phoneNumberProto = phoneUtil.parse(countryModel.getCountryCode()+number, null);
            isNumberValid.postValue(phoneUtil.isValidNumber(phoneNumberProto));
            phone.postValue(new Phone(countryModel,number));
        } catch (NumberParseException e) {
            e.printStackTrace();
        }
       // boolean isValid = phoneUtil.isValidNumber(phoneNumberProto); // returns true if valid
    }

    public void filterCountriesByString(String string) {
        filteredString.postValue(string);
    }

    public MutableLiveData<String> getFilteredString() {
        return filteredString;
    }

    public MutableLiveData<Boolean> getIsNumberValid() {
        return isNumberValid;
    }

    public LiveData<AuthState> getAuthState() {
        return authState;
    }

    public LiveData<AuthError> getAuthError() {
        return authError;
    }

    public LiveData<Phone> getPhone() {
        return phone;
    }



}
