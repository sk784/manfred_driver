package ru.manfred.manfreddriver.managers.sound.sounds;

import ru.manfred.manfreddriver.R;

public class NewOrderSound implements DriverSound {
    @Override
    public int getSound() {
        return R.raw.order;
    }

    @Override
    public boolean isPlayLooped() {
        return true;
    }

    @Override
    public int getPriority() {
        return 1;
    }
}
