/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers.logging;

public class AuthError{
    private final ErrorStatus errorStatus;
    private final String text;

    public AuthError(ErrorStatus errorStatus, String text) {
        this.errorStatus = errorStatus;
        this.text = text;
    }

    public ErrorStatus getErrorStatus() {
        return errorStatus;
    }

    public String getText() {
        return text;
    }

    public enum ErrorStatus{
        INVALID_NUMBER,
        NETWORK_ERROR,
        INVALID_SMS_CODE,
        DRIVER_ARCHIVED
    }

    @Override
    public String toString() {
        return "AuthError{" +
                "errorStatus=" + errorStatus +
                ", text='" + text + '\'' +
                '}';
    }
}