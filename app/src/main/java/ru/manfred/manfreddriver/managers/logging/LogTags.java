/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers.logging;

public enum LogTags {
    NEW_ORDER_RECEIVED("new_order_received"),
    NEW_ORDER_SHOWED("new_order_showed"),
    NEW_ORDER_ACCEPT_CLICK("new_order_accept_click"),
    NEW_PREORDER_SOUNDED("new_preorder_sounded"),
    PREORDER_COUNT_CHANGED("preorder_count_changed"),
    ORDER_ON_PASSENGER_CLICK("order_on_passenger_click"),
    ORDER_START_RIDE_CLICK("order_start_ride_click"),
    ORDER_FINISH_CLICK("order_finish_click"),
    NEW_PREORDER_SHOWED("new_preorder_showed"),
    NEW_PREORDER_ACCEPTING_CLICK("new_preorder_accepting_click"),
    PREORDER_CONFIRMING_SHOW("preorder_confirming_show"),
    PREORDER_CONFIRMING_CLICK("preorder_confirming_click"),
    PREORDER_NOT_CONFIRMING_CLICK("preorder_not_confirming_click"),
    PREORDER_READY_TO_START_SHOWED("preorder_ready_to_start_showed"),
    PREORDER_READY_TO_START_CLICK_START("preorder_ready_to_start_click_start"),
    PREORDER_REMINDER_NOTIFICATION_SOUNDED("preorder_reminder_notification_sounded"),
    USER_ON_SCREEN("user_on_screen"),
    USER_AWAY_FROM_SCREEN("user_away_from_screen"),
    FILTERING("filtering"),
    REQUEST_TIME("request_time"),
    OTHER("other");

    private final String value;

    LogTags(String type) {
        value = type;
    }

    @Override
    public String toString() {
        return value;
    }
}
