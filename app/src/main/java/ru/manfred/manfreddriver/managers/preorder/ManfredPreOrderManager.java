/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers.preorder;

import android.annotation.SuppressLint;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.interactor.repository.DriverRepository;
import ru.manfred.manfreddriver.interactor.repository.ResponseCallback;
import ru.manfred.manfreddriver.interactor.repository.RideRepository;
import ru.manfred.manfreddriver.managers.ManfredSnackbarManager;
import ru.manfred.manfreddriver.managers.ManfredTripManager;
import ru.manfred.manfreddriver.managers.SnackbarManager;
import ru.manfred.manfreddriver.managers.logging.LogTags;
import ru.manfred.manfreddriver.managers.logging.ManfredLoggingManager;
import ru.manfred.manfreddriver.managers.notification.ManfredNotificationManager;
import ru.manfred.manfreddriver.managers.notification.NewOrderStatusBarNotification;
import ru.manfred.manfreddriver.managers.notification.StatusBarNotification;
import ru.manfred.manfreddriver.managers.sound.ManfredSoundManager;
import ru.manfred.manfreddriver.model.api.AcceptingOrder;
import ru.manfred.manfreddriver.model.api.Credentials;
import ru.manfred.manfreddriver.model.api.Driver;
import ru.manfred.manfreddriver.model.api.ManfredError;
import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.utils.SharedPreferencesManager;
import ru.manfred.manfreddriver.utils.TimeDateUtils;

/**
 * Created by begemot on 14.12.17.
 */
@Singleton
public class ManfredPreOrderManager implements PreOrderManager {
    private MutableLiveData<List<Order>>newPreOrders = new MutableLiveData<>();
    private MutableLiveData<List<Order>>myPreOrders = new MutableLiveData<>();
    private MutableLiveData<Order>orderWaitToStart = new MutableLiveData<>();
    private MutableLiveData<ManfredError>errors = new MutableLiveData<>();
    private List<PreOrdersListener> listeners = new ArrayList<>();
    private List<PreOrderEvent> preOrderEvents = new ArrayList<>();
    private MutableLiveData<Boolean>isRefreshing;
    private static final String TAG = "ManfredPreOrderManager";
    private Context context;
    @Inject
    ManfredSoundManager soundManager;
    @Inject
    ManfredTripManager tripManager;
    @Inject
    ManfredLoggingManager loggingManager;
    @Inject
    ManfredSnackbarManager snackbarManager;
    @Inject
    ManfredNotificationManager notificationManager;
    //@Nullable private String currToken;
    @Nullable private Driver driver;
    private DriverRepository driverRepository;

    @Inject
    public ManfredPreOrderManager(Context context) {
        //String setToken = SharedPreferencesManager.getCredentials(context).getToken();
        ManfredApplication.getTripComponent().inject(this);

        isRefreshing=new MutableLiveData<>();
        ((ManfredApplication)context.getApplicationContext()).getCredentialsManager()
                .getCredentials().observeForever(new Observer<Credentials>() {
            @Override
            public void onChanged(@Nullable Credentials credentials) {
                if(credentials!=null){
                    driverRepository = ((ManfredApplication) context.getApplicationContext()).getRepoProvider().getDriverRepository(credentials.getToken());
                    //currToken=credentials.getToken();
                    initPreOrdersList();
                }
            }
        });
        this.context = context;
        ((ManfredApplication)context.getApplicationContext()).getCredentialsManager().getDriver().observeForever(new Observer<Driver>() {
            @Override
            public void onChanged(@Nullable Driver credDriver) {
                driver=credDriver;
            }
        });
    }

    private void initPreOrdersList() {
        Log.d(TAG, "initPreOrdersList: ");
        isRefreshing.postValue(true);
        driverRepository.getPreOrders(new ResponseCallback<List<Order>>() {
            @Override
            public void allOk(List<Order> response) {
                loggingManager.addLog("ManfredPreOrderManager",
                        "initPreOrdersList", "preOrders refreshed", LogTags.OTHER);
                List<Order>myPreOrdersFromServer = new ArrayList<>();
                List<Order>newPreOrdersFromServer = new ArrayList<>();
                for (Order order : response){
                    if((order.getStatus().equals("SCHEDULED_ACCEPTED"))){
                        myPreOrdersFromServer.add(order);
                    }else {
                        newPreOrdersFromServer.add(order);
                    }
                }
                myPreOrders.postValue(myPreOrdersFromServer);
                newPreOrders.postValue(removeSoClosePreOrders(newPreOrdersFromServer));
                isRefreshing.postValue(false);
            }

            @Override
            public void error(ManfredError error) {
                errors.postValue(error);
                isRefreshing.postValue(false);
                scheduleRefreshPreOrders();
            }
        });
    }

    private static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    /**
     * do it when we have some troubles
     */
    private void scheduleRefreshPreOrders(){
        final int delay = 15;
        loggingManager.addLog("ManfredPreOrderManager", "scheduleRefreshPreOrders",
                "we have some troubles with refreshing preOrder schedule it for "+delay+"s", LogTags.OTHER);
        ScheduledExecutorService scheduler
                = Executors.newSingleThreadScheduledExecutor();

        Runnable task = new Runnable() {
            public void run() {
                forceRefresh();
            }
        };
        scheduler.schedule(task, delay, TimeUnit.SECONDS);
        scheduler.shutdown();
    }


    @Override
    public void eventProcessedSuccessfully(PreOrderEvent preOrderEvent) {
        Log.d(TAG, "eventProcessedSuccessfully: we have " + preOrderEvents.size() + " events, it is "+preOrderEvents);
        //Log.d(TAG, "eventProcessedSuccessfully: event " + preOrderEvent.toString() + " succefully processed, removing");
        loggingManager.addLog("ManfredPreOrderManager", "eventProcessedSuccessfully",
                preOrderEvent.toString() + " succefully processed, removing", LogTags.OTHER);
        preOrderEvents.remove(preOrderEvent);
        Log.d(TAG, "eventProcessedSuccessfully: now we have " + preOrderEvents.size() + "events");
    }

    @Override
    public void addPreOrder(Order order) {
        List<Order> currMyPreOrders = getMyPreOrdersList();
        if (isPreOrderToCloseForDisplay(currMyPreOrders, order)) {
            Log.d(TAG, "addPreOrder: "+order.getId()+" is so close for accepted, ignoring");
            return;
        }
        List<Order>currNewPreOrders = newPreOrders.getValue();
        if(currNewPreOrders!=null){
            if(currNewPreOrders.isEmpty()) {
                currNewPreOrders.add(order);
            }else {
                //we can take order second time, we removing old and adding new
                for (Order iteratedOrder : new ArrayList<>(currNewPreOrders) ){
                    if(iteratedOrder.getId()==order.getId()){
                        currNewPreOrders.remove(iteratedOrder);
                    }
                }
                currNewPreOrders.add(order);
            }
        }
        preOrderAddedNotif(order);
        soundManager.newPreOrder();
        //NotificationHelper.showNewPreOrderNotif(order.getId(), context);
        final String message = "Новый предзаказ " + order.getId();
        notificationManager.showNotificationIfNotOnScreen(new NewOrderStatusBarNotification(null, message));
        newPreOrders.postValue(currNewPreOrders);
        loggingManager.addLog("ManfredPreOrderManager", "addPreOrder",
                order.toString(), LogTags.NEW_PREORDER_SOUNDED);
    }

    @Override
    public void removePreOrder(Order order) {
        Log.d(TAG, "removePreOrder: need to remove order "+order.getId());
        List<Order>currNewPreOrders = getNewPreOrdersList();
        List<Order> currMyPreOrders = getMyPreOrdersList();
        for (Order iteratedOrder : currNewPreOrders) {
            if(iteratedOrder.getId()==order.getId()){
                Log.d(TAG, "removePreOrder: find preOrder "+order.getId()+" in new, deleting");
                currNewPreOrders.remove(iteratedOrder);
                break;
            }
        }
        newPreOrders.postValue(currNewPreOrders);

        for (Order iteratedOrder : currMyPreOrders) {
            if(iteratedOrder.getId()==order.getId()){
                Log.d(TAG, "removePreOrder: find preOrder "+order.getId()+" in my, deleting");
                currMyPreOrders.remove(iteratedOrder);
                break;
            }
        }
        Log.d(TAG, "removePreOrder: currMyPreOrders is "+currMyPreOrders.size());
        myPreOrders.postValue(currMyPreOrders);
        removePreOrderWaitingToStart(order);
        removePreOrderStartSoonEvent(order);
    }

    @Override
    public LiveData<List<Order>> getNewPreOrders() {
        return newPreOrders;
    }

    @Override
    public LiveData<List<Order>> getMyPreOrders() {
        return myPreOrders;
    }

    @Override
    public void acceptPreOrder(Order order, ResultListener resultListener) {
        Log.d(TAG, "acceptPreOrder: ");
        //resultListener.ok(order);
        Credentials credentials = SharedPreferencesManager.getCredentials(context);
        if(credentials==null){
            resultListener.error("ошибка чтения токена");
            return;
        }
        final RideRepository rideRepository =
                ((ManfredApplication)context.getApplicationContext()).getRepoProvider().getRideRepository(credentials.getToken());
        rideRepository.acceptOrder(new AcceptingOrder((int) order.getId()), new RideRepository.AcceptCallback() {
            @Override
            public void error(ManfredError manfredError) {
                Log.d(TAG, "error: " + manfredError.getText());
                loggingManager.addLog("ManfredPreOrderManager", "error", manfredError.getText(), LogTags.OTHER);
                resultListener.error(manfredError.getText());
                initPreOrdersList();
            }

            @Override
            public void accepted(Order newOrder) {
                order.setStatus("SCHEDULED_ACCEPTED");
                addOrderToAccepted(order);
                resultListener.ok(newOrder);
            }

            @Override
            public void debugMessage(String message) {
                Log.d(TAG, "debugMessage: " + message);
            }

            @Override
            public void serverNotAvailable() {
                resultListener.error("Сервер недоступен");
            }
        });
    }

    private void addOrderToAccepted(Order order) {
        List<Order> currNewPreOrders = getNewPreOrdersList();
        List<Order> currMyPreOrders = getMyPreOrdersList();
        Log.d(TAG, "addOrderToAccepted: currNewPreOrders "+currNewPreOrders.size());
        for (Order newPreOrder : new ArrayList<>(currNewPreOrders)) {
            if (newPreOrder.getId() == order.getId()) {
                currNewPreOrders.remove(newPreOrder);
                Log.d(TAG, "addOrderToAccepted: adding "+order.getId());
                currMyPreOrders.add(order);
                break;
            }
        }
        myPreOrders.postValue(currMyPreOrders);
        //Log.d(TAG, "addOrderToAccepted: currNewPreOrders "+currNewPreOrders.size());
        currNewPreOrders=removeSoClosePreOrders(currNewPreOrders);
        //Log.d(TAG, "addOrderToAccepted: currNewPreOrders after remove close "+currNewPreOrders.size());
        //Log.d(TAG, "addOrderToAccepted: now newPreOrders size is "+currNewPreOrders.size());
        //Log.d(TAG, "addOrderToAccepted: now myPreOrders size is "+currMyPreOrders.size());
        newPreOrders.postValue(currNewPreOrders);
        preOrderAcceptedSuccessfully(order);
    }

    private @NonNull List<Order> getMyPreOrdersList() {
        List<Order>orderList = getMyPreOrders().getValue();
        if(orderList==null){
            orderList=new ArrayList<>();
        }
        //Log.d(TAG, "getMyPreOrdersList: size "+orderList.size());
        return orderList;
    }

    private @NonNull List<Order> getNewPreOrdersList() {
        List<Order>orderList = getNewPreOrders().getValue();
        if(orderList==null){
            orderList=new ArrayList<>();
        }
        //Log.d(TAG, "getNewPreOrdersList: size "+orderList.size());
        return orderList;
    }

    @Override
    public void cancelOrder(Order order, ResultListener resultListener) {
        //Log.d(TAG, "cancelOrder: ");
        final RideRepository rideRepository =
                ((ManfredApplication)context.getApplicationContext()).getRepoProvider().getRideRepository(SharedPreferencesManager.getCredentials(context).getToken());

        rideRepository.declinePreOrder(order, new RideRepository.AcceptCallback() {
            @Override
            public void error(ManfredError manfredError) {
                resultListener.error(manfredError.getText());
            }

            @Override
            public void accepted(Order acOrd) {
                declineOrder(order);
                preOrderDeclinedByDriverNotify(order);
                resultListener.ok(order);
            }

            @Override
            public void debugMessage(String message) {
                Log.d(TAG, "debugMessage: " + message);

            }

            @Override
            public void serverNotAvailable() {
                resultListener.error("Сервер недоступен");
            }
        });

    }

    private void declineOrder(Order order) {
        Log.d(TAG, "declineOrder: "+order.getId());
        List<Order> currMyPreOrders = getMyPreOrdersList();
        //have concurrentException here((
        for(Order preOrder : new ArrayList<Order>(currMyPreOrders)){
            if(preOrder.getId()==order.getId()){
                currMyPreOrders.remove(order);
            }
        }
        Log.d(TAG, "declineOrder: currMyPreOrders size is "+currMyPreOrders.size());
        myPreOrders.postValue(currMyPreOrders);
    }

    @Override
    public void orderSoon(long id) {
        preOrderSoon(getAcceptedPreOrderById(id));
        soundManager.newPreOrder();
        notificationManager.showNotificationIfNotOnScreen(new StatusBarNotification(null, "Скоро предзаказ, проверьте приложение"));
        //NotificationHelper.showPreOrderSoonNotification("Скоро предзаказ, проверьте приложение", context);
    }

    public void orderReminder(Order order) {
        Log.d(TAG, "orderReminder: preOrder is soon");
        soundManager.preOrderWaitToStart();
        //NotificationHelper.showPreOrderSoonNotification("Время выезжать на предзаказ", context);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat dt = new SimpleDateFormat("hh:mm");
        String preOrderText = "Предзаказ в " + dt.format(order.getCar_need_time());
        notificationManager.showNotificationIfNotOnScreen(new StatusBarNotification("Время выезжать на предзаказ", preOrderText));
        preOrderWaitToStart(order);
    }

    @Override
    public void waitForStart(Order order) {
        //NotificationHelper.showPreOrderSoonNotification("Время выезжать на предзаказ", context);
        notificationManager.showNotificationIfNotOnScreen(new StatusBarNotification(null, "Время выезжать на предзаказ"));
        soundManager.newPreOrder();
        preOrderWaitToStart(order);
    }

    @Override
    public void preOrderTimedOut(Order order) {
        declineOrder(order);
        preOrderTimedOutNotif(order);
    }

    @MainThread
    @Override
    public void preOrderTimedOutBeforeStart(Order order) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (isMy(order)) {
                    preOrderListenersNotify(new PreOrderEvent(EventStatus.PRE_ORDER_TIME_OUT_BEFORE_START, order));
                    removePreOrderWaitingToStart(order);
                    final String errorText = "Вы не выехали на заказ по адресу " + order.getAddress() +
                            " и мы отдали его другому водителю.";
/*                    NotificationHelper.showNotificationIfNotOnScreen("",
                            "Вы не выехали на заказ по адресу " + order.getAddress() +
                            " и мы отдали его другому водителю.",context,700);*/
                    notificationManager.showNotificationIfNotOnScreen(new StatusBarNotification(null, errorText));
                    soundManager.cancelPreOrder();
                }
                removePreOrder(order);
            }
        });
    }

    @Override
    public void preOrderCancelledByPassenger(Order order) {
        Log.d(TAG, "preOrderCancelledByPassenger: " + order.getId());
        if (isMy(order)) {
            soundManager.cancelPreOrder();
/*            NotificationHelper.showNotificationIfNotOnScreen("",
                    "Пассажир отменил предзаказ " + order.getId(), context, 301);*/
            final String errorText = "Пассажир отменил предзаказ " + order.getId();
            notificationManager.showNotificationIfNotOnScreen(new StatusBarNotification(null, errorText));
        }
        removePreOrder(order);
        PreOrderEvent preOrderEvent = new PreOrderEvent(EventStatus.PRE_ORDER_DECLINED, order);
        if(isMy(order)){
            preOrderEvent.setMyPreOrder(true);
        }
        preOrderListenersNotify(preOrderEvent);
    }

    @Override
    public void preOrderCancelledByOperator(Order order) {
        if (isMy(order)) {
            soundManager.cancelPreOrder();
            PreOrderEvent preOrderEvent = new PreOrderEvent(EventStatus.PRE_ORDER_DECLINED, order);
            if(isMy(order)){
                preOrderEvent.setMyPreOrder(true);
            }
            preOrderListenersNotify(preOrderEvent);
            final String errorText = "Оператор отменил предзаказ " + order.getId();
            notificationManager.showNotificationIfNotOnScreen(new NewOrderStatusBarNotification(null, errorText));
/*            NotificationHelper.showNotificationIfNotOnScreen("",
                    "Оператор отменил предзаказ " + order.getId(), context, 301);*/
        }
        removePreOrder(order);
    }

    @Override
    public void preOrderAccepted(Order order) {
        List<Order>currNewPreOrders=getNewPreOrdersList();
        for (Order orderInLocalList : new ArrayList<>(currNewPreOrders)){
            if(orderInLocalList.getId()==order.getId()){
                currNewPreOrders.remove(orderInLocalList);
                break;
            }
        }
        List<Order> clearedPreOrder = removeSoClosePreOrders(currNewPreOrders);
        newPreOrders.postValue(clearedPreOrder);
    }

    private void removePreOrderWaitingToStart(Order preOrder) {
        Order waitStartPreOrder = orderWaitToStart.getValue();
        if(waitStartPreOrder!=null){
            if(waitStartPreOrder.getId()==preOrder.getId()){
                orderWaitToStart.postValue(null);
            }
        }
    }

    private void preOrderTimedOutNotif(Order preOrder) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                preOrderListenersNotify(new PreOrderEvent(EventStatus.PRE_ORDER_TIME_OUT, preOrder));
                removePreOrderStartSoonEvent(preOrder);
            }
        });
    }

    private void removePreOrderStartSoonEvent(Order preOrder) {
        for (PreOrderEvent preOrderEvent : new ArrayList<>(preOrderEvents)) {
            if (preOrderEvent != null && preOrderEvent.getOrder() != null && preOrder != null) {
                if (preOrderEvent.getOrder().getId() == preOrder.getId()) {
                    if (preOrderEvent.getEventStatus() == EventStatus.PRE_ORDER_START_SOON) {
                        preOrderEvents.remove(preOrderEvent);
                    }
                }
            }
        }
    }

    private boolean isMy(Order order) {
        boolean my = false;
        Log.d(TAG, "isMy: curr driver is "+driver);
        Log.d(TAG, "isMy: checking for order "+order);
        if (order.getDriver() != null && order.getCar_need_time() != null && driver!=null) {
            if (order.getDriver().getId() == driver.getId()) {
                my = true;
                Log.d(TAG, "isMy: driver is matched with driver in order");
            }
        }
        return my;
    }

    @Override
    public void addChangePreOrderListener(PreOrdersListener listener) {
        //Log.d(TAG, "addChangePreOrderListener: " + listener);
        listeners.add(listener);
        notifyListener(listener);
        //logListeners(listeners);
    }

    private void logListeners(List<PreOrdersListener> listeners) {
        Log.d(TAG, "logListeners: we have " + listeners.size() + " listeners, it is:");
        for (PreOrdersListener preOrdersListener : listeners) {
            Log.d(TAG, preOrdersListener.toString());
        }
    }

    @Override
    public void removeChangePreOrderListener(PreOrdersListener listener) {
        //Log.d(TAG, "removeChangePreOrderListener: " + listener);
        listeners.remove(listener);
        //Log.d(TAG, "removeChangePreOrderListener: now we have " + listeners.size() + " listeners");
    }

    private void preOrderListenersNotify(PreOrderEvent preOrderEvent) {
        Log.d(TAG, "preOrderListenersNotify: about " + preOrderEvent.toString());
        preOrderEvents.add(preOrderEvent);
        //notifyListeners();
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                for (PreOrdersListener preOrdersListener : new ArrayList<>(listeners)) {
                    preOrdersListener.preOrderNewEvent(preOrderEvent);
                }
            }
        });

    }

    private void preOrderSoon(Order preOrder) {
        //currentPreOrderState=new CurrentPreOrderState(preOrder, CurrentPreOrderState.SoonPreOrderStatus.PRE_ORDER_START_SOON);
        if(preOrder==null)return;
        loggingManager.addLog("ManfredPreOrderManager", "preOrderSoon",
                "notifing listeners about preOrder "+preOrder.getId(), LogTags.OTHER);
        preOrderListenersNotify(new PreOrderEvent(EventStatus.PRE_ORDER_START_SOON, preOrder));
    }

    public LiveData<Order> getOrderWaitToStart() {
        return orderWaitToStart;
    }

    private void preOrderWaitToStart(Order preOrder) {
        //readyToStartOrder=preOrder;
        //currentPreOrderState=new CurrentPreOrderState(preOrder, CurrentPreOrderState.SoonPreOrderStatus.PRE_ORDER_WAIT_TO_START);
        //preOrderListenersNotify(new PreOrderEvent(EventStatus.PRE_ORDER_WAIT_TO_START, preOrder));
        orderWaitToStart.postValue(preOrder);
    }


    public void preOrderGo(Order preOrder) {
        //currentPreOrderState=null;
        removePreOrderWaitingToStart(preOrder);
    }


    private void preOrderAddedNotif(Order order) {
        preOrderListenersNotify(new PreOrderEvent(EventStatus.NEW_PRE_ORDER, order));
        snackbarManager.showSnack(SnackbarManager.Type.NEW_PRE_ORDER);
    }

    @MainThread
    private void preOrderAcceptedSuccessfully(Order order) {
        preOrderListenersNotify(new PreOrderEvent(EventStatus.PRE_ORDER_ACCEPTED, order));
    }

    @MainThread
    private void preOrderDeclinedByDriverNotify(Order order) {
        preOrderListenersNotify(new PreOrderEvent(EventStatus.PRE_ORDER_DECLINED, order, true));
    }

    private void notifyListener(PreOrdersListener preOrdersListener) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                for (PreOrderEvent preOrderEvent : new ArrayList<>(preOrderEvents)) {
                    //Log.d(TAG, "run: about " + preOrderEvent.toString());
                    preOrdersListener.preOrderNewEvent(preOrderEvent);
                }
            }
        });
    }


    private @Nullable
    Order getAcceptedPreOrderById(long id) {
        for (Order o : getMyPreOrdersList()) {
            if (o.getId() == id) return o;
        }
        return null;
    }

    /*
    remove preorders what so close to accepted
     */
    private List<Order> removeSoClosePreOrders(List<Order> notAcceptedPreOrders) {
        Log.d(TAG, "removeSoClosePreOrders: start count of new preorders " + notAcceptedPreOrders.size());
        List<Order> cleanPreOrders = new ArrayList<>();
        List<Order> myAcceptedOrders = getMyPreOrdersList();
        for (Order preOrder : notAcceptedPreOrders) {
            if (!isPreOrderToCloseForDisplay(myAcceptedOrders, preOrder)) {
                cleanPreOrders.add(preOrder);
            }
        }
        Log.d(TAG, "removeSoClosePreOrders: size of preorders after cleaning " + cleanPreOrders.size());
        //cleanPreOrders.addAll(myAcceptedOrders);
        Log.d(TAG, "removeSoClosePreOrders: and after adding my preOrders " + cleanPreOrders.size());
        return cleanPreOrders;
    }

    private boolean isPreOrderToCloseForDisplay(List<Order> myPreOrders, Order checkingPreOrder) {
        final long ONE_MINUTE_IN_MILLIS = 60000;
        for (Order acceptedPreOrder : myPreOrders) {
            final Date acceptedPreOrderDate = acceptedPreOrder.getCar_need_time();
            //Log.d(TAG, "isPreOrderToCloseForDisplay: matching with my date "+acceptedPreOrder.getCar_need_time());
            //Log.d(TAG, "isPreOrderToCloseForDisplay: range of this is "+acceptedPreOrder.getPreorder_2_notification_minutes());
            final long acceptedPreOrderTime = acceptedPreOrderDate.getTime();
            Date minDate = new Date(acceptedPreOrderTime - (acceptedPreOrder.getPreorder_2_notification_minutes() * ONE_MINUTE_IN_MILLIS));
            Date maxDate = new Date(acceptedPreOrderTime + (acceptedPreOrder.getPreorder_2_notification_minutes() * ONE_MINUTE_IN_MILLIS));
            if (TimeDateUtils.isWithinRange(checkingPreOrder.getCar_need_time(), minDate, maxDate)) {
                return true;
            }
        }
        return false;
    }

    public void invalidateCache() {
        listeners.clear();
        myPreOrders.postValue(new ArrayList<>());
        newPreOrders.postValue(new ArrayList<>());
        preOrderEvents.clear();
    }

    public MutableLiveData<Boolean> getIsRefreshing() {
        return isRefreshing;
    }

    public void forceRefresh(){
        if(isRefreshing.getValue()!=null) {
            if (!isRefreshing.getValue()) {
                initPreOrdersList();
            }
        }
    }
}
