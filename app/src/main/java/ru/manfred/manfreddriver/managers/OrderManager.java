/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers;

import ru.manfred.manfreddriver.model.api.Order;

/**
 * Created by begemot on 15.10.17.
 */

public interface OrderManager {
    interface OrderCallback {
        void accepted();

        void statusChanged();

        void cancelled();

        void ignored();
    }

    void newOrder(Order order);

}
