/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers;

public interface ConnectionStatusManager {
    interface ConnectStatusListener {
        void netAvailable();

        void netUnAvailable();

        void serverUnAvailable();

        void tokenInvalid();
    }

    void serverUnAvailable();

    void serverAvailable();

    void addConnectStatusListener(ConnectStatusListener connectStatusListener);

    void removeConnectStatusListener(ConnectStatusListener connectStatusListener);

    boolean isNetAvailable();
}
