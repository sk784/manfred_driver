/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers.network;

public interface NetworkManager {
    void sendRequest(WebSocketRequest request);
    void sentEventAcceptedRequest(EventProcessedAnswer eventProcessedAnswer);
    void addRequestReceiver(NetworkReceiver networkReceiver);
    void addQuery(NetworkQuery networkQuery);
    void removeRequestReceiver(ManfredNetworkReceiver networkReceiver);
    void internetOff();
    void internetOn();
}
