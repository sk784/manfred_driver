/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers;

import java.util.List;

/**
 * Created by begemot on 22.02.18.
 * Manager for show notification
 */

public interface ActivityNotificationManager {
    interface NotificationManagerListener {
        void showNotification(Message message);
    }

    class Message {
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            final Message message = (Message) o;

            if (title != null ? !title.equals(message.title) : message.title != null) return false;
            return text != null ? text.equals(message.text) : message.text == null;
        }

        @Override
        public int hashCode() {
            int result = title != null ? title.hashCode() : 0;
            result = 31 * result + (text != null ? text.hashCode() : 0);
            return result;
        }

        private String title;
        private String text;

        public Message(String title, String text) {
            this.title = title;
            this.text = text;
        }

        public String getTitle() {
            return title;
        }

        public String getText() {
            return text;
        }

        @Override
        public String toString() {
            return "Message{" +
                    "title='" + title + '\'' +
                    ", text='" + text + '\'' +
                    '}';
        }
    }

    void showNotification(Message message);

    void showFinishCostNotification(float allCost, float debit);

    void showSystemNotification(Message message, int id);

    void addNotificationManagerListener(NotificationManagerListener listener);

    void removeNotificationManagerListener(NotificationManagerListener listener);

    void messageShowed(Message message);

    List<Message> getNotShowedMessages();


}
