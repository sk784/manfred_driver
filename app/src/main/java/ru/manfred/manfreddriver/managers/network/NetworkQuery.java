/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers.network;

import android.support.annotation.Nullable;

public class NetworkQuery {
    private final ManfredNetworkReceiver networkReceiver;
    private final WebSocketRequest webSocketRequest;

    public NetworkQuery(ManfredNetworkReceiver networkReceiver, WebSocketRequest webSocketRequest) {
        this.networkReceiver = networkReceiver;
        this.webSocketRequest = webSocketRequest;
    }

    private NetworkQuery(OneRequestBuilder builder) {
        networkReceiver = builder.networkReceiver;
        webSocketRequest = builder.webSocketRequest;
    }

    public ManfredNetworkReceiver getNetworkReceiver() {
        return networkReceiver;
    }

    public WebSocketRequest getWebSocketRequest() {
        return webSocketRequest;
    }

    /**
     * @param <T> request type
     * @param <V> answer type
     */
    public static final class OneRequestBuilder<T,V> {
        private ManfredNetworkReceiver<V> networkReceiver;
        private WebSocketRequest<T> webSocketRequest;
        private String typeOfRequest;
        private WebSocketAnswerCallback<WebSocketAnswer<V>> callback;
        @Nullable
        private Class classOfAnswerPayload =null;
        @Nullable
        private T data;
        private String token="";

        public OneRequestBuilder(String pathOfRequest) {
            this.typeOfRequest=pathOfRequest;
        }

        public OneRequestBuilder setToken(String token){
            this.token = token;
            return this;
        }

        public OneRequestBuilder setDataForTransmit(T data){
            this.data = data;
            return this;
        }

        public OneRequestBuilder setCallback(WebSocketAnswerCallback callback, @Nullable Class classOfAnswerPayload){
            this.callback = callback;
            this.classOfAnswerPayload=classOfAnswerPayload;
            return this;
        }

        public NetworkQuery buildOneRequest() {
            this.webSocketRequest= new WebSocketRequest<>(typeOfRequest,token,data);
            this.networkReceiver = new ManfredNetworkReceiver<>(callback,typeOfRequest, classOfAnswerPayload,webSocketRequest.getId());
            return new NetworkQuery(this);
        }

    }
}
