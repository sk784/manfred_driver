/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers;

import android.arch.lifecycle.LiveData;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.manfred.manfreddriver.R;
import ru.manfred.manfreddriver.model.api.SingleLiveEvent;

/**
 * Created by begemot on 20.02.18.
 */
@Singleton
public class ManfredSnackbarManager implements SnackbarManager {
    //private List<SnackbarManagerListener> listeners = new ArrayList<>();
    private SingleLiveEvent<Snack> lastSnack = new SingleLiveEvent<>();

    @Inject
    public ManfredSnackbarManager() {
    }

    @Override
    public LiveData<Snack> getSnack() {
        return lastSnack;
    }

    @Override
    public void showSnack(Type type) {
        Snack snack = generateSnack(type);
        lastSnack.setValue(snack);
        //notifyListeners(snack);
    }

    private Snack generateSnack(Type type) {
        Snack snack = null;
        switch (type) {
            case PREORDER_TAKE_OTHER_DRIVER:
                snack = new Snack("Заказ принял другой водитель", R.color.snack_red_background, R.color.colorWhite);
                break;
            case PREORDER_ACCEPTED:
                snack = new Snack("Предзаказ принят", R.color.snack_black_background, R.color.colorWhite);
                break;
            case ORDER_MISSED:
                snack = new Snack("Заказ пропущен", R.color.snack_red_background, R.color.colorWhite);
                break;
            case ORDER_TAKE_BY_OTHER_DRIVER:
                snack = new Snack("Заказ принял другой водитель", R.color.snack_red_background, R.color.colorWhite);
                break;
            case ORDER_CANCELLED_BY_PASSENGER:
                snack = new Snack("Пассажир отменил заказ", R.color.snack_red_background, R.color.colorWhite);
                break;
            case NEW_PRE_ORDER:
                snack = new Snack("Новый предзаказ", R.color.snack_black_background, R.color.colorWhite);
                break;
            case PAYMENT_SUCCESSFUL:
                snack = new Snack("Оплата прошла", R.color.snack_black_background, R.color.colorWhite);
                break;
            case ORDER_CANCEL_BY_OPERATOR:
                snack = new Snack("Оператор отменил заказ", R.color.snack_red_background, R.color.colorWhite);
                break;
        }
        return snack;
    }
}
