/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers.preorder;

import android.arch.lifecycle.LiveData;

import java.util.List;

import ru.manfred.manfreddriver.model.api.Order;

/**
 * Created by begemot on 14.12.17.
 */

public interface PreOrderManager {
    interface PreOrdersListener {
        void preOrderNewEvent(PreOrderEvent preOrderEvent);
    }

    enum EventStatus {
        NEW_PRE_ORDER,
        PRE_ORDER_START_SOON,
        //PRE_ORDER_WAIT_TO_START,
        PRE_ORDER_ACCEPTED,
        PRE_ORDER_DECLINED,
        PRE_ORDER_LIST_CHANGED,
        PRE_ORDER_TIME_OUT,
        PRE_ORDER_TIME_OUT_BEFORE_START
    }

    interface ResultListener {
        void ok(Order order);

        void error(String errorText);
    }

    void eventProcessedSuccessfully(PreOrderEvent preOrderEvent);

    void addPreOrder(Order order);

    void removePreOrder(Order order);

    LiveData<List<Order>> getNewPreOrders();

    LiveData<List<Order>> getMyPreOrders();

    void acceptPreOrder(Order order, ResultListener resultListener);

    void cancelOrder(Order order, ResultListener resultListener);

    void orderSoon(long order);

    void waitForStart(Order order);

    void preOrderTimedOut(Order order);

    void preOrderTimedOutBeforeStart(Order order);

    void preOrderCancelledByPassenger(Order order);

    void preOrderCancelledByOperator(Order order);

    void preOrderAccepted(Order order);

    void addChangePreOrderListener(PreOrdersListener listener);

    void removeChangePreOrderListener(PreOrdersListener listener);
}
