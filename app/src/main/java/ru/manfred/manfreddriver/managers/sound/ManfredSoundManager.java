/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers.sound;

import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.support.annotation.Nullable;
import android.support.annotation.RawRes;
import android.util.Log;

import java.io.IOException;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.managers.ManfredTripManager;
import ru.manfred.manfreddriver.managers.TripManager;
import ru.manfred.manfreddriver.managers.logging.LogTags;
import ru.manfred.manfreddriver.managers.logging.ManfredLoggingManager;
import ru.manfred.manfreddriver.managers.sound.sounds.DriverSound;
import ru.manfred.manfreddriver.managers.sound.sounds.NewOrderSound;
import ru.manfred.manfreddriver.managers.sound.sounds.NewPreOrderSound;
import ru.manfred.manfreddriver.managers.sound.sounds.NoMoneySound;
import ru.manfred.manfreddriver.managers.sound.sounds.PreOrderReminder;

/**
 * Created by begemot on 01.11.17.
 */
@Singleton
public class ManfredSoundManager implements SoundManager {
    private static final String TAG = "ManfredSoundManager";
    @Inject
    ManfredLoggingManager loggingManager;
    private MediaPlayer mediaPlayer = new MediaPlayer();
    private Context context;
    private int originalVolume;
    private AudioManager mAudioManager;
    @RawRes private int currPlayingSoundId=0;
    private Comparator<DriverSound> soundPriorityComparator = new SoundPriorityComparator();
    private Queue<DriverSound> soundQueue = new PriorityQueue<>(8, soundPriorityComparator);

    @Inject
    public ManfredSoundManager(Context context, ManfredTripManager tripManager) {
        this.mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        Log.d(TAG, "ManfredSoundManager: creating");
        this.context = context;

        ManfredApplication.getTripComponent().inject(this);
        tripManager.getCurrentState().observeForever(new Observer<TripManager.Status>() {
            @Override
            public void onChanged(@Nullable TripManager.Status incStatus) {
                if (incStatus == null) return;
                if (incStatus.getStatusState() == null) return;
                switch (incStatus.getStatusState()) {
                    case OFFER_TO_RIDE:
                        newOrder();
                        break;
                    case CANCELLED:
                        noMoney();
                        break;
                    case OFFER_SHOW_TIMEOUT:
                        //stopPlaySound();
                        break;
                    case RIDE_TO_PASSENGER:
                        //stopPlaySound();
                        break;
                    case OFFER_ADDITIONAL_PAYMENT_ERROR:
                        noMoney();
                        break;
                }
            }
        });
/*        tripManager.addChangeStatusListener(new TripManager.TripManagerListener() {
            @Override
            public void newStatus(TripManager.Status incStatus) {
                //Log.d(TAG, "newStatus: " + incStatus);
                if (incStatus == null) return;
                if (incStatus.getStatusState() == null) return;
                switch (incStatus.getStatusState()) {
                    case OFFER_TO_RIDE:
                        newOrder();
                        break;
                    case CANCELLED:
                        noMoney();
                        break;
                    case OFFER_SHOW_TIMEOUT:
                        //stopPlaySound();
                        break;
                    case RIDE_TO_PASSENGER:
                        //stopPlaySound();
                        break;
                    case OFFER_ADDITIONAL_PAYMENT_ERROR:
                        noMoney();
                        break;
                }
            }
        });*/

    }

    public void forcedStartTrip() {
        playDriverSound(new PreOrderReminder());
        //playSoundInfinitely(R.raw.notification);
    }

    private void newOrder() {
        //playSoundInfinitely(R.raw.notification);
        playDriverSound(new NewOrderSound());
    }

    public void newPreOrder() {
        Log.d(TAG, "newPreOrder: ");
        //playSoundOnce(R.raw.order);
        playDriverSound(new NewPreOrderSound());
    }

    public void stopNewOrderSound() {
        Log.d(TAG, "stopNewOrderSound: curr sound is "+currPlayingSoundId);
        if (currPlayingSoundId == new NewOrderSound().getSound()) {
            mediaPlayer.stop();
            playNextSound();
        }
    }

    public void cancelPreOrder() {
        Log.d(TAG, "cancelPreOrder: ");
        noMoney();
    }

    public void cancelOrder() {
        Log.d(TAG, "cancelPreOrder: ");
        noMoney();
    }

    public void preOrderWaitToStart() {
        Log.d(TAG, "preOrderWaitToStart: ");
        //playSoundInfinitely(R.raw.notification);
        playDriverSound(new PreOrderReminder());
        loggingManager.addLog("ManfredSoundManager", "preOrderWaitToStart", "", LogTags.PREORDER_REMINDER_NOTIFICATION_SOUNDED);
    }

    private void noMoney() {
        Log.d(TAG, "noMoney: ");
        playDriverSound(new NoMoneySound());

        /*
        playSoundInfinitely(R.raw.notification);
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "run: time to stop sound");
                stopPlaySound();
            }
        }, 4000);*/
    }

    @Deprecated
    private void playSoundOnce(@RawRes int id) {
        playSound(id, false);
    }

    private void playDriverSound(DriverSound driverSound) {
        soundQueue.add(driverSound);
        playNextSound();
    }

    private void startPlayDriverSound(@Nullable DriverSound driverSound) {
        if (driverSound != null) {
            playSound(driverSound.getSound(), driverSound.isPlayLooped());
        }
    }

    private void playNextSound() {
        if (!mediaPlayer.isPlaying()) {
            final DriverSound sound = soundQueue.poll();
            startPlayDriverSound(sound);
        }
    }

    @Deprecated
    private void playSoundInfinitely(@RawRes int id) {
        playSound(id, true);
    }

    private void playSound(@RawRes int id, boolean looping) {
        /*
        if(BuildConfig.DEBUG){
            Log.d(TAG, "playSound: "+id+" it is debug, silence");
            return;
        }*/
        /*if(mediaPlayer.isPlaying()){
            Log.d(TAG, "playSound: player play sound now, ignoring");
            return;
        }
        */
        if (mediaPlayer == null) {
            Log.d(TAG, "playSound: player is null, creating");
            mediaPlayer = new MediaPlayer();
        } else {
            Log.d(TAG, "playSound: player not null, stopping");
            mediaPlayer.reset();
            if (mediaPlayer.isPlaying()) {
                Log.d(TAG, "stopPlaySound: player playing, stopping");
                mediaPlayer.stop();
                mediaPlayer.reset();
            }
        }
        AudioAttributes aa = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .setContentType(AudioAttributes.CONTENT_TYPE_UNKNOWN)
                .build();
        mediaPlayer.setAudioAttributes(aa);
        AssetFileDescriptor afd = context.getResources().openRawResourceFd(id);
        try {
            mediaPlayer.setLooping(looping);
            mediaPlayer.setDataSource(
                    afd.getFileDescriptor(),
                    afd.getStartOffset(),
                    afd.getLength()
            );
            afd.close();
        } catch (IOException | IllegalStateException e) {
            e.printStackTrace();
        }

        //maxSoundLevel();
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                Log.d(TAG, "onPrepared: start play " + id);
                currPlayingSoundId=id;
                mediaPlayer.start();
            }
        });
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                //returnSoundToOriginalLevel();
                Log.d(TAG, "onCompletion: ");
                currPlayingSoundId=0;
                playNextSound();
            }
        });
        try {
            mediaPlayer.prepareAsync();
        } catch (Exception e) {
            Log.e(TAG, "playSound: ", e);
        }
    }


    @Deprecated
    @Override
    public void stopPlaySound() {
        return;
/*        Log.d(TAG, "stopPlaySound: ");
        if (mediaPlayer.isPlaying()) {
            Log.d(TAG, "stopPlaySound: player playing, stopping");
            mediaPlayer.stop();
            mediaPlayer.reset();
        }
        returnSoundToOriginalLevel();*/
    }

    @Deprecated
    private void maxSoundLevel() {
        if (mAudioManager != null) {
            originalVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
        }
    }

    @Deprecated
    private void returnSoundToOriginalLevel() {
        if (originalVolume != 0) {
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, originalVolume, 0);
        }
    }

    public class SoundPriorityComparator implements Comparator<DriverSound> {
        @Override
        public int compare(DriverSound x, DriverSound y) {
            return Integer.compare(y.getPriority(), x.getPriority());
        }
    }
}
