/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers.network;

import java.util.Date;
import java.util.Random;

public class WebSocketRequest<T> {
    private final long id;
    private final long time;
    private final String platform = "android";
    private final String type;
    private final String token;
    private final T payload;

    public WebSocketRequest(String type, String token, T payload) {
        this.time = new Date().getTime();
        Random random = new Random();
        this.id = time + random.nextInt(10000);
        this.type = type;
        this.token = token;
        this.payload = payload;
    }

    public long getId() {
        return id;
    }

    public long getTime() {
        return time;
    }

    public String getPlatform() {
        return platform;
    }

    public String getType() {
        return type;
    }

    public String getToken() {
        return token;
    }

    public T getPayload() {
        return payload;
    }

    @Override
    public String toString() {
        return "WebSocketRequest{" +
                "id=" + id +
                ", time=" + time +
                ", platform='" + platform + '\'' +
                ", type='" + type + '\'' +
                ", token='" + token + '\'' +
                ", payload=" + payload +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final WebSocketRequest<?> that = (WebSocketRequest<?>) o;

        if (id != that.id) return false;
        return time == that.time;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (time ^ (time >>> 32));
        return result;
    }
}
