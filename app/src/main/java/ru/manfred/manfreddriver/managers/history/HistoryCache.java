/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers.history;

import android.support.annotation.Nullable;

import java.util.Date;
import java.util.List;

import ru.manfred.manfreddriver.model.api.Order;

/**
 * Created by begemot on 13.03.18.
 */

public interface HistoryCache {
    void setTodayHistory(List<Order> orders);

    void setWeekHistory(List<Order> orders);

    void setMonthHistory(List<Order> orders);

    void setRangeHistory(Date startDate, Date endDate, List<Order> orders);

    void invalidateCache();

    @Nullable
    List<Order> getTodayHistory();

    @Nullable
    List<Order> getWeekHistory();

    @Nullable
    List<Order> getMonthHistory();

    @Nullable
    List<Order> getRangeHistory(Date startDate, Date endDate);
}
