package ru.manfred.manfreddriver.managers.sound.sounds;

import android.support.annotation.RawRes;

public interface DriverSound {
    @RawRes
    int getSound();
    boolean isPlayLooped();

    /**
     * @return Приоритеты звуков, играть по очереди:
     * 1. Напоминание о предзаказе
     * 2. Новый заказ
     * 3. Отмена заказа
     * 4. Неудачная транзакция
     * 5. Новый предзаказ
     */
    int getPriority();
}
