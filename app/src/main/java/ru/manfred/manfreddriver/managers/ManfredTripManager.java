/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers;

import android.app.NotificationManager;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.activities.ModalActivity;
import ru.manfred.manfreddriver.interactor.repository.ConfirmationRepository;
import ru.manfred.manfreddriver.managers.notification.ManfredNotificationManager;
import ru.manfred.manfreddriver.managers.notification.NewOrderStatusBarNotification;
import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.utils.Constants;
import ru.manfred.manfreddriver.utils.SharedPreferencesManager;
import ru.manfred.manfreddriver.utils.SingleLiveEvent;

/**
 * Created by begemot on 15.10.17.
 * Manager for trip status
 */
@Singleton
public class ManfredTripManager implements TripManager {
    private static final String TAG = "ManfredTripManager";
    //private List<TripManagerListener> listeners = new ArrayList<TripManagerListener>();
    private Context context;
    //mb deprecated because we have liveData
    private Status status;
    @Nullable
    private Order currOrder;
    private MutableLiveData<Boolean> isRidingLiveData = new MutableLiveData<>();
    private SingleLiveEvent<Status> currStatus = new SingleLiveEvent<>();
    @Inject
    ManfredNotificationManager notificationManager;
    /*
    @Inject
    LoggingManager loggingManager;*/
    //private MediaPlayer mediaPlayer;

    @Inject
    public ManfredTripManager(Context context) {
        this.context = context;
        isRidingLiveData.postValue(false);
        //this.loggingManager = loggingManager;
        //ManfredApplication.getTripComponent().inject(this);
        Log.d(TAG, "ManfredTripManager: created");
    }

    /*
    offer to ride
     */
    @Override
    public void offerRide(Order order) {
        setCurrOrder(order);
        notificationManager.showNotification(new NewOrderStatusBarNotification(null, "Новый заказ"));
/*        showNewOrderNotification("Новый заказ", order, order.getCreated(),
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE), context);*/
        //status = StatusState.OFFER_TO_RIDE;
        changeStatus(new Status(StatusState.OFFER_TO_RIDE,new Reason("change_status", "changing"),order));
    }


    @Override
    public void cancelRide(Order order, @Nullable Reason reason) {
        //loggingManager.addLog("ManfredTripManager", "cancelRide", reason!=null ? reason.toString() : "reason is null", LogTags.OTHER);
        //Log.d(TAG, "cancelRide: curr order id is "+currOrder.getId()+", canceled id - "+order.getId());
        if (currOrder == null) {
            Log.d(TAG, "cancelRide: current order is null, ignoring");
            return;
        }
        Log.d(TAG, "cancelRide: order id is " + order.getId() + ", reason - " + reason + ", current orders is "
                + currOrder.getId());
        if (currOrder.getId() == order.getId()) {
            Log.d(TAG, "cancelRide: cancelling...");
            //status = StatusState.CANCELLED;
            notificationManager.hideAllNotification();
            //removeAllNotificationImmediately((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE));
            changeStatus(new Status(StatusState.CANCELLED,reason,order));
            setCurrOrder(null);
            String token = (SharedPreferencesManager.getCredentials(context).getToken());
            if(token!=null){
                ConfirmationRepository confirmationRepository = ((ManfredApplication)context.getApplicationContext())
                        .getRepoProvider().getConfirmationRepository(token);
                confirmationRepository.orderCanceled((int) order.getId());
            }
            if (!ManfredApplication.isForeground()) {
                Intent i = new Intent(context, ModalActivity.class);
                //unsubscribeBase();
                if (reason != null) {
                    i.putExtra(Constants.CANCEL_STRING, reason.getMessage());
                }
                i.putExtra(Constants.NEED_CANCEL_TRIP, true);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        }
    }

    @Override
    public void startRideToDestination(Order order) {

        if (order.getRide_start_time() == null) {
            order.setRide_start_time(new Date());
        }
        setCurrOrder(order);
        changeStatus(new Status(StatusState.RIDE,new Reason("change_status", "changing"),order));
    }

    @Override
    public void finishRide(Order order, String text) {
        changeStatus(new Status(StatusState.COMPLETED,new Reason("change_status", text),order));
        setCurrOrder(null);
    }

    @Override
    public void waitingPassenger(Order order) {
        if (order.getWait_passenger_time() == null) {
            order.setWait_passenger_time(new Date());
        }
        setCurrOrder(order);
        changeStatus(new Status(StatusState.WAIT_PASSENGER, new Reason("change_status", "changing"),order));
    }

    @Override
    public void startRideToPassenger(Order order) {
        if (order.getRide_start_time() == null) {
            order.setRide_start_time(new Date());
        }
        if(order.getCar_need_time()!=null){
            order.setPreorder_start_time(new Date());
        }
        currOrder=order;
        Log.d(TAG, "startRideToPassenger: order is "+order.getId());
        changeStatus(new Status(StatusState.RIDE_TO_PASSENGER, new Reason("change_status", "changing"),order));
    }

    @Override
    public void offerOnScreenTimeOut(Order order, Reason reason) {
        if (currOrder == null) return;
        if (order.getId() == currOrder.getId()) {
            changeStatus(new Status(StatusState.OFFER_SHOW_TIMEOUT,reason,order));
        }
        setCurrOrder(null);
        notificationManager.hideAllNotification();
    }

    @Override
    public void offerAdditionalPaymentError(Order order, Reason reason) {
        changeStatus(new Status(StatusState.OFFER_ADDITIONAL_PAYMENT_ERROR,reason,order));

    }


    @Override
    public void offerCostResult(Order order, Reason reason) {

    }

    @Nullable
    @Override
    public Status getCurrentStatus() {
        return status;
    }

    @Override
    public void paymentSuccessful(Order order, Reason reason) {
        if (currOrder != null & order != null) {
            if (currOrder.getId() == order.getId()) {
                StatusState statusState= getStatusFromOrder(order);
                Log.d(TAG, "paymentSuccessful: order status is " + order.getStatus());
                changeStatus(new Status(statusState,reason, order));
            }
        }
    }

    @Override
    public void orderUpdated(Order order, Reason reason) {
        if (currOrder != null & order != null) {
            if (currOrder.getId() == order.getId()) {
                StatusState statusState= getStatusFromOrder(order);
                Log.d(TAG, "orderUpdated: order status is " + order.getStatus());
                changeStatus(new Status(statusState, reason, order));
            }
        }
    }

    @Override
    public LiveData<Status> getCurrentState() {
        return currStatus;
    }
    private void changeStatus(Status status) {
        Log.d(TAG, "changeStatus: about "+status);
        this.status=status;
        this.currStatus.postValue(status);
        if(isOnRide(status)){
            isRidingLiveData.postValue(true);
        }else {
            isRidingLiveData.postValue(false);
        }
    }

    public LiveData<Boolean> getIsRiding() {
        return isRidingLiveData;
    }

    private boolean isOnRide(Status currStatus){
        boolean riding=false;
        StatusState status=currStatus.getStatusState();
        if(status==null)return false;
        switch (status){
            case FREE:
                break;
            case OFFER_TO_RIDE:
                break;
            case OFFER_SHOW_TIMEOUT:
                break;
            case RIDE_TO_PASSENGER:
                riding=true;
                break;
            case OFFER_ADDITIONAL_PAYMENT_ERROR:
                break;
            case WAIT_PASSENGER:
                riding=true;
                break;
            case RIDE:
                riding=true;
                break;
            case COMPLETED:
                break;
            case CANCELLED:
                break;
        }
        return riding;
    }


    /**
     * parse status from order
     * @param order status strings can be:
     *     "SEARCH_DRIVER";
     *     "RIDE_TO_PASSENGER";
     *     "WAIT_PASSENGER";
     *     "RIDE";
     *     "COMPLETED";
     *     "CANCELLED";
     *     SCHEDULED
     *     SCHEDULED_ACEPTED
     * @return StatusState object
     */
    private StatusState getStatusFromOrder(Order order) {
        StatusState parserStatus;
        switch (order.getStatus()) {
            case "FREE":
                parserStatus = StatusState.FREE;
                break;
            case "OFFER_TO_RIDE":
                parserStatus = StatusState.OFFER_TO_RIDE;
                break;
            case "OFFER_SHOW_TIMEOUT":
                parserStatus = StatusState.OFFER_SHOW_TIMEOUT;
                break;
            case "RIDE_TO_PASSENGER":
                parserStatus = StatusState.RIDE_TO_PASSENGER;
                break;
            case "OFFER_ADDITIONAL_PAYMENT_ERROR":
                parserStatus = StatusState.OFFER_ADDITIONAL_PAYMENT_ERROR;
                break;
            case "WAIT_PASSENGER":
                parserStatus = StatusState.WAIT_PASSENGER;
                break;
            case "RIDE":
                parserStatus = StatusState.RIDE;
                break;
            case "COMPLETED":
                parserStatus = StatusState.COMPLETED;
                break;
            case "CANCELLED":
                parserStatus = StatusState.CANCELLED;
                break;
            default:
                parserStatus = status.getStatusState();
                break;
        }
        return parserStatus;
    }

    private void setCurrOrder(@Nullable Order currOrder) {
        Log.d(TAG, "setCurrOrder: setting order to " + (currOrder != null ? currOrder.toString() : "null"));
        this.currOrder = currOrder;
        Crashlytics.setLong(Constants.FIREBASE_CURR_ORDER_KEY_IN_TRIPMANAGER, this.currOrder != null ? this.currOrder.getId() : 0);
    }
}
