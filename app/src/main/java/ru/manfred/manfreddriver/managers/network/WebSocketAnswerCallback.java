/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers.network;

import ru.manfred.manfreddriver.model.api.ManfredError;

public interface WebSocketAnswerCallback<V extends WebSocketAnswer>{
    void messageReceived(V message);
    void error(ManfredError manfredError);
}