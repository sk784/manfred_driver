/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers;

import android.arch.lifecycle.Observer;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.LongSparseArray;
import android.util.SparseArray;

import com.crashlytics.android.Crashlytics;

import java.util.HashMap;
import java.util.LinkedList;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.managers.logging.LogTags;
import ru.manfred.manfreddriver.managers.logging.ManfredLoggingManager;
import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.utils.Constants;

/**
 * Created by begemot on 15.10.17.
 */
@Singleton
public class ManfredOrderManager implements OrderManager {
    private static final String TAG = "ManfredOrderManager";
    @Nullable
    private Order currentOrder;
    private LongSparseArray<Order> newOrders = new LongSparseArray<Order>();
    //private Context context;
    @Inject
    ManfredTripManager tripManager;
    @Inject
    ManfredLoggingManager loggingManager;

    @Inject
    public ManfredOrderManager(Context context) {
        Log.d(TAG, "ManfredOrderManager: created");
        //this.context = context;
        ManfredApplication.getTripComponent().inject(this);
        tripManager.getCurrentState().observeForever(new Observer<TripManager.Status>() {
            @Override
            public void onChanged(@Nullable TripManager.Status incStatus) {
                if(incStatus!=null){
                    Log.d(TAG, "order now is null? " + (currentOrder == null));
                    //yes, we check for null because have one crash: Attempt to invoke virtual method 'int java.lang.Enum.ordinal()' on a null object reference
                    if (incStatus == null) return;
                    //Log.d(TAG, "newStatus: " + incStatus+", order of incoming status is "+incStatus.getOrder().getId());
                    TripManager.StatusState status = incStatus.getStatusState();
                    if(status==null)return;
                    Order order = incStatus.getOrder();
                    switch (status) {
                        case CANCELLED:
                            if(order!=null) {
                                removeCurrentOrder(order);
                            }
                            break;
                        case RIDE_TO_PASSENGER:
                            setCurrentOrder(order);
                        case RIDE:
                            setCurrentOrder(order);
                            break;
                        case WAIT_PASSENGER:
                            setCurrentOrder(order);
                            break;
                        case COMPLETED:
                            removeCurrentOrder(order);
                            break;
                        case OFFER_SHOW_TIMEOUT:
                            removeCurrentOrder(order);
                            break;
                    }
                    Log.d(TAG, "order now is null? " + (currentOrder == null));
                }
            }
        });
/*        tripManager.addChangeStatusListener(new TripManager.TripManagerListener() {
            @Override
            public void newStatus(TripManager.Status incStatus) {
                Log.d(TAG, "order now is null? " + (currentOrder == null));
                //yes, we check for null because have one crash: Attempt to invoke virtual method 'int java.lang.Enum.ordinal()' on a null object reference
                if (incStatus == null) return;
                //Log.d(TAG, "newStatus: " + incStatus+", order of incoming status is "+incStatus.getOrder().getId());
                TripManager.StatusState status = incStatus.getStatusState();
                if(status==null)return;
                Order order = incStatus.getOrder();
                switch (status) {
                    case CANCELLED:
                        if(order!=null) {
                            removeCurrentOrder(order);
                        }
                        break;
                    case RIDE_TO_PASSENGER:
                        setCurrentOrder(order);
                    case RIDE:
                        setCurrentOrder(order);
                        break;
                    case WAIT_PASSENGER:
                        setCurrentOrder(order);
                        break;
                    case COMPLETED:
                        removeCurrentOrder(order);
                        break;
                    case OFFER_SHOW_TIMEOUT:
                        removeCurrentOrder(order);
                        break;
                }
                Log.d(TAG, "order now is null? " + (currentOrder == null));
            }
        });*/
    }

    /**
     * remove order for current(cancel completed or other reason)
     * @param order for removing
     */
    private void removeCurrentOrder(Order order){
        if(currentOrder==null)return;
        if(order.getId()==currentOrder.getId()){
            currentOrder=null;
        }
        removeOrderFromAccumulator(order);
        offerNewOrder();
    }

    private void removeOrderFromAccumulator(Order order) {
        Order newOrderInGetted = newOrders.get(order.getId(), null);
        if (newOrderInGetted != null) {
            newOrders.remove(order.getId());
        }
    }

    /**
     * offering ride from accumulated orders
     */
    private void offerNewOrder(){
        Log.d(TAG, "offerNewOrder: ");
        if(newOrders.size()>0){
            Log.d(TAG, "offerNewOrder: we have accumulated order, promoting");
            validate(newOrders.valueAt(0),tripManager);
        }
    }

    @Override
    public void newOrder(Order order) {
        String logString = "newOrder: curr order is null? " + (currentOrder == null);
        loggingManager.addLog("ManfredOrderManager", "newOrder", logString, LogTags.OTHER);
        if (currentOrder == null) {
            validate(order, tripManager);
        }else {
            Log.d(TAG, "newOrder: we have now, accumulating order "+order.getId());
            newOrders.append(order.getId(),order);
        }
    }

    private void validate(final Order incomingOrder, final TripManager tripManager) {
        loggingManager.addLog("ManfredOrderManager", "validate",
                "sending order to trip manager", LogTags.OTHER);
        setCurrentOrder(incomingOrder);
        tripManager.offerRide(incomingOrder);
        removeOrderFromAccumulator(incomingOrder);
    }

    @Nullable
    public Order getCurrentOrder() {
        return currentOrder;
    }

    private void setCurrentOrder(@Nullable Order currentOrder) {
        /*if(currentOrder!=null) {
            Log.d(TAG, "setCurrentOrder: order setted to " + currentOrder.toString());
        }*/
        if(currentOrder!=null) {
            Log.d(TAG, "setCurrentOrder: setting to " + currentOrder.getId());
        }else {
            Log.d(TAG, "setCurrentOrder: setting to null");
        }
        this.currentOrder = currentOrder;

        Crashlytics.setLong(Constants.FIREBASE_CURR_ORDER_KEY_IN_ORDERMANAGER, getCurrentOrder() != null ? getCurrentOrder().getId() : 0);
    }
}
