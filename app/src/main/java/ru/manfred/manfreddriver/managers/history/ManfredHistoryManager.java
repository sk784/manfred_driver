/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers.history;

import android.support.annotation.NonNull;
import android.util.Log;

import org.joda.time.LocalDate;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import ru.manfred.manfreddriver.ManfredApplication;
import ru.manfred.manfreddriver.interactor.repository.DriverRepository;
import ru.manfred.manfreddriver.model.api.Order;
import ru.manfred.manfreddriver.utils.TimeDateUtils;

/**
 * Created by begemot on 06.02.18.
 */

public class ManfredHistoryManager implements HistoryManager {
    @Inject
    ManfredHistoryCache historyCache;
    private static final String TAG = "ManfredHistoryManager";
    private DriverRepository driverRepository;

    public ManfredHistoryManager(DriverRepository repository) {
        driverRepository = repository;
        ManfredApplication.getHistoryComponent().inject(this);
    }

    @Override
    public void getTodayHistory(HistoryManagerCallback historyCallback) {
        if (historyCache.getTodayHistory() == null) {
            Log.d(TAG, "getTodayHistory: ");
            //LocalDate today = getLocalDate();
            //LocalDate tomorrow = today.plusDays(1);
            Date today = new Date();
            Date tomorrow = TimeDateUtils.addDays(today, 1);
            driverRepository.getHistoryByDate(today, tomorrow, new DriverRepository.HistoryCallback2() {
                @Override
                public void history(List<Order> history) {
                    historyCache.setTodayHistory(history);
                    historyCallback.success(history);
                }

                @Override
                public void error(String error) {
                    historyCallback.error(error);
                }

                @Override
                public void serverNotAvailable() {
                    historyCallback.serverNotAvailable();
                }
            });
        } else {
            Log.d(TAG, "getTodayHistory: we have " + historyCache.getTodayHistory() + " in cache");
            historyCallback.success(historyCache.getTodayHistory());
        }
    }

    @Override
    public void getWeekHistory(HistoryManagerCallback historyCallback) {
        if (historyCache.getWeekHistory() == null) {
            LocalDate startWeek = getLocalDate().minusWeeks(1);
            LocalDate endWeek = getLocalDate();
            startWeek = startWeek.plusDays(1);
            driverRepository.getHistoryByDate(startWeek.toDate(), endWeek.toDate(), new DriverRepository.HistoryCallback2() {
                @Override
                public void history(List<Order> history) {
                    historyCache.setWeekHistory(history);
                    historyCallback.success(history);
                }

                @Override
                public void error(String error) {
                    historyCallback.error(error);
                }

                @Override
                public void serverNotAvailable() {
                    historyCallback.serverNotAvailable();
                }
            });
        } else {
            historyCallback.success(historyCache.getWeekHistory());
            Log.d(TAG, "getTodayHistory: we have " + historyCache.getWeekHistory().size() + " in cache");
        }
    }

    @Override
    public void getMonthHistory(HistoryManagerCallback historyCallback) {
        if (historyCache.getMonthHistory() == null) {
            LocalDate endMonth = getLocalDate();
            LocalDate startMonth = endMonth.minusMonths(1);
            //startMonth = startMonth;
            Log.d(TAG, "getMonthHistory: ");
            driverRepository.getHistoryByDate(startMonth.toDate(), endMonth.toDate(), new DriverRepository.HistoryCallback2() {
                @Override
                public void history(List<Order> history) {
                    historyCache.setMonthHistory(history);
                    historyCallback.success(history);
                }

                @Override
                public void error(String error) {
                    historyCallback.error(error);
                }

                @Override
                public void serverNotAvailable() {
                    historyCallback.serverNotAvailable();
                }
            });
        } else {
            historyCallback.success(historyCache.getMonthHistory());
        }
    }

    @Override
    public void getPeriodHistory(Date startDate, Date endDate, HistoryManagerCallback historyCallback) {
        List<Order> periodHistory = historyCache.getRangeHistory(startDate, endDate);
        if (periodHistory == null) {
            driverRepository.getHistoryByDate(startDate, endDate, new DriverRepository.HistoryCallback2() {
                @Override
                public void history(List<Order> history) {
                    Log.d(TAG, "getPeriodHistory: "+history.size());
                    historyCache.setRangeHistory(startDate, endDate, history);
                    historyCallback.success(history);
                }

                @Override
                public void error(String error) {
                    historyCallback.error(error);
                }

                @Override
                public void serverNotAvailable() {
                    historyCallback.serverNotAvailable();
                }
            });
        } else {
            Log.d(TAG, "getPeriodHistory: have diapason in cache");
            historyCallback.success(periodHistory);
        }
    }

    @Override
    public HistoryCustomRange getLastPeriod() {
        return historyCache.getHistoryCustomRange();
    }

    @Override
    public void invalidateCache() {
        historyCache.invalidateCache();
    }

    @NonNull
    private LocalDate getLocalDate() {
        return new LocalDate();
    }

    @NonNull
    private Date getDate() {
        return new Date();
    }
}
