package ru.manfred.manfreddriver.managers.sound.sounds;

import ru.manfred.manfreddriver.R;

public class NewPreOrderSound implements DriverSound {
    @Override
    public int getSound() {
        return R.raw.preorder;
    }

    @Override
    public boolean isPlayLooped() {
        return false;
    }

    @Override
    public int getPriority() {
        return 5;
    }
}
