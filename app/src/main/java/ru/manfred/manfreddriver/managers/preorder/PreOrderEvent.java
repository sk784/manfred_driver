/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers.preorder;

import ru.manfred.manfreddriver.model.api.Order;

public class PreOrderEvent {
    private PreOrderManager.EventStatus eventStatus;
    private Order order;
    private boolean isSelfCancelled = false;
    private boolean isMyPreOrder = false;

    public PreOrderEvent(PreOrderManager.EventStatus eventStatus, Order order) {
        this.eventStatus = eventStatus;
        this.order = order;
    }

    public PreOrderEvent(PreOrderManager.EventStatus eventStatus, Order order, boolean isSelfCancelled) {
        this.eventStatus = eventStatus;
        this.order = order;
        this.isSelfCancelled = isSelfCancelled;
    }

    public boolean isMyPreOrder() {
        return isMyPreOrder;
    }

    public void setMyPreOrder(boolean myPreOrder) {
        isMyPreOrder = myPreOrder;
    }

    public PreOrderManager.EventStatus getEventStatus() {
        return eventStatus;
    }

    public Order getOrder() {
        return order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final PreOrderEvent that = (PreOrderEvent) o;

        if (isSelfCancelled != that.isSelfCancelled) return false;
        if (isMyPreOrder != that.isMyPreOrder) return false;
        if (eventStatus != that.eventStatus) return false;
        return order != null ? order.equals(that.order) : that.order == null;
    }

    @Override
    public int hashCode() {
        int result = eventStatus != null ? eventStatus.hashCode() : 0;
        result = 31 * result + (order != null ? order.hashCode() : 0);
        result = 31 * result + (isSelfCancelled ? 1 : 0);
        result = 31 * result + (isMyPreOrder ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PreOrderEvent{" +
                "eventStatus=" + eventStatus +
                ", order=" + (order != null ? String.valueOf(order.getId()) : "null") +
                ", isSelfCancelled=" + isSelfCancelled +
                ", isMyPreOrder=" + isMyPreOrder +
                '}';
    }

    public boolean isSelfCancelled() {
        return isSelfCancelled;
    }
}
