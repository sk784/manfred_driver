/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers;

import android.arch.lifecycle.LiveData;
import android.support.annotation.ColorRes;

/**
 * Created by begemot on 20.02.18.
 */

public interface SnackbarManager {
    enum Type {
        PREORDER_TAKE_OTHER_DRIVER,
        PREORDER_ACCEPTED,
        ORDER_MISSED,
        ORDER_TAKE_BY_OTHER_DRIVER,
        ORDER_CANCELLED_BY_PASSENGER,
        NEW_PRE_ORDER,
        PAYMENT_SUCCESSFUL,
        ORDER_CANCEL_BY_OPERATOR
    }

    class Snack {
        String text;
        @ColorRes
        int backgroundColour;
        @ColorRes
        int textColour;

        public Snack(String text, @ColorRes int backgroundColour, @ColorRes int textColour) {
            this.text = text;
            this.backgroundColour = backgroundColour;
            this.textColour = textColour;
        }

        public String getText() {
            return text;
        }

        public int getBackgroundColour() {
            return backgroundColour;
        }

        public int getTextColour() {
            return textColour;
        }
    }

    interface SnackbarManagerListener {
        void show(Snack snack);
    }

    LiveData<Snack>getSnack();

    void showSnack(Type type);
}
