/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers;

import ru.manfred.manfreddriver.model.api.ResponseAuth;
import ru.manfred.manfreddriver.model.responces.Phone;
import ru.manfred.manfreddriver.model.responces.SMSCodeRequest;

/**
 * Created by begemot on 30.11.17.
 */

public interface LoginManger {
    interface LoginCallback<T> {
        void success(T sucAnswer);
        void netError(String error);
        void incorrectCreditionals();
    }

    @Deprecated
    public void attemptLogin(String email, String password, LoginCallback loginCallback);

    public void attemptLogin(Phone phone, LoginCallback<String> loginCallback);
    public void sendSms(SMSCodeRequest smsCodeRequest, LoginCallback<ResponseAuth> loginCallback);
}
