/*
 * @author Konstantin Kozhanov
 * begemotoff@gmail.com
 * http://kozhanov.pw
 */

package ru.manfred.manfreddriver.managers.sound;

/**
 * Created by begemot on 01.11.17.
 */

public interface SoundManager {
    //void startPlayNotification();
    void stopPlaySound();
}
